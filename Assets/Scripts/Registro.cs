﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Text.RegularExpressions;
using System.Text;
using System.IO;

public class Registro : MonoBehaviour {
    public InputField Email, codigo, MailIng, RecuperarContra;
    public InputField Pass, Pass2, Passwingresa, Reporte;
    [SerializeField]
    Text Aclaracion, Dudas, EmailRecuperar, EncabezadoAclDudas, TextoAclaracionDudas, EncabezadoErrorAclDudas, EmailAclDudas, EncabezadoCorrectoAclDudas,
        SeleccionaProblema;
    [SerializeField] ObjectManager ob;
    [HideInInspector]
    public string correoe, password, password2, correoingre, passingresa, correoc, error, primero, email, correoguardado, contraseñaguardada, iniciosesion, idguardado,
        emailrecuperar, emailusuario, bienvenida, correoFB, Dudasstring;
    [HideInInspector]
    public bool correo, passwordb, correcto, emailinre, passwordingre, ingresa, passcorrecto, cargando, ingreso, valido, carga, ingre, passFine;
    [SerializeField] Guardar save;
    
    public int x, id, m, o;
    [HideInInspector]
    public long idFB;
    [SerializeField] MP mp;
    public List<DBCasate> Casas = new List<DBCasate>();
    [SerializeField] DBCasate casita;
    public Casas casa;
    [SerializeField] MFC mfc;
    [SerializeField] TP tp;
    [SerializeField] Beneficiario ben;
    [SerializeField] AP ap;
    [SerializeField] GameObject Cargando, Cargando2;
    [SerializeField] Button ErrorAclDudas;
    string Reportes;
    readonly bool casas, pagos, pagospasados, beneficiarios, tipopago, historial, miperfil;
    [SerializeField] Array[] Perfil;
    [SerializeField] Sprite CandadoCerrado, CandadoAbierto;
    [SerializeField] Image CandadoIniciar, CandadoRegistro;
    readonly float waitTime = 10;
    [HideInInspector]
    public bool isStart = false;
    public bool logeado = false;//variable para saber si el usuario entro a la pantalla principal en el login
    public bool captureFrame = false;
    int inicioS;
    // Use this for initialization
    void Awake()
    {
       
        isStart = true;
        bienvenida = "Bienvenida(o) a <b>PayHome</b>. \nGracias por registrarte. \n\nCon esta aplicación los pagos y gastos van a ser más fácil de administrar y próximamente de pagar. \nEspera grandes sorpresas en los próximos meses. \n\nAtte.: Equipo <b>PayHome</b>.";
       //PlayerPrefs.SetInt(iniciosesion, 0);
        if (PlayerPrefs.GetInt(iniciosesion) == 1)
        {
            inicioS = PlayerPrefs.GetInt(iniciosesion);
            ob.Click("Inicios");
            id = PlayerPrefs.GetInt("id");
            emailusuario = PlayerPrefs.GetString("email");

            //CargateMayor();
            mp.ObtenerMP();
            email = correoingre;
            ingresa = true;

            //mp.ObtenerMP();
            //ingresa = true;
            //cargando = true;
            //Cargate();
            mp.guardar = PlayerPrefs.GetString("MP");
            mp.split = mp.guardar.Split(new Char[] { ' ', ',', '.', ':', '\t' });

            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                mp.Offline();
                casa.Offl();
                mfc.OffModificar();
                mfc.OffElim();
                ben.Off();
                ap.OfflPago();
                ap.OfflPagoPasado();
                ap.OfflineHistorial();
                tp.Offl();
            }
            else
            {
                mp.Offline();

                PlayerPrefs.DeleteAll();
                PlayerPrefs.SetInt(iniciosesion, inicioS);
                PlayerPrefs.SetString("MP", mp.guardar);
                PlayerPrefs.SetString("email", emailusuario);
                PlayerPrefs.SetInt("id", id);
            }

            //tp.Offl();
            
            //ap.RefBool = true;
            //ap.RefPasadoBool = true;
            //ap.RefBoolHist = true;
            casa.RefBool = true;
            //Checado();
        }
    }
    void Start()
    {
        StartApp();
        System.GC.Collect();

    }

    void StartApp()
    {
        //va por informacion de usuario (casas, pagos e historial)
        if (PlayerPrefs.GetInt(iniciosesion) == 1)
        {
            StartLoad();Debug.LogWarning("pending to load image");
            ob.Click("Inicios");
            id = PlayerPrefs.GetInt("id");
            emailusuario = PlayerPrefs.GetString("email");
            mp.ObtenerMP();
        

            if (casa.IDOffline.Count == 0 && mfc.IdCasaMod.Count == 0 && mfc.IdCasaEliminar.Count == 0 && Application.internetReachability != NetworkReachability.NotReachable)
            {

                casa.Casa.Clear();
                casa.checate = true;
                if (casa.Casass.Count > 0)
                {
                    casa.posicion = 0;
                    mfc.EliminarTodos();

                }

                casa.ReadCard();

            }

            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                ob.DesError.text = "No se puede cargar las casas, ya que no hay conexión con el servidor";
                ob.Click("Errors");
            }


        }
    }

    public void GetData()
    {
        ap.EliminarServ();
        ap.ReadCardPagos();

        ap.ReadCardPagosPasado();
        tp.ReadCard();

        ap.check = true;
        ap.EliminarHistorial();

        StartCoroutine(IsStart());

    }

    IEnumerator IsStart()
    {
        ap.sw.cambioPago = true;
        yield return new WaitForSeconds(10f);
        ap.sw.cambioPago = false;
        casa.FotoTomada = false;

        EndLoad();
        System.GC.Collect();

    }
    IEnumerator WaiiaSmallTime()
    {
        
        yield return new WaitForSeconds (3f);
    }
    public void Reintentar()
    {
        //ob.Click("Inicios");
        id = PlayerPrefs.GetInt("id");
        mp.ObtenerMP();
        ingresa = true;
        //cargando = true;
        //Cargate();
        emailusuario = PlayerPrefs.GetString("email");
    }


    void SuccessElimPagoPasado(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            error = ("" + ((MessageArg<string>)e).responseValue);
            Debug.Log("");
        }
        else if (e is MessageArg<DBPagosPasado>)
        {
            //right = true;
            //register1 = 1;
            //PlayerPrefs.SetInt("Registrado", register1);
            DBPagosPasado dbpagopasado = ((MessageArg<DBPagosPasado>)e).responseValue;
            error = dbpagopasado.IdPagoPasado.ToString();
            //EliminarPasadoServ();
            //ReadCardPagosPasado();
            //id = user.Id.Value;

        }
    }

    // Update is called once per frame
    void Update () {
        
        if (carga == true)
        {
            ob.Click("Cargando");
            m+=8;
            Quaternion target = Quaternion.Euler(180, 180, m);
            Cargando.transform.rotation = Quaternion.Slerp(transform.rotation, target, m);
            if (o > 1500)
            { carga = false;
                o = 0;
            }

        }
        else
        {
            ob.Click("CargandoQuitar");
        }
        if (cargando == true)
        {
            ob.Click("Cargando2");
            o += 8;
            Quaternion target = Quaternion.Euler(180, 180, o);       //animacion de giro
            Cargando2.transform.rotation = Quaternion.Slerp(transform.rotation, target, o);
            if (o > 1500)
            { cargando = false;
                o = 0;
            }
           
        }
        else
        {
            ob.Click("Cargando2Quitar");
        }

        correoingre = MailIng.text;
        correoe = Email.text;
        password = Pass.text;
        password2 = Pass2.text;
        passingresa = Passwingresa.text;        


        if(Pass.text == "" || Pass2.text == "")
        {
            CandadoRegistro.sprite = CandadoAbierto;
        }
        else if(Pass.text != "" || Pass2.text != "")
        {
            //CandadoRegistro.sprite = CandadoCerrado;
        }


        if (password != "" && password2 != "" && (password2 == password) && passFine== true)//regex item pass
        {
            passwordb = true;
            CandadoRegistro.sprite = CandadoCerrado;
        }

        if(correo == true && passwordb == true)
        {
            correcto = true;
        }

        if(correoingre != "")
        {
            emailinre = true;
        }
        passingresa = "\"" + passingresa + "\"";
        if(passingresa != "")
        {
            passwordingre = true;
            
        }

        if(Passwingresa.text != "")
        {
            //CandadoIniciar.sprite = CandadoCerrado;
        }
        else if(Passwingresa.text == "")
        {
            CandadoIniciar.sprite = CandadoAbierto;

        }

        if (emailinre == true && passwordingre == true)
        {
            ingre = true;

        }
//        Debug.Log(logeado + "¿Ha iniciado sesion?");
       // Debug.Log(captureFrame + "__Se ha capturado el frame de la primera pantalla");
    }

    public void ClickEmail()
    {

        Demo(correoe);
    }

    public void ClickRegistrar()
    {

        DemoRegistro(correoe);    
    }

    public void RegistroBueno()
    {
        if (ob.TYCAcepto.isOn == true)
        {

            Aclaracion.text = correoe;

            CreateUser();   //revisar desde aqui
            ob.Click("CompPerfil-CP");

        }
        else
        {
            ob.DesError.text = "Debe aceptar los términos y condiciones para seguir con el registro.";
            iTween.MoveTo(ob.Errors, new Vector2((Screen.width * 0.5f), (Screen.height * 0.625f)), 0.5f);
        }
       
    }

    public void ClickRecuperar()
    {
        valido = false;
        DemoRecuperar(RecuperarContra.text);
    }

    public void CheckContraseña()
    {
        Debug.LogError(passFine);

        passCode(Pass.text);
        if(correo == false)
        {
            ob.DesError.text = "Correo electrónico invalido. Reintentelo con otro";
            ob.Click("Errors");
        }
        if(correo == true && Pass.text.Length >= 8 && (Pass.text == Pass2.text) && passFine == true )
        {
            //correoc = "\"" + correoingre + "\"";
            //ContraMP.text = Pass2.text;
            /*Aclaracion.text = correoe;

            CreateUser();*/

            ob.Click("TermYCond");
        }
        else if(correo == true && Pass.text.Length < 8)
        {
            ob.DesError.text = "La contraseña es invalida, no contiene el mínimo de 8 caracteres";
            ob.Click("Errors");
        }
        else if(correo == true && Pass.text.Length >= 8 && (Pass.text != Pass2.text))
        {
            ob.DesError.text = "La contraseña no coincide con la confirmación de contraseña";
            ob.Click("Errors");
        }
        else if (correo == true && Pass.text.Length >= 8 && (Pass.text == Pass2.text) && passFine == false)
        {
            ob.DesError.text = "La contraseña debe contener al menos una letra mayuscula, una letra minuscula, un número un caracter especial y  contener al menos 8 caracteres";
            ob.Click("Errors");
        }
    }

    public void Click()//registra el click en el boton siguiente de registro de correo
    {
        if (passwordb == true && correo == true)// si el pasword contine los caracteres correctos del regex
            correcto = true;                     //variable igual a true
        //if(passwordb == false || correo == false)
        //{
        //    ob.DesError.text = "El correo no es valido o las contraseñas no coinciden, verifiquelos y vuelve a intentarlo";
        //    ob.Click("Error");
        //}
        if(correcto == true)
        {
            correoc = "\"" + correoingre + "\"";
            //ContraMP.text = Pass2.text;
            Aclaracion.text = correoe;
            
            CreateUser();
        }

        if(ingre == true)
        {
            correoc = "\"" + correoingre + "\"";
            //ob.Click("Inicio");
            //ContraMP.text = Passwingresa.text;
            //CargateMuchoMenor();
            Aclaracion.text = correoingre;
            IniciarSesion();
           
            //ObtenerCasa();
            //casa = Casas[0];
            //primero = casa[0];
            //casita = Casas[0];

        }
    }
    public void Checado()
    {
        WWWForm Form = new WWWForm();
        Form.AddField("EmailUsuario", PlayerPrefs.GetString(correoguardado));
        Form.AddField("ContrasenaUsuario", PlayerPrefs.GetString(contraseñaguardada));
        WebServices.LoginUser(Form, SuccessGuardado, Error);
    }
    void CreateUser()
    {
        //CargateMuchoMenor();
        if (Application.internetReachability != NetworkReachability.NotReachable)
        {
            WWWForm Form = new WWWForm();
            Form.AddField("EmailUsuario", correoe);
            Form.AddField("ContrasenaUsuario", password2);
            
            WebServices.RegisterUser(Form, Success, Error);
        }
        else if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            ob.Click("Errors");
            ob.DesError.text = "No se pudo crear el usuario, checa tu conexión e intentalo de nuevo";
        }
    }
    void IniciarSesion()
    {
        
        WWWForm Form = new WWWForm();
        Form.AddField("EmailUsuario", correoingre);
        Form.AddField("ContrasenaUsuario", Passwingresa.text);
        Form.AddField("IdUsuario", 0);
        WebServices.LoginUser(Form, SuccessIniciar, ErrorIS);
        ob.DesError.text = "No se pudo iniciar sesión, checa tu conexión e intentalo de nuevo";
    }
    public void IniciarSesionFB()
    {

        WWWForm Form = new WWWForm();
        Form.AddField("IdFacebook", idFB.ToString());
        WebServices.LoginUserFB(Form, SuccessIniciarFB, ErrorIS);
        //cargando = true;
        ob.DesError.text = "No se pudo iniciar sesión, checa tu conexión e intentalo de nuevo";
    }
  /*  public void Rapido()
    {
        WWWForm Form = new WWWForm();
        Form.AddField("EmailUsuario", "martin-zr1@hotmail.com");
        Form.AddField("ContrasenaUsuario", "Martin");
        WebServices.LoginUser(Form, SuccessIniciar, Error);
    }*/

    public void Conseguir()
    {
        //if (casa.checar == true)
        //{
        //    casa.checar = false;
        //    if (casa.Casass.Count > 0)
        //    {
        //        for (int h = 0; casa.Casass.Count > h; )
        //        {
        //            mfc.ids = h;
        //            mfc.casat = casa.Casass[h];
        //            mfc.Eliminar();
        //        }
        //        ObtenerCasa();
        //    }
        //}
        ObtenerCasa();


    }

    public void ObtenerCasa()
    {
        WWWForm Form = new WWWForm();
        Form.AddField("IdUsuario", id);
        WebServices.ObtCasas(Form, SuccessObtCasa, Error);
    }
    
    public void ObtenerCategorias()
    {
        WWWForm Form = new WWWForm();
        Form.AddField("IdUsuario", id);
        WebServices.ObtCategoria(Form, SuccessObtCategoria, Error);
    }
    void Success(System.Object Sender, EventArgs e)
    {
      
       // Debug.LogError(e+"_valor de e");
        if (e is MessageArg<string>)
        {
            error = ("" + ((MessageArg<string>)e).responseValue);

            ob.DesError.text = "Usuario ya registrado con este correo electrónico";
            ob.Click("Errors");
            Pass.text = null;
            Pass2.text = null;
            password = null;
            password2 = null;
            passwordb = false;
            correcto = false;
            ob.Click("Atras");
            ob.TyCOff();
            Debug.Log("");
        }
        else if (e is MessageArg<User>)
        {
            //right = true;
            //register1 = 1;
            //PlayerPrefs.SetInt("Registrado", register1);
          //ob.Click("CompPerfil-CP");

            Email.text = null;
            Pass.text = null;
            Pass2.text = null;
            password = null;
            password2 = null;
            passwordb = false;
            correcto = false;
            User user = ((MessageArg<User>)e).responseValue;
            PlayerPrefs.SetInt("id", user.Id.Value);
            PlayerPrefs.SetInt(iniciosesion, 1);
            PlayerPrefs.SetString("email", correoe);
            emailusuario = PlayerPrefs.GetString("email");
            id = user.Id.Value;
            error = user.Id.ToString();
            email = correoe;
            EnviarBienvenida();
        }
    }
    void SuccessIniciar(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            //cargando = false;
            error = ("" + ((MessageArg<string>)e).responseValue);
            ob.DesError.text = "Correo electrónico y/o contraseña incorrectos";
            ob.Click("Errors");
            Debug.Log("");
        }
        else if (e is MessageArg<User>)
        {
            User user = ((MessageArg<User>)e).responseValue;
            PlayerPrefs.SetInt("id", user.Id.Value);
            PlayerPrefs.SetInt(iniciosesion, 1);
            PlayerPrefs.SetString("email", correoingre);
            error = user.Id.ToString();
            id = user.Id.Value;

            StartApp();


            //CargateMayor();
            //mp.ObtenerMP();
            //emailusuario = PlayerPrefs.GetString("email");
            //email = correoingre;
            //ingresa = true;
            //
            //carga = true;
            //
            //ap.EliminarServ();
            //ap.ReadCardPagos();
            //
            //ap.ReadCardPagosPasado();
            //
            //ap.EliminarHistorial();

            ////mp.Ver();
            //casa.ReadCard();
            //tp.ReadCard();
            //ben.ReadCardBeneficiario();
            //ap.ReadCardPagosPasado();
            //ap.ReadCardPagos();
            //ap.ReadCardHistorial();
            ob.Click("Inicios");
        }
    }
    void SuccessIniciarFB(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            //cargando = false;
            error = ("" + ((MessageArg<string>)e).responseValue);
            ob.DesError.text = "Cuenta de Facebook no registrada\nFavor de registrarse primero";
            ob.Click("Registrate");
            ob.Click("Errors");
            Debug.Log("");
        }
        else if (e is MessageArg<User>)
        {
            User user = ((MessageArg<User>)e).responseValue;
            PlayerPrefs.SetInt("id", user.Id.Value);
            PlayerPrefs.SetInt(iniciosesion, 1);
            //PlayerPrefs.SetString("email", email);
            error = user.Id.ToString();
            id = user.Id.Value;

            //Cargate();
            //mp.ObtenerMP();
            StartApp();
            //emailusuario = PlayerPrefs.GetString("email");
            //email = correoingre;
            ingresa = true;

            ////mp.Ver();
            //casa.ReadCard();
            //tp.ReadCard();
            //ben.ReadCardBeneficiario();
            //ap.ReadCardPagosPasado();
            //ap.ReadCardPagos();
            //ap.ReadCardHistorial();
            ob.Click("Inicios");

        }
    }
    void SuccessGuardado(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            error = ("" + ((MessageArg<string>)e).responseValue);
            ob.DesError.text = "Correo electrónico y/o contraseña incorrectos";
            ob.Click("Errors");
            Debug.Log("");
        }
        else if (e is MessageArg<User>)
        {
            //right = true;
            //register1 = 1;
            //PlayerPrefs.SetInt("Registrado", register1);
            User user = ((MessageArg<User>)e).responseValue;
            //PlayerPrefs.SetString(correoguardado, correoingre);
            //PlayerPrefs.SetString(contraseñaguardada, Passwingresa.text);
            //PlayerPrefs.SetInt(iniciosesion, 1);
            error = user.Id.ToString();
            id = user.Id.Value;
            mp.ObtenerMP();
            email = correoingre;
            ingresa = true;
            ////mp.Ver();
            //casa.ReadCard();
            //tp.ReadCard();
            //ben.ReadCardBeneficiario();
            //ap.ReadCardPagosPasado();
            //ap.ReadCardPagos();
            //ap.ReadCardHistorial();
            ob.Click("Inicios");

        }
    }
    void SuccessObtCasa(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            error = ("" + ((MessageArg<string>)e).responseValue);
            //Debug.Log("");
        }
        else if (e is MessageArg<List<DBCasate>>)
        {
            //right = true;
            //register1 = 1;
            //User user = ((MessageArg<User>)e).responseValue;
            //PlayerPrefs.SetInt("Registrado", register1);
            //User user = ((MessageArg<User>)e).responseValue;
            Casas = ((MessageArg<List<DBCasate>>)e).responseValue;
            //user.casas = ((MessageArg<List<DBCasate>>)e).responseValue;
                //= ((MessageArg<List<DBCasa>>)e).responseValue;
            //Casas = user.casas;
            //error = dbcasas.Id.ToString();
            //mp.ObtenerMP();
            //ingresa = true;
            //mp.Ver();

            //ob.Click("Inicios");

        }
    }
    void SuccessObtCategoria(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            error = ("" + ((MessageArg<string>)e).responseValue);
            Debug.Log("");
        }
        else if (e is MessageArg<List<DBCategoria>>)
        {
            //right = true;
            //register1 = 1;
            //User user = ((MessageArg<User>)e).responseValue;
            //PlayerPrefs.SetInt("Registrado", register1);
            //User user = ((MessageArg<User>)e).responseValue;
            //Categorias = ((MessageArg<List<DBCategoria>>)e).responseValue;
            //user.casas = ((MessageArg<List<DBCasate>>)e).responseValue;
            //= ((MessageArg<List<DBCasa>>)e).responseValue;
            //Casas = user.casas;
            //error = dbcasas.Id.ToString();
            //mp.ObtenerMP();
            //ingresa = true;
            //mp.Ver();

            //ob.Click("Inicios");

        }
    }
    void Error(System.Object Sender, EventArgs e)
    {
       
        
        error = ("" + ((MessageArg<string>)e).responseValue);
        carga = false;
        Debug.Log   (error);
        //ob.Click("Error");
        

    }
    void ErrorIS(System.Object Sender, EventArgs e)
    {
        
        error = ("" + ((MessageArg<string>)e).responseValue);

    }
    
    public static bool IsValidEmail(string strIn)
    {
        // Return true if strIn is in valid e-mail format.
        return Regex.IsMatch(strIn,
                @"^(?("")(""[^""]+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
                @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$");
    }
    public void Demo(string corr)
    {
        string emailAddress = corr;

        if (IsValidEmail(emailAddress))
        {
            correo = true;
            Click();
            Debug.Log("Valid: {0}\n" + emailAddress);
        }
        else
        {
            correo = false;
            Click();
            Debug.Log("Invalid: {0}\n" + emailAddress);
        }
    }

    public static bool IsValidPass(string passWo)
    {

        return Regex.IsMatch(passWo,
                             @"((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W_]).{8,})");
    }

    public void passCode(string godpass)
    {
        string passSecure = godpass;
        //Debug.Log("passSecure");
        if(IsValidPass(passSecure))
        {
            passFine = true;
           // Click();
            Debug.LogError("Valid: {0}\n" + passSecure);
        
        }
        else
        {
            passFine = false;
           // Click();
             Debug.LogError("Valid: {0}\n" + passSecure);
        
        }

    }





    public void DemoRegistro(string corr)
    {
        string emailAddress = corr;

        if (IsValidEmail(emailAddress))
        {
            correo = true;
            CheckContraseña();
            Debug.Log("Valid: {0}\n" + emailAddress);
        }
        else
        {
            correo = false;
            CheckContraseña();
            Debug.Log("Invalid: {0}\n" + emailAddress);
        }
    }
    public void DemoRecuperar(string corr)
    {
        string emailAddress = corr;

        if (IsValidEmail(emailAddress))
        {
            valido = false;
            Recuperar();
            Debug.Log("Valid: {0}\n" + emailAddress);
        }
        else
        {
            valido = true;
            Recuperar();
            Debug.Log("Invalid: {0}\n" + emailAddress);
        }
    }
    public void EnviarContacto()
    {
        WWWForm Form = new WWWForm();
        Form.AddField("IdUsuario", id);
        Form.AddField("EmailUsuario", emailusuario);
        Form.AddField("AclaracionUsuario", "Contacto");
        byte[] bytes = Encoding.UTF8.GetBytes(Dudas.text);
        Dudasstring = Encoding.UTF8.GetString(bytes);
        Form.AddField("CuerpoUsuario", Dudasstring);//dudas es un text
       WebServices.EnviarContacto(Form, SuccessDudas, Error);//el web service de contacto no funciona apropiadamente proximamente
        Debug.Log("contacto enviado");

    }
    public void EnviarAclaracion()
    {
        WWWForm Form = new WWWForm();
        Form.AddField("IdUsuario", id);
        Form.AddField("EmailUsuario", emailusuario);
        Form.AddField("AclaracionUsuario", "Aclaracion");
        Form.AddField("CuerpoUsuario", SeleccionaProblema.text +": \n\n" +  Aclaracion.text);//aclaracion es un texto
        WebServices.EnviarAclaracion(Form, SuccessAclaracion, Error);
    }

    public void EnviarReporte()
    {
        WWWForm Form= new WWWForm();
        Form.AddField("idUsuario", id);
        Form.AddField("correoUsuario", emailusuario);
        Form.AddField("cuerpoMensaje", Reportes);//reporte es input field en registro
        //Form.AddField("CuerpoUsuario", Reportes);
        WebServices.EnviarReporte(Form, SuccessReporte, Error);         
        //  WebServices.EnviarReporte(Form, SuccessReporte, Error); este web service no funciona apropiadamente
    //
    }// dejame ver el php

    public void CheckContacto()
    {
        if(Dudas.text != "")
        {
            //cargando = true;
            EnviarContacto();
            Debug.Log("Check contacto");
        }
        else if (Dudas.text == "")
        {
            ob.Click("Errors");
            ob.DesError.text = "No ha llenado el mensaje para que lo contactemos, rellenelo e intentelo de nuevo";
        }
    }
    public void CheckReporte()
    {
        Debug.Log("entro a CheckReporte");
        if (Reporte.text != "")
        {
            Reportes = Reporte.text + " Enviado desde S.O.: " + SystemInfo.operatingSystem + ", Modelo: " + SystemInfo.deviceModel;//quita esto
            //cargando = true;
            EnviarReporte();
        }
        else if (Reporte.text == "")
        {
            ob.Click("Errors");
            ob.DesError.text = "No ha llenado el mensaje para el reporte de fallo, rellenelo e intentelo de nuevo";
        }
    }
    public void CheckAclaracion()
    {
        if (Aclaracion.text != "" && SeleccionaProblema.text != "Seleccionar el tipo de problema")
        {
            //cargando = true;
            EnviarAclaracion();
        }
        else if (Aclaracion.text == "" && SeleccionaProblema.text != "Seleccionar el tipo de problema")
        {
            ob.Click("Errors");
            ob.DesError.text = "No ha llenado el mensaje para la aclaración, rellenelo e intentelo de nuevo";
        }
        else if (Aclaracion.text != "" && SeleccionaProblema.text == "Seleccionar el tipo de problema")
        {
            ob.Click("Errors");
            ob.DesError.text = "Seleccione el tipo de problema que desea que resolvamos e intentelo de nuevo";
        }
        else if (Aclaracion.text == "" && SeleccionaProblema.text == "Seleccionar el tipo de problema")
        {
            ob.Click("Errors");
            ob.DesError.text = "Rellene el mensaje para la aclaración y seleccione el tipo de problema que desea que resolvamos e intentelo de nuevo";
        }

    }
    void SuccessDudas(System.Object Sender, EventArgs e)
    {
        Debug.Log("SuccessDudas");
        if (e is MessageArg<string>)
        {
            error = ("" + ((MessageArg<string>)e).responseValue);
            EncabezadoErrorAclDudas.text = "Contáctanos";
            ErrorAclDudas.onClick.RemoveAllListeners();
            ErrorAclDudas.onClick.AddListener(EnviarContacto);
            ob.Click("Aclaraciones-Error");
            //cargando = false;

            Debug.Log("");
        }
        else if (e is MessageArg<DBAclDudas>)
        {
            //right = true;
            //register1 = 1;
            //PlayerPrefs.SetInt("Registrado", register1);
            //cargando = false;

            DBAclDudas dudas = ((MessageArg<DBAclDudas>)e).responseValue;
            TextoAclaracionDudas.text = "<b>" + mp.NickInicio.text + ",</b> hemos recibido tu mensaje de contacto, responderemos a tu correo electrónico:";
            Dudas.text = "";
            EmailAclDudas.text = PlayerPrefs.GetString("email");
            EncabezadoCorrectoAclDudas.text = "Contáctanos";
            ob.Click("Aclaraciones-Correcto");
            error = dudas.IdUsuario.ToString();

        }
    }
    void SuccessAclaracion(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            error = ("" + ((MessageArg<string>)e).responseValue);//cargando = false;
            EncabezadoErrorAclDudas.text = "Aclaraciones";
            ErrorAclDudas.onClick.RemoveAllListeners();
            ErrorAclDudas.onClick.AddListener(EnviarAclaracion);
            ob.Click("Aclaraciones-Error");
            Debug.Log("");
        }
        else if (e is MessageArg<DBAclDudas>)
        {
            //right = true;
            //register1 = 1;
            //PlayerPrefs.SetInt("Registrado", register1);
            //cargando = false;
            DBAclDudas acl = ((MessageArg<DBAclDudas>)e).responseValue;
            TextoAclaracionDudas.text = "<b>" + mp.NickInicio.text + ",</b> hemos recibido tu mensaje de contacto, responderemos a tu correo electrónico:";
            EmailAclDudas.text = PlayerPrefs.GetString("email");
            EncabezadoCorrectoAclDudas.text = "Aclaraciones";
            ob.Click("Aclaraciones-Correcto");
            Aclaracion.text = "";
            error = acl.IdUsuario.ToString();

        }
    }
  
    void SuccessReporte(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            //cargando = false;
            error = ("" + ((MessageArg<string>)e).responseValue);
            EncabezadoErrorAclDudas.text = "Reporte de fallos";
            ErrorAclDudas.onClick.RemoveAllListeners();
            ErrorAclDudas.onClick.AddListener(EnviarContacto);
            ob.Click("Aclaraciones-Error");
            Debug.Log("");
        }
        else if (e is MessageArg<DBAclDudas>)
        {
            //right = true;
            //register1 = 1;
            //PlayerPrefs.SetInt("Registrado", register1);
            //cargando = false;
            DBAclDudas dudas = ((MessageArg<DBAclDudas>)e).responseValue;
            TextoAclaracionDudas.text = "<b>" + mp.NickInicio.text + ",</b> hemos recibido tu reporte de fallo, muchas gracias por reportarlo. Responderemos a tu correo electrónico cuando ya haya sido arreglado:";
            EmailAclDudas.text = PlayerPrefs.GetString("email");
            EncabezadoCorrectoAclDudas.text = "Reporte de fallo";
            Reporte.text = "";
            ob.Click("Aclaraciones-Correcto");
            error = dudas.IdUsuario.ToString();

        }
    }
    public void CerrarSesion()
    {
      
        StartLoad();
        ob.Click("CerrarMenu");
        ob.Click("QuitarRegistro");

        MailIng.text = "";
        Passwingresa.text = "";
        Email.text = "";
        Pass.text = "";
        Pass2.text = "";
        PlayerPrefs.DeleteAll();
        /*PlayerPrefs.SetInt("id", 0);
        PlayerPrefs.SetInt(iniciosesion, 0);
        PlayerPrefs.SetString("email", null);
        PlayerPrefs.SetString("MP", null);
        PlayerPrefs.SetString("Pagos", null);
        PlayerPrefs.SetString("PagosAgregar", null);
        PlayerPrefs.SetString("PagosPasado", null);
        PlayerPrefs.SetString("MiPerfilSubir", null);
        PlayerPrefs.SetString("Historial", null);
        PlayerPrefs.SetString("Casas", null);
        PlayerPrefs.SetString("CasasAgregar", null);
        PlayerPrefs.SetString("CasasModificar", null);
        PlayerPrefs.SetString("CasasEliminar", null);
        PlayerPrefs.SetString("HistorialAgregar", null);
        PlayerPrefs.SetString("PagosModificar", null);
        PlayerPrefs.SetString("PagosEliminar", null);
        PlayerPrefs.SetString("PagosPasadoAgregar", null);
        PlayerPrefs.SetString("PagosPasadoEliminar", null);
        PlayerPrefs.SetString("Categorias", null);
        PlayerPrefs.SetString("CategoriasAgregar", null);
        PlayerPrefs.SetString("CategoriasModificar", null);
        PlayerPrefs.SetString("CategoriasEliminar", null);
        PlayerPrefs.SetString("Beneficiario", null);
        PlayerPrefs.SetString("BeneficiarioAgregar", null);
        PlayerPrefs.SetString("BeneficiarioElim", null);
        PlayerPrefs.SetString("BeneficiarioMod", null);*/
        if (File.Exists(Application.persistentDataPath + "MiPerfil.jpg"))
        {
            File.Delete(Application.persistentDataPath + "MiPerfil.jpg");
        }
        
        tp.EliminarServ();
        ap.EliminarServ();
        ap.EliminarPasadoServ();
        ap.EliminarHistorial();
        ben.EliminarServ();
        mfc.EliminarTodos();
        mp.FotoTomada = false;
        mp.eliminarFoto = false;
        mp.FotoRegistro.SetActive(false);
        mp.FotoInicio.SetActive(false);
        mp.Foto.SetActive(false);
        mp.NickName = null;
        mp.Ano = null;
        mp.AnoNR.text = DateTime.Now.Year.ToString();
        mp.AnoN.text = DateTime.Now.Year.ToString();
        mp.Mes = null;
        mp.MesN.text = DateTime.Now.Month.ToString();
        mp.MesNR.text = DateTime.Now.Month.ToString();
        mp.Sexo = null;
        mp.SexoF.isOn = true;
        mp.SexoFR.isOn = true;
        casa.IDOffline.Clear();
        casa.NNOffline.Clear();
        casa.UbOffline.Clear();
        casa.CodOffline.Clear();
        casa.IDLocal.Clear();
        casa.ID.Clear();
        casa.NNC.Clear();
        casa.UbC.Clear();
        casa.CodC.Clear();
        casa.FotoC.Clear();
        casa.PosicionCasa.Clear();
        casa.posicion = 0;
        casa.i = 0;
        mfc.IdCasaMod.Clear();
        mfc.NickModOff.Clear();
        mfc.UbModOf.Clear();
        mfc.CodModOff.Clear();
        mfc.IdCasaEliminar.Clear();

        ap.i = 0;
        ap.IdPagoLocal.Clear();

        ap.IdPago.Clear();
        ap.IdCasa.Clear();
        ap.idBeneficiario.Clear();
        ap.CadaCuandos.Clear();
        ap.AlertasAntes.Clear();
        ap.idCategoria.Clear();
        ap.CodBarras.Clear();
        ap.PrecioPagos.Clear();
        ap.Dia.Clear();
        ap.Mes.Clear();
        ap.Año.Clear();
        ap.Nota.Clear();
        ap.ReciboPagos.Clear();
        ap.NombreCategoria.Clear();
        ap.RecordatorioPagos.Clear();
        ap.PagoPersonalizado.Clear();
        ap.idColors.Clear();
        ap.idIconos.Clear();
        ap.IdPPred.Clear();
        ap.Checarpasado.Clear();
        ap.nombreCasas.Clear();

        ap.IdPagoLocalOffline.Clear();
        ap.NombreCategoriaOffline.Clear();
        ap.idColorsOffline.Clear();
        ap.idIconosOffline.Clear();
        ap.IdCasaOffline.Clear();
        ap.PrecioPagosOffline.Clear();
        ap.fechaOffline.Clear();
        ap.RecordatorioPagosOffline.Clear();
        ap.CadaCuandosOffline.Clear();
        ap.AlertasAntesOffline.Clear();
        ap.IdBeneficiarioOffline.Clear();
        ap.NotaOffline.Clear();
        ap.ReciboPagosOffline.Clear();
        ap.CodBarrasOffline.Clear();
        ap.nombreCasasOffline.Clear();


        ap.Posicion.Clear();
        ap.posicion = 0;

        ap.IdPagoModOffline.Clear();
        ap.NombreCategoriaModOffline.Clear();
        ap.idColorsModOffline.Clear();
        ap.idIconosModOffline.Clear();
        ap.IdCasaModOffline.Clear();
        ap.PrecioPagosModOffline.Clear();
        ap.fechaModOffline.Clear();
        ap.RecordatorioPagosModOffline.Clear();
        ap.CadaCuandosModOffline.Clear();
        ap.AlertasAntesModOffline.Clear();
        ap.IdBeneficiarioModOffline.Clear();
        ap.NotaModOffline.Clear();
        ap.ReciboPagosModOffline.Clear();
        ap.CodBarrasModOffline.Clear();

        ap.EliminarPagoOffline.Clear();

        ap.f = 0;
        ap.IdPagoPasadoLocal.Clear();
        ap.IdPagoPasado.Clear();
        ap.IdCasaPasado.Clear();
        ap.idBeneficiarioPasado.Clear();
        ap.idCategoriaPasado.Clear();
        ap.CodBarrasPasado.Clear();
        ap.PrecioPagosPasado.Clear();
        ap.DiaPasado.Clear();
        ap.MesPasado.Clear();
        ap.AñoPasado.Clear();
        ap.NotaPasado.Clear();
        ap.ReciboPagosPasado.Clear();
        ap.NombreCategoriaPasado.Clear();
        ap.PagoPersonalizadoPasado.Clear();
        ap.idColorsPasado.Clear();
        ap.idIconosPasado.Clear();
        ap.IdPPredPasado.Clear();
        ap.Checarpasado.Clear();
        ap.PosicionPasado.Clear();
        ap.IdPagoLocalPasadoOffline.Clear();
        ap.NombreCategoriaPasadoOffline.Clear();
        ap.idColorsPasadoOffline.Clear();
        ap.idIconosPasadoOffline.Clear();
        ap.IdCasaPasadoOffline.Clear();
        ap.PrecioPagosPasadoOffline.Clear();
        ap.fechaPasadoOffline.Clear();
        ap.RecordatorioPagosPasadoOffline.Clear();
        ap.CadaCuandosPasadoOffline.Clear();
        ap.AlertasAntesPasadoOffline.Clear();
        ap.IdBeneficiarioPasadoOffline.Clear();
        ap.NotaPasadoOffline.Clear();
        ap.ReciboPagosPasadoOffline.Clear();
        ap.CodBarrasPasadoOffline.Clear();
        ap.posicionPasado = 0;
        ap.EliminarPasadoOffline.Clear();

        ap.NombreCatOff.Clear();
        ap.idCatOff.Clear();
        ap.idColorOff.Clear();
        ap.idIconoOff.Clear();
        ap.idPagoEnOff.Clear();
        ap.idPagoConOff.Clear();
        ap.precioHistorialOff.Clear();
        ap.fechaHistorialOff.Clear();
        ap.idPagoEnColorOff.Clear();
        ap.idPagoEnIconoOff.Clear();
        ap.mesHistorialOff.Clear();
        ap.añoHistorialOff.Clear();
        ap.idCasaHistorialOff.Clear();
        ap.nombreCasasHist.Clear();

        ap.NombreCatAgregarHist.Clear();
        ap.idCatAgregarHist.Clear();
        ap.idColorAgregarHist.Clear();
        ap.idIconoAgregarHist.Clear();
        ap.idPagoEnAgregarHist.Clear();
        ap.idPagoConAgregarHist.Clear();
        ap.precioHistorialAgregarHist.Clear();
        ap.fechaHistorialAgregarHist.Clear();
        ap.idPagoEnColorAgregarHist.Clear();
        ap.idPagoEnIconoAgregarHist.Clear();
        ap.mesHistorialAgregarHist.Clear();
        ap.añoHistorialAgregarHist.Clear();
        ap.idCasaHistorialAgregarHist.Clear();
        ap.nombreCasasAgregarHist.Clear();

        tp.IDCatPagosModOffline.Clear();
        tp.NombreCategoriaModOffline.Clear();
        tp.intIconModOffline.Clear();
        tp.intColorModOffline.Clear();
        tp.CodBarModOffline.Clear();

        tp.IDCatPagosPer.Clear();
        tp.IDCatPagosLocal.Clear();
        tp.NombreCategoriaPer.Clear();
        tp.intColorPer.Clear();
        tp.intIconPer.Clear();
        tp.CodBarPer.Clear();
        tp.Posicion.Clear();
        tp.posicion = 0;

        tp.IDCatPagosAgregarOffline.Clear();
        tp.NombreCategoriaAgregarOffline.Clear();
        tp.intColorAgregarOffline.Clear();
        tp.intIconAgregarOffline.Clear();
        tp.CodBarAgregarOffline.Clear();

        tp.IDCatPagosElimOffline.Clear();

        ben.i = 0;
        ben.benefIDLocal.Clear();
        ben.benefID.Clear();
        ben.NombreBenef.Clear();
        ben.referBenef.Clear();
        ben.correoBenef.Clear();
        ben.clabeBenef.Clear();
        ben.telefBenef.Clear();
        ben.Posicion.Clear();
        ben.posicion = 0;

        ben.benefIDLocalAgregar.Clear();
        ben.NombreBenefAgregar.Clear();
        ben.referBenefAgregar.Clear();
        ben.correoBenefAgregar.Clear();
        ben.clabeBenefAgregar.Clear();
        ben.telefBenefAgregar.Clear();

        ben.benefIDMod.Clear();
        ben.NombreBenefMod.Clear();
        ben.referBenefMod.Clear();
        ben.correoBenefMod.Clear();
        ben.clabeBenefMod.Clear();
        ben.telefBenefMod.Clear();
        mp.NickNR.text = null;
        mp.AnoNR.text = "Año";
        mp.MesNR.text = "Mes";
        mp.SexoFR.isOn = true;
        mp.SexoMR.isOn = false;
        mp.fotot = 0;
        mp.FotoRegistro.SetActive(false);
        casa.FotoCasaRegistro.SetActive(false);
        casa.FondoRegistro.SetActive(false);
        casa.FotoTomada = false;
        //ob.Atras.Clear();
        FBScript.FBLogOut();
        //carga = false;
        //cargando = false;
        Debug.Log("Se cerró sesión");
        EndLoad();
        logeado = false;
        //captureFrame = false;
    }
    public void Recuperar()
    {
        if(valido == true)
        {
            ob.Click("Errors");
            ob.DesError.text = "Correo no válido, escribalo correctamente e intentelo de nuevo";
        }
        if (valido == false)
        {
            valido = false;
            //cargando = true;
            emailrecuperar = RecuperarContra.text;
            WWWForm Form = new WWWForm();
            Form.AddField("EmailUsuario", emailrecuperar);
            WebServices.RecuperarContrasena(Form, SuccessRecuperar, Error);
            ob.DesError.text = "No se pudo mandar la solicitud de recuperar contraseña porque no hay conexión, intenta de nuevo más tarde";

        }
    }
    void SuccessRecuperar(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            
            error = ("" + ((MessageArg<string>)e).responseValue);
            ob.Click("OlvContraQuitar");
            //cargando = false;
            ob.DesError.text = "El correo electronico no esta registrado en la base de datos de la aplicación, intentelo con otro.";
            RecuperarContra.text = "";
            ob.Click("Errors");
            Debug.Log("");
        }
        else if (e is MessageArg<DBAclDudas>)
        {
            //right = true;
            //register1 = 1;
            //PlayerPrefs.SetInt("Registrado", register1);
            RecuperarContra.text = "";
            ob.Click("OlvContraQuitar");
            ob.Click("Aviso");
            
            ob.avisotexto.text = "El mensaje de recuperación ha sido enviado, revise su bandeja principal o de spam.";
            //cargando = false;
            
            
            DBAclDudas dudas = ((MessageArg<DBAclDudas>)e).responseValue;
            error = dudas.IdUsuario.ToString();

        }
    }




    public void StartLoad()
    {
        //print("Start Load");
        carga = false;
        cargando = true;logeado = true;Debug.LogWarning("pantalla de inicio");//esta variable indica si el usuario se ha logeado en la pantalla donde parece su informacion general
      //  Invoke("Endload",15);

    }
    public void EndLoad()
    {
        //print("End Load");
        carga = false;
        cargando = false;

    }
   /* IEnumerator Cargandos()
    {
        carga = true;
        yield return new WaitForSecondsRealtime(waitTime);
        carga = false;
    }
    IEnumerator CargandosMayor()
    {
        carga = false;
        cargando = true;
        yield return new WaitForSecondsRealtime(waitTime);
        carga = false;
        cargando = false;
    }
    IEnumerator CargandoMenor()
    {
        carga = true;
        yield return new WaitForSecondsRealtime(2);
        carga = false;
    }
    IEnumerator CargandoMuchoMenor()
    {
        carga = true;
        yield return new WaitForSecondsRealtime(5);
        carga = false;
    }
    IEnumerator CerrarCargando()
    {
        carga = false;
        cargando = true;
        yield return new WaitForSecondsRealtime(5);
        carga = false;
        cargando = false;
    }
    public void Cargate()
    {
        StartCoroutine(Cargandos());
    }
    public void CargateMayor()
    {
        StartCoroutine(CargandosMayor ());
    }
    public void CargateMenor()
    {
        StartCoroutine(CargandoMenor());
    }
    public void CargateMuchoMenor()
    {
        StartCoroutine(CargandoMuchoMenor());
    }*/
    public void EnviarBienvenida()
    {
        WWWForm Form = new WWWForm();
        byte[] bytes = Encoding.Default.GetBytes(bienvenida);
        bienvenida = Encoding.UTF8.GetString(bytes);
        Form.AddField("EmailUsuario", emailusuario);
        Form.AddField("Asunto", "Bienvenido a PayHome");
        Form.AddField("Cuerpo", bienvenida);
        WebServices.EnviarRecibo(Form, SuccessBienvenida, Error);
    }

    void SuccessBienvenida(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            error = ("" + ((MessageArg<string>)e).responseValue);
            Debug.Log("");
        }
        else if (e is MessageArg<DBAclDudas>)
        {
            //right = true;
            //register1 = 1;
            //PlayerPrefs.SetInt("Registrado", register1);

            //id = user.Id.Value;

        }
    }
    public void CreateUserFB()
    {
        //CargateMuchoMenor();
        WWWForm Form = new WWWForm();
        Form.AddField("IdFacebook", idFB.ToString());
        WebServices.RegisterUserFB(Form, SuccessCreateFB, Error);
        ob.DesError.text = "No se pudo crear el usuario mediante Facebook, checa tu conexión e intentalo de nuevo";
    }
    void SuccessCreateFB(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            error = ("" + ((MessageArg<string>)e).responseValue);
            ob.DesError.text = "Usuario ya registrado con esta cuenta de Facebook.";
            ob.Click("Errors");
            Pass.text = null;
            Pass2.text = null;
            password = null;
            password2 = null;
            passwordb = false;
            correcto = false;
        }
        else if (e is MessageArg<User>)
        {
            //right = true;
            //register1 = 1;
            //PlayerPrefs.SetInt("Registrado", register1);
            Email.text = null;
            Pass.text = null;
            Pass2.text = null;
            password = null;
            password2 = null;
            passwordb = false;
            correcto = false;
            User user = ((MessageArg<User>)e).responseValue;
            PlayerPrefs.SetInt("id", user.Id.Value);
            PlayerPrefs.SetInt(iniciosesion, 1);
            
            id = user.Id.Value;
            error = user.Id.ToString();
            
            EnviarBienvenida();
            ob.Click("TermYCond");
        }
    }
}
