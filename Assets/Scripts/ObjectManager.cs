﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectManager : MonoBehaviour {
    public GameObject Prime, Registrate, RegEmail, CompPerfil, CompPerfilFinal, TermYCond, CP, Casa, Pago, FPago, Ingresa, OlvContra, Menu, Inicio, Emergencia, FPHUB, Aclara, Contacto, Error, Correcto, TYCM, MiPerfil, AgFP, Op, MisCasasHUB,
        AgregarMisCasas, ModFormaPago, Errors, ModMisCasas, MiHistorial, Historial, MisPagos, AgregarTipoPago, AgregarCategoria, ElegirColorIcono, AgregarPago, ElegirCasa, AlertaPago, RepetirCada, Alertas, FechaPago, Beneficiario,
        AgregarBeneficiario, MisPagosVencidos, OpcionesPago, OpcionesMetPago, OpcionesPagoEn, ErrorDia, Opciones, HistorialAño, HistorialMes, HistorialPagos, OpcionesBen, ImagenMP,
        Eliminar, Pagado, CodigoBarras, Mes2, Mes3, CasaPagosHUB, MisPagos2, MisPagos3, OpcionesPagoPasado, Detalles, BancaEnLinea, Cargando, Aviso, Reporte, Cargando2, NotaPagos, IniciarSesionError,
        ImagenEliminar, Seguridad, PagoUnico, AjustarFoto;
    public bool Envio, cambiosMP, inicioison, DiferenteHist, Pagos, limpiar;
    public Toggle TYCAcepto;
    public Registro reg;
    public Guardar save;
    public MP mp;
    public FP fp;
    public TP tp;
    public AP ap;
    public InfoPago IP;
    public Casas casa;
    public string Bot, PantallaActual;
    public int i, d, z, c;
    public List<GameObject> Atras = new List<GameObject>();
    //public ScrollRect scroll, scroll2, scroll3, scroll4;
    public Text DesError, BeneficiarioPago, Dia, avisotexto;
    public GameObject contentTP, CancelarPago, AtrasNombre;
    public Text NN, Ub, Cod, OpcionesTitulo, OpcionesSeleccionarTexto, DiaP, MesP, AñoP;
    public InputField casatexto, ubicacion, codigo, preciopago, nota;
    public float timer, timeoption, timerdown;
    public Button OpcionesEditar, OpcionesEliminar, OpcionesSeleccionar, PagoEditar, PagoEliminar, PagoPagado, BenEditar, BenEliminar, BenSeleccionar, ImagenCamara, ImagenGaleria,
        EliminarAceptar, PagoEliminarPasado, PagoPasado, ImagenCamaraEliminar, ImagenGaleriaEliminar, ImagenEliminarFoto;
    public InfoCasaMH ICMH;
    public Text PrecioPago, NotaPago, CsB, cas, RepetirCadaTexto, Fechas, AlertasTexto, EliminarTexto, PagadoTexto, errorPago;
    public GameObject atrasIntronull;
    public GameObject atrasCasanull;
    // Use this for initialization
    void Start() {
        
    }

    // Update is called once per frame
    void Update() { 
        //Debug.LogWarning(inicioison +"¿se encuentra en la pantalla de inicio?" );
        if(PantallaActual != "AgregarPago"){
            //ap.QuitarM();
        }

        if(PantallaActual != "AgregarTipoPago" && tp.modificar == false)
        {
            tp.BotonGuardar.SetActive(true);
            tp.BotonMod.SetActive(false);
            tp.Categoria.text = "";
            tp.IcoColor = tp.iconColorBack;
            tp.botonIcono.sprite = tp.botonIconoBack;
            tp.iconColor.color = tp.iconColorBack;
        }
        if (PantallaActual != "AgregarTipoPago" && tp.modificar == true)
        {
            tp.BotonGuardar.SetActive(false);
            tp.BotonMod.SetActive(true);
        }
        //if (PantallaActual != "AgregarMisCasas")
        //{
        //    casa.NickCasa.text = "";
        //    casa.UbicacionCasa.text = "";
        //    casa.CodigoAsociacion.text = "";
        //    casa.FotoTomada = false;
        //}
        //if(PantallaActual != "AgregarPago" && Pagos == false)
        //{
        //    ap.PrecioPago.text = "";
        //    ap.NotaPago.text = "";
        //    ap.CsB.text = "";
        //    //ap.Casa.text = "Mis casas";
        //    ap.RepetirCadaTexto.text = "Repetir cada";
        //    ap.AlertasTexto.text = "Alerta de recordatorio";
        //    ap.Fecha.text = "Fecha de pago";
        //    Pagos = true;
        //    ap.GuardarAgregar.SetActive(true);
        //    ap.GuardarEditar.SetActive(false);
        //    ap.AhoraBack = false;
        //}
        timer += Time.fixedDeltaTime;
        if (Input.GetMouseButtonDown(0))
        {
            timer = 0;
        }
        if (Input.GetMouseButtonUp(0))
        {
            timer = 0;
        }
        
        if (i == 0)
        {
            
            PantallaActual = "Inicio";
            //Debug.LogWarning("sesion Iniciada");
        }
        else if (i > 0)
        {
            if (Atras.Count > 0)
            {
                AtrasNombre = Atras[i - 1];
                PantallaActual = Atras[i - 1].name;
            }
        }

        if(PantallaActual == "HistorialesAños")
        {
            for(int b=0; casa.posicion>b; b++)
            {
                ICMH = casa.Casass2[b].GetComponent<InfoCasaMH>();
                if (ICMH.z > 0)
                {
                    for (c = 0; ICMH.z > c; c++)
                    {
                        ICMH.PagoCreado[c] = 0;
                        Destroy(ICMH.PagoHistoriales[c]);
                        
                        
                    }
                    if (ICMH.z == c)
                    {
                        ICMH.PagoHistoriales.Clear();
                        ICMH.z = 0;
                        ICMH.ms = false;
                        ICMH.añ = false;
                        for (int uix = 0; uix<casa.Casass.Count; uix++)
                        {
                            ICMH = casa.Casass2[uix].GetComponent<InfoCasaMH>();
                            for(int yyz = 0; yyz<ICMH.PagoCreado.Count; yyz++)
                            {
                                ICMH.PagoCreado[yyz] = 0;
                            }
                        }
                    }
                }
            }
        }
        if (PantallaActual == "MiHistorial")
        {
            for (int b = 0; casa.posicion > b; b++)
            {
                ICMH = casa.Casass2[b].GetComponent<InfoCasaMH>();
                if (ICMH.x > 0)
                {
                    for (c = 0; (ICMH.x) > c; c++)
                    {
                        ICMH.MesCreado[c] = 0;
                        Destroy(ICMH.Meses[c]);
                        
                    }
                    if (ICMH.x == c)
                    {
                        ICMH.Meses.Clear();
                        ICMH.x = 0;
                    }
                }
            }
        }
        if (PantallaActual == "MiHistorial")
        {

            for (int b = 0; casa.posicion > b; b++)
            {
                ICMH = casa.Casass2[b].GetComponent<InfoCasaMH>();
                if (ICMH.w > 0)
                {
                    for (c = 0; ICMH.w > c; c++)
                    {
                        ICMH.AñoCreado[c] = 0;
                        Destroy(ICMH.Años[c]);

                    }
                    if (ICMH.w == c)
                    {
                        ICMH.Años.Clear();
                        ICMH.w = 0;
                    }
                }
            }

        }

        if (PantallaActual != "HistorialesAños" || PantallaActual != "HistorialesMeses" || PantallaActual != "HistorialesPagosCasas" || PantallaActual != "HistorialesPagosCasasDetalles")
        {
            DiferenteHist = true;
            
        }
        if (PantallaActual == "HistorialesAños")
        {
            DiferenteHist = false;
        }
        if (PantallaActual == "HistorialesMeses")
        {
            DiferenteHist = false;
        }
        if (PantallaActual == "HistorialesPagosCasas")
        {
            DiferenteHist = false;
        }
        if (PantallaActual == "HistorialesPagosCasasDetalles")
        {
            DiferenteHist = false;
        }
        if (DiferenteHist == true)
        {
            if (FindObjectsOfType<InfoAno>().Length > 0)
            {
                InfoAno[] objs = FindObjectsOfType<InfoAno>();
                for (int i = 0; i < objs.Length; ++i)
                {
                    Destroy(objs[i].gameObject);
                }
            }
            if (FindObjectsOfType<InfoPagoHistorial>().Length > 0)
            {
                InfoPagoHistorial[] objs = FindObjectsOfType<InfoPagoHistorial>();
                for (int i = 0; i < objs.Length; ++i)
                {
                    Destroy(objs[i].gameObject);
                }
            }
            if (d > 0)
            {
                
                for (int b = 0; d > b; b++)
                {

                    ICMH = casa.Casass2[b].GetComponent<InfoCasaMH>();
                    if (ICMH.z > 0)
                    {
                        for (c = 0; ICMH.z > c; c++)
                        {
                            ICMH.PagoCreado[c] = 0;
                            Destroy(ICMH.PagoHistoriales[c]);
                            
                        }
                        if (ICMH.z == c)
                        {
                            ICMH.PagoHistoriales.Clear();
                            ICMH.ms = false;
                            ICMH.añ = false;
                            ICMH.z = 0;
                            for (int uix = 0; uix < casa.Casass.Count; uix++)
                            {
                                ICMH = casa.Casass2[uix].GetComponent<InfoCasaMH>();
                                for (int yyz = 0; yyz < ICMH.PagoCreado.Count; yyz++)
                                {
                                    ICMH.PagoCreado[yyz] = 0;
                                }
                            }
                        }
                    }
                    if (ICMH.x > 0)
                    {
                        for (c = 0; ICMH.x > c; c++)
                        {
                            ICMH.MesCreado[c] = 0;
                            Destroy(ICMH.Meses[c]);
                            
                        }
                        if (ICMH.x == c)
                        {
                            ICMH.Meses.Clear();
                            ICMH.x = 0;
                        }
                    }
                    if (ICMH.w > 0)
                    {
                        for (c = 0; ICMH.w > c; c++)
                        {
                            ICMH.AñoCreado[c] = 0;
                            Destroy(ICMH.Años[c]);
                            
                        }
                        if (ICMH.w == c)
                        {
                            ICMH.Años.Clear();
                            ICMH.w = 0;
                        }
                    }
                    
                }
            }
        }
        if(PantallaActual == "AgregarTipoPago" || PantallaActual == "MisPagosPendientes" || PantallaActual == "MisPagosPendientes2" ||  PantallaActual == "MisPagosPendientes3" || PantallaActual == "AgregarPago" || PantallaActual == "TipodePago" || PantallaActual == "MisPagosVencidos")
        {
            limpiar = false;
        }
        if (ap.Pagos.Count > 0)
        {
            if (PantallaActual != "AgregarTipoPago" && PantallaActual != "MisPagosPendientes" && PantallaActual != "MisPagosPendientes2" && PantallaActual != "MisPagosPendientes3" && PantallaActual != "AgregarPago" && PantallaActual != "TipodePago" && PantallaActual != "MisPagosVencidos")
            {
                limpiar = true;
            }
        }
        if(limpiar == true && PantallaActual != "AgregarPago")
        {
            //for (int po = 0; po < ap.PagosIndex.Count; po++)
            //{
            //    IP = ap.PagosIndex[po].GetComponent<InfoPago>();
            //    ap.PagosIndex[po].GetComponent<InfoPago>().acomodo = 0;
            //}


            //ap.acomodo = false;
            //ap.PagosIndex.Clear();
            ap.idCasa = 0;
            ap.obtenido = false;
            ap.acomodo = true;
            ap.Acomodo();


            //ap.AcomodoPasado();
        }
        /*if(PantallaActual == "MisPagosPendientes2" || PantallaActual == "MisPagosPendientes3" && Atras.Count>3)
        {
            Atras.RemoveAt(Atras.Count - 1);
        }*/
    }

    public void TyCOff()
    {
        TYCAcepto.isOn = false;
    }
    

    public void Click(string NomBoton)
    {
        switch (NomBoton)
        {
            case "Salir":
                reg.MailIng.text = null;
                reg.Email.text = null;
                reg.codigo.text = null;
                reg.Pass.text = null;
                reg.Pass2.text = null;
                reg.Passwingresa.text = null;
                reg.correo = false;
                reg.passwordb = false;
                reg.correcto = false;
                reg.emailinre = false;
                reg.passwordingre = false;
                //reg.ingresa = false;
                reg.correoe = null;
                StartCoroutine(Wait2(Prime));
                break;
            case "Registrate":
                Atras.Add(Registrate);
                i++;
                iTween.MoveTo(Registrate, new Vector2((Screen.width*0.5f),Registrate.transform.position.y), 0.5f);

                break;
            case "RegEmail":
                Atras.Add(RegEmail);
                i++;
                iTween.MoveTo(RegEmail, new Vector2((Screen.width * 0.5f), Registrate.transform.position.y), 0.5f);
                break;
            case "CompPerfil":
                if (TYCAcepto.isOn == true)
                {
                    Atras.Add(CompPerfil);
                    i++;
                    iTween.MoveTo(CompPerfil, new Vector2((Screen.width * 0.5f), Registrate.transform.position.y), 0.5f);
                }
                else
                {
                    DesError.text = "Debe aceptar los términos y condiciones para seguir con el registro.";
                    iTween.MoveTo(Errors, new Vector2((Screen.width * 0.5f), (Screen.height * 0.625f)), 0.5f);
                }
                break;
            case "TermYCond":
                Atras.Add(TermYCond);
                i++;
                Debug.LogError("Estas en el registro de terminos y condiciones");
                iTween.MoveTo(TermYCond, new Vector2((Screen.width * 0.5f), Registrate.transform.position.y), 0.5f);
                break;
            case "CompPerfil-CP":
                //StartCoroutine(Wait());
                if (TYCAcepto.isOn == true)
              {
                    Debug.LogError("Se supone que no puedes ir atras");    
                mp.registro = true; 
                atrasIntronull.SetActive (false);
                    Atras.Add(CP);
                    i++;                    
                    iTween.MoveTo(CP, new Vector2((Screen.width * 0.5f), Registrate.transform.position.y), 0.5f);
               }
              else
                {
                    DesError.text = "Debe aceptar los términos y condiciones para seguir con el registro.";
                    iTween.MoveTo(Errors, new Vector2((Screen.width * 0.5f), (Screen.height * 0.625f)), 0.5f);
                }
                break;
            case "CompPerfil-Casa":
                atrasCasanull.SetActive(false);
                Atras.Add(Casa);
                iTween.MoveTo(Casa, new Vector2((Screen.width * 0.5f), Registrate.transform.position.y), 0.5f);
                break;
            case "Detalles":
                Atras.Add(Detalles);
                i++;
                iTween.MoveTo(Detalles, new Vector2((Screen.width * 0.5f), Registrate.transform.position.y), 0.5f);
                break;
            case "CompPerfil-Pago":
                Atras.Add(Pago);
                i++;
                iTween.MoveTo(Pago, new Vector2((Screen.width * 0.5f), Registrate.transform.position.y), 0.5f);
                break;
            case "CompPerfil-FPago":

                Atras.Add(FPago);
                i++;
                iTween.MoveTo(FPago, new Vector2((Screen.width * 0.5f), Registrate.transform.position.y), 0.5f);
                break;
            case "CompPerfil-Final":
                Atras.Add(FPago);
                i++;
                iTween.MoveTo(CompPerfilFinal, new Vector2((Screen.width * 0.5f), Registrate.transform.position.y), 0.5f);
                break;
            case "Ingresa":
                Atras.Add(Ingresa);
                i++;
                iTween.MoveTo(Ingresa, new Vector2((Screen.width * 0.5f), Registrate.transform.position.y), 0.5f);
                break;
            case "OlvContra":
                iTween.MoveTo(OlvContra, new Vector2((Screen.width * 0.5f), (Screen.height*0.625f)), 0.5f);
                break;
            case "OlvContraQuitar":
                iTween.MoveTo(OlvContra, new Vector2((Screen.width * 0.5f), (-Screen.height*0.75f)), 0.5f);
                break;
            case "Error":
                iTween.MoveTo(Errors, new Vector2((Screen.width * 0.5f), (Screen.height * 0.625f)), 0.5f);
                break;
            case "ElegirIcono":
                tp.Iconos.SetActive(true);
                tp.Colores.SetActive(false);
                iTween.MoveTo(ElegirColorIcono, new Vector2((Screen.width * 0.5f), (Screen.height * 0.5f)), 0.5f);
                break;
            case "ElegirColor":
                tp.Iconos.SetActive(false);
                tp.Colores.SetActive(true);
                iTween.MoveTo(ElegirColorIcono, new Vector2((Screen.width * 0.5f), (Screen.height * 0.5f)), 0.5f);
                break;
            case "ElegirIconoColorQuitar":
                iTween.MoveTo(ElegirColorIcono, new Vector2((Screen.width * 0.5f), (-Screen.height * 0.75f)), 0.5f);
                break;
            case "ErrorQuitar":
                iTween.MoveTo(Errors, new Vector2((Screen.width * 0.5f), (-Screen.height * 0.75f)), 0.5f);
                break;
            case "ElegirCasa":
                //tp.Iconos.SetActive(false);
                //tp.Colores.SetActive(true);
                iTween.MoveTo(ElegirCasa, new Vector2((Screen.width * 0.5f), (Screen.height * 0.5f)), 0.5f);
                break;
            case "ElegirCasaQuitar":
                iTween.MoveTo(ElegirCasa, new Vector2((Screen.width * 0.5f), (-Screen.height * 0.75f)), 0.5f);
                break;
            case "AlertaPago":
                //tp.Iconos.SetActive(false);
                //tp.Colores.SetActive(true);
                iTween.MoveTo(AlertaPago, new Vector2((Screen.width * 0.5f), (Screen.height * 0.5f)), 0.5f);
                break;
            case "AlertaPagoQuitar":
                iTween.MoveTo(AlertaPago, new Vector2((Screen.width * 0.5f), (-Screen.height * 0.75f)), 0.5f);
                break;
            case "FechaPago":
                //tp.Iconos.SetActive(false);
                //tp.Colores.SetActive(true);
                iTween.MoveTo(FechaPago, new Vector2((Screen.width * 0.5f), (Screen.height * 0.5f)), 0.5f);
                break;
            case "FechaPagoQuitar":
                iTween.MoveTo(FechaPago, new Vector2((Screen.width * 0.5f), (-Screen.height * 0.75f)), 0.5f);
                break;
            case "RepetirCada":
                //tp.Iconos.SetActive(false);
                //tp.Colores.SetActive(true);
                if (ap.PagoRecurrente.isOn == true && ap.pagoretrasado == false)
                {
                    Click("AlertaPagoQuitar");
                    iTween.MoveTo(RepetirCada, new Vector2((Screen.width * 0.5f), (Screen.height * 0.5f)), 0.5f);
                }
                //if (ap.PagoEventual.isOn == true && ap.pagoretrasado == false)
                //{

                //    DesError.text = "El pago es eventual, no se puede repetir. Si desea repetirlo debe cambiarlo a pago recurrente";
                //    iTween.MoveTo(Errors, new Vector2((Screen.width * 0.5f), (Screen.height * 0.625f)), 0.5f);

                //}
                if(ap.pagoretrasado == true)
                {
                    DesError.text = "No se puede repetir el pago ya que es la fecha establecida es anterior a la actual";
                    iTween.MoveTo(Errors, new Vector2((Screen.width * 0.5f), (Screen.height * 0.625f)), 0.5f);
                }
                break;
            case "RepetirCadaQuitar":
                Click("AlertaPago");
                iTween.MoveTo(RepetirCada, new Vector2((Screen.width * 0.5f), (-Screen.height * 0.75f)), 0.5f);
                break;
            case "Alertas":
                //tp.Iconos.SetActive(false);
                //tp.Colores.SetActive(true);
                Click("AlertaPagoQuitar");
                iTween.MoveTo(Alertas, new Vector2((Screen.width * 0.5f), (Screen.height * 0.5f)), 0.5f);
                break;
            case "AlertasQuitar":
                Click("AlertaPago");
                iTween.MoveTo(Alertas, new Vector2((Screen.width * 0.5f), (-Screen.height * 0.75f)), 0.5f);
                
                break;
            case "Beneficiario":
                //tp.Iconos.SetActive(false);
                //tp.Colores.SetActive(true);
                //Click("AlertaPagoQuitar");
                iTween.MoveTo(Beneficiario, new Vector2((Screen.width * 0.5f), (Screen.height * 0.5f)), 0.5f);
                break;
            case "BeneficiarioQuitar":
                //Click("AlertaPago");
                iTween.MoveTo(Beneficiario, new Vector2((Screen.width * 0.5f), (-Screen.height * 0.75f)), 0.5f);

                break;
            case "AgregarBeneficiario":
                //tp.Iconos.SetActive(false);
                //tp.Colores.SetActive(true);
                Click("BeneficiarioQuitar");
                iTween.MoveTo(AgregarBeneficiario, new Vector2((Screen.width * 0.5f), (Screen.height * 0.5f)), 0.5f);
                break;
            case "AgregarBeneficiarioQuitar":
                Click("Beneficiario");
                iTween.MoveTo(AgregarBeneficiario, new Vector2((Screen.width * 0.5f), (-Screen.height * 0.75f)), 0.5f);

                break;
                
            case "Menu":
                iTween.MoveTo(Menu, new Vector2((Screen.width * 0.5f), Registrate.transform.position.y), 0.5f);
                break;
            case "CerrarMenu":
                iTween.MoveTo(Menu, new Vector2((Screen.width * 1.5f), Registrate.transform.position.y), 0.5f);
                
                break;

            case "Mes2Quitar":
                iTween.MoveTo(Mes2, new Vector2((Screen.width * 2f), Registrate.transform.position.y), 0.5f);

                break;
            case "Mes3Quitar":
                iTween.MoveTo(Mes3, new Vector2((Screen.width * 2f), Registrate.transform.position.y), 0.5f);

                break;
            case "Emergencia":
                Atras.Add(Emergencia);
                i++;
                iTween.MoveTo(Emergencia, new Vector2((Screen.width * 0.5f), Registrate.transform.position.y), 0.5f);
               
                break;
            case "Inicio":
                //save.Extension("MiPerfil");
                atrasIntronull.SetActive(true);
                atrasCasanull.SetActive(true);
                if (reg.passcorrecto == true && inicioison == false)
                {
                    Atras.Clear();
                    inicioison = true;
                    i = 0;
                    StartCoroutine(Wait(Inicio));
                   
                }
                if(reg.passcorrecto == false)
                {
            //StartCoroutine(Retard());
                 DesError.text = "El correo electrónico o la contraseña son incorrectos, verifiquelos y vuelve a intentarlo.";
                  iTween.MoveTo(Errors, new Vector2((Screen.width * 0.5f), (Screen.height * 0.625f)), 0.5f);
                 }
        //iTween.MoveTo(Inicio, new Vector2((Screen.width * 0.5f), Registrate.transform.position.y), 0.5f);
                 break;
            case "QuitarRegistro":
                Atras.Clear();
                i = 0;
                StartCoroutine(Waiting());
                //save.Extension("MiPerfil");
                //iTween.MoveTo(CompPerfil, new Vector2((-Screen.width * 1f), Registrate.transform.position.y), 0.5f);
                //iTween.MoveTo(Inicio, new Vector2((-Screen.width * 1f), Registrate.transform.position.y), 0.5f);
                //iTween.MoveTo(CompPerfilFinal, new Vector2((-Screen.width * 1f), Registrate.transform.position.y), 0.5f);
                //iTween.MoveTo(CP, new Vector2((-Screen.width * 1f), Registrate.transform.position.y), 0.5f);
                //iTween.MoveTo(Registrate, new Vector2((-Screen.width * 1f), Registrate.transform.position.y), 0.5f);
                //iTween.MoveTo(TermYCond, new Vector2((-Screen.width * 1f), Registrate.transform.position.y), 0.5f);
                //iTween.MoveTo(Casa, new Vector2((-Screen.width * 1f), Registrate.transform.position.y), 0.5f);
                //iTween.MoveTo(RegEmail, new Vector2((-Screen.width * 1f), Registrate.transform.position.y), 0.5f);
                //iTween.MoveTo(Ingresa, new Vector2((-Screen.width * 1f), Registrate.transform.position.y), 0.5f);
                //iTween.MoveTo(Inicio, new Vector2((Screen.width * 0.5f), Registrate.transform.position.y), 0.5f);
                break;
            case "ErrorInicio":
                DesError.text = "El correo electrónico o la contraseña son incorrectos, verifiquelos y vuelve a intentarlo.";
                iTween.MoveTo(Errors, new Vector2((Screen.width * 0.5f), (Screen.height * 0.625f)), 0.5f);
                break;
            case "Inicios":
                Atras.Clear();
                i = 0;
                StartCoroutine(Wait(Inicio));
                reg.ObtenerCasa();
                reg.logeado = true;
                //iTween.MoveTo(Inicio, new Vector2((Screen.width * 0.5f), Registrate.transform.position.y), 0.5f);
                break;
            case "Inicio2":
                Atras.Clear();
                i = 0;
                StartCoroutine(Wait(Inicio));
                //iTween.MoveTo(Inicio, new Vector2((Screen.width * 0.5f), Registrate.transform.position.y), 0.5f);
                break;
            case "MiPerfil":
                mp.PrevFoto();
                cambiosMP = true;
                Atras.Add(MiPerfil);
                i++;
                StartCoroutine(Wait(MiPerfil));
                
                break;
            case "AjustarFoto":
                cambiosMP = true;
                Atras.Add(AjustarFoto);
                i++;
                StartCoroutine(Wait(AjustarFoto));

                break;
            case "FormPagosHUB":
                Atras.Add(FPHUB);
                i++;
                StartCoroutine(Wait(FPHUB));
                
                break;
            case "Aclaraciones":
                if (Application.internetReachability != NetworkReachability.NotReachable)
                {
                    Atras.Add(Aclara);
                    i++;
                    StartCoroutine(Wait(Aclara));

                }
                else
                {
                    Click("CerrarMenu");
                    DesError.text = "Sólo puedes contactarnos si estas conectado a Internet";
                    Click("Errors");
                }
                break;
            case "Reporte":
                if (Application.internetReachability != NetworkReachability.NotReachable)
                {
                    Atras.Add(Reporte);
                    i++;
                    StartCoroutine(Wait(Reporte));
                }
                else
                {
                    Click("CerrarMenu");
                    DesError.text = "Sólo puedes mandarnos un reporte si estas conec+tado a Internet";
                    Click("Errors");
                }
                break;
            case "Contacto":
                if (Application.internetReachability != NetworkReachability.NotReachable)
                {
                    Atras.Add(Contacto);
                    i++;
                    StartCoroutine(Wait(Contacto));
                }
                else
                {
                    Click("CerrarMenu");
                    DesError.text = "Sólo puedes contactarnos si estas conectado a Internet";
                    Click("Errors");
                }
                break;
            case "TermYCondMenu":
                Atras.Add(TYCM);
                i++;
                StartCoroutine(Wait(TYCM));
                
                break;
            case "GuardarPerfil":
                Atras.Remove(Atras[i-1]);
                i--;
                StartCoroutine(Wait(Inicio));
                break;
            case "MisCasasHUB":
                reg.ObtenerCasa();
                Atras.Clear();
                i = 0;
                Atras.Add(MisCasasHUB);
                i++;
                StartCoroutine(Wait(MisCasasHUB));
                
                break;
            case "AgregarMisCasas":
                Atras.Add(AgregarMisCasas);
                i++;
                //scroll.movementType = ScrollRect.MovementType.Elastic;
                //scroll.elasticity = 0;
                //casa.NickCasa.text = null;
                //casa.UbicacionCasa.text = null;
                //casa.CodigoAsociacion.text = null;
                //casa.NNCasa = null;
                //casa.UbCasa = null;
                //casa.CodAsC = null;
                StartCoroutine(Wait(AgregarMisCasas));
                
                break;
            case "AgregaFormPago":
                Atras.Add(AgFP);
                i++;
                //scroll.movementType = ScrollRect.MovementType.Elastic;
                //scroll.elasticity = 0;
                StartCoroutine(Wait(AgFP));
                
                break;
            case "AgregaFormPago-Guardar":
                Atras.Add(AgFP);
                i++;
                iTween.MoveTo(AgFP, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 0.5f);
                break;
            case "IniciarSesionError":
                iTween.MoveTo(IniciarSesionError, new Vector2((Screen.width * 0.5f), Registrate.transform.position.y), 0.5f);
                break;
            case "IniciarSesionErrorQuitar":
                iTween.MoveTo(IniciarSesionError, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 0.5f);
                break;
            case "AgregaFormPago-Cancelar":
                Atras.Add(AgFP);
                i++;
                iTween.MoveTo(AgFP, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 0.5f);
                break;
            case "Aclaraciones-Correcto":
                    StartCoroutine(Wait(Correcto));
                break;
            case "Aclaraciones-Error":
                    StartCoroutine(Wait(Error));
                break;
            case "ModFormaPago":
                Atras.Add(ModFormaPago);
                i++;
                //scroll2.movementType = ScrollRect.MovementType.Elastic;
                //scroll2.elasticity = 0;
                //scroll3.movementType = ScrollRect.MovementType.Elastic;
                //scroll3.elasticity = 0;
                StartCoroutine(Wait(ModFormaPago));
                
                break;
            case "ModMisCasas":
                Atras.Add(ModMisCasas);
                i++;
                //scroll2.movementType = ScrollRect.MovementType.Elastic;
                //scroll2.elasticity = 0;
                //scroll3.movementType = ScrollRect.MovementType.Elastic;
                //scroll3.elasticity = 0;
                StartCoroutine(Wait(ModMisCasas));
                
                break;
            case "MiHistorial":
                Atras.Clear();
                i = 0;
                Atras.Add(MiHistorial);
                i++;
                StartCoroutine(Wait(MiHistorial));
                
                break;
            case "Historial":
                Atras.Add(Historial);
                i++;
                StartCoroutine(Wait(Historial));
                
                break;
            case "MisPagos":
                Atras.Clear();
                BeneficiarioPago.text = "Beneficiario";
                i = 0;
                Atras.Add(CasaPagosHUB);
                i++;
                Atras.Add(MisPagos);
                i++;
                StartCoroutine(Wait(MisPagos));
                
                break;
            case "MisPagos2":
                //Atras.Clear();
                BeneficiarioPago.text = "Beneficiario";
                //i = 0;
                Atras.Add(MisPagos2);
                i++;
                StartCoroutine(Wait(MisPagos2));

                break;
            case "MisPagos3":
                //Atras.Clear();
                BeneficiarioPago.text = "Beneficiario";
                //i = 0;
                Atras.Add(MisPagos3);
                i++;
                StartCoroutine(Wait(MisPagos3));

                break;
            case "MisPagosVencidos":
                Atras.Clear();
                i = 0;
                Atras.Add(CasaPagosHUB);
                i++;
                Atras.Add(MisPagos);
                i++;
                Atras.Add(MisPagosVencidos);
                i++;
                StartCoroutine(Wait(MisPagosVencidos));
                
                break;
            case "CasaPagosHUB":
                Atras.Clear();
                i = 0;
                Atras.Add(CasaPagosHUB);
                i++;
                StartCoroutine(Wait(CasaPagosHUB));

                break;
            case "AgregarCategoria":
                //scroll4.movementType = ScrollRect.MovementType.Elastic;
                //scroll4.elasticity = 0;
                contentTP.transform.localPosition = tp.scrollOriginal;
                Atras.Add(AgregarCategoria);
                i++;
                StartCoroutine(Wait(AgregarCategoria));
                
                break;
            case "AgregarTipoPago":

                Atras.Add(AgregarTipoPago);
                i++;
                ap.RecordPago = 0;
                StartCoroutine(Wait(AgregarTipoPago));
                
                break;
            case "AgregarPago":
                
                Atras.Add(AgregarPago);
               
                i++;
                StartCoroutine(Wait(AgregarPago));
                
                break;

            case "Atras":
                //scroll4.movementType = ScrollRect.MovementType.Elastic;
                //scroll4.elasticity = 3.25f;
                //scroll3.elasticity = 3.25f;
                //scroll2.elasticity = 3.25f;
                //scroll.elasticity = 3.25f;
                //fp.scroll.elasticity = 3.25f;
                StartCoroutine(Wait3(Atras[i-1]));
                break;
            case "AtrasCasa":
                //scroll4.movementType = ScrollRect.MovementType.Elastic;
                //scroll4.elasticity = 3.25f;
                //scroll3.elasticity = 3.25f;
                //scroll2.elasticity = 3.25f;
                //scroll.elasticity = 3.25f;
                //fp.scroll.elasticity = 3.25f;
                NN.text = "";
                Ub.text = "";
                Cod.text = "";
                casatexto.text = "";
                ubicacion.text = "";

                codigo.text = "";
                StartCoroutine(Wait3(Atras[i - 1]));
                break;
            case "AtrasPago":
                //scroll4.movementType = ScrollRect.MovementType.Elastic;
                //scroll4.elasticity = 3.25f;
                //scroll3.elasticity = 3.25f;
                //scroll2.elasticity = 3.25f;
                //scroll.elasticity = 3.25f;
                //fp.scroll.elasticity = 3.25f;
                preciopago.text = "";
                nota.text = "";
                StartCoroutine(Wait3(Atras[i - 1]));
                StartCoroutine(Wait3(Atras[i - 1]));
                
                break;
            case "ErrorTarjeta":
                DesError.text = "Se necesita llenar todos los campos para poder agregar esté método de pago";
                iTween.MoveTo(Errors, new Vector2((Screen.width * 0.5f), (Screen.height * 0.625f)), 0.5f);
                break;
            case "Errors":
                //DesError.text = "Se necesita llenar todos los campos para poder agregar esté método de pago";
                iTween.MoveTo(Errors, new Vector2((Screen.width * 0.5f), (Screen.height * 0.625f)), 0.5f);
                break;
            case "Aviso":
                //DesError.text = "Se necesita llenar todos los campos para poder agregar esté método de pago";
                iTween.MoveTo(Aviso, new Vector2((Screen.width * 0.5f), (Screen.height * 0.625f)), 0.5f);
                break;
            case "AvisoQuitar":
                //DesError.text = "Se necesita llenar todos los campos para poder agregar esté método de pago";
                iTween.MoveTo(Aviso, new Vector2((Screen.width * 0.5f), (-Screen.height * 0.75f)), 0.5f);
                break;

            case "Seguridad":
                //DesError.text = "Se necesita llenar todos los campos para poder agregar esté método de pago";
                iTween.MoveTo(Menu, new Vector2((Screen.width * 1.5f), Registrate.transform.position.y), 0.5f);
                iTween.MoveTo(Seguridad, new Vector2((Screen.width * 0.5f), (Screen.height * 0.625f)), 0.5f);
                break;
            case "SeguridadQuitar":
                //DesError.text = "Se necesita llenar todos los campos para poder agregar esté método de pago";
                iTween.MoveTo(Menu, new Vector2((Screen.width * 1.5f), Registrate.transform.position.y), 0.5f);
                iTween.MoveTo(Seguridad, new Vector2((Screen.width * 0.5f), (-Screen.height * 1f)), 0.5f);
                break;

            case "NotaPago":
                //DesError.text = "Se necesita llenar todos los campos para poder agregar esté método de pago";
                iTween.MoveTo(NotaPagos, new Vector2((Screen.width * 0.5f), (Screen.height * 0.625f)), 0.5f);
                break;
            case "NotaPagoQuitar":
                //DesError.text = "Se necesita llenar todos los campos para poder agregar esté método de pago";
                iTween.MoveTo(NotaPagos, new Vector2((Screen.width * 0.5f), (-Screen.height * 0.75f)), 0.5f);
                break;
            case "Eliminar":
                //DesError.text = "Se necesita llenar todos los campos para poder agregar esté método de pago";
                iTween.MoveTo(Eliminar, new Vector2((Screen.width * 0.5f), (Screen.height * 0.625f)), 0.5f);
                break;
            case "EliminarQuitar":
                //DesError.text = "Se necesita llenar todos los campos para poder agregar esté método de pago";
                iTween.MoveTo(Eliminar, new Vector2((Screen.width * 0.5f), (-Screen.height * 0.75f)), 0.5f);
                break;
            case "Pagado":
                //DesError.text = "Se necesita llenar todos los campos para poder agregar esté método de pago";
                iTween.MoveTo(Pagado, new Vector2((Screen.width * 0.5f), (Screen.height * 0.625f)), 0.5f);
                break;
            case "PagadoQuitar":
                //DesError.text = "Se necesita llenar todos los campos para poder agregar esté método de pago";
                iTween.MoveTo(Pagado, new Vector2((Screen.width * 0.5f), (-Screen.height * 0.75f)), 0.5f);
                break;
            case "ErrorDia":
                //DesError.text = "Se necesita llenar todos los campos para poder agregar esté método de pago";
                iTween.MoveTo(ErrorDia, new Vector2((Screen.width * 0.5f), (Screen.height * 0.625f)), 0.5f);
                break;
            case "ErrorDiaQuitar":
                //DesError.text = "Se necesita llenar todos los campos para poder agregar esté método de pago";
                iTween.MoveTo(ErrorDia, new Vector2((Screen.width * 0.5f), (-Screen.height * 0.75f)), 0.5f);
                break;
            case "PagoUnico":
                //DesError.text = "Se necesita llenar todos los campos para poder agregar esté método de pago";
                iTween.MoveTo(PagoUnico, new Vector2((Screen.width * 0.5f), (Screen.height * 0.625f)), 0.5f);
                break;
            case "PagoUnicoQuitar":
                //DesError.text = "Se necesita llenar todos los campos para poder agregar esté método de pago";
                iTween.MoveTo(PagoUnico, new Vector2((Screen.width * 0.5f), (-Screen.height * 0.75f)), 0.5f);
                break;
            case "Opciones":
                //tp.Iconos.SetActive(false);
                //tp.Colores.SetActive(true);
                //Click("BeneficiarioQuitar");
                iTween.MoveTo(Opciones, new Vector2((Screen.width * 0.5f), (Screen.height * 0.5f)), 0.5f);
                break;
            case "OpcionesBen":
                //tp.Iconos.SetActive(false);
                //tp.Colores.SetActive(true);
                //Click("BeneficiarioQuitar");
                iTween.MoveTo(OpcionesBen, new Vector2((Screen.width * 0.5f), (Screen.height * 0.5f)), 0.5f);
                break;
            case "OpcionesPago":
                //tp.Iconos.SetActive(false);
                //tp.Colores.SetActive(true);
                //Click("BeneficiarioQuitar");
                iTween.MoveTo(OpcionesPago, new Vector2((Screen.width * 0.5f), (Screen.height * 0.5f)), 0.5f);
                break;
            case "OpcionesPagoPasado":
                //tp.Iconos.SetActive(false);
                //tp.Colores.SetActive(true);
                //Click("BeneficiarioQuitar");
                iTween.MoveTo(OpcionesPagoPasado, new Vector2((Screen.width * 0.5f), (Screen.height * 0.5f)), 0.5f);
                break;
            case "OpcionesQuitar":
                //Click("Beneficiario");
                iTween.MoveTo(Opciones, new Vector2((Screen.width * 0.5f), (-Screen.height * 0.75f)), 0.5f);

                break;
            case "OpcionesBenQuitar":
                //Click("Beneficiario");
                iTween.MoveTo(OpcionesBen, new Vector2((Screen.width * 0.5f), (-Screen.height * 0.75f)), 0.5f);

                break;
            case "OpcionesMetPago":
                //tp.Iconos.SetActive(false);
                //tp.Colores.SetActive(true);
                //Click("BeneficiarioQuitar");
                iTween.MoveTo(OpcionesMetPago, new Vector2((Screen.width * 0.5f), (Screen.height * 0.5f)), 0.5f);
                break;

            case "OpcionesMetPagoQuitar":
                //Click("Beneficiario");
                //ap.ReciboPago.isOn = false;
                iTween.MoveTo(OpcionesMetPago, new Vector2((Screen.width * 0.5f), (-Screen.height * 0.75f)), 0.5f);

                break;
            case "OpcionesPagoQuitar":
                //Click("Beneficiario");
                iTween.MoveTo(OpcionesPago, new Vector2((Screen.width * 0.5f), (-Screen.height * 0.75f)), 0.5f);
                break;
            case "OpcionesPagoPasadoQuitar":
                //Click("Beneficiario");
                iTween.MoveTo(OpcionesPagoPasado, new Vector2((Screen.width * 0.5f), (-Screen.height * 0.75f)), 0.5f);
                break;
            case "OpcionesPagoEn":
                //tp.Iconos.SetActive(false);
                //tp.Colores.SetActive(true);
                //Click("BeneficiarioQuitar");
                iTween.MoveTo(OpcionesPagoEn, new Vector2((Screen.width * 0.5f), (Screen.height * 0.5f)), 0.5f);
                break;
            case "OpcionesPagoEnQuitar":
                //Click("Beneficiario");
                //ap.pagardentro = false;
                iTween.MoveTo(OpcionesPagoEn, new Vector2((Screen.width * 0.5f), (-Screen.height * 0.75f)), 0.5f);
            
                break;
            case "HistorialAño":
                Atras.Add(HistorialAño);
                i++;
                StartCoroutine(Wait(HistorialAño));
                
                break;
            case "HistorialMes":
                Atras.Add(HistorialMes);
                i++;
                StartCoroutine(Wait(HistorialMes));
                
                break;
            case "HistorialPagos":
                Atras.Add(HistorialPagos);
                i++;
                StartCoroutine(Wait(HistorialPagos));
             
                break;
            case "Imagen":
                //DesError.text = "Se necesita llenar todos los campos para poder agregar esté método de pago";
                iTween.MoveTo(ImagenMP, new Vector2((Screen.width * 0.5f), (Screen.height * 0.625f)), 0.5f);
                break;
            case "ImagenQuitar":
                //DesError.text = "Se necesita llenar todos los campos para poder agregar esté método de pago";
                iTween.MoveTo(ImagenMP, new Vector2((Screen.width * 0.5f), (-Screen.height * 0.75f)), 0.5f);
                break;
            case "ImagenEliminar":
                //DesError.text = "Se necesita llenar todos los campos para poder agregar esté método de pago";
                iTween.MoveTo(ImagenEliminar, new Vector2((Screen.width * 0.5f), (Screen.height * 0.625f)), 0.5f);
                break;
            case "ImagenEliminarQuitar":
                //DesError.text = "Se necesita llenar todos los campos para poder agregar esté método de pago";
                iTween.MoveTo(ImagenEliminar, new Vector2((Screen.width * 0.5f), (-Screen.height * 0.75f)), 0.5f);
                break;

            case "Lector":
                //DesError.text = "Se necesita llenar todos los campos para poder agregar esté método de pago";
                iTween.MoveTo(CodigoBarras, new Vector2((Screen.width * 0.5f), (Screen.height * 0.625f)), 0.5f);
                break;
            case "LectorQuitar":
                //DesError.text = "Se necesita llenar todos los campos para poder agregar esté método de pago";
                iTween.MoveTo(CodigoBarras, new Vector2((Screen.width * 0.5f), (-Screen.height * 0.75f)), 0.5f);
                break;
            case "Cargando":
                //DesError.text = "Se necesita llenar todos los campos para poder agregar esté método de pago";
                iTween.MoveTo(Cargando, new Vector2((Screen.width * 0.5f), (Screen.height * 0.625f)), 0.5f);
                break;
            case "CargandoQuitar":
                //DesError.text = "Se necesita llenar todos los campos para poder agregar esté método de pago";
                iTween.MoveTo(Cargando, new Vector2((Screen.width * 0.5f), (-Screen.height * 0.75f)), 0.5f);
                break;
            case "Cargando2":
                //DesError.text = "Se necesita llenar todos los campos para poder agregar esté método de pago";
                iTween.MoveTo(Cargando2, new Vector2((Screen.width * 0.5f), (Screen.height * 0.625f)), 0.5f);
                break;
            case "Cargando2Quitar":
                //DesError.text = "Se necesita llenar todos los campos para poder agregar esté método de pago";
                iTween.MoveTo(Cargando2, new Vector2((Screen.width * 0.5f), (-Screen.height * 0.75f)), 0.5f);
                break;
            case "InicioQuitar":
                iTween.MoveTo(Inicio, new Vector2((-Screen.width * 2f), Registrate.transform.position.y), 0.5f);
                break;
            default:
                break;
        }  
    }
    public void Cancelar()
    {
        ap.pagardentro = false;
        ap.eliminar = false;
    }

    IEnumerator Wait(GameObject NumBoton)
    {
        
        iTween.MoveTo(Menu, new Vector2((Screen.width * 1.75f), Registrate.transform.position.y), 0.5f);
        
        yield return new WaitForSecondsRealtime(0.1f);
        if(NumBoton.GetComponent<RectTransform>().GetSiblingIndex() < CompPerfil.GetComponent<RectTransform>().GetSiblingIndex())
            iTween.MoveTo(CompPerfil, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        if (NumBoton.GetComponent<RectTransform>().GetSiblingIndex() < CP.GetComponent<RectTransform>().GetSiblingIndex())
            iTween.MoveTo(CP, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        if (NumBoton.GetComponent<RectTransform>().GetSiblingIndex() < Pago.GetComponent<RectTransform>().GetSiblingIndex())
            iTween.MoveTo(Pago, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        if (NumBoton.GetComponent<RectTransform>().GetSiblingIndex() < Casa.GetComponent<RectTransform>().GetSiblingIndex())
            iTween.MoveTo(Casa, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        if (NumBoton.GetComponent<RectTransform>().GetSiblingIndex() < CompPerfil.GetComponent<RectTransform>().GetSiblingIndex())
            iTween.MoveTo(FPago, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        if (NumBoton.GetComponent<RectTransform>().GetSiblingIndex() < Emergencia.GetComponent<RectTransform>().GetSiblingIndex())
            iTween.MoveTo(Emergencia, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        if (NumBoton.GetComponent<RectTransform>().GetSiblingIndex() < FPHUB.GetComponent<RectTransform>().GetSiblingIndex())
            iTween.MoveTo(FPHUB, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        if (NumBoton.GetComponent<RectTransform>().GetSiblingIndex() < Aclara.GetComponent<RectTransform>().GetSiblingIndex())
            iTween.MoveTo(Aclara, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        if (NumBoton.GetComponent<RectTransform>().GetSiblingIndex() < Contacto.GetComponent<RectTransform>().GetSiblingIndex())
            iTween.MoveTo(Contacto, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        if (NumBoton.GetComponent<RectTransform>().GetSiblingIndex() < Error.GetComponent<RectTransform>().GetSiblingIndex())
            iTween.MoveTo(Error, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        if (NumBoton.GetComponent<RectTransform>().GetSiblingIndex() < Correcto.GetComponent<RectTransform>().GetSiblingIndex())
            iTween.MoveTo(Correcto, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        if (NumBoton.GetComponent<RectTransform>().GetSiblingIndex() < TYCM.GetComponent<RectTransform>().GetSiblingIndex())
            iTween.MoveTo(TYCM, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        if (NumBoton.GetComponent<RectTransform>().GetSiblingIndex() < MiPerfil.GetComponent<RectTransform>().GetSiblingIndex())
            iTween.MoveTo(MiPerfil, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        if (NumBoton.GetComponent<RectTransform>().GetSiblingIndex() < AjustarFoto.GetComponent<RectTransform>().GetSiblingIndex())
        { iTween.MoveTo(AjustarFoto, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f); mp.FotoAjustar.rectTransform.position = Vector3.zero;}
        if (NumBoton.GetComponent<RectTransform>().GetSiblingIndex() < AgFP.GetComponent<RectTransform>().GetSiblingIndex())
            iTween.MoveTo(AgFP, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        //iTween.MoveTo(MisCasasHUB, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        if (NumBoton.GetComponent<RectTransform>().GetSiblingIndex() < AgregarMisCasas.GetComponent<RectTransform>().GetSiblingIndex())
            iTween.MoveTo(AgregarMisCasas, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        if (NumBoton.GetComponent<RectTransform>().GetSiblingIndex() < MisCasasHUB.GetComponent<RectTransform>().GetSiblingIndex())
            iTween.MoveTo(MisCasasHUB, new Vector2((-Screen.width * 0.9f), Registrate.transform.position.y), 0f);
        //if (NumBoton.GetComponent<RectTransform>().GetSiblingIndex() < AgregarMisCasas.GetComponent<RectTransform>().GetSiblingIndex())
        //    iTween.MoveTo(AgregarMisCasas, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        if (NumBoton.GetComponent<RectTransform>().GetSiblingIndex() < ModFormaPago.GetComponent<RectTransform>().GetSiblingIndex())
            iTween.MoveTo(ModFormaPago, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        if (NumBoton.GetComponent<RectTransform>().GetSiblingIndex() < ModMisCasas.GetComponent<RectTransform>().GetSiblingIndex())
            iTween.MoveTo(ModMisCasas, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        if (NumBoton.GetComponent<RectTransform>().GetSiblingIndex() < MiHistorial.GetComponent<RectTransform>().GetSiblingIndex())
            iTween.MoveTo(MiHistorial, new Vector2((-Screen.width * 0.9f), Registrate.transform.position.y), 0f);
        if (NumBoton.GetComponent<RectTransform>().GetSiblingIndex() < Historial.GetComponent<RectTransform>().GetSiblingIndex())
            iTween.MoveTo(Historial, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        if (NumBoton.GetComponent<RectTransform>().GetSiblingIndex() < MisPagos.GetComponent<RectTransform>().GetSiblingIndex())
            iTween.MoveTo(MisPagos, new Vector2((-Screen.width * 0.9f), Registrate.transform.position.y), 1f);
        if (NumBoton.GetComponent<RectTransform>().GetSiblingIndex() < AgregarCategoria.GetComponent<RectTransform>().GetSiblingIndex())
            iTween.MoveTo(AgregarCategoria, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        if (NumBoton.GetComponent<RectTransform>().GetSiblingIndex() < AgregarTipoPago.GetComponent<RectTransform>().GetSiblingIndex())
            iTween.MoveTo(AgregarTipoPago, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        if (NumBoton.GetComponent<RectTransform>().GetSiblingIndex() < AgregarPago.GetComponent<RectTransform>().GetSiblingIndex())
            iTween.MoveTo(AgregarPago, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        if (NumBoton.GetComponent<RectTransform>().GetSiblingIndex() < MisPagosVencidos.GetComponent<RectTransform>().GetSiblingIndex())
            iTween.MoveTo(MisPagosVencidos, new Vector2((-Screen.width * 0.9f), Registrate.transform.position.y), 1f);
        if (NumBoton.GetComponent<RectTransform>().GetSiblingIndex() < HistorialAño.GetComponent<RectTransform>().GetSiblingIndex())
            iTween.MoveTo(HistorialAño, new Vector2((-Screen.width * 0.9f), Registrate.transform.position.y), 1f);
        if (NumBoton.GetComponent<RectTransform>().GetSiblingIndex() < HistorialMes.GetComponent<RectTransform>().GetSiblingIndex())
            iTween.MoveTo(HistorialMes, new Vector2((-Screen.width * 0.9f), Registrate.transform.position.y), 1f);
        if (NumBoton.GetComponent<RectTransform>().GetSiblingIndex() < HistorialPagos.GetComponent<RectTransform>().GetSiblingIndex())
            iTween.MoveTo(HistorialPagos, new Vector2((-Screen.width * 0.9f), Registrate.transform.position.y), 1f);
        if (NumBoton.GetComponent<RectTransform>().GetSiblingIndex() < Mes2.GetComponent<RectTransform>().GetSiblingIndex())
            iTween.MoveTo(Mes2, new Vector2((Screen.width * 2f), Registrate.transform.position.y), 1f);
        if (NumBoton.GetComponent<RectTransform>().GetSiblingIndex() < Mes3.GetComponent<RectTransform>().GetSiblingIndex())
            iTween.MoveTo(Mes3, new Vector2((Screen.width * 2f), Registrate.transform.position.y), 1f);
        if (NumBoton.GetComponent<RectTransform>().GetSiblingIndex() < CasaPagosHUB.GetComponent<RectTransform>().GetSiblingIndex())
            iTween.MoveTo(CasaPagosHUB, new Vector2((-Screen.width * 0.9f), Registrate.transform.position.y), 0f);
        if (NumBoton.GetComponent<RectTransform>().GetSiblingIndex() < Detalles.GetComponent<RectTransform>().GetSiblingIndex())
            iTween.MoveTo(Detalles, new Vector2((-Screen.width * 0.9f), Registrate.transform.position.y), 1f);
        if (NumBoton.GetComponent<RectTransform>().GetSiblingIndex() < Reporte.GetComponent<RectTransform>().GetSiblingIndex())
            iTween.MoveTo(Reporte, new Vector2((-Screen.width * 0.9f), Registrate.transform.position.y), 1f);
        if (NumBoton.GetComponent<RectTransform>().GetSiblingIndex() < IniciarSesionError.GetComponent<RectTransform>().GetSiblingIndex())
            iTween.MoveTo(IniciarSesionError, new Vector2((-Screen.width * 0.9f), Registrate.transform.position.y), 1f);
        
        //if(Atras.Count != 0)
        //iTween.MoveTo(Atras[Atras.Count - 1], new Vector2((-Screen.width * 0.9f), Registrate.transform.position.y), 1f);
        if (NumBoton == MisCasasHUB || NumBoton == CasaPagosHUB || NumBoton == MiHistorial)
        {
            iTween.MoveTo(NumBoton, new Vector2((Screen.width * 0.5f), Registrate.transform.position.y), 0f);
        }
        //yield return new WaitForSecondsRealtime(0.1f);
        if (NumBoton != MisCasasHUB && NumBoton != CasaPagosHUB && NumBoton != MiHistorial)
            iTween.MoveTo(NumBoton, new Vector2((Screen.width * 0.5f), Registrate.transform.position.y), 1f);
    }

    IEnumerator Waiting()
    {
        iTween.MoveTo(Menu, new Vector2((Screen.width * 1.75f), Registrate.transform.position.y), 0.5f);
        yield return new WaitForSecondsRealtime(0.1f);
        iTween.MoveTo(CompPerfil, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(CP, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(Pago, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(Casa, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(FPago, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(Emergencia, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(FPHUB, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(Aclara, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(Contacto, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(Error, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(Correcto, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(TYCM, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(MiPerfil, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(AgFP, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(MisCasasHUB, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(AgregarMisCasas, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(MisCasasHUB, new Vector2((-Screen.width * 0.9f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(AgregarMisCasas, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(ModFormaPago, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(ModMisCasas, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(MiHistorial, new Vector2((-Screen.width * 0.9f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(Historial, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(MisPagos, new Vector2((-Screen.width * 0.9f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(AgregarCategoria, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(AgregarTipoPago, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(AgregarPago, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(MisPagosVencidos, new Vector2((-Screen.width * 0.9f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(HistorialAño, new Vector2((-Screen.width * 0.9f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(HistorialMes, new Vector2((-Screen.width * 0.9f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(HistorialPagos, new Vector2((-Screen.width * 0.9f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(Mes2, new Vector2((Screen.width * 2f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(Mes3, new Vector2((Screen.width * 2f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(CasaPagosHUB, new Vector2((-Screen.width * 0.9f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(Detalles, new Vector2((-Screen.width * 0.9f), Registrate.transform.position.y), 1f);
        //iTween.MoveTo(Inicio, new Vector2((-Screen.width * 0.9f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(Detalles, new Vector2((-Screen.width * 0.9f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(CompPerfil, new Vector2((-Screen.width * 1f), Registrate.transform.position.y), 0.5f);
        iTween.MoveTo(Inicio, new Vector2((-Screen.width * 1f), Registrate.transform.position.y), 0.5f);
        iTween.MoveTo(CompPerfilFinal, new Vector2((-Screen.width * 1f), Registrate.transform.position.y), 0.5f);
        iTween.MoveTo(CP, new Vector2((-Screen.width * 1f), Registrate.transform.position.y), 0.5f);
        iTween.MoveTo(Registrate, new Vector2((-Screen.width * 1f), Registrate.transform.position.y), 0.5f);
        iTween.MoveTo(TermYCond, new Vector2((-Screen.width * 1f), Registrate.transform.position.y), 0.5f);
        iTween.MoveTo(Casa, new Vector2((-Screen.width * 1f), Registrate.transform.position.y), 0.5f);
        iTween.MoveTo(RegEmail, new Vector2((-Screen.width * 1f), Registrate.transform.position.y), 0.5f);
        iTween.MoveTo(Ingresa, new Vector2((-Screen.width * 1f), Registrate.transform.position.y), 0.5f);
        iTween.MoveTo(Reporte, new Vector2((-Screen.width * 0.9f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(IniciarSesionError, new Vector2((-Screen.width * 0.9f), Registrate.transform.position.y), 1f);
        yield return new WaitForSecondsRealtime(0.1f);
        
    }

    IEnumerator Wait2(GameObject NumBoton)
    {
        iTween.MoveTo(Menu, new Vector2((Screen.width * 1.75f), Registrate.transform.position.y), 0.5f);
        yield return new WaitForSecondsRealtime(0.1f);
        //iTween.MoveTo(Atras[i - 1], new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(Registrate, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(RegEmail, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(CompPerfil, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(CP, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(Pago, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(Casa, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(FPago, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(Emergencia, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(FPHUB, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(Aclara, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(Contacto, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(Error, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(Correcto, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(TYCM, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(MiPerfil, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(AgFP, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(Ingresa, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(TermYCond, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(Inicio, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(Mes2, new Vector2((Screen.width * 2f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(Mes3, new Vector2((Screen.width * 2f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(MisPagos2, new Vector2((Screen.width * 2f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(MisPagos3, new Vector2((Screen.width * 2f), Registrate.transform.position.y), 1f);
        iTween.MoveTo(CasaPagosHUB, new Vector2((-Screen.width * 0.75f), Registrate.transform.position.y), 1f);
        yield return new WaitForSecondsRealtime(0.1f);
        iTween.MoveTo(NumBoton, new Vector2((Screen.width * 0.5f), Registrate.transform.position.y), 1f);
    }
    IEnumerator Wait3(GameObject Op)
    {
        Pagos = false;
        iTween.MoveTo(Op, new Vector2((-Screen.width * 0.9f), Registrate.transform.position.y), 1f);

        Atras.Remove(Atras[i - 1]);
        i--;
        yield return new WaitForSecondsRealtime(0.1f);
        if(Atras.Count != 0)
        iTween.MoveTo(Atras[i-1], new Vector2((Screen.width * 0.5f), Registrate.transform.position.y), 1f);
        
    }
    //IEnumerator Retard()
    //{
    //    yield return new WaitForSeconds(25f);
    //    if (reg.passcorrecto == false)
    //    {
    //        DesError.text = "El correo electrónico o la contraseña son incorrectos, verifiquelos y vuelve a intentarlo.";
    //        iTween.MoveTo(Errors, new Vector2((Screen.width * 0.5f), (Screen.height * 0.5f)), 0.25f);
    //    }
    //    if (reg.passcorrecto == true)
    //    {
    //        DesError.text = "El correo electrónico o la contraseña son incorrectos, verifiquelos y vuelve a intentarlo.";
    //        iTween.MoveTo(Errors, new Vector2((Screen.width * 0.5f), (-Screen.height * 0.5f)), 1f);
    //    }
    //    yield return new WaitForSeconds(10f);
    //}
}
