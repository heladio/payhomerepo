﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VoxelBusters.Utility;
using VoxelBusters.Utility.UnityGUI.MENU;
using VoxelBusters.NativePlugins;
using System;
using System.IO;

public class Casas : MonoBehaviour {
    [SerializeField]
    TakePhotos takePhotos;

    [SerializeField] Text NickCasa, UbicacionCasa, CodigoAsociacion, NickCasaR, UbicacionCasaR, CodigoAsociacionR, NickC;
    public string NNCasa, UbCasa, CodAsC, NNCasaR, UbCasaR, CodAsCR, NNCasaBajo, NNCasaComp, error, FotoCs, linkfoto;
    [SerializeField] InputField NickCa, UbiCa, CodCa;
    [SerializeField] ObjectManager ob;
    [HideInInspector]
    public List<string> NNC, UbC, CodC, FotoC, NNOffline, UbOffline, CodOffline = new List<string>();
    
    public List<int> ID, Foto, idFoto, PosicionCasa, IDLocal, IDOffline= new List<int>();
    
    public List<GameObject> Casass, Casass2, CasaPagos = new List<GameObject>();
    [HideInInspector]
    public int i = 0, b = 0, foto, idFot, p, posicion, y, IdCasa, IdCasaFoto, IdCasaLocal;
    [HideInInspector]
    public GameObject casa, casa2, casas, casas2, casax, casax2, casaclone, casaclone2, HistorialCasa, HistorialX, HistorialClone, Historiales, casa3, casas3, casax3, casaclone3;
    [HideInInspector]
    public Vector2 G, G2, G3;
    public InfoCasa InfoC;
    public InfoCasaMH InfoMH;
    public InfoCasaPago InfoCP;
    public AP ap;
    //public Guardar save;
    public float s = 0, d, d2;
    public ScrollRect scroll, scroll2, scroll3;
    public Text HeaderHistorial;
    public Vector2 scrollOriginal, LimiteSuperior, LimiteInferior, PrimerTP, UltimoTP, posicioncontent, scrollinferior, contentActual;
    public Vector2 scrollOriginal2, LimiteSuperior2, LimiteInferior2, PrimerTP2, UltimoTP2, posicioncontent2, scrollinferior2, contentActual2;
    public Vector2 scrollOriginalPagos, LimiteSuperiorPagos, LimiteInferiorPagos, PrimerTPPagos, UltimoTPPagos, posicioncontentPagos, scrollinferiorPagos, contentActualPagos;
    public GameObject content, limsup, liminf;
    public GameObject content2, limsup2, liminf2;
    public GameObject contentPagos, limsupPagos, liminfPagos;
    public bool bajo, bajo2, bajoPagos, checar = true, norepetido;
    public Swipe sw;
    public GameObject HistorialAñoX, HistorialMeseX, HistorialCasas, HistorialPagoX;
    private List<string> m_results = new List<string>();
    public GameObject FotoCasass, FotoCasaRegistro, FotoCasaHistorial, FotoCasaPago, FondoRegistro, FondoC;
    public Image FotoCasas, FotoCasaHist, FotoCasaP, FotoCasaReg, FondoReg, FondoCasa;
    public bool FotoTomada, igual, checate, registro;
    public Sprite FotoCasa;
    public DBCasate casate;
    public Registro reg;
    public List<DBCasate> Casa = new List<DBCasate>();
    public MFC mfc;
    public Text CasaAñosHistorial, CasaPagosHistorial;
    public MP mp;
    public byte[] bytes;
    Texture2D fotota;
    public GameObject contentCasa, Refrescar, circulo;
    public bool RefBool;
    float TimerRef, pa;
    [HideInInspector]
    public string Offline, NoEnviadoAgregar;
    string[] Off, escribir, NEAgregar, escribirAgregar, nombreCasa;
    Char delimiter = ';';
    Char del = ',';
    byte[] bytes2;

    // Use this for initialization
    public void Offl()
    {
        Offline = PlayerPrefs.GetString("Casas");
        //print("OFFLINE " + Offline);
        Off = Offline.Split(delimiter);
        NoEnviadoAgregar = PlayerPrefs.GetString("CasasAgregar");
        NEAgregar = NoEnviadoAgregar.Split(delimiter);
        for(int gh = 0; gh<Off.Length; gh++)
        {
            escribir = null;
            escribir = Off[gh].Split(del);
            if (escribir[0] != "")
            {
                NNCasa = escribir[2];
                CodAsC = escribir[4];
                UbCasa = escribir[3];
                Int32.TryParse(escribir[0], out IdCasaLocal);
                Int32.TryParse(escribir[1], out IdCasa);
                IDLocal.Add(IdCasaLocal);
                ID.Add(IdCasa);
                NNC.Add(NNCasa);
                UbC.Add(UbCasa);
                CodC.Add(CodAsC);
                FotoC.Add("");
                PosicionCasa.Add(posicion);
                posicion++;
                i = IdCasaLocal;
                X();
            }
        }
        for (int gh = 0; gh < NEAgregar.Length; gh++)
        {
            escribirAgregar = null;
            escribirAgregar = NEAgregar[gh].Split(del);
            if (escribirAgregar[0] != "")
            {
                NNCasa = escribirAgregar[1];
                CodAsC = escribirAgregar[3];
                UbCasa = escribirAgregar[2];
                Int32.TryParse(escribirAgregar[0], out IdCasaLocal);
                IDOffline.Add(IdCasaLocal);
                NNOffline.Add(NNCasa);
                UbOffline.Add(UbCasa);
                CodOffline.Add(CodAsC);
            }
        }
        if(IDOffline.Count > 0)
        {
            AgregarCasa();
        }
    }
    // Update is called once per frame
    void Update()
    {
        //if(NNOffline.Count > 0)
        //{
        //    AgregarCasa();
        //}
        if (FotoTomada == true)
        {
            //Foto.SetActive(true);
            FotoCasaRegistro.SetActive(true);
            FotoCasass.SetActive(true);
            FondoRegistro.SetActive(true);
            FondoC.SetActive(true);            
            

            //FondoCasa.preserveAspect = true;

            FotoCasaReg.sprite = FotoCasa;
            //FotoCasaReg.SetNativeSize();


            FondoReg.sprite = FotoCasa;
            //FondoReg.SetNativeSize();


            //OrderImages(FotoCasas);
            //OrderImages(FondoCasa);
            //OrderImages(FotoCasaReg);
            //OrderImages(FondoReg);

            //FondoReg.preserveAspect = true;
            foto = 1;
        }
        else
        {
            FotoCasaRegistro.SetActive(false);
            FotoCasass.SetActive(false);
            FondoRegistro.SetActive(false);
            FondoC.SetActive(false);
            FotoCasa = null;
            foto = 0;
        }
        

        //NNCasa = NickCasa.text;

        //UbCasa = UbicacionCasa.text;
        //CodAsC = CodigoAsociacion.text;
        NNCasaR = NickCasaR.text;
        UbCasaR = UbicacionCasaR.text;
        CodAsCR = CodigoAsociacionR.text;

        
        /*if (TimerRef > 0.25)
        {
            if (IDOffline.Count == 0 && mfc.IdCasaMod.Count == 0 && mfc.IdCasaEliminar.Count == 0 && Application.internetReachability != NetworkReachability.NotReachable)
            {
                if (!reg.isStart)
                    ob.Click("MisCasasHUB");
                reg.carga = true;
                Casa.Clear();
                checate = true;
                if (Casass.Count > 0)
                {
                    posicion = 0;
                    mfc.EliminarTodos();

                }
                
                    ReadCard();
                
            }
            else if(IDOffline.Count > 0)
            {
                ob.DesError.text = "No se puede recargar las casas, ya que faltan actualizar las casas a la nube";
                ob.Click("Errors");
            }
            else if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                ob.DesError.text = "No se puede recargar las casas, ya que no hay conexión con el servidor";
                ob.Click("Errors");
            }
        }*/

    }

    void OrderImages(Image img)
    {
        if (img.sprite!=null)
        {
            img.GetComponent<AspectRatioFitter>().aspectMode = AspectRatioFitter.AspectMode.EnvelopeParent;
            if (img.sprite.texture.width > img.sprite.texture.height)
            {
                img.GetComponent<AspectRatioFitter>().aspectRatio = 2f;

            }
            else
            {
                //FotoCasas.GetComponent<AspectRatioFitter>().aspectMode = AspectRatioFitter.AspectMode.EnvelopeParent;
                img.GetComponent<AspectRatioFitter>().aspectRatio = 0.001f;
            }
        }
    }

    public void ReadCard()
    {
        
        //reg.cargando = true;
        ObtenerCasa();
    }

    public void Guardar()
    {
        igual = false;
        NNCasaBajo = NickCa.text.Replace(" ", "");
        NNCasaBajo = NNCasaBajo.ToLower();
        for (int u = 0; Casass.Count > u; u++) {
            NNCasaComp = Casass[u].GetComponent<InfoCasa>().NickNameC.Replace(" ", "");

            NNCasaComp = NNCasaComp.ToLower();
            if(NNCasaComp == NNCasaBajo)
            {
                igual = true;
            }
        }
        if (igual == false)
        {
            if (NickCa.text != "")
            {
                AgregarCasaOffline();
                //AgregarCasa();
                //ob.Click("AtrasCasa");

            }

            else
            {
                ob.Click("ErrorTarjeta");
                igual = false;
            }
        }
        else
        {
            ob.DesError.text = "No se puede agregar esta casa ya que ya tienes una con el mismo nombre";
            ob.Click("Errors");
        }

    }
    public void GuardarVer()
    {
        if (NickCasaR.text != "")
        {
    //        scroll.movementType = ScrollRect.MovementType.Elastic;
    //scroll.elasticity = 0;
        
            //Foto.Add(foto);
            if (FotoTomada == true)
            {

                //idFoto.Add(p);
            }

            AgregarCasaOfflineR();
        ob.Click("CompPerfil-Final");
    }
        else
        {
            ob.DesError.text = "No se puede agregar esta casa ya que ya tienes una con el mismo nombre";
            ob.Click("Errors");
        }
    }

    public void X()
    {
        //print("hago casa");
        G = contentCasa.transform.position;
        G2 = content2.transform.position;
        G3 = contentPagos.transform.position;

        //if (NickCasa.text != "" && UbicacionCasa.text != "" && CodigoAsociacion.text != "")
        //{
        //    NickCasa.text = null;
        //    UbicacionCasa.text = null;
        //    CodigoAsociacion.text = null;
        //}
        //if (b == 0 && i == 1)
        //{
        //    b++;
        casaclone = Instantiate(casa, G, Quaternion.identity, casas.transform);
        casaclone2 = Instantiate(casa2, G2, Quaternion.identity, casas2.transform);
        casaclone3 = Instantiate(casa3, G3, Quaternion.identity, casas3.transform);
        //Instantiate(HistorialCasa, G2, Quaternion.identity, HistorialCasa.transform);
        //casaclone = GameObject.Find("Casax(Clone)");
        casaclone.name = "Casa" + posicion;
        //casaclone2 = GameObject.Find("CasaMHx(Clone)");
        casaclone2.name = "CasaMH" + posicion;
        //casaclone3 = GameObject.Find("CasaPagoX 1(Clone)");
        casaclone3.name = "CasaPago" + posicion;
        //HistorialClone = GameObject.Find("HistorialCasaX(Clone)");
        //HistorialClone.name = "HistorialCasa" + i;
        InfoC = casaclone.GetComponent<InfoCasa>();
        InfoMH = casaclone2.GetComponent<InfoCasaMH>();
        InfoCP = casaclone3.GetComponent<InfoCasaPago>();
        InfoC.FotoCasa = FotoCs;
        Casass.Add(casaclone);
        Casass2.Add(casaclone2);
        CasaPagos.Add(casaclone3);
        //Historial.Add(HistorialClone);
        InfoMH.NN.text = NNCasa;
        InfoMH.NickName = NNCasa;
        InfoMH.ob = ob;
        InfoMH.Casa = HeaderHistorial;
        InfoMH.PagoHistorialX = HistorialPagoX;
        InfoCP.NN.text = NNCasa;
        InfoCP.NickName = NNCasa;
        InfoCP.ob = ob;
        InfoC.NickNameCasa.text = NNCasa;
        InfoC.NickNameC = NNCasa;
        InfoC.CodigoC = CodAsC;
        InfoC.UbicacionC = UbCasa;
        InfoC.idCasaLocal = IdCasaLocal;
        InfoCP.CasaIDLocal = IdCasaLocal;
        InfoMH.CasaIDLocal = IdCasaLocal;
        InfoC.FotoCasa = FotoCs;
        InfoC.fotos = foto;
        InfoMH.fotos = foto;
        InfoCP.fotos = foto;

        if (FotoTomada == true)
        {
            //print("foto tomada");
            //InfoC.idFoto = p;
            InfoC.FotoTomada = FotoTomada;
            InfoC.foto = FotoCasa;
            InfoC.cambio = true;
            //InfoMH.idFoto = p;
            InfoMH.FotoTomada = FotoTomada;
            InfoMH.foto = FotoCasa;
            InfoMH.cambio = true;
            //InfoCP.idFoto = p;
            InfoCP.FotoTomada = FotoTomada;
            InfoCP.foto = FotoCasa;
            InfoCP.cambio = true;         
        }
        InfoC.idCasa = IdCasa;
        InfoMH.CasaID = IdCasa;
        InfoCP.CasaID = IdCasa;
        InfoMH.Historial = HistorialClone;
        InfoC.ob = ob;
        //return;
        //InfoMH.ap = ap;
        InfoC.casa = this;
        InfoC.posicion = posicion - 1;
        InfoCP.posicion = posicion - 1;
        casaclone = null;
        casaclone2 = null;
        casaclone3 = null;
        NNCasa = null;
        UbCasa = null;
        CodAsC = null;
        FotoCs = null;
        //FotoTomada = false;
        NNCasa = null;
        UbCasa = null;
        CodAsC = null;
        for (int yh = 0; yh < IDLocal.Count; yh++)
        {
            if (yh == 0)
                Offline = IDLocal[yh] + "," + ID[yh] + "," + NNC[yh] + "," + UbC[yh] + "," + CodC[yh];
            else if (yh > 0)
                Offline += ";" + IDLocal[yh] + "," + ID[yh] + "," + NNC[yh] + "," + UbC[yh] + "," + CodC[yh];


        }
        PlayerPrefs.SetString("Casas", Offline);

        //NickCasa.text = "";
        //UbicacionCasa.text = "";
        //CodigoAsociacion.text = "";        
    }

    public void TomarFoto()
    {
        ob.Click("Imagen");
        ob.ImagenCamara.onClick.RemoveAllListeners();
        ob.ImagenCamara.onClick.AddListener(PickImageFromCamera);
        ob.ImagenGaleria.onClick.RemoveAllListeners();
        ob.ImagenGaleria.onClick.AddListener(PickImageFromAlbum);
    }

    public void PickImageFromAlbum()
    {
        reg.StartLoad();
        StartCoroutine(Album());
    }

    IEnumerator Album()
    {
        yield return new WaitForSeconds(0.5f);

        ob.Click("ImagenQuitar");
        takePhotos.isFinished += TakePhoto;
#if UNITY_IOS
        // Set popover to last touch position
        NPBinding.UI.SetPopoverPointAtLastTouchPosition();
#else
        // Pick image
        //takePhotos.PickImage(2048);
#endif
   //     NPBinding.MediaLibrary.PickImage(eImageSource.ALBUM, 1, takePhotos.PickImageFinished);
    }

    public void PickImageFromCamera()
    {
        reg.StartLoad();
        StartCoroutine(Camera());
    }

    IEnumerator Camera()
    {
        yield return new WaitForSeconds(0.5f);
        ob.Click("ImagenQuitar");
        takePhotos.isFinished += TakePhoto;
#if UNITY_IOS
        // Set popover to last touch position
        NPBinding.UI.SetPopoverPointAtLastTouchPosition();
        NPBinding.MediaLibrary.PickImage(eImageSource.CAMERA, 1, takePhotos.PickImageFinished);
#else
        //wasPhoto = true;
        takePhotos.TakePicture(2048);
#endif
    }

    void TakePhoto()
    {
        //SetAllowsImageEditing();
        Texture2D image3 = takePhotos.photoImage;

        bytes2 = image3.EncodeToJPG(50);
        ////image2.Resize(image2.width / 2, image2.height / 2);
        TextureScale.Point(image3, image3.width, image3.height);
        FotoCasa = Sprite.Create(takePhotos.photoImage, new Rect(/*(takePhotos.photoImage.width / 4)*/0, /*(takePhotos.photoImage.height / 4)*/0, (takePhotos.photoImage.width /* 2*/), (takePhotos.photoImage.height /* 2*/)), Vector2.zero);
        FotoTomada = true;
        p++;


        FotoCasas.sprite = FotoCasa;
        FotoCasas.SetNativeSize();

        //FotoCasas.preserveAspect = true;
        FondoCasa.sprite = FotoCasa;
        FondoCasa.SetNativeSize();


        OrderImages(FotoCasas);
        OrderImages(FondoCasa);

        //ob.Click("AtrasCasa");
        //ob.Click("ModMisCasas");
        takePhotos.isFinished -= TakePhoto;
        reg.EndLoad();
        System.GC.Collect();
        OrderImages(InfoC.casapersonalizada);
    }

    /* public void PickImageFinished(ePickImageFinishReason _reason, Texture2D _image)
     {
         AddNewResult("Request to pick image from gallery finished. Reason for finish is " + _reason + ".");
         AppendResult(string.Format("Selected image is {0}.", (_image == null ? "NULL" : _image.ToString())));

         if (_image != null)
         {
             //SetAllowsImageEditing();
             Texture2D image3 = _image;
             bytes2 = image3.EncodeToJPG();
             ////image2.Resize(image2.width / 2, image2.height / 2);
             //TextureScale.Point(image3, image3.width / 4, image3.height / 4);
             FotoCasa = Sprite.Create(_image, new Rect((_image.width / 4), (_image.height / 4), (_image.width / 2), (_image.height / 2)), Vector2.zero);
             FotoTomada = true;
             p++;


             //ob.Click("AtrasCasa");
             //ob.Click("ModMisCasas");

         }

     }
     private void SetAllowsImageEditing()
     {
         NPBinding.MediaLibrary.SetAllowsImageEditing(true);
     }

     private static eImageSource GetBOTH()
     {
         return eImageSource.BOTH;
     }
     private static eImageSource GetCAMERA()
     {
         return eImageSource.CAMERA;
     }

     private static eImageSource GetALBUM()
     {
         return eImageSource.ALBUM;
     }
     protected void AppendResult(string _result)
     {
         m_results.Add(_result);
     }

     protected void AddNewResult(string _result)
     {
         m_results.Clear();
         m_results.Add(_result);
     }*/
    IEnumerator DelayGetCasas()
    {
        //print("get casas");
        if (reg.ingresa == false)
        {
            //reg.CargateMenor();
        }
        yield return new WaitForSeconds(1f);
        if (reg.ingresa == true)
        {
            ap.tp.ReadCard();
        }
        checate = false;
        for (y = 0; y < reg.Casas.Count; y++)
        {
            //reg.cargando = true;
            norepetido = true;
            casate = Casa[y];
            for (int i = 0; i < Casass.Count; i++)
            {
                if (casate.IdCasa == Casass[i].GetComponent<InfoCasa>().idCasa)
                {

                    norepetido = false;
                }

            }
            if (IDLocal.Count < Casa.Count)
            {
                IDLocal.Add(IdCasaLocal);
                IdCasaLocal++;
                ID.Add(casate.IdCasa);
                IdCasa = casate.IdCasa;
                NNC.Add(casate.NombreCasa);
                UbC.Add(casate.UbicacionCasa);
                CodC.Add(casate.CodigoActivacion);
                FotoC.Add(casate.FotoCasa);

                //if(casate.FotoCasa != null)
                //{
                //    StartCoroutine(Starts(casate.FotoCasa));
                //}
                
                NNCasa = casate.NombreCasa;
                UbCasa = casate.UbicacionCasa;
                CodAsC = casate.CodigoActivacion;
                if (casate.FotoCasa != "" && casate.FotoCasa != null)
                {
                    FotoCs = casate.FotoCasa;
                    FotoTomada = true;
                    //StartCoroutine(Starts(FotoCs));
                }
                else if (casate.FotoCasa == "" || casate.FotoCasa == null)
                {
                    FotoCs = null;
                    FotoTomada = false;
                }
                if (casate.IdCasa == IdCasaFoto)
                {
                    FotoCs = linkfoto;
                    IdCasaFoto = 0;
                    FotoTomada = true;

                }
                PosicionCasa.Add(posicion);
                posicion++;
                i = casate.IdCasa;
                X();
            }

        }
        /*if (y == reg.Casas.Count)
        {
            //reg.Casas = null;
            //casate = null;
            //reg.cargando = false;
        }*/

        reg.GetData();

    }
    public void AgregarCasaOfflineR()
    {
        registro = true;
        IdCasa = 0;
        IdCasaLocal++;
        NNCasa = NickCasaR.text;
        CodAsC = CodigoAsociacionR.text;
        UbCasa = UbicacionCasaR.text;
        ID.Add(IdCasa);
        IDLocal.Add(IdCasaLocal);
        NNC.Add(NNCasa);
        UbC.Add(UbCasa);
        CodC.Add(CodAsC);
        FotoC.Add("");
        PosicionCasa.Add(posicion);
        IDOffline.Add(IdCasaLocal);
        NNOffline.Add(NickCasaR.text);
        UbOffline.Add(UbicacionCasaR.text);
        CodOffline.Add(CodigoAsociacionR.text);
        posicion++;
        i = IdCasaLocal;
        X();
        NickCasaR.text = null;
        UbicacionCasaR.text = null;
        CodigoAsociacionR.text = null;
        //ob.Click("MisCasasHUB");
        Offline = null;
        Off = null;
        for (int yh = 0; yh < IDLocal.Count; yh++)
        {
            if (yh == 0)
                Offline = IDLocal[yh] + "," + ID[yh] + "," + NNC[yh] + "," + UbC[yh] + "," + CodC[yh];
            else if (yh > 0)
                Offline += ";" + IDLocal[yh] + "," + ID[yh] + "," + NNC[yh] + "," + UbC[yh] + "," + CodC[yh];


        }
        PlayerPrefs.SetString("Casas", Offline);
        NoEnviadoAgregar = null;
        for (int hot = 0; hot < IDOffline.Count; hot++)
        {
            if (hot == 0)

                NoEnviadoAgregar = IDOffline[hot] + "," + NNOffline[hot] + "," + UbOffline[hot] + "," + CodOffline[hot];

            else if (hot > 0)
                NoEnviadoAgregar += ";" + IDOffline[hot] + "," + NNOffline[hot] + "," + UbOffline[hot] + "," + CodOffline[hot];
        }
        PlayerPrefs.SetString("CasasAgregar", NoEnviadoAgregar);
        Off = Offline.Split(delimiter);
        AgregarCasaR();
    }
    public void AgregarCasaOffline()
    {
        IdCasa = 0;
        IdCasaLocal++;
        NNCasa = NickCa.text;
        CodAsC = CodCa.text;
        UbCasa = UbiCa.text;
        ID.Add(IdCasa);
        IDLocal.Add(IdCasaLocal);
        NNC.Add(NNCasa);
        UbC.Add(UbCasa);
        CodC.Add(CodAsC);
        FotoC.Add("");
        PosicionCasa.Add(posicion);
        IDOffline.Add(IdCasaLocal);
        NNOffline.Add(NickCa.text);
        UbOffline.Add(UbiCa.text);
        CodOffline.Add(CodCa.text);
        posicion++;
        i = IdCasaLocal;
        //if (FotoTomada == true)
        //{
        //    File.WriteAllBytes(Application.persistentDataPath + "/" + IdCasa +".jpg", bytes);
        //}
        X();
        ob.Click("MisCasasHUB");
        NickCa.text = null;
        CodCa.text = null;
        UbiCa.text = null;
        Offline = null;
        Off = null;
        for(int yh =0; yh<NNC.Count; yh++)
        {
            if(yh == 0)
                Offline = IDLocal[yh] + "," + ID[yh] + "," + NNC[yh] + "," + UbC[yh] + "," + CodC[yh]; 
            else if (yh > 0)
                Offline += ";" +IDLocal[yh] + "," + ID[yh] + "," + NNC[yh] + "," + UbC[yh] + "," + CodC[yh];
            

        }
        PlayerPrefs.SetString("Casas", Offline);
        NoEnviadoAgregar = null;
        for (int hot = 0; hot<NNOffline.Count; hot++)
        {
            if (hot == 0)

                NoEnviadoAgregar = IDOffline[hot] + "," + NNOffline[hot] + "," + UbOffline[hot] + "," + CodOffline[hot];

            else if (hot > 0)
                NoEnviadoAgregar += ";" + IDOffline[hot] + "," + NNOffline[hot] + "," + UbOffline[hot] + "," + CodOffline[hot];
        }
        PlayerPrefs.SetString("CasasAgregar", NoEnviadoAgregar);
        Off = Offline.Split(delimiter);
        
        AgregarOffline();
    }

    public void AgregarOffline()
    {
        for (int AgOff = 0; NNOffline.Count > AgOff; AgOff++)
        {
            AgregarCasa();
        }
    }

    public void AgregarCasa()
    {
        if (IDOffline.Count != 0)
        {
            WWWForm Form = new WWWForm();
            Form.AddField("IdUsuario", reg.id);
            Form.AddField("NombreCasa", NNOffline[0]);
            //Form.AddBinaryData("FotoCasa", bytes2);
            Form.AddField("UbicacionCasa", UbOffline[0]);
            Form.AddField("CodigoActivacion", CodOffline[0]);
        
        WebServices.AgregarCasas(Form, SuccessAgregarCasa, ErrorAgregar);
    }
        //reg.carga = true;
        //ob.DesError.text = "No se pudo agregar la casa, checa tu conexión e intentalo de nuevo";
    }
    public void AgregarCasaR()
    {
        WWWForm Form = new WWWForm();
        Form.AddField("IdUsuario", reg.id);
        Form.AddField("NombreCasa", NNOffline[0]);
        //Form.AddField("FotoCasa", "");
        Form.AddField("UbicacionCasa", UbOffline[0]);
        Form.AddField("CodigoActivacion", CodOffline[0]);
        WebServices.AgregarCasas(Form, SuccessAgregarCasaR, Error);
        //reg.carga = true;
        //ob.DesError.text = "No se pudo agregar la casa, checa tu conexión e intentalo de nuevo";
    }

    void SuccessAgregarCasa(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            reg.carga = false;

            error = ("" + ((MessageArg<string>)e).responseValue);
            //print("!");
        }
        else if (e is MessageArg<DBCasate>)
        {
            //right = true;
            //register1 = 1;
            //PlayerPrefs.SetInt("Registrado", register1);
            //NickCasa.text = null;
            //UbicacionCasa.text = null;
            //CodigoAsociacion.text = null;

            DBCasate dbCasame = ((MessageArg<DBCasate>)e).responseValue;
            ob.Click("Aviso");
            ob.avisotexto.text = "Se agregó correctamente la casa";
            for (int id = 0; id < Casass.Count; id++)
            {
                if (Casass[id].GetComponent<InfoCasa>().idCasaLocal == IDOffline[0])
                {
                    ID[id] = dbCasame.IdCasa;
                    Casass[id].GetComponent<InfoCasa>().idCasa = dbCasame.IdCasa;
                    Casass2[id].GetComponent<InfoCasaMH>().CasaID = dbCasame.IdCasa;
                    CasaPagos[id].GetComponent<InfoCasaPago>().CasaID = dbCasame.IdCasa;
                    if (FotoTomada == true)
                    {
                        File.WriteAllBytes(Application.persistentDataPath + "/" + dbCasame.IdCasa + ".jpg", bytes2);
                        byte[] bytes = File.ReadAllBytes(Application.persistentDataPath + "/" + dbCasame.IdCasa + ".jpg");
                        fotota = new Texture2D(1600, 1600);
                        fotota.LoadImage(bytes);
                        Sprite foto = Sprite.Create(fotota, new Rect(/*(fotota.width / 4)*/0, /*(fotota.height / 4)*/0, (fotota.width /* 2*/), (fotota.height /* 2*/)), Vector2.zero);
                        Casass[id].GetComponent<InfoCasa>().casaper.SetActive(true);
                        Casass2[id].GetComponent<InfoCasaMH>().casaper.SetActive(true);
                        CasaPagos[id].GetComponent<InfoCasaPago>().casafoto.SetActive(true);

                        Casass[id].GetComponent<InfoCasa>().casapersonalizada.sprite = foto;
                        Casass2[id].GetComponent<InfoCasaMH>().casapersonalizada.sprite = foto;
                        CasaPagos[id].GetComponent<InfoCasaPago>().casapersonalizada.sprite = foto;

                        OrderImages(Casass[id].GetComponent<InfoCasa>().casapersonalizada);
                        OrderImages(Casass2[id].GetComponent<InfoCasaMH>().casapersonalizada);
                        OrderImages(CasaPagos[id].GetComponent<InfoCasaPago>().casapersonalizada);
                        //Casass[id].GetComponent<InfoCasa>().OrderImages();

                        /*Casass[id].GetComponent<InfoCasa>().casapersonalizada.preserveAspect = true;
                        Casass2[id].GetComponent<InfoCasaMH>().casapersonalizada.preserveAspect = true;
                        CasaPagos[id].GetComponent<InfoCasaPago>().casapersonalizada.preserveAspect = true;*/


                        //Casass[buscarid].GetComponent<InfoCasa>().casaper 
                    }

                    for (int por = 0; por < ap.Pagos.Count; por++)
                    {
                        if (ap.Pagos[por].GetComponent<InfoPago>().nombreCasa == NNOffline[0])
                        {
                            ap.Pagos[por].GetComponent<InfoPago>().idCasa = dbCasame.IdCasa;
                            ap.IdCasa[por] = dbCasame.IdCasa;
                        }
                    }
                    for (int per = 0; per < ap.PagosPasado.Count; per++)
                    {
                        if (ap.PagosPasado[per].GetComponent<InfoPago>().nombreCasa == NNOffline[0])
                        {
                            ap.PagosPasado[per].GetComponent<InfoPago>().idCasa = dbCasame.IdCasa;
                            ap.IdCasaPasado[per] = dbCasame.IdCasa;
                            ap.ids = ap.PagosPasado[per].GetComponent<InfoPago>().PagoIDLocal;
                        }
                    }
                    for (int pxr = 0; pxr < ap.IdPagoLocalPasadoOffline.Count; pxr++)
                    {
                        if (ap.IdPagoLocalPasadoOffline[pxr] == ap.ids)
                        {
                            ap.IdCasaPasadoOffline[pxr] = dbCasame.IdCasa;
                        }
                    }
                    for (int pyr = 0; pyr < ap.IdPagoPasadoLocal.Count; pyr++)
                    {
                        if (ap.IdPagoPasadoLocal[pyr] == ap.ids)
                        {
                            ap.IdCasaPasado[pyr] = dbCasame.IdCasa;
                        }
                    }
                    for (int pur = 0; pur < ap.idCasaHistorialOff.Count; pur++)
                    {
                        if (ap.nombreCasasHist[pur] == NNOffline[0])
                        {
                            ap.idCasaHistorialOff[pur] = dbCasame.IdCasa;
                        }
                    }
                    for (int phr = 0; phr < ap.idCasaHistorialAgregarHist.Count; phr++)
                    {
                        if (ap.nombreCasasAgregarHist[phr] == NNOffline[0])
                        {
                            ap.idCasaHistorialAgregarHist[phr] = dbCasame.IdCasa;
                        }
                    }

                    for (int par = 0; par < ap.IdCasaOffline.Count; par++)
                    {
                        if (ap.nombreCasasOffline[par] == NNOffline[0])
                        {
                            ap.IdCasaOffline[par] = dbCasame.IdCasa;
                        }
                    }

                    if (ap.nombreCasasOffline.Count != 0)
                    {
                        ap.NoEnviadoPagosAgregar = null;
                        for (int gii = 0; ap.IdPagoLocalOffline.Count > gii; gii++)
                        {
                            if (gii == 0)
                            {
                                ap.NoEnviadoPagosAgregar = ap.IdPagoLocalOffline[gii] + "," + ap.NombreCategoriaOffline[gii] + "," + ap.idColorsOffline[gii] + "," + ap.idIconosOffline[gii] + "," + ap.IdCasaOffline[gii] + "," +
                                    ap.PrecioPagosOffline[gii] + "," + ap.fechaOffline[gii] + "," + ap.RecordatorioPagosOffline[gii] + "," + ap.CadaCuandosOffline[gii] + "," + ap.AlertasAntesOffline[gii] + "," + ap.IdBeneficiarioOffline[gii] +
                                    "," + ap.NotaOffline[gii] + "," + ap.ReciboPagosOffline[gii] + "," + ap.CodBarrasOffline[gii] + "," + ap.nombreCasasOffline[gii];
                            }
                            else if (gii > 0)
                            {
                                ap.NoEnviadoPagosAgregar += ";" + ap.IdPagoLocalOffline[gii] + "," + ap.NombreCategoriaOffline[gii] + "," + ap.idColorsOffline[gii] + "," + ap.idIconosOffline[gii] + "," + ap.IdCasaOffline[gii] + "," +
                                    ap.PrecioPagosOffline[gii] + "," + ap.fechaOffline[gii] + "," + ap.RecordatorioPagosOffline[gii] + "," + ap.CadaCuandosOffline[gii] + "," + ap.AlertasAntesOffline[gii] + "," + ap.IdBeneficiarioOffline[gii] +
                                    "," + ap.NotaOffline[gii] + "," + ap.ReciboPagosOffline[gii] + "," + ap.CodBarrasOffline[gii] + "," + ap.nombreCasasOffline[gii];
                            }
                        }
                        PlayerPrefs.SetString("PagosAgregar", ap.NoEnviadoPagosAgregar);
                        ap.AgregarPagoPasadoNE = null;
                        for (int hu = 0; hu < ap.IdPagoLocalPasadoOffline.Count; hu++)
                        {
                            if (hu == 0)
                            {
                                ap.AgregarPagoPasadoNE = ap.IdPagoLocalPasadoOffline[hu] + "," + ap.NombreCategoriaPasadoOffline[hu] + "," + ap.idColorsPasadoOffline[hu] + "," + ap.idIconosPasadoOffline[hu] +
                                    "," + ap.IdCasaPasadoOffline[hu] + "," + ap.PrecioPagosPasadoOffline[hu] + "," + ap.fechaPasadoOffline[hu] + "," + ap.RecordatorioPagosPasadoOffline[hu] + "," +
                                    ap.CadaCuandosPasadoOffline[hu] + "," + ap.AlertasAntesPasadoOffline[hu] + "," + ap.IdBeneficiarioPasadoOffline[hu] + "," + ap.NotaPasadoOffline[hu] + "," +
                                    ap.ReciboPagosPasadoOffline[hu] + "," + ap.CodBarrasPasadoOffline[hu];
                            }
                            else if (hu > 0)
                            {
                                ap.AgregarPagoPasadoNE += ";" + ap.IdPagoLocalPasadoOffline[hu] + "," + ap.NombreCategoriaPasadoOffline[hu] + "," + ap.idColorsPasadoOffline[hu] + "," + ap.idIconosPasadoOffline[hu] +
                                    "," + ap.IdCasaPasadoOffline[hu] + "," + ap.PrecioPagosPasadoOffline[hu] + "," + ap.fechaPasadoOffline[hu] + "," + ap.RecordatorioPagosPasadoOffline[hu] + "," +
                                    ap.CadaCuandosPasadoOffline[hu] + "," + ap.AlertasAntesPasadoOffline[hu] + "," + ap.IdBeneficiarioPasadoOffline[hu] + "," + ap.NotaPasadoOffline[hu] + "," +
                                    ap.ReciboPagosPasadoOffline[hu] + "," + ap.CodBarrasPasadoOffline[hu];
                            }
                        }
                        PlayerPrefs.SetString("PagosPasadoAgregar", ap.AgregarPagoPasadoNE);
                        if (ap.IdPagoLocalOffline.Count != 0)
                        {
                            ap.AgregarPago();
                        }
                    }
                }
            }
            NNOffline.RemoveAt(0);
            UbOffline.RemoveAt(0);
            CodOffline.RemoveAt(0);
            //id = user.Id.Value,
            //mfc.Eliminar();
            //ReadCard();
            IDOffline.RemoveAt(0);

            NoEnviadoAgregar = null;
            for (int hot = 0; hot < NNOffline.Count; hot++)
            {
                if (hot == 0)

                    NoEnviadoAgregar = IDOffline[hot] + "," + NNOffline[hot] + "," + UbOffline[hot] + "," + CodOffline[hot];

                else if (hot > 0)
                    NoEnviadoAgregar += ";" + IDOffline[hot] + "," + NNOffline[hot] + "," + UbOffline[hot] + "," + CodOffline[hot];
            }
            PlayerPrefs.SetString("CasasAgregar", NoEnviadoAgregar);
            Offline = null;
            Off = null;
            for (int yh = 0; yh < NNC.Count; yh++)
            {
                if (yh == 0)
                    Offline = IDLocal[yh] + "," + ID[yh] + "," + NNC[yh] + "," + UbC[yh] + "," + CodC[yh];
                else if (yh > 0)
                    Offline += ";" + IDLocal[yh] + "," + ID[yh] + "," + NNC[yh] + "," + UbC[yh] + "," + CodC[yh];


            }
            PlayerPrefs.SetString("Casas", Offline);
            NickCa.text = "";
            UbiCa.text = "";
            CodCa.text = "";
            IdCasa = dbCasame.IdCasa;
            error = dbCasame.IdCasa.ToString();
            if (FotoTomada == true)
            {
                EnviarFoto();
            }
            else if (FotoTomada == false)
            {
                //ReadCard();
            }
            if (IDOffline.Count > 0)
            {
                AgregarCasa();
            }
            ob.Click("MisCasasHUB");
            //ob.Click("TermYCond");
        }
    }
    void SuccessAgregarCasaR(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            error = ("" + ((MessageArg<string>)e).responseValue);
            reg.carga = false;
            //ob.DesError.text = "No se pudo agregar la casa, checa tu conexión e intentalo de nuevo";

        }
        else if (e is MessageArg<DBCasate>)
        {
            //right = true;
            //register1 = 1;
            //PlayerPrefs.SetInt("Registrado", register1);
            NickCasaR.text = null;
            UbicacionCasaR.text = null;
            CodigoAsociacionR.text = null;
            mp.NickNR.text = null;
            mp.AnoNR.text = "Año";
            mp.MesNR.text = "Mes";
            mp.SexoFR.isOn = true;
            mp.SexoMR.isOn = false;
            DBCasate casame = ((MessageArg<DBCasate>)e).responseValue;
            ob.Click("Aviso");
            ob.avisotexto.text = "Se agregó correctamente la casa";
            for (int buscarid = 0; buscarid < Casass.Count; buscarid++)
            {
                if (Casass[buscarid].GetComponent<InfoCasa>().idCasaLocal == IDOffline[0])
                {
                    ID[buscarid] = casame.IdCasa;
                    Casass[buscarid].GetComponent<InfoCasa>().idCasa = casame.IdCasa;
                    Casass2[buscarid].GetComponent<InfoCasaMH>().CasaID = casame.IdCasa;
                    CasaPagos[buscarid].GetComponent<InfoCasaPago>().CasaID = casame.IdCasa;
                    if (FotoTomada == true)
                    {
                        File.WriteAllBytes(Application.persistentDataPath + "/" + casame.IdCasa + ".jpg", bytes2);
                        byte[] bytes = File.ReadAllBytes(Application.persistentDataPath + "/" + casame.IdCasa + ".jpg");
                        fotota = new Texture2D(1600, 1600);
                        fotota.LoadImage(bytes);
                        Sprite foto = Sprite.Create(fotota, new Rect(/*(fotota.width / 4)*/0, /*(fotota.height / 4)*/0, (fotota.width /* 2*/), (fotota.height /* 2*/)), Vector2.zero);
                        Casass[buscarid].GetComponent<InfoCasa>().casaper.SetActive(true);
                        Casass2[buscarid].GetComponent<InfoCasaMH>().casaper.SetActive(true);
                        CasaPagos[buscarid].GetComponent<InfoCasaPago>().casafoto.SetActive(true);

                        Casass[buscarid].GetComponent<InfoCasa>().casapersonalizada.sprite = foto;
                        Casass2[buscarid].GetComponent<InfoCasaMH>().casapersonalizada.sprite = foto;
                        CasaPagos[buscarid].GetComponent<InfoCasaPago>().casapersonalizada.sprite = foto;

                        OrderImages(Casass[buscarid].GetComponent<InfoCasa>().casapersonalizada);
                        OrderImages(Casass2[buscarid].GetComponent<InfoCasaMH>().casapersonalizada);
                        OrderImages(CasaPagos[buscarid].GetComponent<InfoCasaPago>().casapersonalizada);

                        //Casass[buscarid].GetComponent<InfoCasa>().casaper 
                    }
                    for (int por = 0; por < ap.Pagos.Count; por++)
                    {
                        if (ap.Pagos[por].GetComponent<InfoPago>().nombreCasa == NNOffline[0])
                        {
                            ap.Pagos[por].GetComponent<InfoPago>().idCasa = casame.IdCasa;
                            ap.IdCasa[por] = casame.IdCasa;
                        }
                    }
                    for (int per = 0; per < ap.PagosPasado.Count; per++)
                    {
                        if (ap.PagosPasado[per].GetComponent<InfoPago>().nombreCasa == NNOffline[0])
                        {
                            ap.PagosPasado[per].GetComponent<InfoPago>().idCasa = casame.IdCasa;
                            ap.IdCasaPasado[per] = casame.IdCasa;
                            ap.ids = ap.PagosPasado[per].GetComponent<InfoPago>().PagoIDLocal;
                        }
                    }
                    for (int pxr = 0; pxr < ap.IdPagoLocalPasadoOffline.Count; pxr++)
                    {
                        if (ap.IdPagoLocalPasadoOffline[pxr] == ap.ids)
                        {
                            ap.IdCasaPasadoOffline[pxr] = casame.IdCasa;
                        }
                    }
                    for (int pyr = 0; pyr < ap.IdPagoPasadoLocal.Count; pyr++)
                    {
                        if (ap.IdPagoPasadoLocal[pyr] == ap.ids)
                        {
                            ap.IdCasaPasado[pyr] = casame.IdCasa;
                        }
                    }
                    for (int pur = 0; pur < ap.idCasaHistorialOff.Count; pur++)
                    {
                        if (ap.nombreCasasHist[pur] == NNOffline[0])
                        {
                            ap.idCasaHistorialOff[pur] = casame.IdCasa;
                        }
                    }
                    for (int phr = 0; phr < ap.idCasaHistorialAgregarHist.Count; phr++)
                    {
                        if (ap.nombreCasasAgregarHist[phr] == NNOffline[0])
                        {
                            ap.idCasaHistorialAgregarHist[phr] = casame.IdCasa;
                        }
                    }

                    for (int par = 0; par < ap.IdCasaOffline.Count; par++)
                    {
                        if (ap.nombreCasasOffline[par] == NNOffline[0])
                        {
                            ap.IdCasaOffline[par] = casame.IdCasa;
                        }
                    }

                    if (ap.nombreCasasOffline.Count != 0)
                    {
                        ap.NoEnviadoPagosAgregar = null;
                        for (int gii = 0; ap.IdPagoLocalOffline.Count > gii; gii++)
                        {
                            if (gii == 0)
                            {
                                ap.NoEnviadoPagosAgregar = ap.IdPagoLocalOffline[gii] + "," + ap.NombreCategoriaOffline[gii] + "," + ap.idColorsOffline[gii] + "," + ap.idIconosOffline[gii] + "," + ap.IdCasaOffline[gii] + "," +
                                    ap.PrecioPagosOffline[gii] + "," + ap.fechaOffline[gii] + "," + ap.RecordatorioPagosOffline[gii] + "," + ap.CadaCuandosOffline[gii] + "," + ap.AlertasAntesOffline[gii] + "," + ap.IdBeneficiarioOffline[gii] +
                                    "," + ap.NotaOffline[gii] + "," + ap.ReciboPagosOffline[gii] + "," + ap.CodBarrasOffline[gii] + "," + ap.nombreCasasOffline[gii];
                            }
                            else if (gii > 0)
                            {
                                ap.NoEnviadoPagosAgregar += ";" + ap.IdPagoLocalOffline[gii] + "," + ap.NombreCategoriaOffline[gii] + "," + ap.idColorsOffline[gii] + "," + ap.idIconosOffline[gii] + "," + ap.IdCasaOffline[gii] + "," +
                                    ap.PrecioPagosOffline[gii] + "," + ap.fechaOffline[gii] + "," + ap.RecordatorioPagosOffline[gii] + "," + ap.CadaCuandosOffline[gii] + "," + ap.AlertasAntesOffline[gii] + "," + ap.IdBeneficiarioOffline[gii] +
                                    "," + ap.NotaOffline[gii] + "," + ap.ReciboPagosOffline[gii] + "," + ap.CodBarrasOffline[gii] + "," + ap.nombreCasasOffline[gii];
                            }
                        }
                        PlayerPrefs.SetString("PagosAgregar", ap.NoEnviadoPagosAgregar);
                        ap.AgregarPagoPasadoNE = null;
                        for (int hu = 0; hu < ap.IdPagoLocalPasadoOffline.Count; hu++)
                        {
                            if (hu == 0)
                            {
                                ap.AgregarPagoPasadoNE = ap.IdPagoLocalPasadoOffline[hu] + "," + ap.NombreCategoriaPasadoOffline[hu] + "," + ap.idColorsPasadoOffline[hu] + "," + ap.idIconosPasadoOffline[hu] +
                                    "," + ap.IdCasaPasadoOffline[hu] + "," + ap.PrecioPagosPasadoOffline[hu] + "," + ap.fechaPasadoOffline[hu] + "," + ap.RecordatorioPagosPasadoOffline[hu] + "," +
                                    ap.CadaCuandosPasadoOffline[hu] + "," + ap.AlertasAntesPasadoOffline[hu] + "," + ap.IdBeneficiarioPasadoOffline[hu] + "," + ap.NotaPasadoOffline[hu] + "," +
                                    ap.ReciboPagosPasadoOffline[hu] + "," + ap.CodBarrasPasadoOffline[hu];
                            }
                            else if (hu > 0)
                            {
                                ap.AgregarPagoPasadoNE += ";" + ap.IdPagoLocalPasadoOffline[hu] + "," + ap.NombreCategoriaPasadoOffline[hu] + "," + ap.idColorsPasadoOffline[hu] + "," + ap.idIconosPasadoOffline[hu] +
                                    "," + ap.IdCasaPasadoOffline[hu] + "," + ap.PrecioPagosPasadoOffline[hu] + "," + ap.fechaPasadoOffline[hu] + "," + ap.RecordatorioPagosPasadoOffline[hu] + "," +
                                    ap.CadaCuandosPasadoOffline[hu] + "," + ap.AlertasAntesPasadoOffline[hu] + "," + ap.IdBeneficiarioPasadoOffline[hu] + "," + ap.NotaPasadoOffline[hu] + "," +
                                    ap.ReciboPagosPasadoOffline[hu] + "," + ap.CodBarrasPasadoOffline[hu];
                            }
                        }
                        PlayerPrefs.SetString("PagosPasadoAgregar", ap.AgregarPagoPasadoNE);
                        if (ap.IdPagoLocalOffline.Count != 0)
                        {
                            ap.AgregarPago();
                        }
                    }
                }
            }
            NNOffline.RemoveAt(0);
            UbOffline.RemoveAt(0);
            CodOffline.RemoveAt(0);
            //id = user.Id.Value,
            //mfc.Eliminar();
            //ReadCard();
            IDOffline.RemoveAt(0);
            NoEnviadoAgregar = null;
            for (int hot = 0; hot < NNOffline.Count; hot++)
            {
                if (hot == 0)

                    NoEnviadoAgregar = IDOffline[hot] + "," + NNOffline[hot] + "," + UbOffline[hot] + "," + CodOffline[hot];

                else if (hot > 0)
                    NoEnviadoAgregar += ";" + IDOffline[hot] + "," + NNOffline[hot] + "," + UbOffline[hot] + "," + CodOffline[hot];
            }
            PlayerPrefs.SetString("CasasAgregar", NoEnviadoAgregar);
            Offline = null;
            Off = null;
            for (int yh = 0; yh < NNC.Count; yh++)
            {
                if (yh == 0)
                    Offline = IDLocal[yh] + "," + ID[yh] + "," + NNC[yh] + "," + UbC[yh] + "," + CodC[yh];
                else if (yh > 0)
                    Offline += ";" + IDLocal[yh] + "," + ID[yh] + "," + NNC[yh] + "," + UbC[yh] + "," + CodC[yh];


            }
            PlayerPrefs.SetString("Casas", Offline);
            NickCa.text = "";
            UbiCa.text = "";
            CodCa.text = "";
            IdCasa = casame.IdCasa;
            error = casame.IdCasa.ToString();
            if (FotoTomada == true)
            {
                //FotoTomada = false;
                EnviarFoto();
            }
            else if (FotoTomada == false)
            {
                //ReadCard();
            }
            if (IDOffline.Count > 0)
            {
                AgregarCasa();
            }
            registro = false;
            //ob.Click("TermYCond");
            ob.Click("CompPerfil-Final");
            error = casame.IdCasa.ToString();
            //ob.Click("TermYCond");
        }
    }
    void ErrorAgregar(System.Object Sender, EventArgs e)
    {
        error = ("" + ((MessageArg<string>)e).responseValue);
        AgregarCasa();
        //ob.Click("Errors");

    }
    void Error(System.Object Sender, EventArgs e)
    {
        error = ("" + ((MessageArg<string>)e).responseValue);
        
        //ob.Click("Errors");

    }
    public void ObtenerCasa()
    {
        WWWForm Form = new WWWForm();
        Form.AddField("IdUsuario", reg.id);
        WebServices.ObtCasas(Form, SuccessObtCasa, Error);
        //ob.DesError.text = "No se pudo obtener las casas, checa tu conexión e intentalo de nuevo";
    }
    void SuccessObtCasa(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            error = ("" + ((MessageArg<string>)e).responseValue);
            Casa.Clear();
            Debug.Log("");
        }
        else if (e is MessageArg<List<DBCasate>>)
        {
            Casa.Clear();
            Casa = null;
            //right = true;
            //register1 = 1;
            //User user = ((MessageArg<User>)e).responseValue;
            //PlayerPrefs.SetInt("Registrado", register1);
            //User user = ((MessageArg<User>)e).responseValue;
            Casa = ((MessageArg<List<DBCasate>>)e).responseValue;
            reg.Casas = Casa;
            StartCoroutine(DelayGetCasas());

            //ReadCard();
            //user.casas = ((MessageArg<List<DBCasate>>)e).responseValue;
            //= ((MessageArg<List<DBCasa>>)e).responseValue;
            //Casas = user.casas;
            //error = dbcasas.Id.ToString();
            //mp.ObtenerMP();
            //ingresa = true;
            //mp.Ver();

            //ob.Click("Inicios");

        }
    }

    public void AddHouse()
    {
        FotoTomada = false;
        mfc.foundFoto = false;
        ob.Click("AgregarMisCasas");
    }

    IEnumerator Starts(string endDir)
    {
        // Start a download of the given URL
        string url = "https://payhome.azurewebsites.net/serv/";
        WWW www = new WWW(url + endDir);

        // Wait for download to complete
        yield return www;

        // assign texture
        fotota = www.texture;
        FotoCasa = Sprite.Create(fotota, new Rect((fotota.width / 4), (fotota.height / 4), (fotota.width / 2), (fotota.height / 2)), Vector2.zero);
        Texture2D texture = www.texture;

        //this.GetComponent<Renderer>().material.mainTexture = texture;
        bytes2 = texture.EncodeToJPG(50);
        File.WriteAllBytes(Application.persistentDataPath + "/" + IdCasa + ".jpg", bytes2);
        //ReadCard();
        //InfoC.FotoTomada = true;
        //InfoCP.FotoTomada = true;
        //InfoMH.FotoTomada = true;
        //InicioFoto.sprite = FotoPerfil;
        //MPFoto.sprite = FotoPerfil;

        //FotoRegistro.SetActive(true);
        //Foto.SetActive(true);
        //FotoTomada = true;
        //FotoInicio.SetActive(true);
        //reg.casa.ReadCard();
        //Ver();
        //oj.sprite = FotoPerfil;
    }
    public void EnviarFoto()
    {
        bytes2 = TextureScale.bytes;
        WWWForm Form = new WWWForm();
        Form.AddField("IdUsuario", reg.id);
        Form.AddField("IdCasa", IdCasa);
        Form.AddBinaryData("archivoImagen", bytes2);
        WebServices.SubirFotoCasa(Form, SuccessFoto, Error);
    }
    void SuccessFoto(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            //cargando = false;
            ////error = ("" + ((MessageArg<string>)e).responseValue);
            //EncabezadoErrorAclDudas.text = "Reporte de fallos";
            //ErrorAclDudas.onClick.AddListener(EnviarContacto);
            //ob.Click("Aclaraciones-Error");

            Debug.Log("");
        }
        else if (e is MessageArg<dbphoto>)
        {
            //right = true;
            //register1 = 1;
            //PlayerPrefs.SetInt("Registrado", register1);
            //cargando = false;
            dbphoto dbp = ((MessageArg<dbphoto>)e).responseValue;
            IdCasaFoto = dbp.IdCasa;
            linkfoto = dbp.FotoUsuario;
            FotoTomada = false;
            //reg.carga = true;
            //ReadCard();
            //ReadCard();
            //foto = dbp.FotoUsuario;
            //StartCoroutine(Starts(foto));
            //TextoAclaracionDudas.text = "<b>" + mp.NickInicio.text + ",</b> hemos recibido tu reporte de fallo, muchas gracias por reportarlo. Responderemos a tu correo electrónico cuando ya haya sido arreglado:";
            //EmailAclDudas.text = PlayerPrefs.GetString("email");
            //EncabezadoCorrectoAclDudas.text = "Reporte de fallo";
            //Reporte.text = "";
            //ob.Click("Aclaraciones-Correcto");
            //error = dudas.IdUsuario.ToString();
        }
    }
    IEnumerator DelayFoto()
    {
        
        yield return new WaitForSeconds(0.5f);
        //ReadCard();
    }
    public void VerNota()
    {
            ap.NotaTexto.text = "Próximamente podrás tener los pagos/gastos de tu casa/departamento sin necesidad de agregarlos. \nMantente pendiente porque habrá grandes sorpresas en siguientes actualizaciones de <b>PayHome</b>.";


        ob.Click("NotaPago");
    }

}
