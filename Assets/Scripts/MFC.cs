﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VoxelBusters.Utility;
using VoxelBusters.Utility.UnityGUI.MENU;
using VoxelBusters.NativePlugins;
using System;
using System.Text.RegularExpressions;
using System.IO;

public class MFC : MonoBehaviour {
    [SerializeField] TakePhotos takePhotos;
    public InputField NickCasa, UbicacionCasa, CodigoAsociacionCasa;
    
    public string NNCasa, UbCasa, CodAsC, error, UbCasaComp, CodAsCComp, NickCasaBack;
    [SerializeField] ObjectManager ob;
    
    public int i = 0, b = 0, ids, c, h, foto, idFot, p, idComp, idsback, idCasa;
    public GameObject casat, mover, mover2, mover3, mover4, mover5, mover6, casat2, casat3;
    Vector2 G, Ward, Ward2, Ward3, Ward4, Ward5, Ward6;
    public InfoCasa Info, Info2;
    [SerializeField] InfoCasaMH Info3, Info5;
    [SerializeField] InfoCasaPago Info4, Info6;
    [SerializeField] Guardar save;
    public Casas casa;
    float s = 0, d;
    private List<string> m_results = new List<string>();
    [SerializeField] GameObject FotoCasass, FondoC;
    [Header("Casas")] [SerializeField] Image FotoCasas, FondoCasa;
    private bool changed = false;
    
    public bool FotoTomada, eliminar, igual, FotoModificada, eliminarFoto;
    public Sprite FotoCasa;
    [SerializeField] Registro reg;
    [SerializeField] ScrollRect scroll;
    private string NNCasaBajo, NNCasaComp, EliminarNoHecho, NickBack;
    byte[] bytes;
    string OfflineModificar, NoEnviadoModificar, NoEnviadorEliminar;
    string[] OffMod, escribirMod, NEModif, escribirEliminar, nombreCasa, OffEliminar;
    public List<int> IdCasaMod, IdCasaEliminar = new List<int>();
    public List<string>NickModOff, UbModOf, CodModOff = new List<string>();
    int intOffline;
    byte[] bytes2;
    Char delimiter = ';';
    Char del = ',';
    [SerializeField] AP ap;
    bool elim = false;
    public bool foundFoto = false;
    // Use this for initialization
    void Start () {
        //if (i > b)
        //{
        //    b++;
        //    Instantiate(tarjeta, new Vector2(0, 0), Quaternion.identity, tarjetas.transform);
        //}
        
	}
    public void OffModificar()
    {
        NoEnviadoModificar = PlayerPrefs.GetString("CasasModificar");
        OffMod = NoEnviadoModificar.Split(delimiter);
        for(int kl = 0; kl<OffMod.Length; kl++)
        {
            escribirMod = null;
            escribirMod = OffMod[kl].Split(del);
            if(escribirMod[0] != "")
            {
                Int32.TryParse(escribirMod[0], out intOffline);
                IdCasaMod.Add(intOffline);
                NickModOff.Add(escribirMod[1]);
                UbModOf.Add(escribirMod[2]);
                CodModOff.Add(escribirMod[3]);
            }
        }
        if(IdCasaMod.Count != 0)
        {
            ModificarCasa();
        }
    }
    public void OffElim()
    {
        NoEnviadorEliminar = PlayerPrefs.GetString("CasasModificar");
        OffEliminar = NoEnviadorEliminar.Split(delimiter);
        for (int kl = 0; kl < OffEliminar.Length; kl++)
        {
            if (OffEliminar[0] != "")
            {
                Int32.TryParse(OffEliminar[kl], out intOffline);
                IdCasaEliminar.Add(intOffline);
            }
        }
        if (IdCasaEliminar.Count != 0)
        {
            EliminarCasa();
        }
    }

    // Update is called once per frame
    void Update () {
   

        NNCasa = NickCasa.text;
        UbCasa = UbicacionCasa.text;
        CodAsC = CodigoAsociacionCasa.text;

        
    }

    public void UpdateImage()
    {
        if (foundFoto == true)
        {
            FotoCasass.SetActive(true);
            FondoC.SetActive(true);

            FotoCasas.sprite = FotoCasa;
            FotoCasas.SetNativeSize();
            FotoCasas.preserveAspect = true;

            FondoCasa.sprite = FotoCasa;
            FondoCasa.SetNativeSize();
            FondoCasa.preserveAspect = true;

            OrderImages(FotoCasas);
            OrderImages(FondoCasa);

            foto = 1;
        }
        else
        {
            FotoCasass.SetActive(false);
            FondoC.SetActive(false);
            foto = 0;
            FotoCasa = null;
        }
    }

    void OrderImages(Image img)
    {
        if (img.sprite != null)
        {
            img.GetComponent<AspectRatioFitter>().aspectMode = AspectRatioFitter.AspectMode.EnvelopeParent;
            if (img.sprite.texture.width > img.sprite.texture.height)
            {
                img.GetComponent<AspectRatioFitter>().aspectRatio = 2f;

            }
            else
            {
                //FotoCasas.GetComponent<AspectRatioFitter>().aspectMode = AspectRatioFitter.AspectMode.EnvelopeParent;
                img.GetComponent<AspectRatioFitter>().aspectRatio = 0.001f;
            }
        }
    }

    public void ReadCard()
    {
        
        //if (ID == save.id)
        //{

        //}
    }

    public void Guardar()
    {
        //scroll.movementType = ScrollRect.MovementType.Elastic;
        //    scroll.elasticity = 0;
        //if (changed)
        //{
            //changed = false;
            igual = false;
            NNCasaBajo = NNCasa.Replace(" ", "");
            NNCasaBajo = NNCasaBajo.ToLower();
            for (int u = 0; casa.Casass.Count > u; u++)
            {
                NNCasaComp = casa.Casass[u].GetComponent<InfoCasa>().NickNameC.Replace(" ", "");
                idComp = casa.Casass[u].GetComponent<InfoCasa>().idCasa;
                NNCasaComp = NNCasaComp.ToLower();
                if (NNCasaComp == NNCasaBajo && idCasa != idComp)
                {
                    igual = true;
                }
            }
            if (igual == false)
            {  
                if (NNCasa != "" && UbCasa != "")
                {
                ModOff();



                //ModificarCasa();
                //ob.Click("AtrasCasa");

                }
                 else if (NNCasa == "")
                 {
                // ob.Click("ErrorTarjeta");//textSe necesita llenar todos los campos para poder agregar esté método de pago"
                ob.DesError.text = "No puedes dejar la casa sin un nombre";
                ob.Click("Errors");
                igual = false;
                 }
                 else if (UbCasa == "")
                 {
                // ob.Click("ErrorTarjeta");//textSe necesita llenar todos los campos para poder agregar esté método de pago"
                ob.DesError.text = "No puedes dejar la casa sin direccion";
                ob.Click("Errors");
                igual = false;
                 }
               /* if (NNCasa != "")
                {
                    ModOff();
                //ModificarCasa();
                //ob.Click("AtrasCasa");
                }
                else
                {
                    ob.Click("ErrorTarjeta");
                    igual = false;
                }*/
            }
            else
            {
                ob.DesError.text = "No se puede modificar esta casa ya que ya tienes una con el mismo nombre";
                ob.Click("Errors");
            }
        //}
        //else
        //{
           // ob.Click("MisCasasHUB");
           // print("nothing changed");
        //}
    }
    public void Back()
    {
        FotoModificada = false;
        FotoTomada = false;
        eliminar = false;
        eliminarFoto = false;
    }

    public void ModOff()
    {
        idCasa = Info.idCasa;
        for(int io =0; io<casa.Casass.Count; io++)
        {
            if(Info.NickNameC == casa.Casass[io].GetComponent<InfoCasa>().NickNameC)
            {
                Info3 = casa.Casass2[io].GetComponent<InfoCasaMH>();
                Info4 = casa.CasaPagos[io].GetComponent<InfoCasaPago>();
            }
        }
        NickCasaBack = casa.NNC[ids];
        casa.NNC[ids] = NickCasa.text;
        casa.UbC[ids] = UbicacionCasa.text;
        casa.CodC[ids] = CodigoAsociacionCasa.text;
        Info3.NN.text = NickCasa.text;
        Info3.NickName = NickCasa.text;
        Info4.NN.text = NickCasa.text;
        Info4.NickName = NickCasa.text;
        Info.NickNameCasa.text = NickCasa.text;
        Info.NickNameC = NickCasa.text;
        Info.UbicacionC = UbicacionCasa.text;
        Info.CodigoC = CodigoAsociacionCasa.text;
        ob.Click("MisCasasHUB");

        for (int yh = 0; yh < casa.NNC.Count; yh++)
        {
            if (yh == 0)
                casa.Offline = casa.IDLocal[yh] + "," + casa.ID[yh] + "," + casa.NNC[yh] + "," + casa.UbC[yh] + "," + casa.CodC[yh];
            else if (yh > 0)
                casa.Offline += ";" + casa.IDLocal[yh] + "," + casa.ID[yh] + "," + casa.NNC[yh] + "," + casa.UbC[yh] + "," + casa.CodC[yh];


        }
        PlayerPrefs.SetString("Casas", casa.Offline);
        if(idCasa == 0)
        {
            for(int modCasaBuscar = 0; modCasaBuscar < casa.NNOffline.Count; modCasaBuscar++)
            {
                if(NickCasaBack == casa.NNOffline[modCasaBuscar])
                {
                    casa.NNOffline[modCasaBuscar] = NickCasa.text;
                    casa.UbOffline[modCasaBuscar] = UbicacionCasa.text;
                    casa.CodOffline[modCasaBuscar] = CodigoAsociacionCasa.text;
                }
            }
            casa.NoEnviadoAgregar = null;
            for (int hot = 0; hot < casa.NNOffline.Count; hot++)
            {
                if (hot == 0)

                    casa.NoEnviadoAgregar = casa.IDOffline[hot] + "," + casa.NNOffline[hot] + "," + casa.UbOffline[hot] + "," + casa.CodOffline[hot];

                else if (hot > 0)
                    casa.NoEnviadoAgregar += ";" + casa.IDOffline[hot] + "," + casa.NNOffline[hot] + "," + casa.UbOffline[hot] + "," + casa.CodOffline[hot];
            }
            PlayerPrefs.SetString("CasasAgregar", casa.NoEnviadoAgregar);
        }
        else if(idCasa != 0)
        {
            IdCasaMod.Add(idCasa);
            NickModOff.Add(NickCasa.text);
            UbModOf.Add(UbicacionCasa.text);
            CodModOff.Add(CodigoAsociacionCasa.text);
            NoEnviadoModificar = null;
            for(int hut = 0; hut < IdCasaMod.Count; hut++)
            {
                if(hut == 0)
                {
                    NoEnviadoModificar = IdCasaMod[hut] + "," + NickModOff[hut] + "," + UbModOf[hut] + "," + CodModOff[hut];
                }
                else if(hut > 0)
                {
                    NoEnviadoModificar += ";" + IdCasaMod[hut] + "," + NickModOff[hut] + "," + UbModOf[hut] + "," + CodModOff[hut];
                }
            }
            if(FotoTomada == true)
            {
                File.WriteAllBytes(Application.persistentDataPath + "/" + idCasa + ".jpg", bytes2);
                byte[] bytes = File.ReadAllBytes(Application.persistentDataPath + "/" + idCasa + ".jpg");
                Texture2D fotota = new Texture2D(1600, 1600);
                fotota.LoadImage(bytes);
                Sprite foto = Sprite.Create(fotota, new Rect(/*(fotota.width / 4)*/0, /*(fotota.height / 4)*/0, (fotota.width /* 2*/), (fotota.height /* 2*/)), Vector2.zero);
                Info.casaper.SetActive(true);
                Info3.casaper.SetActive(true);
                Info4.casafoto.SetActive(true);
                Info.casapersonalizada.sprite = foto;
                Info3.GetComponent<InfoCasaMH>().casapersonalizada.sprite = foto;
                Info4.GetComponent<InfoCasaPago>().casapersonalizada.sprite = foto;

                OrderImages(Info.casapersonalizada);
                OrderImages(Info3.GetComponent<InfoCasaMH>().casapersonalizada);
                OrderImages(Info4.GetComponent<InfoCasaPago>().casapersonalizada);

                Info.FotoTomada = true;
                Info3.FotoTomada = true;
                Info4.FotoTomada = true;
                Info.foto = foto;
                //Info.OrderImages();
            }
            PlayerPrefs.SetString("CasasModificar", NoEnviadoModificar);
            ModificarCasa();
        }
        for(int ui = 0; ap.Pagos.Count>ui; ui++)
        {
            if(ap.Pagos[ui].GetComponent<InfoPago>().nombreCasa == NickCasaBack)
            {
                ap.Pagos[ui].GetComponent<InfoPago>().nombreCasa = NickCasa.text;
                ap.Pagos[ui].GetComponent<InfoPago>().Casa.text = NickCasa.text;
            }
        }
        for(int uij = 0; ap.PagosPasado.Count>uij; uij++)
        {
            if(ap.PagosPasado[uij].GetComponent<InfoPago>().nombreCasa == NickCasaBack)
            {
                ap.PagosPasado[uij].GetComponent<InfoPago>().nombreCasa = NickCasa.text;
                ap.PagosPasado[uij].GetComponent<InfoPago>().Casa.text = NickCasa.text;
            }
        }
        for(int uis = 0; ap.nombreCasasOffline.Count>uis; uis++)
        {
            if(ap.nombreCasasOffline[uis] == NickCasaBack)
            {
                ap.nombreCasasOffline[uis] = NickCasa.text;
            }
        }
        for(int uix = 0; ap.nombreCasasModOffline.Count>uix; uix++)
        {
            if(ap.nombreCasasModOffline[uix] == NickCasaBack)
            {
                ap.nombreCasasModOffline[uix] = NickCasa.text;
            }
        }
        for(int uik = 0; ap.nombreCasasPasadoOffline.Count > uik; uik++)
        {
            if(ap.nombreCasasPasadoOffline[uik] == NickCasaBack)
            {
                ap.nombreCasasPasadoOffline[uik] = NickCasa.text;
            }
        }
        for(int yui = 0; yui<ap.nombreCasasAgregarHist.Count; yui++)
        {
            if(ap.nombreCasasAgregarHist[yui] == NickCasaBack)
            {
                ap.nombreCasasAgregarHist[yui] = NickCasa.text;
            }
        }
        for(int yuju = 0; yuju<ap.nombreCasasHist.Count; yuju++)
        {
            if(ap.nombreCasasHist[yuju] == NickCasaBack)
            {
                ap.nombreCasasHist[yuju] = NickCasa.text;
            }
        }
        for(int upi = 0; upi< ap.nombreCasas.Count; upi++)
        {
            if(ap.nombreCasas[upi] == NickCasaBack)
            {
                ap.nombreCasas[upi] = NickCasa.text;
            }
        }

        ap.NoEnviadoPagosAgregar = null;
        for (int gii = 0; ap.IdPagoLocalOffline.Count > gii; gii++)
        {
            if (gii == 0)
            {
                ap.NoEnviadoPagosAgregar = ap.IdPagoLocalOffline[gii] + "," + ap.NombreCategoriaOffline[gii] + "," + ap.idColorsOffline[gii] + "," + ap.idIconosOffline[gii] + "," + ap.IdCasaOffline[gii] + "," +
                    ap.PrecioPagosOffline[gii] + "," + ap.fechaOffline[gii] + "," + ap.RecordatorioPagosOffline[gii] + "," + ap.CadaCuandosOffline[gii] + "," + ap.AlertasAntesOffline[gii] + "," + ap.IdBeneficiarioOffline[gii] +
                    "," + ap.NotaOffline[gii] + "," + ap.ReciboPagosOffline[gii] + "," + ap.CodBarrasOffline[gii] + "," + ap.nombreCasasOffline[gii];
            }
            else if (gii > 0)
            {
                ap.NoEnviadoPagosAgregar += ";" + ap.IdPagoLocalOffline[gii] + "," + ap.NombreCategoriaOffline[gii] + "," + ap.idColorsOffline[gii] + "," + ap.idIconosOffline[gii] + "," + ap.IdCasaOffline[gii] + "," +
                    ap.PrecioPagosOffline[gii] + "," + ap.fechaOffline[gii] + "," + ap.RecordatorioPagosOffline[gii] + "," + ap.CadaCuandosOffline[gii] + "," + ap.AlertasAntesOffline[gii] + "," + ap.IdBeneficiarioOffline[gii] +
                    "," + ap.NotaOffline[gii] + "," + ap.ReciboPagosOffline[gii] + "," + ap.CodBarrasOffline[gii] + "," + ap.nombreCasasOffline[gii];
            }
        }
        PlayerPrefs.SetString("PagosAgregar", ap.NoEnviadoPagosAgregar);
        ap.HistorialOff = null;
        for (int qw = 0; qw < ap.NombreCatOff.Count; qw++)
        {
            if (qw == 0)
            {
                ap.HistorialOff = ap.NombreCatOff[qw] + "," + ap.idCatOff[qw] + "," + ap.idColorOff[qw] + "," + ap.idIconoOff[qw] + "," + ap.idPagoEnOff[qw] + "," + ap.idPagoConOff[qw] + "," + ap.precioHistorialOff[qw] + "," +
                    ap.fechaHistorialOff[qw] + "," + ap.idPagoEnColorOff[qw] + "," + ap.idPagoEnIconoOff[qw] + "," + ap.mesHistorialOff[qw] + "," + ap.añoHistorialOff[qw] + "," + ap.idCasaHistorialOff[qw] + "," +
                    ap.nombreCasasHist[qw];
            }
            else if (qw != 0)
            {
                ap.HistorialOff += ";" + ap.NombreCatOff[qw] + "," + ap.idCatOff[qw] + "," + ap.idColorOff[qw] + "," + ap.idIconoOff[qw] + "," + ap.idPagoEnOff[qw] + "," + ap.idPagoConOff[qw] + "," + ap.precioHistorialOff[qw] + "," +
                    ap.fechaHistorialOff[qw] + "," + ap.idPagoEnColorOff[qw] + "," + ap.idPagoEnIconoOff[qw] + "," + ap.mesHistorialOff[qw] + "," + ap.añoHistorialOff[qw] + "," + ap.idCasaHistorialOff[qw] + "," +
                    ap.nombreCasasHist[qw];
            }
        }
        PlayerPrefs.SetString("Historial", ap.HistorialOff);

        ap.Offline = null;
        for (int yh = 0; yh < ap.Pagos.Count; yh++)
        {
            if (yh == 0)
                ap.Offline = ap.IdPagoLocal[yh] + "," + ap.IdPago[yh] + "," + ap.IdCasa[yh] + "," + ap.idBeneficiario[yh] + "," + ap.CadaCuandos[yh] + "," + ap.AlertasAntes[yh] + "," + ap.idCategoria[yh] + "," + ap.CodBarras[yh] + ","
                    + ap.PrecioPagos[yh] + "," + ap.Dia[yh] + "," + ap.Mes[yh] + "," + ap.Año[yh] + "," + ap.Nota[yh] + "," + ap.ReciboPagos[yh] + "," + ap.RecordatorioPagos[yh] + "," + ap.idColors[yh] + "," +
                    ap.idIconos[yh] + "," + ap.IdPPred[yh] + "," + ap.Checarpasado[yh] + "," + ap.NombreCategoria[yh] + "," + ap.PagoPersonalizado[yh] + "," + ap.nombreCasas[yh];
            else if (yh > 0)
                ap.Offline += ";" + ap.IdPagoLocal[yh] + "," + ap.IdPago[yh] + "," + ap.IdCasa[yh] + "," + ap.idBeneficiario[yh] + "," + ap.CadaCuandos[yh] + "," + ap.AlertasAntes[yh] + "," + ap.idCategoria[yh] + "," + ap.CodBarras[yh] + ","
                    + ap.PrecioPagos[yh] + "," + ap.Dia[yh] + "," + ap.Mes[yh] + "," + ap.Año[yh] + "," + ap.Nota[yh] + "," + ap.ReciboPagos[yh] + "," + ap.RecordatorioPagos[yh] + "," + ap.idColors[yh] + "," +
                    ap.idIconos[yh] + "," + ap.IdPPred[yh] + "," + ap.Checarpasado[yh] + "," + ap.NombreCategoria[yh] + "," + ap.PagoPersonalizado[yh] + "," + ap.nombreCasas[yh];
        }
        PlayerPrefs.SetString("Pagos", ap.Offline);

        ap.AgregarHistorialNE = null;
        for (int qw = 0; qw < ap.NombreCatAgregarHist.Count; qw++)
        {
            if (qw == 0)
            {
                ap.AgregarHistorialNE = ap.NombreCatAgregarHist[qw] + "," + ap.idCatAgregarHist[qw] + "," + ap.idColorAgregarHist[qw] + "," + ap.idIconoAgregarHist[qw] + "," + ap.idPagoEnAgregarHist[qw] + "," +
                    ap.idPagoConAgregarHist[qw] + "," + ap.precioHistorialAgregarHist[qw] + "," + ap.fechaHistorialAgregarHist[qw] + "," + ap.idPagoEnColorAgregarHist[qw] + "," +
                    ap.idPagoEnIconoAgregarHist[qw] + "," + ap.mesHistorialAgregarHist[qw] + "," + ap.añoHistorialAgregarHist[qw] + "," + ap.idCasaHistorialAgregarHist[qw] + "," + ap.nombreCasasAgregarHist[qw];
            }
            else if (qw != 0)
            {
                ap.AgregarHistorialNE += ";" + ap.NombreCatAgregarHist[qw] + "," + ap.idCatAgregarHist[qw] + "," + ap.idColorAgregarHist[qw] + "," + ap.idIconoAgregarHist[qw] + "," + ap.idPagoEnAgregarHist[qw] + "," +
                    ap.idPagoConAgregarHist[qw] + "," + ap.precioHistorialAgregarHist[qw] + "," + ap.fechaHistorialAgregarHist[qw] + "," + ap.idPagoEnColorAgregarHist[qw] + "," +
                    ap.idPagoEnIconoAgregarHist[qw] + "," + ap.mesHistorialAgregarHist[qw] + "," + ap.añoHistorialAgregarHist[qw] + "," + ap.idCasaHistorialAgregarHist[qw] + "," + ap.nombreCasasAgregarHist[qw];
            }
        }
        PlayerPrefs.SetString("HistorialAgregar", ap.AgregarHistorialNE);

    }
    
    public void PopUpEliminar()
    {
        if(casa.Casass.Count <= 1)
        {
            ob.DesError.text = "No puedes borrar todas las casas, debes contar con al menos una. Consejo: Si deseas borrar la informacion de todas tus casas genera una nueva antes de tratar de eliminar las anteriores.";
            ob.Click("Errors");
        }
        if (casa.Casass.Count > 1)
        {
            ob.Click("Eliminar");
            ob.EliminarTexto.text = "Seguro desea eliminar la casa: " + NNCasa + ", ya no será posible recuperarla después de eliminarla.";
            elim = true;
            ob.EliminarAceptar.onClick.RemoveAllListeners();
            ob.EliminarAceptar.onClick.AddListener(Eliminate);
        }
    }
    public void Eliminate()
    {
        ob.Click("EliminarQuitar");
        if (elim)
            Eliminar();
    }

    public void EliminarTodos()
    {
        for (int i = 0; i < casa.Casass.Count; i = 0)
        {
            //ids = yho;
            //Eliminar()
            casat = casa.Casass[i];
            casat2 = casa.Casass2[i];
            casat3 = casa.CasaPagos[i];
            casa.NNC.RemoveAt(i);
            casa.UbC.RemoveAt(i);
            casa.CodC.RemoveAt(i);
            casa.FotoC.RemoveAt(i);
            casa.Casass.RemoveAt(i);
            casa.Casass2.RemoveAt(i);
            casa.CasaPagos.RemoveAt(i);
            if (File.Exists(Application.persistentDataPath + "/" + casa.ID[i] + ".jpg"))
            {
                File.Delete(Application.persistentDataPath + "/" + casa.ID[i] + ".jpg");
            }
            casa.ID.RemoveAt(i);

            casa.IDLocal.RemoveAt(i);
            casa.PosicionCasa.RemoveAt(i);
            Destroy(casat);
            Destroy(casat2);
            Destroy(casat3);


        }
    }
    //elimina casa seleccionada
    public void Eliminar()
    {
        //c = casa.Casass.Count;
        //for (int kjk = 0; kjk < c; kjk++)
        //{

        //eliminar historial de esta casa de algun modo



        if (eliminar == true && casa.posicion == 1)
        {
            ob.Click("EliminarQuitar");
            ob.DesError.text = "No se puede eliminar porque te quedarías sin casa para registrar pagos";
            ob.Click("Errors");
        }

        //scroll.movementType = ScrollRect.MovementType.Elastic;
        //scroll.elasticity = 0;
        mover2 = casa.Casass2[ids];
        mover3 = casa.CasaPagos[ids];
        //casat2 = casa.Casass2[ids];
        //casat3 = casa.CasaPagos[ids];

        for (int i = ids; i < casa.posicion; i++)
        {
            casa.PosicionCasa[i]--;
            mover = casa.Casass[i];

            Info2 = mover.GetComponent<InfoCasa>();
            Info2.posicion--;

            Info5 = casa.Casass2[i].GetComponent<InfoCasaMH>();
            Info5.posicion--;

            Info6 = casa.CasaPagos[i].GetComponent<InfoCasaPago>();
            Info6.posicion--;

        }

        casat = casa.Casass[ids];
        casat2 = casa.Casass2[ids];
        casat3 = casa.CasaPagos[ids];
        
        casa.NNC.RemoveAt(ids);
        casa.UbC.RemoveAt(ids);
        casa.CodC.RemoveAt(ids);
        casa.FotoC.RemoveAt(ids);
        casa.Casass.RemoveAt(ids);
        casa.Casass2.RemoveAt(ids);
        casa.CasaPagos.RemoveAt(ids);
        casa.ID.RemoveAt(ids);
        casa.IDLocal.RemoveAt(ids);
        casa.PosicionCasa.RemoveAt(ids);

        Destroy(casat);
        Destroy(casat2);
        Destroy(casat3);

        casa.posicion--;
        casa.b--;
        ob.d--;
        casa.p--;

        casa.Offline = null;
        for (int i = 0; i < casa.NNC.Count; i++)
        {
            if (i == 0)
            {
                casa.Offline = casa.IDLocal[i] + "," + casa.ID[i] + "," + casa.NNC[i] + "," + casa.UbC[i] + "," + casa.CodC[i];
            }
            else if (i > 0)
            {
                casa.Offline += ";" + casa.IDLocal[i] + "," + casa.ID[i] + "," + casa.NNC[i] + "," + casa.UbC[i] + "," + casa.CodC[i];
            }
        }

        PlayerPrefs.SetString("Casas", casa.Offline);
        ob.Click("MisCasasHUB");
        if (idCasa == 0)
        {
            for (int i = 0; casa.NNOffline.Count > i; i++)
            {
                if (casa.NNOffline[i] == NickCasaBack)
                {
                    casa.IDOffline.RemoveAt(i);
                    casa.NNOffline.RemoveAt(i);
                    casa.UbOffline.RemoveAt(i);
                    casa.CodOffline.RemoveAt(i);
                }
            }
            casa.NoEnviadoAgregar = null;
            for (int i = 0; i < casa.NNOffline.Count; i++)
            {
                if (i == 0)

                    casa.NoEnviadoAgregar = casa.IDOffline[i] + "," + casa.NNOffline[i] + "," + casa.UbOffline[i] + "," + casa.CodOffline[i];

                else if (i > 0)
                    casa.NoEnviadoAgregar += ";" + casa.IDOffline[i] + "," + casa.NNOffline[i] + "," + casa.UbOffline[i] + "," + casa.CodOffline[i];
            }
            PlayerPrefs.SetString("CasasAgregar", casa.NoEnviadoAgregar);
        }
        else if (idCasa != 0)
        {
            NoEnviadorEliminar = null;

            IdCasaEliminar.Add(idCasa);
            
            if (File.Exists(Application.persistentDataPath + "/" + idCasa + ".jpg"))
            {
                File.Delete(Application.persistentDataPath + "/" + idCasa + ".jpg");
            }
            for (int i = 0; i < IdCasaEliminar.Count; i++)
            {
                if (i == 0)
                {
                    NoEnviadorEliminar = IdCasaEliminar[i] + ";";
                }
               /* else if (i > 0)
                {
                    NoEnviadorEliminar += IdCasaEliminar[i] + ";";
                }*/
            }
            //elimina casa del servidor
            PlayerPrefs.SetString("CasaEliminar", NoEnviadorEliminar);
            EliminarCasa();
            NoEnviadorEliminar = null;
        }
        

        //c = 0;
        //FotoTomada = false;
        //fp.TCL[ids] = TCt;
        //fp.TCmL[ids] = TCm;
        //fp.NTL[ids] = NombTitu;
        //fp.ApL[ids] = Apell;
        //fp.ML[ids] = Mes;
        //fp.AnL[ids] = Ano;
        //fp.NickL[ids] = NickName;
        //fp.BancL[ids] = Banco;
        //fp.TipoL[ids] = Tipo;
        //fp.MarcaL[ids] = Marca;
        //TCL.Add(TCt);
        //TCmL.Add(TCm);
        //NTL.Add(NombTitu);
        //ApL.Add(Apell);
        //ML.Add(Mes);
        //AnL.Add(Ano);
        //NickL.Add(NickName);
        //BancL.Add(Banco);
        //TipoL.Add(Tipo);
        //MarcaL.Add(Marca);
        //ID.Add(i);
        //scroll.movementType = ScrollRect.MovementType.Elastic;
        //        scroll.elasticity = 3.25f;
        //casa.checar = true;
        //ob.Click("MisCasasHUB");
        //}

    }
    public void TomarFoto()
    {
        if (Application.internetReachability != NetworkReachability.NotReachable )
        {
            if (foto == 0)
            {
                ob.ImagenGaleria.onClick.RemoveAllListeners();
                ob.ImagenCamara.onClick.RemoveAllListeners();
                ob.Click("Imagen");
                ob.ImagenCamara.onClick.AddListener(PickImageFromCamera);
                ob.ImagenGaleria.onClick.AddListener(PickImageFromAlbum);
            }
            else if (foto == 1)
            {
                ob.ImagenCamaraEliminar.onClick.RemoveAllListeners();
                ob.ImagenGaleriaEliminar.onClick.RemoveAllListeners();
                ob.ImagenEliminarFoto.onClick.RemoveAllListeners();
                ob.Click("ImagenEliminar");
                ob.ImagenCamaraEliminar.onClick.AddListener(PickImageFromCamera);
                ob.ImagenGaleriaEliminar.onClick.AddListener(PickImageFromAlbum);
                ob.ImagenEliminarFoto.onClick.AddListener(EliminarFoto);
            }
        }
        else
        {
            ob.DesError.text = "No puedes modificar/eliminar la foto sin Internet";
            ob.Click("Errors");
        }
    }

    public void PickImageFromAlbum()
    {
        reg.StartLoad();
        StartCoroutine(Album());
        
    }

    IEnumerator Album()
    {
        yield return new WaitForSeconds(0.5f);
        if (foto == 0)
            ob.Click("ImagenQuitar");
        else if (foto == 1)
            ob.Click("ImagenEliminarQuitar");
        takePhotos.isFinished += TakePhoto;
#if UNITY_IOS
        // Set popover to last touch position
        NPBinding.UI.SetPopoverPointAtLastTouchPosition();
#else
        // Pick image
        //takePhotos.PickImage(2048);
#endif
 //       NPBinding.MediaLibrary.PickImage(eImageSource.ALBUM, 1, takePhotos.PickImageFinished);
    }

    public void PickImageFromCamera()
    {
        reg.StartLoad();
        StartCoroutine(Camera());
    }

    IEnumerator Camera()
    {
        yield return new WaitForSeconds(0.5f);
        if (foto == 0)
            ob.Click("ImagenQuitar");
        else if (foto == 1)
            ob.Click("ImagenEliminarQuitar");
        takePhotos.isFinished += TakePhoto;
#if UNITY_IOS
        // Set popover to last touch position
        NPBinding.UI.SetPopoverPointAtLastTouchPosition();
        NPBinding.MediaLibrary.PickImage(eImageSource.CAMERA, 1, takePhotos.PickImageFinished);
#else
        //wasPhoto = true;
        takePhotos.TakePicture(2048);
#endif
    }

    public void EliminarFoto()
    {
        ob.Click("ImagenEliminarQuitar");
        FotoCasass.SetActive(false);
        FondoC.SetActive(false);
        eliminarFoto = true;
        FotoTomada = false;
        foundFoto = false;
        FotoModificada = false;
        foto = 0;
    }

    void TakePhoto()
    {
        // SetAllowsImageEditing();
        Texture2D image3 = takePhotos.photoImage;

        bytes2 = image3.EncodeToJPG(50);
        ////image2.Resize(image2.width / 2, image2.height / 2);
        TextureScale.Point(image3, image3.width, image3.height);
        FotoCasa = Sprite.Create(takePhotos.photoImage, new Rect(/*(takePhotos.photoImage.width / 4)*/0, 0/*(takePhotos.photoImage.height / 4)*/, (takePhotos.photoImage.width /* 2*/), (takePhotos.photoImage.height /* 2*/)), Vector2.zero);

        eliminarFoto = false;
        FotoTomada = true;
        foundFoto = true;
        FotoModificada = true;
        //Foto.SetActive(true);
        FotoCasass.SetActive(true);
        FondoC.SetActive(true);

        FotoCasas.sprite = FotoCasa;
        FotoCasas.preserveAspect = true;
        FotoCasas.SetNativeSize();
        OrderImages(FotoCasas);

        //if (FotoCasas.gameObject.GetComponent<RectTransform>().sizeDelta.magnitude > new Vector2(600, 600).magnitude)
        //{
        //FotoCasas.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(FotoCasa.texture.width/4 , FotoCasa.texture.height/4 );
        //}
        //else if (FotoCasas.gameObject.GetComponent<RectTransform>().sizeDelta.magnitude < new Vector2(600, 600).magnitude)
        //{
        //FotoCasas.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(FotoCasa.texture.width , FotoCasa.texture.height );
        //}
        FondoCasa.sprite = FotoCasa;
        FondoCasa.preserveAspect = true;
        FondoCasa.SetNativeSize();
        OrderImages(FondoCasa);


        //FondoCasa.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(FotoCasa.texture.width , FotoCasa.texture.height );
        casa.p++;

        //ob.Click("AtrasCasa");
        takePhotos.isFinished -= TakePhoto;
        reg.EndLoad();
        System.GC.Collect();


    }

    /* public void PickImageFinished(ePickImageFinishReason _reason, Texture2D _image)
     {
         AddNewResult("Request to pick image from gallery finished. Reason for finish is " + _reason + ".");
         AppendResult(string.Format("Selected image is {0}.", (_image == null ? "NULL" : _image.ToString())));

         if (_image != null)
         {
            // SetAllowsImageEditing();
             Texture2D image3 = _image;
             bytes2 = image3.EncodeToJPG();
             ////image2.Resize(image2.width / 2, image2.height / 2);
             //TextureScale.Point(image3, image3.width / 4, image3.height / 4);
             FotoCasa = Sprite.Create(_image, new Rect((_image.width / 4), (_image.height / 4), (_image.width / 2), (_image.height / 2)), Vector2.zero);

             eliminarFoto = false;
             FotoTomada = true;
             FotoModificada = true;
             //Foto.SetActive(true);
             FotoCasass.SetActive(true);
             FondoC.SetActive(true);

             FotoCasas.sprite = FotoCasa;
             //if (FotoCasas.gameObject.GetComponent<RectTransform>().sizeDelta.magnitude > new Vector2(600, 600).magnitude)
             //{
             //FotoCasas.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(FotoCasa.texture.width/4 , FotoCasa.texture.height/4 );
             //}
             //else if (FotoCasas.gameObject.GetComponent<RectTransform>().sizeDelta.magnitude < new Vector2(600, 600).magnitude)
             //{
             //FotoCasas.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(FotoCasa.texture.width , FotoCasa.texture.height );
             //}
             FondoCasa.sprite = FotoCasa;
             //FondoCasa.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(FotoCasa.texture.width , FotoCasa.texture.height );
             casa.p++;

             ob.Click("AtrasCasa");
         }

     }
     private void SetAllowsImageEditing()
     {
         NPBinding.MediaLibrary.SetAllowsImageEditing(true);
     }

     private static eImageSource GetBOTH()
     {
         return eImageSource.BOTH;
     }
     private static eImageSource GetCAMERA()
     {
         return eImageSource.CAMERA;
     }

     private static eImageSource GetALBUM()
     {
         return eImageSource.ALBUM;
     }
     protected void AppendResult(string _result)
     {
         m_results.Add(_result);
     }

     protected void AddNewResult(string _result)
     {
         m_results.Clear();
         m_results.Add(_result);
     }*/
    public void ModificarCasa()
    {
        WWWForm Form = new WWWForm();
        Form.AddField("IdUsuario", reg.id);
        Form.AddField("IdCasa", IdCasaMod[0]);
        idsback = IdCasaMod[0];
        Form.AddField("NombreCasa", NickModOff[0]);
        Form.AddField("FotoCasa", "");
        Form.AddField("UbicacionCasa", UbModOf[0]);
        Form.AddField("CodigoActivacion", CodModOff[0]);
        WebServices.ModCasas(Form, SuccessModCasa, ErrorModCasa);
        //reg.carga = true;
        //ob.DesError.text = "No se pudo modificar la casa, checa tu conexión e intentalo de nuevo";
    }
    void SuccessModCasa(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            reg.carga = false;
            //Debug.Log("");
        }
        else if (e is MessageArg<DBCasate>)
        {
            //right = true;
            //register1 = 1;
            //PlayerPrefs.SetInt("Registrado", register1);
            DBCasate casame = ((MessageArg<DBCasate>)e).responseValue;
            //id = user.Id.Value;
            NickCasa.text = "";
            UbicacionCasa.text = "";
            CodigoAsociacionCasa.text = "";
            IdCasaMod.RemoveAt(0);
            NickModOff.RemoveAt(0);
            UbModOf.RemoveAt(0);
            CodModOff.RemoveAt(0);
            NoEnviadoModificar = null;
            for (int hut = 0; hut < IdCasaMod.Count; hut++)
            {
                if (hut == 0)
                {
                    NoEnviadoModificar = IdCasaMod[hut] + "," + NickModOff[hut] + "," + UbModOf[hut] + "," + CodModOff[hut];
                }
                else if (hut > 0)
                {
                    NoEnviadoModificar += ";" + IdCasaMod[hut] + "," + NickModOff[hut] + "," + UbModOf[hut] + "," + CodModOff[hut];
                }
            }
            PlayerPrefs.SetString("CasasModificar", NoEnviadoModificar);
            if(IdCasaMod.Count != 0)
            {
                ModificarCasa();
            }
            //Eliminar();
            if (FotoModificada == true)
            {
                FotoModificada = false;
                EnviarFoto();
            }
            //else if (FotoModificada == false && eliminarFoto == false)
            //{
            //    casa.ReadCard();
            //}
            else if (eliminarFoto == true)
            {
                EliminarCasaFoto();
            }
            FotoTomada = false;
            foundFoto = false;

            //ob.Click("Aviso");
            //ob.avisotexto.text = "Se modificó correctamente la casa";
            ob.Click("MisCasasHUB");
            error = casame.IdCasa.ToString();
            //ob.Click("TermYCond");
        }
    }

    public void EliminarCasa()
    {
        WWWForm Form = new WWWForm();
        Form.AddField("IdUsuario", reg.id);
        Form.AddField("IdCasa", IdCasaEliminar[0]);
        WebServices.ElimCasas(Form, SuccessElimCasa, ErrorElimCasa);
        //reg.carga = true;
        eliminarFoto = false;
        //ob.DesError.text = "No se pudo eliminar la casa, checa tu conexión e intentalo de nuevo";
    }
    void SuccessElimCasa(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            //casa.Casa.Clear();
            reg.carga = false;
            //Debug.Log("");
        }
        else if (e is MessageArg<DBCasate>)
        {
            //right = true;
            //register1 = 1;
            //PlayerPrefs.SetInt("Registrado", register1);
            DBCasate casame = ((MessageArg<DBCasate>)e).responseValue;
            //id = user.Id.Value;            

            EliminarPagos();

            NickCasa.text = "";
            UbicacionCasa.text = "";
            CodigoAsociacionCasa.text = "";

            //casa.Casa.Clear();
            ////Eliminar();
            ////casa.ReadCard();
            IdCasaEliminar.Clear();            

            ob.Click("Aviso");
            ob.avisotexto.text = "Se eliminó correctamente la casa";
            //ob.Click("MisCasasHUB");
            error = casame.IdCasa.ToString();
            //ob.Click("TermYCond");
            elim = false;
        }
    }

    void EliminarPagos()
    {
        //elimina los pagos de esta casa
        List<InfoPago> infoList = new List<InfoPago>();
        for (int i = 0; i < ap.Pagos.Count; i++)
        {
            InfoPago ip = ap.Pagos[i].GetComponent<InfoPago>();
            if (ip.idCasa == idCasa)
            {
                infoList.Add(ip);
                /*ap.eliminar = true;
                ap.nombrePago = ip.gameObject.name;

                ip.Eliminar();*/
            }
        }

        foreach (var item in infoList)
        {
            ap.eliminar = true;
            ap.nombrePago = item.gameObject.name;

            item.Eliminar();
        }
    }

    public void EliminarCasaFoto()
    {
        WWWForm Form = new WWWForm();
        Form.AddField("IdUsuario", reg.id);
        Form.AddField("IdCasa", idsback);
        WebServices.ElimCasasFoto(Form, SuccessElimCasaFoto, Error);
        //reg.carga = true;
        //ob.DesError.text = "No se pudo eliminar la foto de la casa, checa tu conexión e intentalo de nuevo";
    }
    void SuccessElimCasaFoto(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            casa.Casa.Clear();
            reg.carga = false;
            //Debug.Log("");
        }
        else if (e is MessageArg<DBCasate>)
        {
            //right = true;
            //register1 = 1;
            //PlayerPrefs.SetInt("Registrado", register1);
            DBCasate casame = ((MessageArg<DBCasate>)e).responseValue;
            //id = user.Id.Value;
            NickCasa.text = "";
            UbicacionCasa.text = "";
            CodigoAsociacionCasa.text = "";
            eliminarFoto = false;
            for(int yu = 0; yu<casa.Casass.Count; yu++)
            {
                if(casa.Casass[yu].GetComponent<InfoCasa>().idCasa == idsback)
                {
                    casa.Casass[yu].GetComponent<InfoCasa>().FotoTomada = false;
                    casa.Casass2[yu].GetComponent<InfoCasaMH>().FotoTomada = false;
                    casa.CasaPagos[yu].GetComponent<InfoCasaPago>().FotoTomada = false;
                    casa.CasaPagos[yu].GetComponent<InfoCasaPago>().casafoto.SetActive(false);
                    //casa.CasaPagos[yu].GetComponent<InfoCasaPago>().

                }
            }
            File.Delete(Application.persistentDataPath +"/" + idsback + ".jpg");
            ob.Click("Aviso");
            ob.avisotexto.text = "Se eliminó correctamente la foto de la casa";
            ob.Click("MisCasasHUB");
            error = casame.IdCasa.ToString();
            //ob.Click("TermYCond");
        }
    }
    void Error(System.Object Sender, EventArgs e)
    {
        error = ("" + ((MessageArg<string>)e).responseValue);
        reg.carga = false;
        //ob.Click("Errors");
    }
    void ErrorElimCasa(System.Object Sender, EventArgs e)
    {
        error = ("" + ((MessageArg<string>)e).responseValue);
        EliminarCasa();
    }
    void ErrorModCasa(System.Object Sender, EventArgs e)
    {
        error = ("" + ((MessageArg<string>)e).responseValue);
        ModificarCasa();
    }
    void SaveTextureToFile(Texture2D texture, string filename)
    {
        System.IO.File.WriteAllBytes(filename, texture.EncodeToJPG());
    }
    public void EnviarFoto()
    {
        bytes2 = TextureScale.bytes;
        WWWForm Form = new WWWForm();
        Form.AddField("IdUsuario", reg.id);
        Form.AddField("IdCasa", idsback);
        Form.AddBinaryData("archivoImagen", bytes2);
        WebServices.SubirFotoCasa(Form, SuccessFoto, Error);
    }
    void SuccessFoto(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            //cargando = false;
            ////error = ("" + ((MessageArg<string>)e).responseValue);
            //EncabezadoErrorAclDudas.text = "Reporte de fallos";
            //ErrorAclDudas.onClick.AddListener(EnviarContacto);
            //ob.Click("Aclaraciones-Error");

            Debug.Log("");
        }
        else if (e is MessageArg<dbphoto>)
        {
            //print("mod foto");
            //right = true;
            //register1 = 1;
            //PlayerPrefs.SetInt("Registrado", register1);
            //cargando = false;
            dbphoto dbp = ((MessageArg<dbphoto>)e).responseValue;
            //Eliminar();
            casa.IdCasaFoto = dbp.IdCasa;
            casa.linkfoto = dbp.FotoUsuario;
            //reg.carga = true;
            //casa.ReadCard();
            //StartCoroutine(Delay());
            //foto = dbp.FotoUsuario;
            //StartCoroutine(Starts(foto));
            //TextoAclaracionDudas.text = "<b>" + mp.NickInicio.text + ",</b> hemos recibido tu reporte de fallo, muchas gracias por reportarlo. Responderemos a tu correo electrónico cuando ya haya sido arreglado:";
            //EmailAclDudas.text = PlayerPrefs.GetString("email");
            //EncabezadoCorrectoAclDudas.text = "Reporte de fallo";
            //Reporte.text = "";
            //ob.Click("Aclaraciones-Correcto");
            //error = dudas.IdUsuario.ToString();

        }
    }
    IEnumerator Delay()
    {
        
        yield return new WaitForSeconds(5);
        
    }
}
