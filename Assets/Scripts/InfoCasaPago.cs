﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
public class InfoCasaPago : MonoBehaviour {
    public int CasaID, CasaIDLocal;
    public string NickName;
    public Text NN, Casa;
    public GameObject Historial;
    public ObjectManager ob;
    public AP ap;
    public bool FotoTomada, cambio;
    public int fotos, idFoto, posicion;
    public Sprite foto;
    public Image casapersonalizada;
    public GameObject casafoto;
    // Use this for initialization
    void Start () {
        
        //GetComponent<Button>().onClick.AddListener(Click);
        ap = GameObject.Find("AgregarPago").GetComponent<AP>();
        //StartCoroutine(Delay());
        /* if (File.Exists(Application.persistentDataPath + "/" + CasaID + ".jpg"))
         {
             byte[] bytes = File.ReadAllBytes(Application.persistentDataPath + "/" + CasaID + ".jpg");
             Texture2D fotota = new Texture2D(1600, 1600);
             fotota.LoadImage(bytes);
             foto = Sprite.Create(fotota, new Rect(/*(fotota.width /* 4)*///0, /*(fotota.height /* 4)*/0, (fotota.width /* 2*/), (fotota.height /* 2*/)), Vector2.zero);
                                                                          /*casafoto.SetActive(true);
                                                                          casapersonalizada.sprite = foto;
                                                                          casapersonalizada.preserveAspect = true;
                                                                          OrderImages(casapersonalizada);
                                                                      }*/
        System.GC.Collect();

    }
    IEnumerator Delay()
    {
        yield return new WaitForSeconds(1f);
        if (File.Exists(Application.persistentDataPath + "/" + CasaID + ".jpg"))
        {
            byte[] bytes = File.ReadAllBytes(Application.persistentDataPath + "/" + CasaID + ".jpg");
            Texture2D fotota = new Texture2D(1600, 1600);
            fotota.LoadImage(bytes);
            foto = Sprite.Create(fotota, new Rect(/*(fotota.width / 4)*/0, /*(fotota.height / 4)*/0, (fotota.width /*/ 2*/), (fotota.height /* 2*/)), Vector2.zero);
            if (casafoto != null)
            {
                //print("csa pago");

                casafoto.SetActive(true);
                casapersonalizada.sprite = foto;
                casapersonalizada.preserveAspect = true;
                OrderImages(casapersonalizada);
            }
        }
    }

    public void OrderImages(Image img)
    {
        if (img.sprite != null)
        {
            img.GetComponent<AspectRatioFitter>().aspectMode = AspectRatioFitter.AspectMode.EnvelopeParent;
            if (img.sprite.texture.width > img.sprite.texture.height)
            {
                img.GetComponent<AspectRatioFitter>().aspectRatio = 2f;
            }
            else if (img.sprite.texture.width < img.sprite.texture.height)
            {
                //casapersonalizada.GetComponent<AspectRatioFitter>().aspectMode = AspectRatioFitter.AspectMode.WidthControlsHeight;
                casapersonalizada.GetComponent<AspectRatioFitter>().aspectRatio = 0.001f;
            }
        }

    }
    // Update is called once per frame
    void Update () {
        //if (File.Exists(Application.persistentDataPath + "/" + CasaID + ".jpg"))
        //{
        //    casaper.SetActive(true);
        //    FotoTomada = true;
        //}
        //else
        //{
        //    casaper.SetActive(false);
        //    FotoTomada = false;
        //}
        
        if (cambio == true)
        {
            cambio = false;
            if (File.Exists(Application.persistentDataPath + "/" + CasaID + ".jpg"))
            {
                byte[] bytes = File.ReadAllBytes(Application.persistentDataPath + "/" + CasaID + ".jpg");
                Texture2D fotota = new Texture2D(1600, 1600);
                fotota.LoadImage(bytes);
                foto = Sprite.Create(fotota, new Rect(/*(fotota.width / 4)*/0, /*(fotota.height / 4)*/0, (fotota.width /*/ 2*/), (fotota.height /* 2*/)), Vector2.zero);
                casafoto.SetActive(true);
                casapersonalizada.sprite = foto;
                casapersonalizada.preserveAspect = true;
                casapersonalizada.SetNativeSize();
                OrderImages(casapersonalizada);
            }
            System.GC.Collect();
        }
    }

    public void Click()
    {
        ap.idCasa = CasaID;
        ap.idCasaBack = CasaID;
        ap.posicionCasa = posicion;
        ap.ICP = this;
        ap.Casa.text = "Mis casas: " + NickName;
        ap.load = true;
        ap.loadPasado = true;
        ap.nombreCasa = NickName;
        ap.nombreCasaMenu = NickName;

        ap.StartCoroutine(ap.AcomodoSeconds());

        //ap.EliminarServ();
        //ap.ReadCardPagos();
        //ap.Acomodo();

        //ap.AcomodoPasado();
        ap.fondoString = NickName;
        ob.Click("MisPagos");
    }
}
