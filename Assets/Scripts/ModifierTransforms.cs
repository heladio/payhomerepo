﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ModifierTransforms : MonoBehaviour
{
    public Text textDebug;

    public Transform objectToModifyTraslation;
    public Transform objectToModifyRotationScale;
    public int touches;
    //Traslation
    Vector2 previousPositionTraslation;
    bool firstFrameTraslation = true;

    public const float SPEED_DAMPER = 0.01f;

    private float mScaleSpeed = 0.002f;

    private bool prevFrameMouseDown = false;

    private Vector2 mMousePos = Vector2.zero;
    private Vector2 mPrevMousePosition = Vector2.zero;

    [Header("Traslation")]
    public float speedTraslation = 0.02f;//1f;

    void Start()
    {
        //speedTraslation = float.Parse(XMLConfiguration.GetSettings()["ManualAdjustSpeedTraslation"]);
    }
    
    public void ResetValues()
    {
        objectToModifyTraslation.localPosition = Vector3.zero;
        objectToModifyRotationScale.localScale = Vector3.one;
        objectToModifyRotationScale.rotation = Quaternion.Euler(Vector3.zero);
    }
    public void SwapModifiers(GameObject containerRawImage)
    {
        //objectToModifyTraslation.rotation = Quaternion.Euler(Vector3.zero);
        //objectToModifyTraslation.localScale = Vector3.one;
        objectToModifyTraslation.localPosition = containerRawImage.transform.localPosition;
        containerRawImage.transform.localPosition = Vector3.zero;

        objectToModifyRotationScale.localScale = containerRawImage.transform.localScale;
        objectToModifyRotationScale.rotation = containerRawImage.transform.rotation;
        containerRawImage.transform.localScale = Vector3.one;
        containerRawImage.transform.rotation = Quaternion.Euler(Vector3.zero);

        //Debug.LogError("objectToModifyTraslation=" + objectToModifyTraslation.position);
        //Debug.LogError("objectToModifyTraslation=" + objectToModifyTraslation.localPosition);
        //objectToModifyTraslation.localScale = Vector3.one;
        // objectToModifyTraslation.rotation = Quaternion.Euler(Vector3.zero);
    }
    void Update()
    {
        //Debug.Log("Input.touches.Length="+ Input.touches.Length);
        //Debug.Log("Input.touchesCount=" + Input.touchCount);
        //Debug.Log("firstFrameTraslation=" + firstFrameTraslation);
        //textDebug.text = "Input.touches.Length=" + Input.touches.Length;
        //textDebug.text += "\nInput.touchesCount=" + Input.touchCount;
        ////textDebug.text += "\nfirstFrameTraslation=" + firstFrameTraslation;

        //if (Input.GetMouseButtonDown(0))
        //{

        //    //Debug.Log("Start traslating");
        //    if (firstFrameTraslation)
        //    {
        //        firstFrameTraslation = false;
        //        previousPositionTraslation = Input.mousePosition;
        //    }
        //    Vector2 currentPosition = Input.mousePosition;
        //    Vector2 delta = previousPositionTraslation - currentPosition;
        //    objectToModifyTraslation.Translate(-delta);
        //    previousPositionTraslation = currentPosition;
        //}
        //else
        //{
        //    firstFrameTraslation = true;
        //}


        if (Input.touchCount == 1)
        {
            if (touches > 0)
            {
                if (firstFrameTraslation)
                {
                    firstFrameTraslation = false;
                    previousPositionTraslation = Input.mousePosition;
                }
                Vector2 currentPosition = Input.mousePosition;
                Vector2 delta = (previousPositionTraslation - currentPosition) * speedTraslation;
                objectToModifyTraslation.Translate(-delta);
                previousPositionTraslation = currentPosition;
            }
        }
        else
        {
            firstFrameTraslation = true;
        }
           
        if (Input.touchCount == 2)
        {
            //SCALING
            Touch currentTouchA = Input.GetTouch(0);
            Touch currentTouchB = Input.GetTouch(1);

            Vector2 prevTouchA = currentTouchA.position - currentTouchA.deltaPosition;
            Vector2 prevTouchB = currentTouchB.position - currentTouchB.deltaPosition;

            float prevTouchDeltaMag = (prevTouchA - prevTouchB).magnitude;
            float touchDeltaMag = (currentTouchA.position - currentTouchB.position).magnitude;
            float deltaMagDiff = prevTouchDeltaMag - touchDeltaMag;

            Vector3 newScale = objectToModifyRotationScale.transform.localScale;
            float totalScale = deltaMagDiff * mScaleSpeed * -1;
            float proportion = objectToModifyRotationScale.transform.localScale.y / objectToModifyRotationScale.transform.localScale.x;

            float checkScale = newScale.x + totalScale;
            if (checkScale > 0)
            {
                newScale.x += totalScale;
                newScale.y += totalScale;
                //newScale.y += totalScale * proportion;
                objectToModifyRotationScale.transform.localScale = newScale;
            }
            //ROTATION:
            // Debug.Log("Son dos toques");
            Vector2 mousePos = new Vector2(Input.GetTouch(1).position.x - Screen.width / 2.0f,
                                            Input.GetTouch(1).position.y - Screen.height / 2.0f);
            Rotate(mousePos);
        }
        else
        {
            mMousePos = Vector2.zero;
            mPrevMousePosition = Vector2.zero;
            prevFrameMouseDown = false;
        }

        //if (Input.touches.Length >= 2)
        //{
        //    TouchManualAdjustWeb.StopTraslating();
        //    //TouchManualAdjustWeb.myslf.canTraslate = false;
        //    //TouchManualAdjustWeb.myslf.comesFromTwoTouches = true;
        //}
        //else if (Input.touches.Length == 1)
        //    TouchManualAdjustWeb.myslf.canTraslate = true;
        //else if (Input.touchCount == 0)
        //{
        //    TouchManualAdjustWeb.myslf.canTraslate = true;
        //    TouchManualAdjustWeb.myslf.comesFromTwoTouches = false;
        //}
    }

    private void Rotate(Vector3 mousePos)
    {
        //Debug.Log("Rotate");
        if (!prevFrameMouseDown)
        {
            mPrevMousePosition = mousePos;
            mMousePos = mousePos;
            prevFrameMouseDown = true;
        }
        else if (prevFrameMouseDown)
        {
            mPrevMousePosition = mMousePos;
            mMousePos = mousePos;
        }

        if (mPrevMousePosition != -((Vector2)mMousePos) && prevFrameMouseDown)
        {
            float rotationAmount = ReturnSignedAngleBetweenVectors(mPrevMousePosition, mMousePos);
            objectToModifyRotationScale.transform.Rotate(Vector3.forward, rotationAmount * SPEED_DAMPER * Mathf.Rad2Deg, Space.World);
        }
    }

    private float ReturnSignedAngleBetweenVectors(Vector2 vectorA, Vector2 vectorB)
    {
        Vector3 vector3A = new Vector3(vectorA.x, vectorA.y, 0f);
        Vector3 vector3B = new Vector3(vectorB.x, vectorB.y, 0f);

        if (vector3A == vector3B)
        {
            return 0f;
        }

        // refVector is a 90cw rotation of vector3A
        Vector3 refVector = Vector3.Cross(vector3A, Vector3.forward);
        float dotProduct = Vector3.Dot(refVector, vector3B);

        if (dotProduct > 0)
        {
            return -Vector3.Angle(vector3A, vector3B);
        }
        else if (dotProduct < 0)
        {
            return Vector3.Angle(vector3A, vector3B);
        }
        else
        {
            throw new System.InvalidOperationException("the vectors are opposite, vectorA = -vectorB");
        }
    }

    public void SetTouches(int touchme)
    {
        touches += touchme;
    }
}
