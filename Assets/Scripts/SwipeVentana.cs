﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeVentana : MonoBehaviour {

    // Use this for initialization
    public float Toquex, ToqueGuar, ToqueDown, aspectw, aspecth, aspect, aspectg, aspectc;
    public Sprite PS, PL;
    public int distance, h = 0;
    public decimal g;
    public bool SwipeHecho, SwipeDer, SwipeIzq, Me, distances;
    public Vector2 P1, PGuard, P2Guard, P3Guard, P2, P3, ORV;
    public GameObject primero, segundo, tercero;
    public VerPagosMeses VPM;
    // Use this for initialization
    void Start()
    {

        PGuard = primero.transform.position;
        P2Guard = segundo.transform.position;
        P3Guard = tercero.transform.position;
        aspecth = Screen.height;
        aspectw = Screen.width;
        aspect = (aspectw / aspecth);
        aspectg = (float)(Math.Round((double)aspect, 1));
        aspectc = (float)(Math.Round((double)aspect, 2));
    }

    // Update is called once per frame
    void Update()
    {
        P2 = segundo.transform.position;
        P3 = tercero.transform.position;
        Toquex = Input.mousePosition.x;
        if (Input.GetMouseButtonDown(0))
        {
            SwipeHecho = true;
            ToqueDown = Input.mousePosition.x;
        }
        if (Input.GetMouseButtonUp(0))
        {
            SwipeHecho = false;
            Me = false;
            SwipeDer = false;
            SwipeIzq = false;
            distance = 0;
            distances = false;
        }
        
    }

    public void X(int X)
    {
        ToqueGuar = Input.mousePosition.x;
        SwipeHecho = true;
        if (Toquex < ToqueGuar && SwipeHecho == true && SwipeIzq == false)
        {
            SwipeIzq = true;
            SwipeDer = false;
            distance = 0;
        }

        if (Toquex > ToqueGuar && SwipeHecho == true && SwipeDer == false)
        {
            SwipeIzq = false;
            SwipeDer = true;
            distance = 0;
        }
        if (SwipeIzq == true)
        {
            distance++;
        }

        if (SwipeDer == true)
        {
            distance++;
        }

        if (distance >= 2)
        {
            distances = true;
            distance = 0;
        }
        else
        {
            distances = false;
        }

        //aspect ratio 16:9

        if (X <= 0 && SwipeDer == true && SwipeHecho == true && VPM.i > 1 && aspect == 0.5625f)
        {
            iTween.MoveTo(segundo, new Vector2((Screen.height * 0.2375f), primero.transform.position.y), 0.5f);
            //iTween.MoveTo(tercero, new Vector2(tercero.transform.position.x + (-Screen.height * 0.635f), tercero.transform.position.y), 0.5f);
            h = 1;
            SwipeDer = false;
            SwipeHecho = false;
        }
        if (X == 1 && SwipeDer == true && SwipeHecho == true && VPM.i > 2 && aspect == 0.5625f)
        {

            //iTween.MoveTo(primero, new Vector2(primero.transform.position.x + (-Screen.height * 0.635f), primero.transform.position.y), 0.5f);
            //iTween.MoveTo(segundo, new Vector2(segundo.transform.position.x + (-Screen.height * 0.635f), segundo.transform.position.y), 0.5f);
            iTween.MoveTo(tercero, new Vector2((Screen.height * 0.2375f), primero.transform.position.y), 0.5f);
            h = 2;
            SwipeDer = false;
            SwipeHecho = false;
        }

        if (X >= 2 && SwipeIzq == true && SwipeHecho == true && aspect == 0.5625f)
        {

            //iTween.MoveTo(primero, new Vector2(primero.transform.position.x + (Screen.height * 0.5f), primero.transform.position.y), 0.5f);
            //iTween.MoveTo(segundo, new Vector2(segundo.transform.position.x + (Screen.height * 1f), segundo.transform.position.y), 0.5f);
            iTween.MoveTo(tercero, new Vector2((Screen.height * 1.25f), primero.transform.position.y), 0.5f);
            h = 1;
            SwipeIzq = false;
            SwipeHecho = false;
        }
        if (X == 1 && SwipeIzq == true && SwipeHecho == true && VPM.i > 1 && aspect == 0.5625f)
        {

            //iTween.MoveTo(primero, new Vector2(primero.transform.position.x + (Screen.height * 1.25f), primero.transform.position.y), 0.5f);
            iTween.MoveTo(segundo, new Vector2((Screen.height * 1.25f), primero.transform.position.y), 0.5f);
            //iTween.MoveTo(tercero, new Vector2(tercero.transform.position.x + (Screen.height * 0.635f), tercero.transform.position.y), 0.5f);
            h = 0;
            SwipeIzq = false;
            SwipeHecho = false;
        }

        ////aspect ratio 18:9


        //if (X <= 0 && SwipeDer == true && SwipeHecho == true && VPM.i > 1 && aspect == 0.5)
        //{
        //    iTween.MoveTo(segundo, new Vector2(segundo.transform.position.x + (-Screen.height * 0.8f), segundo.transform.position.y), 0.5f);
        //    //iTween.MoveTo(tercero, new Vector2(tercero.transform.position.x + (-Screen.height * 0.635f), tercero.transform.position.y), 0.5f);
        //    h = 1;
        //    SwipeDer = false;
        //    SwipeHecho = false;
        //}

        if (X <= 0 && SwipeDer == true && SwipeHecho == true && VPM.i > 1 && aspect == 0.5f)
        {
            iTween.MoveTo(segundo, new Vector2((Screen.height * 0.25f), primero.transform.position.y), 0.5f);
            //iTween.MoveTo(tercero, new Vector2(tercero.transform.position.x + (-Screen.height * 0.635f), tercero.transform.position.y), 0.5f);
            h = 1;
            SwipeDer = false;
            SwipeHecho = false;
        }
        if (X == 1 && SwipeDer == true && SwipeHecho == true && VPM.i > 2 && aspect == 0.5f)
        {

            //iTween.MoveTo(primero, new Vector2(primero.transform.position.x + (-Screen.height * 0.635f), primero.transform.position.y), 0.5f);
            //iTween.MoveTo(segundo, new Vector2(segundo.transform.position.x + (-Screen.height * 0.635f), segundo.transform.position.y), 0.5f);
            iTween.MoveTo(tercero, new Vector2((Screen.height * 0.25f), primero.transform.position.y), 0.5f);
            h = 2;
            SwipeDer = false;
            SwipeHecho = false;
        }

        if (X >= 2 && SwipeIzq == true && SwipeHecho == true && aspect == 0.5f)
        {

            //iTween.MoveTo(primero, new Vector2(primero.transform.position.x + (Screen.height * 0.5f), primero.transform.position.y), 0.5f);
            //iTween.MoveTo(segundo, new Vector2(segundo.transform.position.x + (Screen.height * 1f), segundo.transform.position.y), 0.5f);
            iTween.MoveTo(tercero, new Vector2((Screen.height * 1.25f), primero.transform.position.y), 0.5f);
            h = 1;
            SwipeIzq = false;
            SwipeHecho = false;
        }
        if (X == 1 && SwipeIzq == true && SwipeHecho == true && VPM.i > 1 && aspect == 0.5f)
        {

            //iTween.MoveTo(primero, new Vector2(primero.transform.position.x + (Screen.height * 1.25f), primero.transform.position.y), 0.5f);
            iTween.MoveTo(segundo, new Vector2((Screen.height * 1.25f), primero.transform.position.y), 0.5f);
            //iTween.MoveTo(tercero, new Vector2(tercero.transform.position.x + (Screen.height * 0.635f), tercero.transform.position.y), 0.5f);
            h = 0;
            SwipeIzq = false;
            SwipeHecho = false;
        }


        ////aspect ratio 4:3

        if (X <= 0 && SwipeDer == true && SwipeHecho == true && VPM.i > 1 && aspectc == 0.75f)
        {
            iTween.MoveTo(segundo, new Vector2((Screen.height * 0.375f), primero.transform.position.y), 0.5f);
            //iTween.MoveTo(tercero, new Vector2(tercero.transform.position.x + (-Screen.height * 0.635f), tercero.transform.position.y), 0.5f);
            h = 1;
            SwipeDer = false;
            SwipeHecho = false;
        }
        if (X == 1 && SwipeDer == true && SwipeHecho == true && VPM.i > 2 && aspectc == 0.75f)
        {

            //iTween.MoveTo(primero, new Vector2(primero.transform.position.x + (-Screen.height * 0.635f), primero.transform.position.y), 0.5f);
            //iTween.MoveTo(segundo, new Vector2(segundo.transform.position.x + (-Screen.height * 0.635f), segundo.transform.position.y), 0.5f);
            iTween.MoveTo(tercero, new Vector2((Screen.height * 0.375f), primero.transform.position.y), 0.5f);
            h = 2;
            SwipeDer = false;
            SwipeHecho = false;
        }

        if (X >= 2 && SwipeIzq == true && SwipeHecho == true && aspectc == 0.75f)
        {

            //iTween.MoveTo(primero, new Vector2(primero.transform.position.x + (Screen.height * 0.5f), primero.transform.position.y), 0.5f);
            //iTween.MoveTo(segundo, new Vector2(segundo.transform.position.x + (Screen.height * 1f), segundo.transform.position.y), 0.5f);
            iTween.MoveTo(tercero, new Vector2((Screen.height * 1.25f), primero.transform.position.y), 0.5f);
            h = 1;
            SwipeIzq = false;
            SwipeHecho = false;
        }
        if (X == 1 && SwipeIzq == true && SwipeHecho == true && VPM.i > 1 && aspectc == 0.75f)
        {

            //iTween.MoveTo(primero, new Vector2(primero.transform.position.x + (Screen.height * 1.25f), primero.transform.position.y), 0.5f);
            iTween.MoveTo(segundo, new Vector2((Screen.height * 1.25f), primero.transform.position.y), 0.5f);
            //iTween.MoveTo(tercero, new Vector2(tercero.transform.position.x + (Screen.height * 0.635f), tercero.transform.position.y), 0.5f);
            h = 0;
            SwipeIzq = false;
            SwipeHecho = false;
        }


        ////aspect ratio 18.5:9
        //if (X <= 0 && SwipeDer == true && SwipeHecho == true && VPM.i > 1 && aspect == 0.4864865f)
        //{
        //    iTween.MoveTo(segundo, new Vector2(segundo.transform.position.x + (-Screen.height * 0.785f), segundo.transform.position.y), 0.5f);
        //    //iTween.MoveTo(tercero, new Vector2(tercero.transform.position.x + (-Screen.height * 0.635f), tercero.transform.position.y), 0.5f);
        //    h = 1;
        //    SwipeDer = false;
        //    SwipeHecho = false;
        //}
        if (X <= 0 && SwipeDer == true && SwipeHecho == true && VPM.i > 1 && aspect == 0.4864865f)
        {
            iTween.MoveTo(segundo, new Vector2((Screen.height * 0.2375f), primero.transform.position.y), 0.5f);
            //iTween.MoveTo(tercero, new Vector2(tercero.transform.position.x + (-Screen.height * 0.635f), tercero.transform.position.y), 0.5f);
            h = 1;
            SwipeDer = false;
            SwipeHecho = false;
        }
        if (X == 1 && SwipeDer == true && SwipeHecho == true && VPM.i > 2 && aspect == 0.4864865f)
        {

            //iTween.MoveTo(primero, new Vector2(primero.transform.position.x + (-Screen.height * 0.635f), primero.transform.position.y), 0.5f);
            //iTween.MoveTo(segundo, new Vector2(segundo.transform.position.x + (-Screen.height * 0.635f), segundo.transform.position.y), 0.5f);
            iTween.MoveTo(tercero, new Vector2((Screen.height * 0.2375f), primero.transform.position.y), 0.5f);
            SwipeDer = false;
            SwipeHecho = false;
            h = 2;
        }

        if (X >= 2 && SwipeIzq == true && SwipeHecho == true && aspect == 0.4864865f)
        {

            //iTween.MoveTo(primero, new Vector2(primero.transform.position.x + (Screen.height * 0.5f), primero.transform.position.y), 0.5f);
            //iTween.MoveTo(segundo, new Vector2(segundo.transform.position.x + (Screen.height * 1f), segundo.transform.position.y), 0.5f);
            iTween.MoveTo(tercero, new Vector2((Screen.height * 1.25f), primero.transform.position.y), 0.5f);
            h = 1;
            SwipeIzq = false;
            SwipeHecho = false;
        }
        if (X == 1 && SwipeIzq == true && SwipeHecho == true && VPM.i > 1 && aspect == 0.4864865f)
        {

            //iTween.MoveTo(primero, new Vector2(primero.transform.position.x + (Screen.height * 1.25f), primero.transform.position.y), 0.5f);
            iTween.MoveTo(segundo, new Vector2((Screen.height * 1.25f), primero.transform.position.y), 0.5f);
            //iTween.MoveTo(tercero, new Vector2(tercero.transform.position.x + (Screen.height * 0.635f), tercero.transform.position.y), 0.5f);
            h = 0;
            SwipeIzq = false;
            SwipeHecho = false;
        }

    }
}
