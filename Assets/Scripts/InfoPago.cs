﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Globalization;
using VoxelBusters.NativePlugins;
using VoxelBusters.NativePlugins.Internal;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class InfoPago : MonoBehaviour {
    public int PagoID, idColor, idIcono, Personalizado, idCasa, idBen, CadaCuando, AlertaAntes, idCat, idPPred, RecPago, RecordPago, DiaW, MesW, AñoW, DiasMes, Recordatorio,
        MesNum, DiaNum, AñoNum, Pag, Pas, DiaRec, pasados, posicion, posicionPasado, index, acomodo, ventana, fechasantes, acomodoindex, PagoIDLocal;
    public string NombreCatPago, Dia, Mes, Año, Nota, PrecioPago, CodBarras, MesText, Fec, FecPasado, FecAntes, FecRec, FecProximo, FechaLetra;
    public Text NCP;
    public Image Icon, IconColor;
    //public GameObject Historial;
    public Sprite Icono;
    public Color color;
    public ObjectManager ob;
    public TP tp;
    public Text NombCat, TotalPago, FechaPago, PagadoCon, Casa, Recurrencia, TipoRecurrencia;
    public Button Comprobante, checar, NotaBoton;
    public GameObject PagoPred;
    public bool pasado, pagado, primero = true, Ward, Ward2, modificado, pasadoeliminar, casaencontrada, beneficiarioencontrado, pasadobool;
    public Fecha Date;
    public AP ap;
    public Beneficiario ben;
    public bool X, recordadodia = true, recordadoantes = true, pasadosx, pas = true, listo, NoP;
    public float timer, timerdown;
    public DateTime date, dt, fecharecordatorio, fechaDeHoy, dateproximopago, dates, FecPast;
    public string fechahoy, fecha, fechas, fecharecordatorios, fechashoy, fechapasado, pagadocon, pagadoen, titulo, contenido, nombrecasa, nombrebenef, nombreCasa;
 //   public TestNotification TN;
    public SwipeVentana SP;
    public Registro reg;
    // Use this for initialization
    void Awake()
    {
        
        
    }
    void Start () {
        ob = GameObject.Find("ObjectManager").GetComponent<ObjectManager>();
        tp = GameObject.Find("EligeColorIcono").GetComponent<TP>();
        ap = GameObject.Find("AgregarPago").GetComponent<AP>();
       // TN = this.gameObject.GetComponent<TestNotification>();
        //if (Casa.text == "" || Casa.text == "Casa Polanco" || Casa.text == "Casa condesa" || nombreCasa == "")
        //{
        //    for (int uj = 0; uj < ap.casa.Casass.Count; uj++)
        //    {
        //        if (ap.casa.Casass[uj].GetComponent<InfoCasa>().idCasa == idCasa)
        //        {
        //            Casa.text = ap.casa.Casass[uj].GetComponent<InfoCasa>().NickNameC;
        //            nombreCasa = ap.casa.Casass[uj].GetComponent<InfoCasa>().NickNameC;
        //        }
        //        else if ((Casa.text == "" || Casa.text == "Casa Polanco" || Casa.text == "Casa condesa") && uj == ap.casa.Casass.Count - 1)
        //        {
        //            Casa.text = "Casa Desconocida o Eliminada";
        //            nombreCasa = "Casa Desconocida o Eliminada";
        //        }
        //    }
        //}
        //EventTrigger trigger = this.gameObject.GetComponent<EventTrigger>();
        //evento.eventID = EventTriggerType.Drag;
        //evento.callback.AddListener((data) => { OnBeginDragDelegate((PointerEventData)data); });
        //trigger.triggers.Add(evento);
        for (int ui = 0; ui < 10; ui++)
        {

            if (NombreCatPago == tp.NombreCategoria[ui])
            {
                NoP = true;
            }
        }
        if (/*idCat < 10 || */NoP == true)
        {
            Icon.sprite = tp.PagoPredeterminado[idIcono];
            IconColor.color = tp.iconColorBack;
        }
        else if (/*idCat > 9 || */NoP == false)
        {
            Icon.sprite = tp.IC[idIcono];
            IconColor.color = tp.CO[idColor];
        }
        if (pasados == 1)
        {
            //for(int a = 0; ap.casa.Casass.Count > a; a++)
            //{
            //    if(ap.casa.Casass[a].GetComponent<InfoCasa>().idCasa == idCasa)
            //    {
            //        Casa.text = ap.casa.Casass[a].GetComponent<InfoCasa>().NickNameC;
            //    }
            //}
            if (Mes == "Enero")
                MesNum = 1;
            if (Mes == "Febrero")
                MesNum = 2;
            if (Mes == "Marzo")
                MesNum = 3;
            if (Mes == "Abril")
                MesNum = 4;
            if (Mes == "Mayo")
                MesNum = 5;
            if (Mes == "Junio")
                MesNum = 6;
            if (Mes == "Julio")
                MesNum = 7;
            if (Mes == "Agosto")
                MesNum = 8;
            if (Mes == "Septiembre")
                MesNum = 9;
            if (Mes == "Octubre")
                MesNum = 10;
            if (Mes == "Noviembre")
                MesNum = 11;
            if (Mes == "Diciembre")
                MesNum = 12;
            AñoNum = Int32.Parse(Año);
        }
        if (pasados < 1)
        {
            //for (int a = 0; ap.casa.Casass.Count > a; a++)
            //{
            //    if (ap.casa.Casass[a].GetComponent<InfoCasa>().idCasa == idCasa)
            //    {
            //        Casa.text = ap.casa.Casass[a].GetComponent<InfoCasa>().NickNameC;
            //    }
            //}
            if (CadaCuando == 1)
            {
                Recordatorio = 7;
            }
            if (CadaCuando == 2)
            {
                Recordatorio = 15;
            }
            if (CadaCuando == 3)
            {
                Recordatorio = 21;
            }
            if (CadaCuando == 4)
            {
                Recordatorio = 30;
            }
            if (CadaCuando == 5)
            {
                Recordatorio = 60;
            }
            if (CadaCuando == 6)
            {
                Recordatorio = 120;
            }
            if (CadaCuando == 7)
            {
                Recordatorio = 180;
            }
            if (CadaCuando == 8)
            {
                Recordatorio = 365;
            }
            Icono = Icon.sprite;
            if (Mes == "Enero")
                MesNum = 1;
            if (Mes == "Febrero")
                MesNum = 2;
            if (Mes == "Marzo")
                MesNum = 3;
            if (Mes == "Abril")
                MesNum = 4;
            if (Mes == "Mayo")
                MesNum = 5;
            if (Mes == "Junio")
                MesNum = 6;
            if (Mes == "Julio")
                MesNum = 7;
            if (Mes == "Agosto")
                MesNum = 8;
            if (Mes == "Septiembre")
                MesNum = 9;
            if (Mes == "Octubre")
                MesNum = 10;
            if (Mes == "Noviembre")
                MesNum = 11;
            if (Mes == "Diciembre")
                MesNum = 12;
            FechaPago.text = FechaLetra;
            //this.GetComponent<Button>().onClick.AddListener(Click);
            DiaNum = Int32.Parse(Dia);
            AñoNum = Int32.Parse(Año);
            //MesNum = Int32.Parse(Mes);
            fecha = Dia + "/" + MesNum + "/" + Año;
            DateTime.TryParseExact(fecha, "d/M/yyyy", new CultureInfo("es-MX"), DateTimeStyles.AdjustToUniversal, out date);
            fechashoy = DateTime.Now.ToString("d/M/yyyy");
            //fechas = dt.ToString();
            fechas = date.ToString("d/M/yyyy");
            dates = date.AddHours(8);
            fecharecordatorio = dates.AddDays(-AlertaAntes);
            fecharecordatorios = fecharecordatorio.ToString("d/M/yyyy");
            Fec = dates.ToString();
            FecPast = dates.AddDays(1);
            fechapasado = dates.AddDays(1).ToString("d/M/yyyy");
            dateproximopago = dates.AddDays(Recordatorio);
            FecProximo = dateproximopago.ToString();
            ap.change = true;

            fechasantes = AlertaAntes;
            //if(idCat < 10)
            //{
            //    Icon.sprite = ap.tp.Icon[idCat];
            //}
            //else if (idCat > 9)
            //{
            //    Icon.sprite = ap.tp.Icon[idIcono];
            //    IconColor.color = ap.tp.Colo[idColor];
            //}
            //ap.VPM.chequeo = true;
            FechaPago.text = FechaLetra;
            listo = true;
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (CadaCuando == 0 || CadaCuando == 9)
        {
            TipoRecurrencia.text = "Sólo una vez";
            RecordPago = 0;
        }
        nombreCasa = Casa.text;
        //Casa.text = nombreCasa;
        if (MesNum == 1)
            Mes = "Enero";
        if (MesNum == 2)
            Mes = "Febrero";
        if (MesNum == 3)
            Mes = "Marzo";
        if (MesNum == 4)
            Mes = "Abril";
        if (MesNum == 5)
            Mes = "Mayo";
        if (MesNum == 6)
            Mes = "Junio";
        if (MesNum == 7)
            Mes = "Julio";
        if (MesNum == 8)
            Mes = "Agosto";
        if (MesNum == 9)
            Mes = "Septiembre";
        if (MesNum == 10)
            Mes = "Octubre";
        if (MesNum == 11)
            Mes = "Noviembre";
        if (MesNum == 12)
            Mes = "Diciembre";
        FechaLetra = Dia + "/" + Mes + "/" + Año;
        FechaPago.text = FechaLetra;
        index = this.gameObject.GetComponent<RectTransform>().GetSiblingIndex();
        Fec = dates.ToString();
        FecRec = fecharecordatorio.ToString();
        FecPasado = fechapasado.ToString();
        FecProximo = dateproximopago.ToString();
        fechashoy = DateTime.Now.ToString("d/M/yyyy");
        if (pasados < 1)
        {
            fechahoy = DateTime.Now.ToString("d/M/yyyy");
            if (modificado == true)
            {
                modificado = false;
                if (CadaCuando == 1)
                {
                    Recordatorio = 7;
                }
                else if (CadaCuando == 2)
                {
                    Recordatorio = 15;
                }
                else if (CadaCuando == 3)
                {
                    Recordatorio = 21;
                }
                else if (CadaCuando == 4)
                {
                    Recordatorio = 30;
                }
                else if (CadaCuando == 5)
                {
                    Recordatorio = 60;
                }
                else if (CadaCuando == 6)
                {
                    Recordatorio = 120;
                }
                else if (CadaCuando == 7)

                {
                    Recordatorio = 180;
                }
                else if (CadaCuando == 8)
                {
                    Recordatorio = 365;
                }
                else if (CadaCuando == 0)
                {
                    Recordatorio = 365;
                }
                if (CadaCuando == 1)
                    TipoRecurrencia.text = "Semanal";
                else if (CadaCuando == 2)
                    TipoRecurrencia.text = "Cada quince días";
                else if (CadaCuando == 3)
                    TipoRecurrencia.text = "Cada tres semanas";
                else if (CadaCuando == 4)
                    TipoRecurrencia.text = "Mensual";
                else if (CadaCuando == 5)
                    TipoRecurrencia.text = "Bimestral";
                else if (CadaCuando == 6)
                    TipoRecurrencia.text = "Cuatrimestral";
                else if (CadaCuando == 7)
                    TipoRecurrencia.text = "Semestral";
                else if (CadaCuando == 8)
                    TipoRecurrencia.text = "Anual";
                else if (CadaCuando == 0 || CadaCuando == 9)
                {
                    TipoRecurrencia.text = "Sólo una vez";
                    RecordPago = 0;
                }
                if (RecordPago == 0)
                {
                    Recurrencia.text = "Pago: ";
                    //TipoRecurrencia.text = "";
                }
                else if (RecordPago == 1)
                {
                    Recurrencia.text = "Pago: ";
                }
                DiaNum = Int32.Parse(Dia);
                AñoNum = Int32.Parse(Año);
                //MesNum = Int32.Parse(Mes);
                if (Mes == "Enero")
                    MesNum = 1;
                else if (Mes == "Febrero")
                    MesNum = 2;
                else if (Mes == "Marzo")
                    MesNum = 3;
                else if (Mes == "Abril")
                    MesNum = 4;
                else if (Mes == "Mayo")
                    MesNum = 5;
                else if (Mes == "Junio")
                    MesNum = 6;
                else if (Mes == "Julio")
                    MesNum = 7;
                else if (Mes == "Agosto")
                    MesNum = 8;
                else if (Mes == "Septiembre")
                    MesNum = 9;
                else if (Mes == "Octubre")
                    MesNum = 10;
                else if (Mes == "Noviembre")
                    MesNum = 11;
                else if (Mes == "Diciembre")
                    MesNum = 12;
                fecha = Dia + "/" + MesNum + "/" + Año;
                FechaLetra = Dia + "/" + Mes + "/" + Año;
                FechaPago.text = FechaLetra;
                DateTime.TryParseExact(fecha, "d/M/yyyy", new CultureInfo("es-MX"), DateTimeStyles.AdjustToUniversal, out date);
                dates = date.AddHours(8);
                fecharecordatorio = dates.AddDays(-AlertaAntes);
                fecharecordatorios = fecharecordatorio.ToString("d/M/yyyy");
                fechas = dates.ToString("d/M/yyyy");
                fechapasado = dates.AddDays(1).ToString("d/M/yyyy");
                modificado = false;
                recordadodia = true;
                recordadoantes = true;
                ap.change = true;
                //ap.VPM.chequeo = true;
            }
            if (fechahoy == fechas && recordadodia == true)
            {
                titulo = "Recordatorio de pago";
                contenido = "Hoy es el ultimo día para pagar: " + NombreCatPago + " de la casa: " + Casa.text;
               // TN.ScheduleLocalNotification(TN.CreateNotification(dates, eNotificationRepeatInterval.NONE, titulo, contenido));
                //TN.CreateNotification(dates, eNotificationRepeatInterval.NONE, );
                recordadodia = false;
            }
            if (DateTime.Now == fecharecordatorio && recordadoantes == true)
            {
                titulo = "Alerta de pago";
                contenido = "Recordatorio de pagar " + NombreCatPago + "de la casa: " + Casa.text;
               // TN.ScheduleLocalNotification(TN.CreateNotification(fecharecordatorio, eNotificationRepeatInterval.NONE, titulo, contenido));

                //TN.CreateNotification(fecharecordatorio, eNotificationRepeatInterval.NONE, "Recordatorio de pagar " + NombreCatPago + "de la casa" + Casa.text);
                if (AlertaAntes == fechasantes && fechasantes > 0)
                {
                    fechasantes--;
                }
                if (fechasantes < AlertaAntes && fechasantes > 0)
                {
                    fecharecordatorio.AddDays(-1);
                    fecharecordatorios = fecharecordatorio.ToString("d/M/yyyy");
                }
                if (fechasantes == 0)
                {
                    recordadoantes = false;
                }

            }
            if (pagado == true && RecordPago == 1)
            {
                //print("entro");
                pagado = false;
                dt = dates.AddDays(Recordatorio);
                fechas = dt.ToString("d/M/yyyy");
                date = dt;
                dates = dt;
                fechapasado = dates.AddDays(1).ToString("d/M/yyyy");
                fecharecordatorio = dates.AddDays(-AlertaAntes);
                fecharecordatorios = fecharecordatorio.ToString("d/M/yyyy");

                dateproximopago = dates.AddDays(Recordatorio);
                DiaNum = dt.Day;
                MesNum = dt.Month;
                AñoNum = dt.Year;
                Dia = dates.Day.ToString();
                //Mes = dates.Month.ToString();
                if (dt.Month == 1)
                    Mes = "Enero";
                else if (dt.Month == 2)
                    Mes = "Febrero";
                else if (dt.Month == 3)
                    Mes = "Marzo";
                else if (dt.Month == 4)
                    Mes = "Abril";
                else if (dt.Month == 5)
                    Mes = "Mayo";
                else if (dt.Month == 6)
                    Mes = "Junio";
                else if (dt.Month == 7)
                    Mes = "Julio";
                else if (dt.Month == 8)
                    Mes = "Agosto";
                else if (dt.Month == 9)
                    Mes = "Septiembre";
                else if (dt.Month == 10)
                    Mes = "Octubre";
                else if (dt.Month == 11)
                    Mes = "Noviembre";
                else if (dt.Month == 12)
                    Mes = "Diciembre";
                FechaLetra = Dia + "/" + Mes + "/" + Año;
                FechaPago.text = FechaLetra;
                Año = dates.Year.ToString();
                fecha = Dia + "/" + MesNum + "/" + Año;
                recordadodia = true;
                ap.change = true;
                fechasantes = AlertaAntes;
                recordadoantes = true;
                //print(posicion);
                if(posicion<0)
                {
                    posicion = 0;
                }
                try
                {

                ap.Dia[posicion] = Dia;
                ap.Mes[posicion] = Mes;
                ap.Año[posicion] = Año;
                }
                catch(Exception e)
                {
                    ap.Dia[ap.Dia.Count-1] = Dia;
                    ap.Mes[ap.Mes.Count-1] = Mes;
                    ap.Año[ap.Año.Count-1] = Año;
                    Debug.Log("error");
                }
                ap.Offline = null;
                for (int yh = 0; yh < ap.Pagos.Count; yh++)
                {
                    if (yh == 0)
                        ap.Offline = ap.IdPagoLocal[yh] + "," + ap.IdPago[yh] + "," + ap.IdCasa[yh] + "," + ap.idBeneficiario[yh] + "," + ap.CadaCuandos[yh] + "," + ap.AlertasAntes[yh] + "," + ap.idCategoria[yh] + "," + ap.CodBarras[yh] + ","
                            + ap.PrecioPagos[yh] + "," + ap.Dia[yh] + "," + ap.Mes[yh] + "," + ap.Año[yh] + "," + ap.Nota[yh] + "," + ap.ReciboPagos[yh] + "," + ap.RecordatorioPagos[yh] + "," + ap.idColors[yh] + "," +
                            ap.idIconos[yh] + "," + ap.IdPPred[yh] + "," + ap.Checarpasado[yh] + "," + ap.NombreCategoria[yh] + "," + ap.PagoPersonalizado[yh] + "," + ap.nombreCasas[yh];
                    else if (yh > 0)
                        ap.Offline += ";" + ap.IdPagoLocal[yh] + "," + ap.IdPago[yh] + "," + ap.IdCasa[yh] + "," + ap.idBeneficiario[yh] + "," + ap.CadaCuandos[yh] + "," + ap.AlertasAntes[yh] + "," + ap.idCategoria[yh] + "," + ap.CodBarras[yh] + ","
                            + ap.PrecioPagos[yh] + "," + ap.Dia[yh] + "," + ap.Mes[yh] + "," + ap.Año[yh] + "," + ap.Nota[yh] + "," + ap.ReciboPagos[yh] + "," + ap.RecordatorioPagos[yh] + "," + ap.idColors[yh] + "," +
                            ap.idIconos[yh] + "," + ap.IdPPred[yh] + "," + ap.Checarpasado[yh] + "," + ap.NombreCategoria[yh] + "," + ap.PagoPersonalizado[yh] + "," + ap.nombreCasas[yh];
                }
                PlayerPrefs.SetString("Pagos", ap.Offline);

                if(PagoID == 0)
                {
                    for(int ji = 0; ji<ap.IdPagoLocalOffline.Count; ji++)
                    {
                        if(ap.IdPagoLocalOffline[ji] == PagoIDLocal)
                        {
                            ap.fechaOffline[ji] = FechaLetra;
                        }
                    }
                    ap.NoEnviadoPagosAgregar = null;
                    for (int gii = 0; ap.IdPagoLocalOffline.Count > gii; gii++)
                    {
                        if (gii == 0)
                        {
                            ap.NoEnviadoPagosAgregar = ap.IdPagoLocalOffline[gii] + "," + ap.NombreCategoriaOffline[gii] + "," + ap.idColorsOffline[gii] + "," + ap.idIconosOffline[gii] + "," + ap.IdCasaOffline[gii] + "," +
                                ap.PrecioPagosOffline[gii] + "," + ap.fechaOffline[gii] + "," + ap.RecordatorioPagosOffline[gii] + "," + ap.CadaCuandosOffline[gii] + "," + ap.AlertasAntesOffline[gii] + "," + ap.IdBeneficiarioOffline[gii] +
                                "," + ap.NotaOffline[gii] + "," + ap.ReciboPagosOffline[gii] + "," + ap.CodBarrasOffline[gii] + "," + ap.nombreCasasOffline[gii];
                        }
                        else if (gii > 0)
                        {
                            ap.NoEnviadoPagosAgregar += ";" + ap.IdPagoLocalOffline[gii] + "," + ap.NombreCategoriaOffline[gii] + "," + ap.idColorsOffline[gii] + "," + ap.idIconosOffline[gii] + "," + ap.IdCasaOffline[gii] + "," +
                                ap.PrecioPagosOffline[gii] + "," + ap.fechaOffline[gii] + "," + ap.RecordatorioPagosOffline[gii] + "," + ap.CadaCuandosOffline[gii] + "," + ap.AlertasAntesOffline[gii] + "," + ap.IdBeneficiarioOffline[gii] +
                                "," + ap.NotaOffline[gii] + "," + ap.ReciboPagosOffline[gii] + "," + ap.CodBarrasOffline[gii] + "," + ap.nombreCasasOffline[gii];
                        }
                    }
                    PlayerPrefs.SetString("PagosAgregar", ap.NoEnviadoPagosAgregar);
                }
                else if(PagoID != 0)
                {
                    ap.IdPagoModOffline.Add(PagoID);
                    ap.NombreCategoriaModOffline.Add(NombreCatPago);
                    ap.idColorsModOffline.Add(idColor);
                    ap.idIconosModOffline.Add(idIcono);
                    ap.IdCasaModOffline.Add(idCasa);
                    ap.PrecioPagosModOffline.Add(PrecioPago);
                    ap.fechaModOffline.Add(Dia + "/" + Mes + "/" + Año);
                    ap.RecordatorioPagosModOffline.Add(RecordPago);
                    ap.CadaCuandosModOffline.Add(CadaCuando);
                    ap.AlertasAntesModOffline.Add(AlertaAntes);
                    ap.IdBeneficiarioModOffline.Add(idBen);
                    if (Nota == null || Nota == "")
                    {
                        Nota = "";
                        ap.NotaModOffline.Add(Nota);
                    }
                    else if(Nota != null|| Nota != "")
                    {
                        ap.NotaModOffline.Add(Nota);
                    }
                    ap.ReciboPagosModOffline.Add(RecPago);
                    ap.CodBarrasModOffline.Add(CodBarras);
                    ap.ModificarPagosNE = null;
                    for (int gii = 0; ap.IdPagoModOffline.Count > gii; gii++)
                    {
                        if (gii == 0)
                        {
                            ap.ModificarPagosNE = ap.IdPagoModOffline[gii] + "," + ap.NombreCategoriaModOffline[gii] + "," + ap.idColorsModOffline[gii] + "," + ap.idIconosModOffline[gii] + "," + ap.IdCasaModOffline[gii] + "," +
                                ap.PrecioPagosModOffline[gii] + "," + ap.fechaModOffline[gii] + "," + ap.RecordatorioPagosModOffline[gii] + "," + ap.CadaCuandosModOffline[gii] + "," + ap.AlertasAntesModOffline[gii] + "," + ap.IdBeneficiarioModOffline[gii] +
                                "," + ap.NotaModOffline[gii] + "," + ap.ReciboPagosModOffline[gii] + "," + ap.CodBarrasModOffline[gii];
                        }
                        else if (gii > 0)
                        {
                            ap.ModificarPagosNE += ";" + ap.IdPagoModOffline[gii] + "," + ap.NombreCategoriaModOffline[gii] + "," + ap.idColorsModOffline[gii] + "," + ap.idIconosModOffline[gii] + "," + ap.IdCasaModOffline[gii] + "," +
                                ap.PrecioPagosModOffline[gii] + "," + ap.fechaModOffline[gii] + "," + ap.RecordatorioPagosModOffline[gii] + "," + ap.CadaCuandosModOffline[gii] + "," + ap.AlertasAntesModOffline[gii] + "," + ap.IdBeneficiarioModOffline[gii] +
                                "," + ap.NotaModOffline[gii] + "," + ap.ReciboPagosModOffline[gii] + "," + ap.CodBarrasModOffline[gii];
                        }
                    }
                    PlayerPrefs.SetString("PagosModificar", ap.ModificarPagosNE);
                    ap.ModPago();
                }
                //ap.VPM.chequeo = true;
                
            }
            if (pagado == true && RecordPago == 0)
            {

                Eliminar();
            }
            if (DateTime.Now >= FecPast && FecPast.Year != 0001 && RecordPago == 1 && pas == true && (reg.cargando == false || reg.carga == false))
            {
                pas = false;
                //pasadosx = true;
                if (Año == DateTime.Now.Year.ToString())
                {
                    ap.IdPagoPasadoLocal.Add(ap.f);
                    ap.IdPagoPasado.Add(0);
                    ap.IdCasaPasado.Add(idCasa);
                    ap.idBeneficiarioPasado.Add(idBen);
                    //CadaCuandosPasado.Add(CadaCuando);
                    //AlertasAntesPasado.Add(AlertaAntes);
                    ap.idCategoriaPasado.Add(idCat);
                    ap.CodBarrasPasado.Add(CodBarras);
                    ap.PrecioPagosPasado.Add(PrecioPago);
                    ap.DiaPasado.Add(Dia);
                    ap.MesPasado.Add(Mes);
                    ap.AñoPasado.Add(Año);
                    ap.NotaPasado.Add(Nota);
                    ap.ReciboPagosPasado.Add(RecPago);
                    ap.NombreCategoriaPasado.Add(NombreCatPago);
                    //RecordatorioPagosPasado.Add(RecordPago);
                    ap.PagoPersonalizadoPasado.Add(Personalizado);
                    ap.idColorsPasado.Add(idColor);
                    ap.idIconosPasado.Add(idIcono);
                    ap.IdPPredPasado.Add(idPPred);
                    ap.Checarpasado.Add(1);
                    ap.PosicionPasado.Add(ap.posicionPasado);

                    ap.IdPagoLocalPasadoOffline.Add(ap.f);
                    ap.NombreCategoriaPasadoOffline.Add(NombreCatPago);
                    ap.idColorsPasadoOffline.Add(idColor);
                    ap.idIconosPasadoOffline.Add(idIcono);
                    ap.IdCasaPasadoOffline.Add(idCasa);
                    ap.PrecioPagosPasadoOffline.Add(PrecioPago);
                    ap.fechaPasadoOffline.Add(Dia + "/" + Mes + "/" + Año);
                    ap.RecordatorioPagosPasadoOffline.Add(0);
                    ap.CadaCuandosPasadoOffline.Add(0);
                    ap.AlertasAntesPasadoOffline.Add(0);
                    ap.IdBeneficiarioPasadoOffline.Add(idBen);
                    if(Nota == null)
                    {
                        ap.NotaPasadoOffline.Add("");
                    }
                    else
                    {
                        ap.NotaPasadoOffline.Add(Nota);
                    }
                    //ap.NotaPasadoOffline.Add(Nota);
                    ap.ReciboPagosPasadoOffline.Add(RecPago);
                    ap.CodBarrasPasadoOffline.Add(CodBarras);
                    ap.OfflinePasado = null;
                    for (int yh = 0; yh < ap.PagosPasado.Count; yh++)
                    {
                        if (yh == 0)
                            ap.OfflinePasado = ap.IdPagoPasadoLocal[yh] + "," + ap.IdPagoPasado[yh] + "," + ap.IdCasaPasado[yh] + "," + ap.idBeneficiarioPasado[yh] + "," + ap.idCategoriaPasado[yh] + "," + ap.CodBarrasPasado[yh] + ","
                                + ap.PrecioPagosPasado[yh] + "," + ap.DiaPasado[yh] + "," + ap.MesPasado[yh] + "," + ap.AñoPasado[yh] + "," + ap.NotaPasado[yh] + "," + ap.ReciboPagosPasado[yh] + "," + ap.idColorsPasado[yh] + "," +
                                ap.idIconosPasado[yh] + "," + ap.IdPPredPasado[yh] + "," + ap.Checarpasado[1] + "," + ap.NombreCategoriaPasado[yh] + "," + ap.PagoPersonalizadoPasado[yh];
                        else if (yh > 0)
                            ap.OfflinePasado += ";" + ap.IdPagoPasadoLocal[yh] + "," + ap.IdPagoPasado[yh] + "," + ap.IdCasaPasado[yh] + "," + ap.idBeneficiarioPasado[yh] + "," + ap.idCategoriaPasado[yh] + "," + ap.CodBarrasPasado[yh] + ","
                                + ap.PrecioPagosPasado[yh] + "," + ap.DiaPasado[yh] + "," + ap.MesPasado[yh] + "," + ap.AñoPasado[yh] + "," + ap.NotaPasado[yh] + "," + ap.ReciboPagosPasado[yh] + "," + ap.idColorsPasado[yh] + "," +
                                ap.idIconosPasado[yh] + "," + ap.IdPPredPasado[yh] + "," + ap.Checarpasado[1] + "," + ap.NombreCategoriaPasado[yh] + "," + ap.PagoPersonalizadoPasado[yh];
                    }
                    PlayerPrefs.SetString("PagosPasado", ap.OfflinePasado);
                    ap.AgregarPagoPasadoNE = null;
                    for (int hu = 0; hu < ap.IdPagoLocalPasadoOffline.Count; hu++)
                    {
                        if (hu == 0)
                        {
                            ap.AgregarPagoPasadoNE = ap.IdPagoLocalPasadoOffline[hu] + "," + ap.NombreCategoriaPasadoOffline[hu] + "," + ap.idColorsPasadoOffline[hu] + "," + ap.idIconosPasadoOffline[hu] +
                                "," + ap.IdCasaPasadoOffline[hu] + "," + ap.PrecioPagosPasadoOffline[hu] + "," + ap.fechaPasadoOffline[hu] + "," + ap.RecordatorioPagosPasadoOffline[hu] + "," +
                                ap.CadaCuandosPasadoOffline[hu] + "," + ap.AlertasAntesPasadoOffline[hu] + "," + ap.IdBeneficiarioPasadoOffline[hu] + "," + ap.NotaPasadoOffline[hu] + "," +
                                ap.ReciboPagosPasadoOffline[hu] + "," + ap.CodBarrasPasadoOffline[hu];
                        }
                        else if (hu > 0)
                        {
                            ap.AgregarPagoPasadoNE += ";" + ap.IdPagoLocalPasadoOffline[hu] + "," + ap.NombreCategoriaPasadoOffline[hu] + "," + ap.idColorsPasadoOffline[hu] + "," + ap.idIconosPasadoOffline[hu] +
                                "," + ap.IdCasaPasadoOffline[hu] + "," + ap.PrecioPagosPasadoOffline[hu] + "," + ap.fechaPasadoOffline[hu] + "," + ap.RecordatorioPagosPasadoOffline[hu] + "," +
                                ap.CadaCuandosPasadoOffline[hu] + "," + ap.AlertasAntesPasadoOffline[hu] + "," + ap.IdBeneficiarioPasadoOffline[hu] + "," + ap.NotaPasadoOffline[hu] + "," +
                                ap.ReciboPagosPasadoOffline[hu] + "," + ap.CodBarrasPasadoOffline[hu];
                        }
                    }
                    PlayerPrefs.SetString("PagosPasadoAgregar", ap.AgregarPagoPasadoNE);
                    ap.f++;
                    ap.posicionPasado++;
                    ap.PasadoX();
                    if (ap.IdCasaPasadoOffline[0] != 0)
                    {
                        ap.AgregarPagoPasado();
                    }
                    //ap.AgregarPagoPasado();
                    //NomCat.text = null;
                    dt = dates.AddDays(Recordatorio);
                    fechas = dt.ToString("d/M/yyyy");
                    date = dt;
                    dates = dt;
                    FecPast = dates.AddDays(1);
                    fechapasado = dates.AddDays(1).ToString("d/M/yyyy");
                    fecharecordatorio = dates.AddDays(-AlertaAntes);
                    fecharecordatorios = fecharecordatorio.ToString("d/M/yyyy");

                    dateproximopago = dt;
                    DiaNum = dt.Day;
                    MesNum = dt.Month;
                    AñoNum = dt.Year;
                    Dia = dateproximopago.Day.ToString();
                    //Mes = dateproximopago.Month.ToString();
                    Año = dateproximopago.Year.ToString();
                    //fecha = Dia + "/" + Mes + "/" + Año;
                    if (dateproximopago.Month.ToString() == "1")
                        Mes = "Enero";
                    else if (dateproximopago.Month.ToString() == "2")
                        Mes = "Febrero";
                    else if (dateproximopago.Month.ToString() == "3")
                        Mes = "Marzo";
                    else if (dateproximopago.Month.ToString() == "4")
                        Mes = "Abril";
                    else if (dateproximopago.Month.ToString() == "5")
                        Mes = "Mayo";
                    else if (dateproximopago.Month.ToString() == "6")
                        Mes = "Junio";
                    else if (dateproximopago.Month.ToString() == "7")
                        Mes = "Julio";
                    else if (dateproximopago.Month.ToString() == "8")
                        Mes = "Agosto";
                    else if (dateproximopago.Month.ToString() == "9")
                        Mes = "Septiembre";
                    else if (dateproximopago.Month.ToString() == "10")
                        Mes = "Octubre";
                    else if (dateproximopago.Month.ToString() == "11")
                        Mes = "Noviembre";
                    else if (dateproximopago.Month.ToString() == "12")
                        Mes = "Diciembre";
                    FechaLetra = Dia + "/" + Mes + "/" + Año;
                    //ap.fechaModOffline.Add(FechaLetra);
                    ap.Categoria.text = NombreCatPago;

                    ap.fechaMod = FechaLetra;
                    ap.PrecioPago.text = PrecioPago;
                    ap.idIcono = idIcono;
                    ap.idColor = idColor;
                    ap.RecordPago = RecordPago;
                    //FechaPago.text = FechaLetra;
                    recordadoantes = true;
                    recordadodia = true;
                    ap.change = true;
                    ap.pagar = true;
                    fechasantes = AlertaAntes;
                    ap.ids = posicion;
                    ap.idCasa = idCasa;
                    ap.idIcono = idIcono;
                    ap.idColor = idColor;
                    ap.CadaCuando = CadaCuando;
                    ap.AlertaAntes = AlertaAntes;
                    //ap.idCat = idCat;
                    ap.idBen = idBen;
                    ap.RecPago = RecPago;
                    //ap.PagPred = idPPred;
                    //ap.PagPer = Personalizado;
                    ap.RecordPago = RecordPago;
                    ap.PrecioPago.text = PrecioPago;
                    if (CodBarras != "" && CodBarras != null)
                    {
                        ap.codbar = Int32.Parse(CodBarras);
                    }
                    else
                    {
                        ap.codbar = 0;
                    }
                    ap.NotaPago.text = Nota;
                    //ap.NomCat.text = NombreCatPago;
                    ap.fecha = FechaPago.text;
                    //ap.Modificar();
                    //ap.ModPago();
                    for(int hy = 0; hy<ap.IdPagoLocal.Count; hy++)
                    {
                        if(PagoIDLocal == ap.IdPagoLocal[hy])
                        {
                            ap.Dia[hy] = Dia;
                            ap.Mes[hy] = Mes;
                            ap.Año[hy] = Año;
                        }
                    }
                    ap.Offline = null;
                    for (int yh = 0; yh < ap.Pagos.Count; yh++)
                    {
                        if (yh == 0)
                            ap.Offline = ap.IdPagoLocal[yh] + "," + ap.IdPago[yh] + "," + ap.IdCasa[yh] + "," + ap.idBeneficiario[yh] + "," + ap.CadaCuandos[yh] + "," + ap.AlertasAntes[yh] + "," + ap.idCategoria[yh] + "," + ap.CodBarras[yh] + ","
                                + ap.PrecioPagos[yh] + "," + ap.Dia[yh] + "," + ap.Mes[yh] + "," + ap.Año[yh] + "," + ap.Nota[yh] + "," + ap.ReciboPagos[yh] + "," + ap.RecordatorioPagos[yh] + "," + ap.idColors[yh] + "," +
                                ap.idIconos[yh] + "," + ap.IdPPred[yh] + "," + ap.Checarpasado[1] + "," + ap.NombreCategoria[yh] + "," + ap.PagoPersonalizado[yh] + "," + ap.nombreCasas[yh];
                        else if (yh > 0)
                            ap.Offline += ";" + ap.IdPagoLocal[yh] + "," + ap.IdPago[yh] + "," + ap.IdCasa[yh] + "," + ap.idBeneficiario[yh] + "," + ap.CadaCuandos[yh] + "," + ap.AlertasAntes[yh] + "," + ap.idCategoria[yh] + "," + ap.CodBarras[yh] + ","
                                + ap.PrecioPagos[yh] + "," + ap.Dia[yh] + "," + ap.Mes[yh] + "," + ap.Año[yh] + "," + ap.Nota[yh] + "," + ap.ReciboPagos[yh] + "," + ap.RecordatorioPagos[yh] + "," + ap.idColors[yh] + "," +
                                ap.idIconos[yh] + "," + ap.IdPPred[yh] + "," + ap.Checarpasado[1] + "," + ap.NombreCategoria[yh] + "," + ap.PagoPersonalizado[yh] + "," + ap.nombreCasas[yh];
                    }
                    PlayerPrefs.SetString("Pagos", ap.Offline);

                    if (PagoID == 0)
                    {
                        for(int ui = 0; ui<ap.IdPagoLocalOffline.Count; ui++)
                        {
                            if(PagoIDLocal == ap.IdPagoLocalOffline[ui])
                            {
                                ap.PrecioPagosOffline[ui] = PrecioPago;
                                ap.fechaOffline[ui] = FechaLetra;
                                ap.RecordatorioPagosOffline[ui] = RecordPago;
                                ap.CadaCuandosOffline[ui] = CadaCuando;
                                ap.AlertasAntesOffline[ui] = AlertaAntes;
                                ap.IdBeneficiarioOffline[ui] = idBen;
                                ap.NotaOffline[ui] = Nota;
                                ap.ReciboPagosOffline[ui] = RecPago;
                                ap.CodBarrasOffline[ui] = CodBarras;
                            }
                        }
                        ap.NoEnviadoPagosAgregar = null;
                        for (int gii = 0; ap.IdPagoLocalOffline.Count > gii; gii++)
                        {
                            if (gii == 0)
                            {
                                ap.NoEnviadoPagosAgregar = ap.IdPagoLocalOffline[gii] + "," + ap.NombreCategoriaOffline[gii] + "," + ap.idColorsOffline[gii] + "," + ap.idIconosOffline[gii] + "," + ap.IdCasaOffline[gii] + "," +
                                    ap.PrecioPagosOffline[gii] + "," + ap.fechaOffline[gii] + "," + ap.RecordatorioPagosOffline[gii] + "," + ap.CadaCuandosOffline[gii] + "," + ap.AlertasAntesOffline[gii] + "," + ap.IdBeneficiarioOffline[gii] +
                                    "," + ap.NotaOffline[gii] + "," + ap.ReciboPagosOffline[gii] + "," + ap.CodBarrasOffline[gii] + "," + ap.nombreCasasOffline[gii];
                            }
                            else if (gii > 0)
                            {
                                ap.NoEnviadoPagosAgregar += ";" + ap.IdPagoLocalOffline[gii] + "," + ap.NombreCategoriaOffline[gii] + "," + ap.idColorsOffline[gii] + "," + ap.idIconosOffline[gii] + "," + ap.IdCasaOffline[gii] + "," +
                                    ap.PrecioPagosOffline[gii] + "," + ap.fechaOffline[gii] + "," + ap.RecordatorioPagosOffline[gii] + "," + ap.CadaCuandosOffline[gii] + "," + ap.AlertasAntesOffline[gii] + "," + ap.IdBeneficiarioOffline[gii] +
                                    "," + ap.NotaOffline[gii] + "," + ap.ReciboPagosOffline[gii] + "," + ap.CodBarrasOffline[gii] + "," + ap.nombreCasasOffline[gii];
                            }
                        }
                        PlayerPrefs.SetString("PagosAgregar", ap.NoEnviadoPagosAgregar);

                    }
                    else if (PagoID != 0)
                    {
                        ap.IdPagoModOffline.Add(PagoID);
                        ap.NombreCategoriaModOffline.Add(NombreCatPago);
                        ap.idColorsModOffline.Add(idColor);
                        ap.idIconosModOffline.Add(idIcono);
                        ap.IdCasaModOffline.Add(idCasa);
                        ap.PrecioPagosModOffline.Add(PrecioPago);
                        ap.fechaModOffline.Add(FechaLetra);
                        ap.RecordatorioPagosModOffline.Add(RecordPago);
                        ap.CadaCuandosModOffline.Add(CadaCuando);
                        ap.AlertasAntesModOffline.Add(AlertaAntes);
                        ap.IdBeneficiarioModOffline.Add(idBen);
                        ap.NotaModOffline.Add(Nota);
                        ap.ReciboPagosModOffline.Add(RecPago);
                        ap.CodBarrasModOffline.Add(CodBarras);
                        ap.ModificarPagosNE = null;
                        for (int gii = 0; ap.IdPagoModOffline.Count > gii; gii++)
                        {
                            if (gii == 0)
                            {
                                ap.ModificarPagosNE = ap.IdPagoModOffline[gii] + "," + ap.NombreCategoriaModOffline[gii] + "," + ap.idColorsModOffline[gii] + "," + ap.idIconosModOffline[gii] + "," + ap.IdCasaModOffline[gii] + "," +
                                    ap.PrecioPagosModOffline[gii] + "," + ap.fechaModOffline[gii] + "," + ap.RecordatorioPagosModOffline[gii] + "," + ap.CadaCuandosModOffline[gii] + "," + ap.AlertasAntesModOffline[gii] + "," + ap.IdBeneficiarioModOffline[gii] +
                                    "," + ap.NotaModOffline[gii] + "," + ap.ReciboPagosModOffline[gii] + "," + ap.CodBarrasModOffline[gii];
                            }
                            else if (gii > 0)
                            {
                                ap.ModificarPagosNE += ";" + ap.IdPagoModOffline[gii] + "," + ap.NombreCategoriaModOffline[gii] + "," + ap.idColorsModOffline[gii] + "," + ap.idIconosModOffline[gii] + "," + ap.IdCasaModOffline[gii] + "," +
                                    ap.PrecioPagosModOffline[gii] + "," + ap.fechaModOffline[gii] + "," + ap.RecordatorioPagosModOffline[gii] + "," + ap.CadaCuandosModOffline[gii] + "," + ap.AlertasAntesModOffline[gii] + "," + ap.IdBeneficiarioModOffline[gii] +
                                    "," + ap.NotaModOffline[gii] + "," + ap.ReciboPagosModOffline[gii] + "," + ap.CodBarrasModOffline[gii];
                            }
                        }
                        PlayerPrefs.SetString("PagosModificar", ap.ModificarPagosNE);
                        ap.ModPago();
                    }



                    //Form.AddField("IdUsuario", reg.id);
                    //Form.AddField("IdPago", IdPagoModOffline[0]);
                    //Form.AddField("NombrePago", NombreCategoriaModOffline[0]);
                    //Form.AddField("ColorPago", idColorsModOffline[0]);
                    //Form.AddField("IconoPago", idIconosModOffline[0]);
                    //Form.AddField("IdCasa", IdCasaModOffline[0]);
                    //Form.AddField("MontoPago", PrecioPagosModOffline[0]);
                    //Form.AddField("FechaPago", fechaModOffline[0]);
                    //Form.AddField("RecurrentePago", RecordatorioPagosModOffline[0]);
                    //Form.AddField("RepetirPago", CadaCuandosModOffline[0]);
                    //Form.AddField("FechaRecordatorioPago", AlertasAntesModOffline[0]);
                    //Form.AddField("IdBeneficiario", IdBeneficiarioModOffline[0]);
                    //Form.AddField("NotaPago", NotaModOffline[0]);
                    //Form.AddField("ReciboPago", ReciboPagosModOffline[0]);
                    //Form.AddField("CodigoPago", CodBarrasModOffline[0]);



                    //ap.VPM.chequeo = true;
                }
            }

            if (DateTime.Now >= FecPast && RecordPago == 0 && pas == true)
            {
                pas = false;
                pasadobool = true;
                ap.IdPagoPasadoLocal.Add(ap.f);
                ap.IdPagoPasado.Add(0);
                ap.IdCasaPasado.Add(idCasa);
                ap.idBeneficiarioPasado.Add(idBen);
                //CadaCuandosPasado.Add(CadaCuando);
                //AlertasAntesPasado.Add(AlertaAntes);
                ap.idCategoriaPasado.Add(idCat);
                ap.CodBarrasPasado.Add(CodBarras);
                ap.PrecioPagosPasado.Add(PrecioPago);
                ap.DiaPasado.Add(Dia);
                ap.MesPasado.Add(Mes);
                ap.AñoPasado.Add(Año);
                ap.NotaPasado.Add(Nota);
                ap.ReciboPagosPasado.Add(RecPago);
                ap.NombreCategoriaPasado.Add(NombreCatPago);
                //RecordatorioPagosPasado.Add(RecordPago);
                ap.PagoPersonalizadoPasado.Add(Personalizado);
                ap.idColorsPasado.Add(idColor);
                ap.idIconosPasado.Add(idIcono);
                ap.IdPPredPasado.Add(idPPred);
                ap.Checarpasado.Add(1);
                ap.PosicionPasado.Add(ap.posicionPasado);

                ap.IdPagoLocalPasadoOffline.Add(ap.f);
                ap.NombreCategoriaPasadoOffline.Add(NombreCatPago);
                ap.idColorsPasadoOffline.Add(idColor);
                ap.idIconosPasadoOffline.Add(idIcono);
                ap.IdCasaPasadoOffline.Add(idCasa);
                ap.PrecioPagosPasadoOffline.Add(PrecioPago);
                ap.fechaPasadoOffline.Add(Dia + "/" + Mes + "/" + Año);
                ap.RecordatorioPagosPasadoOffline.Add(0);
                ap.CadaCuandosPasadoOffline.Add(0);
                ap.AlertasAntesPasadoOffline.Add(0);
                ap.IdBeneficiarioPasadoOffline.Add(idBen);
                if (Nota == null)
                {
                    ap.NotaPasadoOffline.Add("");
                }
                else
                {
                    ap.NotaPasadoOffline.Add(Nota);
                }
                //ap.NotaPasadoOffline.Add(Nota);
                ap.ReciboPagosPasadoOffline.Add(RecPago);
                ap.CodBarrasPasadoOffline.Add(CodBarras);
                ap.OfflinePasado = null;
                for (int yh = 0; yh < ap.PagosPasado.Count; yh++)
                {
                    if (yh == 0)
                        ap.OfflinePasado = ap.IdPagoPasadoLocal[yh] + "," + ap.IdPagoPasado[yh] + "," + ap.IdCasaPasado[yh] + "," + ap.idBeneficiarioPasado[yh] + "," + ap.idCategoriaPasado[yh] + "," + ap.CodBarrasPasado[yh] + ","
                            + ap.PrecioPagosPasado[yh] + "," + ap.DiaPasado[yh] + "," + ap.MesPasado[yh] + "," + ap.AñoPasado[yh] + "," + ap.NotaPasado[yh] + "," + ap.ReciboPagosPasado[yh] + "," + ap.idColorsPasado[yh] + "," +
                            ap.idIconosPasado[yh] + "," + ap.IdPPredPasado[yh] + "," + ap.Checarpasado[1] + "," + ap.NombreCategoriaPasado[yh] + "," + ap.PagoPersonalizadoPasado[yh];
                    else if (yh > 0)
                        ap.OfflinePasado += ";" + ap.IdPagoPasadoLocal[yh] + "," + ap.IdPagoPasado[yh] + "," + ap.IdCasaPasado[yh] + "," + ap.idBeneficiarioPasado[yh] + "," + ap.idCategoriaPasado[yh] + "," + ap.CodBarrasPasado[yh] + ","
                            + ap.PrecioPagosPasado[yh] + "," + ap.DiaPasado[yh] + "," + ap.MesPasado[yh] + "," + ap.AñoPasado[yh] + "," + ap.NotaPasado[yh] + "," + ap.ReciboPagosPasado[yh] + "," + ap.idColorsPasado[yh] + "," +
                            ap.idIconosPasado[yh] + "," + ap.IdPPredPasado[yh] + "," + ap.Checarpasado[1] + "," + ap.NombreCategoriaPasado[yh] + "," + ap.PagoPersonalizadoPasado[yh];
                }
                PlayerPrefs.SetString("PagosPasado", ap.OfflinePasado);
                ap.AgregarPagoPasadoNE = null;
                for (int hu = 0; hu < ap.IdPagoLocalPasadoOffline.Count; hu++)
                {
                    if (hu == 0)
                    {
                        ap.AgregarPagoPasadoNE = ap.IdPagoLocalPasadoOffline[hu] + "," + ap.NombreCategoriaPasadoOffline[hu] + "," + ap.idColorsPasadoOffline[hu] + "," + ap.idIconosPasadoOffline[hu] +
                            "," + ap.IdCasaPasadoOffline[hu] + "," + ap.PrecioPagosPasadoOffline[hu] + "," + ap.fechaPasadoOffline[hu] + "," + ap.RecordatorioPagosPasadoOffline[hu] + "," +
                            ap.CadaCuandosPasadoOffline[hu] + "," + ap.AlertasAntesPasadoOffline[hu] + "," + ap.IdBeneficiarioPasadoOffline[hu] + "," + ap.NotaPasadoOffline[hu] + "," +
                            ap.ReciboPagosPasadoOffline[hu] + "," + ap.CodBarrasPasadoOffline[hu];
                    }
                    else if (hu > 0)
                    {
                        ap.AgregarPagoPasadoNE += ";" + ap.IdPagoLocalPasadoOffline[hu] + "," + ap.NombreCategoriaPasadoOffline[hu] + "," + ap.idColorsPasadoOffline[hu] + "," + ap.idIconosPasadoOffline[hu] +
                            "," + ap.IdCasaPasadoOffline[hu] + "," + ap.PrecioPagosPasadoOffline[hu] + "," + ap.fechaPasadoOffline[hu] + "," + ap.RecordatorioPagosPasadoOffline[hu] + "," +
                            ap.CadaCuandosPasadoOffline[hu] + "," + ap.AlertasAntesPasadoOffline[hu] + "," + ap.IdBeneficiarioPasadoOffline[hu] + "," + ap.NotaPasadoOffline[hu] + "," +
                            ap.ReciboPagosPasadoOffline[hu] + "," + ap.CodBarrasPasadoOffline[hu];
                    }
                }
                PlayerPrefs.SetString("PagosPasadoAgregar", ap.AgregarPagoPasadoNE);
                ap.f++;
                ap.posicionPasado++;
                PasadoEliminar();
                if (ap.IdCasaPasadoOffline[0] != 0)
                {
                    ap.AgregarPagoPasado();
                }
                //PasadoEliminar();

            }
            if (pasado == true && RecordPago == 1)
            {
                pasado = false;
                //PagoID = ap.PagoID;
                Pasado();
                ap.ids = PagoID;
                dt = dates.AddDays(Recordatorio);
                fechas = dt.ToString("d/M/yyyy");
                date = dt;
                dates = dt;
                fechapasado = dates.AddDays(1).ToString("d/M/yyyy");
                fecharecordatorio = dates.AddDays(-AlertaAntes);
                fecharecordatorios = fecharecordatorio.ToString("d/M/yyyy");

                dateproximopago = dates.AddDays(Recordatorio);
                DiaNum = dt.Day;
                MesNum = dt.Month;
                AñoNum = dt.Year;
                Dia = dates.Day.ToString();
                Mes = dates.Month.ToString();
                Año = dates.Year.ToString();
                fecha = Dia + "/" + Mes + "/" + Año;
                if (dt.Month == 1)
                    Mes = "Enero";
                else if (dt.Month == 2)
                    Mes = "Febrero";
                else if (dt.Month == 3)
                    Mes = "Marzo";
                else if (dt.Month == 4)
                    Mes = "Abril";
                else if (dt.Month == 5)
                    Mes = "Mayo";
                else if (dt.Month == 6)
                    Mes = "Junio";
                else if (dt.Month == 7)
                    Mes = "Julio";
                else if (dt.Month == 8)
                    Mes = "Agosto";
                else if (dt.Month == 9)
                    Mes = "Septiembre";
                else if (dt.Month == 10)
                    Mes = "Octubre";
                else if (dt.Month == 11)
                    Mes = "Noviembre";
                else if (dt.Month == 12)
                    Mes = "Diciembre";
                FechaLetra = Dia + "/" + Mes + "/" + Año;
                ap.fechaMod = FechaLetra;
                FechaPago.text = FechaLetra;
                recordadoantes = true;
                recordadodia = true;
                ap.change = true;
                fechasantes = AlertaAntes;



                //ap.ModPago();



                //ap.VPM.chequeo = true;
            }
            if (pasado == true && RecordPago == 0)
            {
                ap.ids = PagoID;
                PasadoEliminar();
                dt = date.AddDays(Recordatorio);
                fechas = dt.ToString("d/M/yyyy");
                date = dt;
                fechapasado = date.AddDays(1).ToString("d/M/yyyy");
                fecharecordatorio = date.AddDays(-AlertaAntes);
                fecharecordatorios = fecharecordatorio.ToString("d/M/yyyy");

                DiaNum = dt.Day;
                MesNum = dt.Month;
                AñoNum = dt.Year;
                if (dt.Month == 1)
                    Mes = "Enero";
                else if (dt.Month == 2)
                    Mes = "Febrero";
                else if (dt.Month == 3)
                    Mes = "Marzo";
                else if (dt.Month == 4)
                    Mes = "Abril";
                else if (dt.Month == 5)
                    Mes = "Mayo";
                else if (dt.Month == 6)
                    Mes = "Junio";
                else if (dt.Month == 7)
                    Mes = "Julio";
                else if (dt.Month == 8)
                    Mes = "Agosto";
                else if (dt.Month == 9)
                    Mes = "Septiembre";
                else if (dt.Month == 10)
                    Mes = "Octubre";
                else if (dt.Month == 11)
                    Mes = "Noviembre";
                else if (dt.Month == 12)
                    Mes = "Diciembre";
                FechaLetra = Dia + "/" + Mes + "/" + Año;
                FechaPago.text = FechaLetra;
                pasado = false;
            }
            if (pasadoeliminar == true)
            {
                EliminarPasado();
                pasadoeliminar = false;
            }
            if (fechahoy == fechapasado && RecordPago == 1)
            {
                pasado = true;
                ap.change = true;
                //ap.VPM.chequeo = true;
            }
        }




        //if(DiaNum == System.DateTime.)
    }

    public void Click()
    {
        //if (ap.timer < ap.timerdown)
        //{
        //    //ap.Beneficiario.text = "Beneficiario: " + NombreBeneficiario;
        //    //ap.IB = this;
        //    //ap.idBen = BeneficiarioID;
        //    //ob.Click("BeneficiarioQuitar");
        //}

        if (pasados == 0)
        {
            ob.OpcionesTitulo.text = "Acciones mis pagos";
            ob.PagoPagado.onClick.RemoveAllListeners();
            ob.PagoPagado.onClick.AddListener(Pagar);
            ob.PagoEditar.onClick.RemoveAllListeners();
            ob.PagoEditar.onClick.AddListener(Editar);
            ap.eliminar = true;
            ob.PagoEliminar.onClick.RemoveAllListeners();
            ob.PagoEliminar.onClick.AddListener(PopUpEliminar);
            //ob.OpcionesSeleccionarTexto.text = "Seleccionar categoría";
            ap.nombrePago = gameObject.name;
            ob.Click("OpcionesPago");
        }
        if(pasados == 1)
        {
            ob.PagoPasado.onClick.RemoveAllListeners();
            ob.PagoPasado.onClick.AddListener(Pagar);
            ap.eliminar = true;
            ob.PagoEliminarPasado.onClick.RemoveAllListeners();
            ob.PagoEliminarPasado.onClick.AddListener(PopUpEliminarPasado);
            //ob.OpcionesSeleccionarTexto.text = "Seleccionar categoría";
            ap.nombrePago = gameObject.name;

            ob.Click("OpcionesPagoPasado");
        }
    }
    public void PopUpEliminar()
    {
        ob.Click("Eliminar");
        ob.EliminarTexto.text = "Seguro desea eliminar el pago a realizar con la categoría: " + NombreCatPago + " de la casa: " + Casa.text + ", ya no será posible recuperarla después de eliminarlo.";
        ob.EliminarAceptar.onClick.RemoveAllListeners();
        ob.EliminarAceptar.onClick.AddListener(Eliminar);
        ap.nombrePago = this.gameObject.name;

    }
    public void PopUpEliminarPasado()
    {
        ob.Click("Eliminar");
        ob.EliminarTexto.text = "Seguro desea eliminar el pago no realizado con la categoría: " + NombreCatPago + " de la casa: " + Casa.text + ", ya no será posible recuperarla después de eliminarlo.";
        ob.EliminarAceptar.onClick.RemoveAllListeners();
        ob.EliminarAceptar.onClick.AddListener(EliminarPasado);
        ap.nombrePago = this.gameObject.name;

    }
    public void Editar()
    {
        ap.eliminar = false;
		ap.botonph.SetActive(false);
        Date.fecha = true;
        ap.editar = true;
        ap.modp = true;
        ap.IP = this;
        ob.Click("OpcionesPagoQuitar");
        ap.GuardarAgregar.SetActive(false);
        ap.GuardarEditar.SetActive(true);
        ap.modo = 1;
        ob.Click("AgregarPago");
        ap.ids = posicion;
        ap.idCasa = idCasa;
        ap.idIcono = idIcono;
        ap.idColor = idColor;
        ap.CadaCuando = CadaCuando;
        ap.AlertaAntes = AlertaAntes;
        ap.idCat = idCat;
        ap.idBen = idBen;
        ap.RecPago = RecPago;
        ap.PagPred = idPPred;
        ap.PagPer = Personalizado;
        ap.RecordPago = RecordPago;
        ap.AñoP.text = Año;
        ap.PagoID = PagoID;
        ap.IdPagoLocalB = PagoIDLocal;

        ap.dias = DiaNum;
        ap.meses = MesNum;
        ap.años = AñoNum;

        //ap.MesP.text = Mes;
        if (Mes == "Enero")
        {
            ap.MesP.text = "Enero";
            ap.Date.MesP.value = 0;
        }
        else if (Mes == "Febrero")
        {
            ap.MesP.text = "Febrero";
            ap.Date.MesP.value = 1;
        }
        else if (Mes == "Marzo")
        {
            ap.MesP.text = "Marzo";
            ap.Date.MesP.value = 2;
        }
        else if (Mes == "Abril")
        {
            ap.MesP.text = "Abril";
            ap.Date.MesP.value = 3;
        }
        else if (Mes == "Mayo")
        {
            ap.MesP.text = "Mayo";
            ap.Date.MesP.value = 4;
        }
        else if (Mes == "Junio")
        {
            ap.MesP.text = "Junio";
            ap.Date.MesP.value = 5;
        }
        else if (Mes == "Julio")
        {
            ap.MesP.text = "Julio";
            ap.Date.MesP.value = 6;
        }
        else if (Mes == "Agosto")
        {
            ap.MesP.text = "Agosto";
            ap.Date.MesP.value = 7;
        }
        else if (Mes == "Septiembre")
        {
            ap.MesP.text = "Septiembre";
            ap.Date.MesP.value = 8;
        }
        else if (Mes == "Octubre")
        {
            ap.MesP.text = "Octubre";
            ap.Date.MesP.value = 9;
        }
        else if (Mes == "Noviembre")
        {
            ap.MesP.text = "Noviembre";
            ap.Date.MesP.value = 10;
        }
        else if (Mes == "Diciembre")
        {
            ap.MesP.text = "Diciembre";
            ap.Date.MesP.value = 11;
        }
        ap.DiaP.text = DiaNum.ToString();
        ap.Date.DiaP.value = DiaNum - 1;
        //ap.Date.DiaP.value = Int32.Parse(DiaNum) - 1;
        ap.NombreCategoriaString = NombreCatPago;
        ap.PrecioPago.text = PrecioPago;
        ap.CsB.text = CodBarras;
        ap.NotaPago.text = Nota;
        //ap.NomCat.text = NombreCatPago;
        
        for(int uy = 0; uy < ap.casa.Casass.Count; uy++)
        {
            if(idCasa == ap.casa.Casass[uy].GetComponent<InfoCasa>().idCasa)
            {
                casaencontrada = true;
                nombrecasa = ap.casa.Casass[uy].GetComponent<InfoCasa>().NickNameC;
            }
        }
        //if (casaencontrada == false)
        //{
        //    ap.Casa.text = "Mis casas: " + "Casa eliminada";
        //}
        //if (casaencontrada == true)
        //{
        //    casaencontrada = false;
        //    ap.Casa.text = "Mis casas: " + nombrecasa;
        //}

        for (int ey = 0; ey < ben.Beneficiarios.Count; ey++)
        {
            if (idBen == ben.Beneficiarios[ey].GetComponent<InfoBeneficiario>().BeneficiarioID)
            {
                beneficiarioencontrado = true;
                nombrebenef = ben.Beneficiarios[ey].GetComponent<InfoBeneficiario>().NombreBeneficiario;
            }
        }
        if (beneficiarioencontrado == false && idBen > 0 && CodBarras == "")
        {
            ap.Beneficiario.text = "Beneficiario: " + "Beneficiario eliminado";
        }
        if (beneficiarioencontrado == false && idBen == 0 && CodBarras != "")
        {
         
        }
        if (beneficiarioencontrado == true)
        {
            beneficiarioencontrado = false;
            ap.Beneficiario.text = "Beneficiario: " + nombrebenef; 
        }
        //ap.Casa.text = "Mis casas: " + ap.casa.NNC[idCasa];

        if (NoP == true)
        {
            ap.botonIcono.sprite = ap.tp.PagoPredeterminado[idIcono];
            ap.iconColor.color = ap.tp.iconColorBack;
        }
        else if (NoP == false)
        {
            ap.botonIcono.sprite = ap.tp.IC[idIcono];
            ap.iconColor.color = ap.tp.CO[idColor];
        }
		if(idBen > 0 && (CodBarras == "" || CodBarras == "0"))
        {
            ap.Benef.SetActive(true);
            ap.CB.SetActive(false);
        }
        //if (idBen == 0 && CodBarras == "")
        //{
        //    ap.Benef.SetActive(true);
        //    ap.CB.SetActive(false);
        //}
        if (idBen == 0)
        {
            ap.Benef.SetActive(false);
            if(CodBarras == "" || CodBarras == "0")
            {
                ap.CsB.text = "";
            }
            ap.CB.SetActive(true);
        }
        ap.Fecha.text = "Fecha de pago: " + date.Day.ToString("d") + "/" + Mes + "/" + date.Year.ToString();

        if (AlertaAntes == 1)
        {
            ap.AlertasTexto.text = "Alertas: 1 día antes de la fecha de vencimiento";
        }
        else if (AlertaAntes == 2)
        {
            ap.AlertasTexto.text = "Alertas: Durante 2 días antes de la fecha de vencimiento";
        }
        else if (AlertaAntes == 3)
        {
            ap.AlertasTexto.text = "Alertas: Durante 3 días antes de la fecha de vencimiento";
        }
        if (CadaCuando == 1)
        {
            ap.RepetirCadaTexto.text = "Repetir cada: Semanal";
        }
        else if (CadaCuando == 2)
        {
            ap.RepetirCadaTexto.text = "Repetir cada: Cada quince días";
        }
        else if (CadaCuando == 3)
        {
            ap.RepetirCadaTexto.text = "Repetir cada: Cada tres semanas";
        }
        else if (CadaCuando == 4)
        {
            ap.RepetirCadaTexto.text = "Repetir cada: Mensual";
        }
        else if (CadaCuando == 5)
        {
            ap.RepetirCadaTexto.text = "Repetir cada: Bimestral";
        }
        else if (CadaCuando == 6)
        {
            ap.RepetirCadaTexto.text = "Repetir cada: Cuatrimestral";
        }
        else if (CadaCuando == 7)
        {
            ap.RepetirCadaTexto.text = "Repetir cada: Semestral";
        }
        else if (CadaCuando == 8)
        {
            ap.RepetirCadaTexto.text = "Repetir cada: Anual";
        }
        else if (CadaCuando == 0 || CadaCuando == 9)
        {
            ap.RepetirCadaTexto.text = "Repetir cada: Sólo una vez";
        }
        modificado = true;

        ap.lastCadaCuando = ap.CadaCuando;
        ap.lastNotas = ap.NotaPago.text;
        ap.lastRecordarPago = ap.RecordPago;
        ap.lastRepetirCadaCuandoText = ap.RepetirCadaTexto.text;
        ap.lastAlertaAntesText = ap.AlertasTexto.text;
        ap.lastAlertasAntes = ap.AlertaAntes;

    }


    public void Eliminar()
    {
        ap.nombrePago = gameObject.name;

        ob.Click("EliminarQuitar");
        ob.Click("OpcionesPagoQuitar");
        //ob.Click("BeneficiarioQuitar");
        //ap.Beneficiario.text = "Beneficiario";
        ap.nombrePago = gameObject.name;

        ap.ids = posicion;
        //ap.index = index;
        //ap.ElimPago();
        ap.PagoID = PagoID;
        ap.nombrePago = gameObject.name;

        ap.IdPagoLocalB = PagoIDLocal;
        ap.nombrePago = gameObject.name;
        ap.IP = null;
        ap.IP = this;
        ap.Eliminar();
        

    }
    public void EliminarPasado()
    {

        ob.Click("EliminarQuitar");
        ob.Click("OpcionesPagoPasadoQuitar");
        //ob.Click("BeneficiarioQuitar");
        //ap.Beneficiario.text = "Beneficiario";
        ap.Pagost = this.gameObject;
        //ap.ids = posicionPasado;
        for (int i = 0; i < ap.PagosPasado.Count; i++)
        {
            if(ap.PagosPasado[i].GetComponent<InfoPago>().PagoID == PagoID)
            {
                ap.ids = i;
                break;
            }

        }
        ap.IdPagoLocalB = PagoIDLocal;
        ap.PagoID = PagoID;
        ap.idCasa = idCasa;
        ap.EliminarPasado();
        //ap.EliminarPasado();
        ap.past = true;

    }
    public void Pasado()
    {
        if (pasadosx == true)
        {
            pasadosx = false;
            ap.IP3 = this;
            ap.GuardarAgregar.SetActive(true);
            ap.GuardarEditar.SetActive(false);
            ap.ids = posicion;
            ap.NombreCategoriaString = NombreCatPago;
            ap.idCasa = idCasa;
            ap.idIcono = idIcono;
            ap.idColor = idColor;
            ap.CadaCuando = CadaCuando;
            ap.AlertaAntes = AlertaAntes;
            //ap.idCat = idCat;
            ap.idBen = idBen;
            ap.RecPago = RecPago;
            //ap.PagPred = idPPred;
            //ap.PagPer = Personalizado;
            ap.RecordPago = RecordPago;
            ap.PrecioPago.text = PrecioPago;
            /*ap.codbar = */Int32.TryParse(CodBarras, out ap.codbar);
            ap.NotaPago.text = Nota;
            //ap.NomCat.text = NombreCatPago;
            ap.fecha = FechaPago.text;
            ap.PasadoX();
            //ap.AgregarPagoPasado();


            pasado = false;
        }
    }
    public void PasadoMod()
    {
        if (pasadosx == true)
        {
            pasadosx = false;
            ap.IP3 = this;
            ap.GuardarAgregar.SetActive(true);
            ap.GuardarEditar.SetActive(false);
            ap.ids = PagoID;
            ap.NombreCategoriaString = NombreCatPago;
            ap.idCasa = idCasa;
            ap.idIcono = idIcono;
            ap.idColor = idColor;
            ap.CadaCuando = CadaCuando;
            ap.AlertaAntes = AlertaAntes;
            //ap.idCat = idCat;
            ap.idBen = idBen;
            ap.RecPago = RecPago;
            //ap.PagPred = idPPred;
            //ap.PagPer = Personalizado;
            ap.RecordPago = RecordPago;
            ap.PrecioPago.text = PrecioPago;
            ap.codbar = Int32.Parse(CodBarras);
            ap.NotaPago.text = Nota;
            //ap.NomCat.text = NombreCatPago;
            ap.fecha = FechaPago.text;
            ap.Modificar();
            ap.PasadoX();


            //ap.ModPago();
            //ap.AgregarPagoPasado();



            pasado = false;
        }
    }
    public void PasadoEliminar()
    {
        if (pasadobool == true)
        {
            pasadobool = false;
            if(this != null)
            ap.IP3 = this;
            ap.nombrePago = gameObject.name;
            ap.nombrePago = gameObject.name;

            ap.GuardarAgregar.SetActive(true);
            ap.GuardarEditar.SetActive(false);
            ap.ids = posicion;
            ap.PagoID = PagoID;
            ap.IdPagoLocalB = PagoIDLocal;
            ap.NombreCategoriaString = NombreCatPago;
            ap.idCasa = idCasa;
            ap.idIcono = idIcono;
            ap.idColor = idColor;
            ap.CadaCuando = CadaCuando;
            ap.nombrePago = gameObject.name;

            ap.AlertaAntes = AlertaAntes;
            //ap.idCat = idCat;
            ap.idBen = idBen;
            ap.RecPago = RecPago;
            ap.nombrePago = gameObject.name;

            //ap.PagPred = idPPred;
            //ap.PagPer = Personalizado;
            ap.RecordPago = Recordatorio;
            ap.PrecioPago.text = PrecioPago;
            if (CodBarras != "" && CodBarras != null)
            {
                ap.codbar = Int32.Parse(CodBarras);
            }
            else
            {
                ap.codbar = 0;
            }
            ap.NotaPago.text = Nota;
            //ap.NomCat.text = NombreCatPago;
            ap.fecha = FechaPago.text;
            ap.nombrePago = gameObject.name;

            ap.nombrePago = gameObject.name;

            ap.eliminar = true;
            ap.Eliminar();
            ap.PasadoX();


            //ap.ElimPago();
            //ap.AgregarPagoPasado();




            ap.change = true;
            //ap.VPM.chequeo = true;
        }
    }
    public void Pagar()
    {
        ap.IP = this;
        ap.PagoID = PagoID;
        ap.IdPagoLocalB = PagoIDLocal;
        ap.idCasa = idCasa;
        ap.apagar = float.Parse(PrecioPago);
        ap.AhoraBack = true;
        
        //ap.posicion = posicion;
        ap.nombreCasa = nombreCasa;
        ap.ids = posicion;
        
        
        //ap.NombreCategoriaString
        
        if(pasados == 1)
        {
            //ap.posicion = posicionPasado;
            ap.past = true;
        }
        ob.Click("OpcionesPagoQuitar");
        ob.Click("OpcionesPagoPasadoQuitar");
        ob.Click("OpcionesMetPago");
        
    }
    public void VerNota()
    {
        if(Nota == "" || Nota == null)
        {
            ap.NotaTexto.text = "Nota sin información.";
        }
        else if (Nota != "" || Nota != null)
        {
            ap.NotaTexto.text = Nota;
        }
        

        ob.Click("NotaPago");
    }
    
}

