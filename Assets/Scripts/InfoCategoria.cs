﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class InfoCategoria : MonoBehaviour {
    public int CatPagoID, idColor, idIcono, personalizada, idPP, codbar, posicion, index, CatPagoIDLocal;
    public string NombreCatPago, agregar;
    public Text NCP;
    public Image Icon, IconColor;
    //public GameObject Historial;
    public Sprite Icono;
    public Color color;
    public ObjectManager ob;
    public AP ap;
    public Casas casas;
    public bool CBB;
    public TP tp;
    public bool X;
 
    
	// Use this for initialization
	void Start () {
        Icono = Icon.sprite;
        if(codbar == 1)
        {
            CBB = true;
        }
        else if (codbar == 0)
        {
            CBB = false;
        }
        //GetComponent<Button>().onClick.AddListener(Click);
	}
	
	// Update is called once per frame
	void Update () {
        //if(agregar == "Agregar")
        //{
        //    this.gameObject.GetComponent<RectTransform>().SetAsLastSibling();
        //}
        ////index = this.gameObject.GetComponent<RectTransform>().GetSiblingIndex();
	}

    public void Click()
    {

        ob.OpcionesTitulo.text = "Categoría de usuario";

        ob.OpcionesSeleccionar.onClick.RemoveAllListeners();
        ob.OpcionesSeleccionar.onClick.AddListener(Seleccionar);

        ob.OpcionesSeleccionarTexto.text = "Usar categoría";
        ob.OpcionesEditar.interactable = true;
        ob.OpcionesEliminar.interactable = true;

        ob.OpcionesEditar.onClick.RemoveAllListeners();
        ob.OpcionesEditar.onClick.AddListener(Editar);

        ob.OpcionesEliminar.onClick.RemoveAllListeners();
        ob.OpcionesEliminar.onClick.AddListener(PopUpEliminar);

        ob.Click("Opciones");
    }
    public void Editar()
    {

        tp.modificar = true;
        ob.Click("OpcionesQuitar");
        //tp.HeaderCat.text = "Modificar categoría";
        tp.BotonGuardar.SetActive(false);
        tp.BotonMod.SetActive(true);
        tp.Menu.SetActive(false);
        tp.Atras.SetActive(false);
        tp.MenuSuperior.SetActive(false);
        
        
        tp.NomCat.text = NombreCatPago;
        tp.Ico = Icono;
        tp.IcoColor = color;
        tp.botonIcono.sprite = Icono;
        tp.iconColor.color = color;
        tp.idColor = idColor;
        tp.idIcono = idIcono;
        tp.ids = posicion - 10;
        tp.CatID = CatPagoID;
        tp.CatIDLocal = CatPagoIDLocal;
        tp.codbar = codbar;
        tp.pos = posicion - 10;
        tp.IP = this;
        if (CBB == true)
        {
            
            tp.CB.isOn = true;
        }
        else
        {
            tp.CB.isOn = false;
        }
        ob.Click("AgregarCategoria");
    }
    public void PopUpEliminar()
    {
        ob.Click("Eliminar");
        ob.EliminarTexto.text = "Seguro desea eliminar la categoria: " + NombreCatPago + ", ya no será posible recuperarla después de eliminarla.";
        ob.EliminarAceptar.onClick.AddListener(Eliminar);
    }
    public void Eliminar()
    {
        ob.Click("EliminarQuitar");
        ob.Click("OpcionesQuitar");
        tp.CatID = CatPagoID;
        tp.CatIDLocal = CatPagoIDLocal;
        tp.pos = posicion;
        tp.IP = this;
        if (CBB == true)
        {
            tp.CB.isOn = true;
        }
        else
        {
            tp.CB.isOn = false;
        }
        tp.Eliminar();

    }
    public void Seleccionar()
    {
        if (casas.posicion > 0)
        {
			ap.botonph.SetActive (true);
            ap.modp = false;
            ob.Click("OpcionesQuitar");
            //NCP.text = NombreCatPago;
            ap.iconColor.color = color;
            ap.IC = this;
            ap.botonIcono.sprite = Icono;
            ap.NombreCategoriaString = NombreCatPago;
            if (personalizada == 1)
            {
                ap.PagPer = 1;
                ap.idColor = idColor;
                ap.idIcono = idIcono;
            }
            if (personalizada == 0)
            {
                ap.PagPer = 0;
                ap.idColor = 1;
                ap.idIcono = idPP;
            }
            ap.idCat = CatPagoID;
            if (codbar == 1)
            {
                ap.Benef.SetActive(false);
                ap.CB.SetActive(true);
                //ap.BPG.SetActive(true);
                //ap.BG.SetActive(false);
            }
            else if (codbar == 0)
            {
                ap.CB.SetActive(false);
                ap.Benef.SetActive(true);
                //ap.BPG.SetActive(false);
                //ap.BG.SetActive(true);
            }

            ap.dias = ap.theDay;
            ap.meses = ap.theMonth;
            ap.años = ap.theYear;

            ap.lastCadaCuando = ap.CadaCuando;
            ap.lastNotas = ap.NotaPago.text;
            ap.lastRecordarPago = ap.RecordPago;
            ap.lastRepetirCadaCuandoText = ap.RepetirCadaTexto.text;
            ap.lastAlertaAntesText = ap.AlertasTexto.text;
            ap.lastAlertasAntes = ap.AlertaAntes;

            ob.Click("AgregarPago");
        }
        else
        {
            ob.DesError.text = "Para poder agregar un pago, primero debes agregar una casa";
            ob.Click("Errors");
        }
    }
}
