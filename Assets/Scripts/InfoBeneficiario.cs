﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class InfoBeneficiario : MonoBehaviour {
    public int BeneficiarioID, posicion, BeneficiarioIDLocal;
    public string NombreBeneficiario, referBenef, correoBenef, clabeBenef, telefBenef;
    public Text NoBen;
    public Image Icon, IconColor;
    //public GameObject Historial;
    public ObjectManager ob;
    public AP ap;
    public Beneficiario ben;
    public bool X;
    public Fecha Date;

	// Use this for initialization
	void Start () {
        //Icono = Icon.sprite;
        //GetComponent<Button>().onClick.AddListener(Click);
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    public void Click()
    {
        ob.BenSeleccionar.onClick.RemoveAllListeners();
        ob.BenSeleccionar.onClick.AddListener(Seleccionar);

        ob.BenEditar.onClick.RemoveAllListeners();
        ob.BenEditar.onClick.AddListener(Editar);

        ob.BenEliminar.onClick.RemoveAllListeners();
        ob.BenEliminar.onClick.AddListener(PopUpEliminar);

        ob.Click("OpcionesBen");
    }
    public void Editar()
    {

        ob.Click("OpcionesBenQuitar");
        ben.BeneGuardar.SetActive(false);
        ben.BeneModificar.SetActive(true);
        ob.Click("AgregarBeneficiario");
        ben.NB.text = NombreBeneficiario;
        ben.RB.text = referBenef;
        ben.CoB.text = correoBenef;
        ben.CB.text = clabeBenef;
        ben.TB.text = telefBenef;
        ben.benID = BeneficiarioID;
        ben.IDBenLocal = BeneficiarioIDLocal;
        ben.ids = posicion;
        ben.IB = this;
        
    }
    public void PopUpEliminar()
    {
        ob.Click("Eliminar");
        ob.EliminarTexto.text = "Seguro desea eliminar el beneficiario: " + NombreBeneficiario + ", ya no será posible recuperarla después de eliminarlo.";
        ob.EliminarAceptar.onClick.AddListener(Eliminar);
    }

    public void Eliminar()
    {
        ob.Click("EliminarQuitar");
        ob.Click("OpcionesBenQuitar");
        ob.Click("BeneficiarioQuitar");
        ap.Beneficiario.text = "Beneficiario";
        ben.benID = BeneficiarioID;
        ben.IDBenLocal = BeneficiarioIDLocal;
        ben.ids = posicion;
        ben.Eliminar();

        

    }
    public void Seleccionar()
    {
        ob.Click("OpcionesBenQuitar");
        ap.Beneficiario.text = "Beneficiario: " + NombreBeneficiario;
        ap.IB = this;
        ap.idBen = BeneficiarioID;
        ob.Click("BeneficiarioQuitar");
    }
}
