﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fecha : MonoBehaviour {
    public Dropdown Año, Año2, Año3, Año4, Año1, DiaP, MesP, AñoP;
    public List<int> Ano, Ano2, Ano3, Ano4, Ano1, AnoP, Dia= new List<int>();
    public int theYear, theYear2, viejo, i, l, m, mes, theYear3, theDay, theMonth, theYearP, j, año;
    public string MesActualElegido, MesAnteriorElegido, AñoActualElegido, AñoAnteriorElegido;
    public Text dias, meses, años;
    public AP ap;
    public bool fecha;
    // Use this for initialization
    void Awake()
    {
        
        theYear = System.DateTime.Now.Year;
        theYear3 = theYear - 100;
        theYear2 = System.DateTime.Now.Year + 9;
        theYearP = System.DateTime.Now.Year;
        theDay = System.DateTime.Now.Day;
        theMonth = System.DateTime.Now.Month;
        dias.text = theDay.ToString();
        DiaP.value = theDay-1;
        años.text = theYearP.ToString();
        AñoP.value = 0;
        mes = theMonth;
        AñoAnteriorElegido = theYear.ToString();
        año = theYear;
        if(mes == 1)
        {
            meses.text = "Enero";
            MesP.value = 0;
            MesAnteriorElegido = "Enero";
			j = 1;
			mes = 31;
			Dia.Clear();

			DiaP.ClearOptions();
			if (fecha == true)
			{
				fecha = false;
				dias.text = ap.IP.Dia;

			}
			else
			{
				dias.text = "1";
			}
			for (int i = 0; i < mes; i++)
			{
				Dia.Add(j);
				DiaP.options.Add(new Dropdown.OptionData { text = Dia[i].ToString() });
				j++;
				DiaP.value = 0;
			}
        }
        if (mes == 2)
        {
            meses.text = "Febrero";
            MesP.value = 1;
            MesAnteriorElegido = "Febrero";
			j = 1;
			if (System.DateTime.IsLeapYear(año) == true)
			{
				mes = 29;
			}
			else
			{
				mes = 28;
			}
			Dia.Clear();

			DiaP.ClearOptions();
			if (fecha == true)
			{
				fecha = false;
				dias.text = ap.IP.Dia;
			}
			else
			{
				dias.text = "1";


			}
			for (int i = 0; i < mes; i++)
			{
				Dia.Add(j);
				DiaP.options.Add(new Dropdown.OptionData { text = Dia[i].ToString() });
				j++;
				DiaP.value = 0;
			}
        }
        if (mes == 3)
        {
            meses.text = "Marzo";
            MesP.value = 2;
            MesAnteriorElegido = "Marzo";
			j = 1;
			mes = 31;
			Dia.Clear();

			DiaP.ClearOptions();
			if (fecha == true)
			{
				fecha = false;
				dias.text = ap.IP.Dia;
			}
			else
			{
				dias.text = "1";

			}
			for (int i = 0; i < mes; i++)
			{
				Dia.Add(j);
				DiaP.options.Add(new Dropdown.OptionData { text = Dia[i].ToString() });
				j++;
				DiaP.value = 0;
			}
        }
        if (mes == 4)
        {
            meses.text = "Abril";
            MesP.value = 3;
            MesAnteriorElegido = "Abril";
			j = 1;
			mes = 30;
			Dia.Clear();

			DiaP.ClearOptions();
			if (fecha == true)
			{
				fecha = false;
				dias.text = ap.IP.Dia;
			}
			else
			{
				dias.text = "1";

			}
			for (int i = 0; i < mes; i++)
			{
				Dia.Add(j);
				DiaP.options.Add(new Dropdown.OptionData { text = Dia[i].ToString() });
				j++;
				DiaP.value = 0;
			}
        }
        if (mes == 5)
        {
            meses.text = "Mayo";
            MesP.value = 4;
            MesAnteriorElegido = "Mayo";
			j = 1;
			mes = 31;
			Dia.Clear();

			DiaP.ClearOptions();
			if (fecha == true)
			{
				fecha = false;
				dias.text = ap.IP.Dia;
			}
			else
			{
				dias.text = "1";

			}
			for (int i = 0; i < mes; i++)
			{
				Dia.Add(j);
				DiaP.options.Add(new Dropdown.OptionData { text = Dia[i].ToString() });
				j++;
				DiaP.value = 0;
			}
        }
        if (mes == 6)
        {
            meses.text = "Junio";
            MesP.value = 5;
            MesAnteriorElegido = "Junio";
			j = 1;
			mes = 30;
			Dia.Clear();

			DiaP.ClearOptions();
			if (fecha == true)
			{
				fecha = false;
				dias.text = ap.IP.Dia;
			}
			else
			{
				dias.text = "1";

			}
			for (int i = 0; i < mes; i++)
			{
				Dia.Add(j);
				DiaP.options.Add(new Dropdown.OptionData { text = Dia[i].ToString() });
				j++;
				DiaP.value = 0;
			}
        }
        if (mes == 7)
        {
            meses.text = "Julio";
            MesP.value = 6;
            MesAnteriorElegido = "Julio";
			j = 1;
			mes = 31;
			Dia.Clear();

			DiaP.ClearOptions();
			if (fecha == true)
			{
				fecha = false;
				dias.text = ap.IP.Dia;
			}
			else
			{
				dias.text = "1";

			}
			for (int i = 0; i < mes; i++)
			{
				Dia.Add(j);
				DiaP.options.Add(new Dropdown.OptionData { text = Dia[i].ToString() });
				j++;
				DiaP.value = 0;
			}
        }
        if (mes == 8)
        {
            meses.text = "Agosto";
            MesP.value = 7;
            MesAnteriorElegido = "Agosto";
			j = 1;
			mes = 31;
			Dia.Clear();

			DiaP.ClearOptions();
			if (fecha == true)
			{
				fecha = false;
				dias.text = ap.IP.Dia;
			}
			else
			{
				dias.text = "1";
			}
			for (int i = 0; i < mes; i++)
			{
				Dia.Add(j);
				DiaP.options.Add(new Dropdown.OptionData { text = Dia[i].ToString() });
				j++;
				DiaP.value = 0;
			}
        }
        if (mes == 9)
        {
            meses.text = "Septiembre";
            MesP.value = 8;
            MesAnteriorElegido = "Septiembre";
			j = 1;
			mes = 30;
			Dia.Clear();

			DiaP.ClearOptions();
			if (fecha == true)
			{
				fecha = false;
				//dias.text = ap.IP.Dia;
			}
			else
			{
				dias.text = "1";

			}
			for (int i = 0; i < mes; i++)
			{
				Dia.Add(j);
				DiaP.options.Add(new Dropdown.OptionData { text = Dia[i].ToString() });
				j++;
				DiaP.value = 0;
			}
        }
        if (mes == 10)
        {
            meses.text = "Octubre";
            MesP.value = 9;
            MesAnteriorElegido = "Octubre";
			j = 1;
			mes = 31;
			Dia.Clear();

			DiaP.ClearOptions();
			if (fecha == true)
			{
				fecha = false;
				dias.text = ap.IP.Dia;
			}
			else
			{
				dias.text = "1";

			}
			for (int i = 0; i < mes; i++)
			{
				Dia.Add(j);
				DiaP.options.Add(new Dropdown.OptionData { text = Dia[i].ToString() });
				j++;
				DiaP.value = 0;
			}
        }
        if (mes == 11)
        {
            meses.text = "Noviembre";
            MesP.value = 10;
            MesAnteriorElegido = "Noviembre";
			j = 1;
			mes = 30;
			Dia.Clear();

			DiaP.ClearOptions();
			if (fecha == true)
			{
				fecha = false;
				dias.text = ap.IP.Dia;
			}
			else
			{
				dias.text = "1";

			}
			for (int i = 0; i < mes; i++)
			{
				Dia.Add(j);
				DiaP.options.Add(new Dropdown.OptionData { text = Dia[i].ToString() });
				j++;
				DiaP.value = 0;
			}
        }
        if (mes == 12)
        {
            meses.text = "Diciembre";
            MesP.value = 11;
            MesAnteriorElegido = "Diciembre";
			j = 1;
			mes = 31;
			Dia.Clear();

			DiaP.ClearOptions();
			if (fecha == true)
			{
				fecha = false;
				dias.text = ap.IP.Dia;
			}
			else
			{
				dias.text = "1";

			}
			for (int i = 0; i < mes; i++)
			{
				Dia.Add(j);
				DiaP.options.Add(new Dropdown.OptionData { text = Dia[i].ToString() });
				j++;
				DiaP.value = 0;
			}
        }

        for (viejo = theYear - 10;  viejo > theYear3; viejo--)
        {
            Ano.Add(viejo);
            Ano1.Add(viejo);
            Año.options.Add(new Dropdown.OptionData { text = Ano[i].ToString()});
            Año1.options.Add(new Dropdown.OptionData { text = Ano1[i].ToString() });
            i++;
        }
        for (int viejo2 = theYear; viejo2 < theYear2; viejo2++)
        {
            Ano2.Add(viejo2);
            Ano3.Add(viejo2);
            Ano4.Add(viejo2);
            AnoP.Add(viejo2);
            Año2.options.Add(new Dropdown.OptionData { text = Ano2[l].ToString() });
            Año3.options.Add(new Dropdown.OptionData { text = Ano3[l].ToString() });
            Año4.options.Add(new Dropdown.OptionData { text = Ano4[l].ToString() });
            AñoP.options.Add(new Dropdown.OptionData { text = AnoP[l].ToString() });
            l++;
        }
        //for (int viejo3 = 0; viejo < 31; viejo3++)
        //{
        //    Dia.Add(viejo3);
        //    DiaP.options.Add(new Dropdown.OptionData { text = Dia[m].ToString() });
        //    m++;
        //}
    }
    void Start () {
		
	}

    // Update is called once per frame
    public void ChangeDate()
    {
        año = Int32.Parse(años.text);
        MesActualElegido = meses.text;
        AñoActualElegido = años.text;
        if (MesActualElegido != MesAnteriorElegido || AñoActualElegido!=AñoAnteriorElegido)
        {
            MesAnteriorElegido = meses.text;
            AñoAnteriorElegido = años.text;
            if (meses.text == "Enero")
            {

                j = 1;
                mes = 31;
                Dia.Clear();
                
                DiaP.ClearOptions();
                if (fecha == true)
                {
                    fecha = false;
                    dias.text = ap.IP.Dia;
                    
                }
                else
                {
                    dias.text = "1";
                }
                for (int i = 0; i < mes; i++)
                {
                    Dia.Add(j);
                    DiaP.options.Add(new Dropdown.OptionData { text = Dia[i].ToString() });
                    j++;
                    DiaP.value = 0;
                }
            }
            if (meses.text == "Febrero")
            {
                j = 1;
                if (System.DateTime.IsLeapYear(año) == true)
                {
                    mes = 29;
                }
                else
                {
                    mes = 28;
                }
                Dia.Clear();
                
                DiaP.ClearOptions();
                if (fecha == true)
                {
                    fecha = false;
                    dias.text = ap.IP.Dia;
                }
                else
                {
                    dias.text = "1";
                    

                }
                for (int i = 0; i < mes; i++)
                {
                    Dia.Add(j);
                    DiaP.options.Add(new Dropdown.OptionData { text = Dia[i].ToString() });
                    j++;
                    DiaP.value = 0;
                }
            }
            if (meses.text == "Marzo")
            {
                j = 1;
                mes = 31;
                Dia.Clear();
                
                DiaP.ClearOptions();
                if (fecha == true)
                {
                    fecha = false;
                    dias.text = ap.IP.Dia;
                }
                else
                {
                    dias.text = "1";
                    
                }
                for (int i = 0; i < mes; i++)
                {
                    Dia.Add(j);
                    DiaP.options.Add(new Dropdown.OptionData { text = Dia[i].ToString() });
                    j++;
                    DiaP.value = 0;
                }
            }
            if (meses.text == "Abril")
            {
                j = 1;
                mes = 30;
                Dia.Clear();
                
                DiaP.ClearOptions();
                if (fecha == true)
                {
                    fecha = false;
                    dias.text = ap.IP.Dia;
                }
                else
                {
                    dias.text = "1";
                    
                }
                for (int i = 0; i < mes; i++)
                {
                    Dia.Add(j);
                    DiaP.options.Add(new Dropdown.OptionData { text = Dia[i].ToString() });
                    j++;
                    DiaP.value = 0;
                }
            }
            if (meses.text == "Mayo")
            {
                j = 1;
                mes = 31;
                Dia.Clear();
                
                DiaP.ClearOptions();
                if (fecha == true)
                {
                    fecha = false;
                    dias.text = ap.IP.Dia;
                }
                else
                {
                    dias.text = "1";
                    
                }
                for (int i = 0; i < mes; i++)
                {
                    Dia.Add(j);
                    DiaP.options.Add(new Dropdown.OptionData { text = Dia[i].ToString() });
                    j++;
                    DiaP.value = 0;
                }
            }
            if (meses.text == "Junio")
            {
                j = 1;
                mes = 30;
                Dia.Clear();
                
                DiaP.ClearOptions();
                if (fecha == true)
                {
                    fecha = false;
                    dias.text = ap.IP.Dia;
                }
                else
                {
                    dias.text = "1";
                    
                }
                for (int i = 0; i < mes; i++)
                {
                    Dia.Add(j);
                    DiaP.options.Add(new Dropdown.OptionData { text = Dia[i].ToString() });
                    j++;
                    DiaP.value = 0;
                }
            }
            if (meses.text == "Julio")
            {
                j = 1;
                mes = 31;
                Dia.Clear();
                
                DiaP.ClearOptions();
                if (fecha == true)
                {
                    fecha = false;
                    dias.text = ap.IP.Dia;
                }
                else
                {
                    dias.text = "1";
                    
                }
                for (int i = 0; i < mes; i++)
                {
                    Dia.Add(j);
                    DiaP.options.Add(new Dropdown.OptionData { text = Dia[i].ToString() });
                    j++;
                    DiaP.value = 0;
                }
            }
            if (meses.text == "Agosto")
            {
                j = 1;
                mes = 31;
                Dia.Clear();
                
                DiaP.ClearOptions();
                if (fecha == true)
                {
                    fecha = false;
                    dias.text = ap.IP.Dia;
                }
                else
                {
                    dias.text = "1";
                }
                for (int i = 0; i < mes; i++)
                {
                    Dia.Add(j);
                    DiaP.options.Add(new Dropdown.OptionData { text = Dia[i].ToString() });
                    j++;
                    DiaP.value = 0;
                }
            }
            if (meses.text == "Septiembre")
            {
                j = 1;
                mes = 30;
                Dia.Clear();
                
                DiaP.ClearOptions();
                if (fecha == true)
                {
                    fecha = false;
                    //dias.text = ap.IP.Dia;
                }
                else
                {
                    dias.text = "1";
                    
                }
                for (int i = 0; i < mes; i++)
                {
                    Dia.Add(j);
                    DiaP.options.Add(new Dropdown.OptionData { text = Dia[i].ToString() });
                    j++;
                    DiaP.value = 0;
                }
            }
            if (meses.text == "Octubre")
            {
                j = 1;
                mes = 31;
                Dia.Clear();
                
                DiaP.ClearOptions();
                if (fecha == true)
                {
                    fecha = false;
                    dias.text = ap.IP.Dia;
                }
                else
                {
                    dias.text = "1";
                    
                }
                for (int i = 0; i < mes; i++)
                {
                    Dia.Add(j);
                    DiaP.options.Add(new Dropdown.OptionData { text = Dia[i].ToString() });
                    j++;
                    DiaP.value = 0;
                }
            }
            if (meses.text == "Noviembre")
            {
                j = 1;
                mes = 30;
                Dia.Clear();
                
                DiaP.ClearOptions();
                if (fecha == true)
                {
                    fecha = false;
                    dias.text = ap.IP.Dia;
                }
                else
                {
                    dias.text = "1";
                    
                }
                for (int i = 0; i < mes; i++)
                {
                    Dia.Add(j);
                    DiaP.options.Add(new Dropdown.OptionData { text = Dia[i].ToString() });
                    j++;
                    DiaP.value = 0;
                }
            }
            if (meses.text == "Diciembre")
            {
                j = 1;
                mes = 31;
                Dia.Clear();
                
                DiaP.ClearOptions();
                if (fecha == true)
                {
                    fecha = false;
                    dias.text = ap.IP.Dia;
                }
                else
                {
                    dias.text = "1";
                    
                }
                for (int i = 0; i < mes; i++)
                {
                    Dia.Add(j);
                    DiaP.options.Add(new Dropdown.OptionData { text = Dia[i].ToString() });
                    j++;
                    DiaP.value = 0;
                }
            }

        }
    }
}
