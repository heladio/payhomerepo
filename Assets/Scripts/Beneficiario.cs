﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Beneficiario : MonoBehaviour
{
    public ObjectManager ob;
    //public List<string> NNC, UbC, CodC = new List<string>();
    //public List<int> ID = new List<int>();
    /*public List<GameObject> CatPagos = new List<GameObject>(); *//*Casass2, Historial = new List<GameObject>();*/
    public int i = 1, b = 1, ids, IDBenLocal;
    public GameObject beneficiario, beneficiariox, beneficiarioclone, beneficiarios, beneficiariot, move, content;
    public Vector2 G, G2;
    public InfoBeneficiario IB, IB2;
    public AP ap;
    public int benID, h, c, posicion, y, intOff;
    public float s = 0, d, pa, TimerRef;
    public ScrollRect scroll;
    public InputField NB, RB, CoB, BID, CB, TB;
    public string NomB, RefB, CorB, BenID, ClabeB, Telefono, error, BeneficiarioAgregarOff, BeneficiarioModOff, BeneficiarioElimOff, BeneficiarioOff;
    public InputField NomBen;
    public List<int> benefID, Posicion, benefIDLocal = new List<int>();
    public List<string> NombreBenef, referBenef, correoBenef, clabeBenef, telefBenef = new List<string>();
    public List<GameObject> Beneficiarios = new List<GameObject>();
    public Casas casas;
    public Vector2 scrollOriginal, LimiteSuperior, LimiteInferior, PrimerTP, UltimoTP, posicioncontent, scrollinferior, contentActual, Ward, Ward2;
    public Swipe sw;
    public GameObject BeneGuardar, BeneModificar, mover;
    public bool bajo, norepetido, RefBool;
    public GameObject limsup, liminf, Refrescar, circulo;
    public Registro reg;
    public List<DBBeneficiario> Benef = new List<DBBeneficiario>();
    public DBBeneficiario beneficiarote;

    public List<int> benefIDLocalAgregar, benefIDMod, benefIDEliminar = new List<int>();
    public List<string> NombreBenefAgregar, referBenefAgregar, correoBenefAgregar, clabeBenefAgregar, telefBenefAgregar = new List<string>();
    public List<string> NombreBenefMod, referBenefMod, correoBenefMod, clabeBenefMod, telefBenefMod = new List<string>();
    public string[] BenOff, BenAgregarOff, BenModOff, BenElimOff, escribirOff, escribirAgregarOff, escribirModOff, escribirElimOff;
    Char delimiter = ';';
    Char del = ',';

    public void Off()
    {
        BeneficiarioOff = PlayerPrefs.GetString("Beneficiario");
        BenOff = BeneficiarioOff.Split(delimiter);
        for(int yus = 0; yus<BenOff.Length; yus++)
        {
            escribirOff = null;
            escribirOff = BenOff[yus].Split(del);
            if(escribirOff[0] != "")
            {
                Int32.TryParse(escribirOff[0], out intOff);
                i = intOff;
                benefIDLocal.Add(intOff);
                Int32.TryParse(escribirOff[1], out intOff);
                benefID.Add(intOff);
                NombreBenef.Add(escribirOff[2]);
                referBenef.Add(escribirOff[3]);
                correoBenef.Add(escribirOff[4]);
                clabeBenef.Add(escribirOff[5]);
                telefBenef.Add(escribirOff[6]);
                Posicion.Add(posicion);
                posicion++;
                i++;
                X();
                
            }
            
        }
        BeneficiarioAgregarOff = PlayerPrefs.GetString("BeneficiarioAgregar");
        BenAgregarOff = BeneficiarioAgregarOff.Split(delimiter);
        for (int yus = 0; yus < BenAgregarOff.Length; yus++)
        {
            escribirAgregarOff= null;
            escribirAgregarOff = BenAgregarOff[yus].Split(del);
            if (escribirAgregarOff[0] != "")
            {
                Int32.TryParse(escribirAgregarOff[0], out intOff);
                benefIDLocalAgregar.Add(intOff);
                NombreBenefAgregar.Add(escribirAgregarOff[1]);
                referBenefAgregar.Add(escribirAgregarOff[2]);
                correoBenefAgregar.Add(escribirAgregarOff[3]);
                clabeBenefAgregar.Add(escribirAgregarOff[4]);
                telefBenefAgregar.Add(escribirAgregarOff[5]);
            }
        }
        if(benefIDLocalAgregar.Count != 0)
        {
            AgregarBeneficiario();
        }
        BeneficiarioModOff = PlayerPrefs.GetString("BeneficiarioMod");
        BenModOff = BeneficiarioModOff.Split(delimiter);
        for (int yus = 0; yus < BenModOff.Length; yus++)
        {
            escribirModOff = null;
            escribirModOff = BenModOff[yus].Split(del);
            if (escribirModOff[0] != "")
            {
                Int32.TryParse(escribirModOff[0], out intOff);
                benefIDMod.Add(intOff);
                NombreBenefMod.Add(escribirModOff[1]);
                referBenefMod.Add(escribirModOff[2]);
                correoBenefMod.Add(escribirModOff[3]);
                clabeBenefMod.Add(escribirModOff[4]);
                telefBenefMod.Add(escribirModOff[5]);
            }
        }
        if (benefIDMod.Count != 0)
        {
            ModBeneficiario();
        }
        BeneficiarioElimOff = PlayerPrefs.GetString("BeneficiarioElim");
        BenElimOff = BeneficiarioModOff.Split(delimiter);
        for (int yus = 0; yus < BenElimOff.Length; yus++)
        {
            if (BenElimOff[0] != "0")
            {
                Int32.TryParse(BenElimOff[0], out intOff);
                benefIDEliminar.Add(intOff);
            }
        }
        if (benefIDEliminar.Count != 0)
        {
            EliminarBeneficiario();
        }
    }
    // Use this for initialization
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {
        if (content.transform.localPosition.y < -5)
        {
            RefBool = true;
            Refrescar.SetActive(true);
        }
        else if (content.transform.localPosition.y > -5)
        {
            RefBool = false;
            Refrescar.SetActive(false);
        }
        if (RefBool == true)
        {
            TimerRef += Time.fixedUnscaledDeltaTime;
            pa += 8;
            Quaternion target = Quaternion.Euler(180, 180, pa);
            circulo.transform.rotation = Quaternion.Slerp(transform.rotation, target, pa);
        }
        else
        {
            TimerRef = 0;
        }
        if (TimerRef > 0.8 && benefIDLocalAgregar.Count == 0 && benefIDMod.Count == 0 && benefIDEliminar.Count == 0  && Application.internetReachability != NetworkReachability.NotReachable)
        {
            //ob.Click("CasaPagosHUB");
            reg.carga = true;
            EliminarServ();
            ReadCardBeneficiario();
        }
    }
    public void Guardar()
    {
        if (NB.text != "" && CoB.text != "" && CB.text != "" && TB.text != "")
        {
            i++;
            //benID = i;
            NombreBenef.Add(NB.text);
            referBenef.Add(RB.text);
            correoBenef.Add(CoB.text);
            benefIDLocal.Add(i);
            benefID.Add(benID);
            clabeBenef.Add(CB.text);
            telefBenef.Add(TB.text);
            Posicion.Add(posicion);
            posicion++;
            benefIDLocalAgregar.Add(i);
            NombreBenefAgregar.Add(NB.text);
            referBenefAgregar.Add(RB.text);
            correoBenefAgregar.Add(CoB.text);
            clabeBenefAgregar.Add(CB.text);
            telefBenefAgregar.Add(TB.text);

            BeneficiarioOff = null;
            for (int kl = 0; kl < benefID.Count; kl++)
            {
                if (kl == 0)
                {
                    BeneficiarioOff = benefIDLocal[kl] + "," + benefID[kl] + "," + NombreBenef[kl] + "," + referBenef[kl] + "," + correoBenef[kl] + "," + clabeBenef[kl] + "," + telefBenef[kl];
                }
                else if (kl != 0)
                {
                    BeneficiarioOff += ";" + benefIDLocal[kl] + "," + benefID[kl] + "," + NombreBenef[kl] + "," + referBenef[kl] + "," + correoBenef[kl] + "," + clabeBenef[kl] + "," + telefBenef[kl];
                }
            }
            PlayerPrefs.SetString("Beneficiario", BeneficiarioOff);



            BeneficiarioAgregarOff = null;
                for (int ko = 0; benefIDLocalAgregar.Count > ko; ko++)
                {
                    if (ko == 0)
                    {
                        BeneficiarioAgregarOff = benefIDLocalAgregar[ko] + "," + NombreBenefAgregar[ko] + "," + referBenefAgregar[ko] + "," + correoBenefAgregar[ko] + "," +
                            clabeBenefAgregar[ko] + "," + telefBenefAgregar[ko];
                    }
                    else if (ko != 0)
                    {
                        BeneficiarioAgregarOff += ";" + benefIDLocalAgregar[ko] + "," + NombreBenefAgregar[ko] + "," + referBenefAgregar[ko] + "," + correoBenefAgregar[ko] + "," +
                            clabeBenefAgregar[ko] + "," + telefBenefAgregar[ko];
                    }
                }
            PlayerPrefs.SetString("BeneficiarioAgregar", BeneficiarioAgregarOff);
            AgregarBeneficiario();
            X();
            //AgregarBeneficiario();
            ob.Click("AgregarBeneficiarioQuitar");
        }
        else
        {
            ob.DesError.text = "Se necesita llenar todos los campos para poder agregar a esté beneficiario";
            ob.Click("Errors");
        }
    }

    public void X()
    {
        G = content.transform.position;

        //G2 = casax2.transform.position;
        Instantiate(beneficiario, G, Quaternion.identity, beneficiarios.transform);
        //Instantiate(casa2, G2, Quaternion.identity, casas2.transform);
        //Instantiate(HistorialCasa, G2, Quaternion.identity, HistorialCasa.transform);
        beneficiarioclone = GameObject.Find("Beneficiariox(Clone)");
        beneficiarioclone.name = "Beneficiario" + i;
        //casaclone2 = GameObject.Find("CasaMHx(Clone)");
        //casaclone2.name = "CasaMH" + i;

        //HistorialClone = GameObject.Find("HistorialCasaX(Clone)");
        //HistorialClone.name = "HistorialCasa" + i;
        IB = beneficiarioclone.GetComponent<InfoBeneficiario>();
        //InfoMH = casaclone2.GetComponent<InfoCasaMH>();
        Beneficiarios.Add(beneficiarioclone);
        //Casass2.Add(casaclone2);
        //Historial.Add(HistorialClone);
        IB.NombreBeneficiario = NombreBenef[posicion - 1];
        IB.NoBen.text = IB.NombreBeneficiario;
        IB.clabeBenef = clabeBenef[posicion - 1];
        IB.telefBenef = telefBenef[posicion - 1];
        IB.referBenef = referBenef[posicion - 1];
        IB.correoBenef = correoBenef[posicion - 1];
        IB.posicion = Posicion[posicion - 1];
        IB.BeneficiarioID = benefID[posicion - 1];
        IB.BeneficiarioIDLocal = benefIDLocal[posicion - 1];
        IB.ben = this;
        IB.ob = ob;
        IB.ap = ap;
        IB.ben = this;
        beneficiarioclone = null;

    }
    public void Modificar()
    {
        if (NB.text != "" && CoB.text != "" && CB.text != "" && TB.text != "")

        {
            IB.NombreBeneficiario = NB.text;
            IB.referBenef = RB.text;
            IB.correoBenef = CoB.text;
            IB.clabeBenef = CB.text;
            IB.telefBenef = TB.text;
            NombreBenef[ids] = NB.text;
            referBenef[ids] = RB.text;
            correoBenef[ids] = CoB.text;
            clabeBenef[ids] = CB.text;
            telefBenef[ids] = TB.text;
            IB.NoBen.text = NB.text;
            BeneficiarioOff = null;
            for (int kl = 0; kl < benefID.Count; kl++)
            {
                if (kl == 0)
                {
                    BeneficiarioOff = benefIDLocal[kl] + "," + benefID[kl] + "," + NombreBenef[kl] + "," + referBenef[kl] + "," + correoBenef[kl] + "," + clabeBenef[kl] + "," + telefBenef[kl];
                }
                else if (kl != 0)
                {
                    BeneficiarioOff += ";" + benefIDLocal[kl] + "," + benefID[kl] + "," + NombreBenef[kl] + "," + referBenef[kl] + "," + correoBenef[kl] + "," + clabeBenef[kl] + "," + telefBenef[kl];
                }
            }
            PlayerPrefs.SetString("Beneficiario", BeneficiarioOff);

            if (benID == 0)
            {
                for(int hu = 0; hu<benefIDLocalAgregar.Count; hu++)
                {
                    if(IDBenLocal == benefIDLocalAgregar[hu])
                    {
                        NombreBenefAgregar[hu] = NB.text;
                        referBenefAgregar[hu] = RB.text;
                        correoBenefAgregar[hu] = CoB.text;
                        clabeBenefAgregar[hu] = CB.text;
                        telefBenefAgregar[hu] = TB.text;
                    }
                }
                BeneficiarioAgregarOff = null;
                for (int ko = 0; benefIDLocalAgregar.Count > ko; ko++)
                {
                    if (ko == 0)
                    {
                        BeneficiarioAgregarOff = benefIDLocalAgregar[ko] + "," + NombreBenefAgregar[ko] + "," + referBenefAgregar[ko] + "," + correoBenefAgregar[ko] + "," +
                            clabeBenefAgregar[ko] + "," + telefBenefAgregar[ko];
                    }
                    else if (ko != 0)
                    {
                        BeneficiarioAgregarOff += ";" + benefIDLocalAgregar[ko] + "," + NombreBenefAgregar[ko] + "," + referBenefAgregar[ko] + "," + correoBenefAgregar[ko] + "," +
                            clabeBenefAgregar[ko] + "," + telefBenefAgregar[ko];
                    }
                }
                PlayerPrefs.SetString("BeneficiarioAgregar", BeneficiarioAgregarOff);
            }
            else if(benID != 0)
            {
                benefIDMod.Add(benID);
                NombreBenefMod.Add(NB.text);
                referBenefMod.Add(RB.text);
                correoBenefMod.Add(CoB.text);
                clabeBenefMod.Add(CB.text);
                telefBenefMod.Add(TB.text);
                BeneficiarioModOff = null;
                for(int hus = 0; hus<benefIDMod.Count; hus++)
                {
                    if(hus == 0)
                    {
                        BeneficiarioModOff = benefIDMod[hus] + "," + NombreBenefMod[hus] + "," + referBenefMod[hus] + "," + correoBenefMod[hus] + "," + clabeBenefMod[hus] + "," + telefBenefMod[hus];
                    }
                    else if (hus != 0)
                    {
                        BeneficiarioModOff += ";" + benefIDMod[hus] + "," + NombreBenefMod[hus] + "," + referBenefMod[hus] + "," + correoBenefMod[hus] + "," + clabeBenefMod[hus] + "," + telefBenefMod[hus];
                    }
                }
                PlayerPrefs.SetString("BeneficiarioMod", BeneficiarioModOff);
                if(benefIDMod.Count != 0)
                ModBeneficiario();
            }
            IB.X = false;
            Quitar();




            //ModBeneficiario();
        }
        else
        {
            ob.DesError.text = "Se necesita llenar todos los campos para poder agregar a esté beneficiario";
            ob.Click("Errors");
        }
    }
    public void Quitar()
    {
        ob.Click("AgregarBeneficiarioQuitar");
        ob.Click("Beneficiario");
        NB.text = null;
        RB.text = null;
        CoB.text = null;
        TB.text = null;
        CB.text = null;
        IB.X = false;
        benID = 0;
        BeneGuardar.SetActive(true);
        BeneModificar.SetActive(false);
        IB = null;
    }



    public void Eliminar()
    {
        beneficiariot = Beneficiarios[ids];
        c = ids - 1;
        //if(h == 0)
        //{
        //    fp.s = 0;
        //}

        //if (c == fp.i && fp.i == 0)
        //{
        //    fp.s = fp.tarjetax.transform.position.y;
        //    i++;
        //}

        for (int d = ids; d < posicion; d++)
        {
            c++;
            Posicion[c]--;
            mover = Beneficiarios[c];
            IB2 = mover.GetComponent<InfoBeneficiario>();
            IB2.posicion--;

        }

        Destroy(beneficiariot);
        benefID.RemoveAt(ids);
        benefIDLocal.RemoveAt(ids);
        Beneficiarios.RemoveAt(ids);
        NombreBenef.RemoveAt(ids);
        referBenef.RemoveAt(ids);
        correoBenef.RemoveAt(ids);
        clabeBenef.RemoveAt(ids);
        telefBenef.RemoveAt(ids);
        Posicion.RemoveAt(ids);
        posicion--;
        BeneficiarioOff = null;
        for (int kl = 0; kl < benefID.Count; kl++)
        {
            if (kl == 0)
            {
                BeneficiarioOff = benefIDLocal[kl] + "," + benefID[kl] + "," + NombreBenef[kl] + "," + referBenef[kl] + "," + correoBenef[kl] + "," + clabeBenef[kl] + "," + telefBenef[kl];
            }
            else if (kl != 0)
            {
                BeneficiarioOff += ";" + benefIDLocal[kl] + "," + benefID[kl] + "," + NombreBenef[kl] + "," + referBenef[kl] + "," + correoBenef[kl] + "," + clabeBenef[kl] + "," + telefBenef[kl];
            }
        }
        PlayerPrefs.SetString("Beneficiario", BeneficiarioOff);
        if (benID == 0)
        {
            for(int ggg = 0; ggg<benefIDLocalAgregar.Count; ggg++)
            {
                if(IDBenLocal == benefIDLocalAgregar[ggg])
                {
                    benefIDLocalAgregar.RemoveAt(ggg);
                    NombreBenefAgregar.RemoveAt(ggg);
                    referBenefAgregar.RemoveAt(ggg);
                    correoBenefAgregar.RemoveAt(ggg);
                    clabeBenefAgregar.RemoveAt(ggg);
                    telefBenefAgregar.RemoveAt(ggg);
                }
            }
            BeneficiarioAgregarOff = null;
            for (int ko = 0; benefIDLocalAgregar.Count > ko; ko++)
            {
                if (ko == 0)
                {
                    BeneficiarioAgregarOff = benefIDLocalAgregar[ko] + "," + NombreBenefAgregar[ko] + "," + referBenefAgregar[ko] + "," + correoBenefAgregar[ko] + "," +
                        clabeBenefAgregar[ko] + "," + telefBenefAgregar[ko];
                }
                else if (ko != 0)
                {
                    BeneficiarioAgregarOff += ";" + benefIDLocalAgregar[ko] + "," + NombreBenefAgregar[ko] + "," + referBenefAgregar[ko] + "," + correoBenefAgregar[ko] + "," +
                        clabeBenefAgregar[ko] + "," + telefBenefAgregar[ko];
                }
            }
            PlayerPrefs.SetString("BeneficiarioAgregar", BeneficiarioAgregarOff);
        }
        else if(benID != 0)
        {
            benefIDEliminar.Add(benID);
            BeneficiarioElimOff = null;
            for(int ku = 0; ku<benefIDEliminar.Count; ku++)
            {
                if (ku == 0)
                {
                    BeneficiarioElimOff = benefIDEliminar[ku] + ";";
                }
                else if(ku != 0)
                {
                    BeneficiarioElimOff += benefIDEliminar[ku] + ";";
                }
            }
            PlayerPrefs.SetString("BeneficiarioElim", BeneficiarioElimOff);
            EliminarBeneficiario();
        }

        b--;
    }
    public void EliminarServ()
    {
        c = Beneficiarios.Count;
        for (int yi = 0; yi < c; yi++)
        {
            ids = 0;
            beneficiariot = Beneficiarios[ids];
            //c = ids - 1;
            //if(h == 0)
            //{
            //    fp.s = 0;
            //}

            //if (c == fp.i && fp.i == 0)
            //{
            //    fp.s = fp.tarjetax.transform.position.y;
            //    i++;
            //}

            Destroy(beneficiariot);
            benefID.RemoveAt(ids);
            benefIDLocal.RemoveAt(ids);
            Beneficiarios.RemoveAt(ids);
            NombreBenef.RemoveAt(ids);
            referBenef.RemoveAt(ids);
            correoBenef.RemoveAt(ids);
            clabeBenef.RemoveAt(ids);
            telefBenef.RemoveAt(ids);
            Posicion.RemoveAt(ids);
            posicion = 0;
        }
    }
    public void AgregarBeneficiario()
    {
        if (NombreBenefAgregar.Count != 0)
        {
            WWWForm Form = new WWWForm();
            Form.AddField("IdUsuario", reg.id);
            Form.AddField("NombreBeneficiario", NombreBenefAgregar[0]);
            Form.AddField("CLABEBeneficiario", clabeBenefAgregar[0]);
            Form.AddField("ReferenciaBeneficiario", "0");
            Form.AddField("EmailBeneficiario", correoBenefAgregar[0]);
            Form.AddField("TelefonoBeneficiario", telefBenefAgregar[0]);
            WebServices.AgregarBenef(Form, SuccessAgregarBeneficiario, ErrorAgregarBenef);
        }
        //ob.DesError.text = "No se pudo agregar el beneficiario, checa tu conexión e intentalo de nuevo";
    }
    public void ModBeneficiario()
    {
        if (benefIDMod.Count != 0)
        {
            WWWForm Form = new WWWForm();
            Form.AddField("IdUsuario", reg.id);
            Form.AddField("IdBeneficiario", benefIDMod[0]);
            Form.AddField("NombreBeneficiario", NombreBenefMod[0]);
            Form.AddField("CLABEBeneficiario", clabeBenefMod[0]);
            Form.AddField("ReferenciaBeneficiario", "0");
            Form.AddField("EmailBeneficiario", correoBenefMod[0]);
            Form.AddField("TelefonoBeneficiario", telefBenefMod[0]);
            WebServices.ModBenef(Form, SuccessModBeneficiario, ErrorModBenef);
        }
        //ob.DesError.text = "No se pudo modificar el beneficiario, checa tu conexión e intentalo de nuevo";
    }
    public void ObtenerBeneficiario()
    {
        WWWForm Form = new WWWForm();
        Form.AddField("IdUsuario", reg.id);
        WebServices.ObtBenef(Form, SuccessObtBenef, Error);
    
        //ob.DesError.text = "No se pudo obtener los beneficiarios, checa tu conexión e intentalo de nuevo";
    }
    public void EliminarBeneficiario()
    {
        if (benefIDEliminar.Count != 0)
        {
            WWWForm Form = new WWWForm();
            Form.AddField("IdUsuario", reg.id);
            Form.AddField("IdBeneficiario", benefIDEliminar[0]);
            WebServices.ElimBenef(Form, SuccessElimBeneficiario, ErrorElimBenef);
        }
        //ob.DesError.text = "No se pudo eliminar el beneficiario, checa tu conexión e intentalo de nuevo";
    }

    void SuccessObtBenef(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            error = ("" + ((MessageArg<string>)e).responseValue);
            reg.carga = false;
            Benef.Clear();
            Debug.Log("");
        }
        else if (e is MessageArg<List<DBBeneficiario>>)
        {
            //right = true;
            //register1 = 1;
            //User user = ((MessageArg<User>)e).responseValue;
            //PlayerPrefs.SetInt("Registrado", register1);
            //User user = ((MessageArg<User>)e).responseValue;
            Benef = ((MessageArg<List<DBBeneficiario>>)e).responseValue;
        StartCoroutine(DelayGetBeneficiario());
            //user.casas = ((MessageArg<List<DBCasate>>)e).responseValue;
            //= ((MessageArg<List<DBCasa>>)e).responseValue;
            //Casas = user.casas;
            //error = dbcasas.Id.ToString();
            //mp.ObtenerMP();
            //ingresa = true;
            //mp.Ver();

            //ob.Click("Inicios");

        }
    }
    void SuccessAgregarBeneficiario(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            reg.carga = false;
            //Debug.Log("");
        }
        else if (e is MessageArg<DBBeneficiario>)
        {
            //right = true;
            //register1 = 1;
            //PlayerPrefs.SetInt("Registrado", register1);
            DBBeneficiario beneficiario = ((MessageArg<DBBeneficiario>)e).responseValue;
            //Benef.Clear();
            ////id = user.Id.Value,
            //EliminarServ();
            //ReadCardBeneficiario();
            //ob.Click("Aviso");
            //ob.avisotexto.text = "Se agregó correctamente el beneficiario";
            for(int huj = 0; Beneficiarios.Count>huj; huj++)
            {
                if(benefIDLocalAgregar[0] == Beneficiarios[huj].GetComponent<InfoBeneficiario>().BeneficiarioIDLocal)
                {
                    Beneficiarios[huj].GetComponent<InfoBeneficiario>().BeneficiarioID = beneficiario.IdBeneficiario;
                    benefID[huj] = beneficiario.IdBeneficiario;
                }
            }
            benefIDLocalAgregar.RemoveAt(0);
            NombreBenefAgregar.RemoveAt(0);
            clabeBenefAgregar.RemoveAt(0);
            referBenefAgregar.RemoveAt(0);
            correoBenefAgregar.RemoveAt(0);
            telefBenefAgregar.RemoveAt(0);
            BeneficiarioAgregarOff = null;
            for (int ko = 0; benefIDLocalAgregar.Count > ko; ko++)
            {
                if (ko == 0)
                {
                    BeneficiarioAgregarOff = benefIDLocalAgregar[ko] + "," + NombreBenefAgregar[ko] + "," + referBenefAgregar[ko] + "," + correoBenefAgregar[ko] + "," +
                        clabeBenefAgregar[ko] + "," + telefBenefAgregar[ko];
                }
                else if (ko != 0)
                {
                    BeneficiarioAgregarOff += ";" + benefIDLocalAgregar[ko] + "," + NombreBenefAgregar[ko] + "," + referBenefAgregar[ko] + "," + correoBenefAgregar[ko] + "," +
                        clabeBenefAgregar[ko] + "," + telefBenefAgregar[ko];
                }
            }
            PlayerPrefs.SetString("BeneficiarioAgregar", BeneficiarioAgregarOff);
            if(benefIDLocalAgregar.Count != 0)
            {
                AgregarBeneficiario();
            }
                error = beneficiario.IdBeneficiario.ToString();
            NB.text = null;
            RB.text = null;
            CoB.text = null;
            CB.text = null;
            TB.text = null;
            //ob.Click("TermYCond");
        }
    }
    void SuccessModBeneficiario(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            reg.carga = false;
            //Debug.Log("");
        }
        else if (e is MessageArg<DBBeneficiario>)
        {
            //right = true;
            //register1 = 1;
            //PlayerPrefs.SetInt("Registrado", register1);
            DBBeneficiario beneficiario = ((MessageArg<DBBeneficiario>)e).responseValue;
            //Benef.Clear();
            //ob.Click("Aviso");
            //ob.avisotexto.text = "Se modificó correctamente el beneficiario";
            ////id = user.Id.Value,
            //EliminarServ();
            //ReadCardBeneficiario();
            BeneficiarioModOff = null;
            benefIDMod.RemoveAt(0);
            NombreBenefMod.RemoveAt(0);
            referBenefMod.RemoveAt(0);
            correoBenefMod.RemoveAt(0);
            clabeBenefMod.RemoveAt(0);
            telefBenefMod.RemoveAt(0);
            for (int hus = 0; hus < benefIDMod.Count; hus++)
            {
                if (hus == 0)
                {
                    BeneficiarioModOff = benefIDMod[hus] + "," + NombreBenefMod[hus] + "," + referBenefMod[hus] + "," + correoBenefMod[hus] + "," + clabeBenefMod[hus] + "," + telefBenefMod[hus];
                }
                else if (hus != 0)
                {
                    BeneficiarioModOff += ";" + benefIDMod[hus] + "," + NombreBenefMod[hus] + "," + referBenefMod[hus] + "," + correoBenefMod[hus] + "," + clabeBenefMod[hus] + "," + telefBenefMod[hus];
                }
            }
            PlayerPrefs.SetString("BeneficiarioMod", BeneficiarioModOff);
            if (benefIDMod.Count != 0)
            {
                ModBeneficiario();
            }
            Quitar();
            error = beneficiario.IdBeneficiario.ToString();
            //ob.Click("TermYCond");
        }
    }
    void SuccessElimBeneficiario(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            //Debug.Log("");
            reg.carga = false;
            Benef.Clear();
        }
        else if (e is MessageArg<DBBeneficiario>)
        {
            //right = true;
            //register1 = 1;
            //PlayerPrefs.SetInt("Registrado", register1);
            DBBeneficiario beneficiario = ((MessageArg<DBBeneficiario>)e).responseValue;
            //Benef.Clear();
            //ob.Click("Aviso");
            //ob.avisotexto.text = "Se eliminó correctamente el beneficiario";
            ////id = user.Id.Value,
            //EliminarServ();
            //ReadCardBeneficiario();
            benefIDEliminar.RemoveAt(0);
            BeneficiarioElimOff = null;
            for (int ku = 0; ku < benefIDEliminar.Count; ku++)
            {
                if (ku == 0)
                {
                    BeneficiarioElimOff = benefIDEliminar[ku] + ";";
                }
                else if (ku != 0)
                {
                    BeneficiarioElimOff += benefIDEliminar[ku] + ";";
                }
            }
            PlayerPrefs.SetString("BeneficiarioElim", BeneficiarioElimOff);
            if (benefIDEliminar.Count != 0)
            {
                EliminarBeneficiario();
            }
            error = beneficiario.IdBeneficiario.ToString();
            //ob.Click("TermYCond");
        }
    }
    void Error(System.Object Sender, EventArgs e)
    {
        error = ("" + ((MessageArg<string>)e).responseValue);
        //ob.Click("Errors");

    }
    void ErrorAgregarBenef(System.Object Sender, EventArgs e)
    {
        error = ("" + ((MessageArg<string>)e).responseValue);
        //ob.Click("Errors");
        AgregarBeneficiario();

    }
    void ErrorModBenef(System.Object Sender, EventArgs e)
    {
        error = ("" + ((MessageArg<string>)e).responseValue);
        //ob.Click("Errors");
        if (benefIDMod.Count != 0)
            ModBeneficiario();

    }
    void ErrorElimBenef(System.Object Sender, EventArgs e)
    {
        error = ("" + ((MessageArg<string>)e).responseValue);
        //ob.Click("Errors");
        EliminarBeneficiario();

    }
    IEnumerator DelayGetBeneficiario()
    {
        //reg.cargando = true;
        if (reg.ingresa == false)
        {
            //reg.CargateMenor();
        }
        yield return new WaitForSeconds(.5f);
        if (reg.ingresa == true)
        {
            ap.ReadCardPagosPasado();
        }
        for (y = 0; y < Benef.Count; y++)
        {
            //reg.cargando = true;
            beneficiarote = Benef[y];
            if (NombreBenef.Count < Benef.Count)
            {
                i++;
                NombreBenef.Add(beneficiarote.NombreBeneficiario);
                benefID.Add(beneficiarote.IdBeneficiario);
                benefIDLocal.Add(i);
                clabeBenef.Add(beneficiarote.ClabeBeneficiario);
                telefBenef.Add(beneficiarote.TelefonoBeneficiario);
                referBenef.Add(beneficiarote.ReferenciaBeneficiario);
                correoBenef.Add(beneficiarote.EmailBeneficiario);
                BeneficiarioOff = null;
                for (int kl = 0; kl < benefID.Count; kl++)
                {
                    if (kl == 0)
                    {
                        BeneficiarioOff = benefIDLocal[kl] + "," + benefID[kl] + "," + NombreBenef[kl] + "," + referBenef[kl] + "," + correoBenef[kl] + "," + clabeBenef[kl] + "," + telefBenef[kl];
                    }
                    else if (kl != 0)
                    {
                        BeneficiarioOff += ";" + benefIDLocal[kl] + "," + benefID[kl] + "," + NombreBenef[kl] + "," + referBenef[kl] + "," + correoBenef[kl] + "," + clabeBenef[kl] + "," + telefBenef[kl];
                    }
                }
                PlayerPrefs.SetString("Beneficiario", BeneficiarioOff);
                //else {
                //    FotoTomada = true;
                //}
                Posicion.Add(posicion);
                posicion++;
                //i = categoriate.id;
                X();
            }

        }
        if (y == Benef.Count)
        {
            //reg.cargando = false;
        }
    }
    public void ReadCardBeneficiario()
    {
        //reg.cargando = true;
        ObtenerBeneficiario();
        //ob.Click("MisCasasHUB");
        //if (ID == save.id)
        //{


    }
}
