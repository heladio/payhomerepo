﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VoxelBusters.Utility;
using VoxelBusters.Utility.UnityGUI.MENU;
using VoxelBusters.NativePlugins;
using System;
using System.IO;

public class MP : MonoBehaviour
{
    public InputField NickN, NickNR;
    public Text AnoN, MesN, AnoNR, MesNR;
    public Toggle SexoM, SexoF, SexoMR, SexoFR, SexoNER, SexoNE;
    public Text NickInicio, Bienv, aclara;
    [SerializeField] bool Nick, FotoCambiada, grood;
    public bool registro;
    [SerializeField] Registro reg;
    public ObjectManager ob;
    public string NickName, Ano, Mes, Sexo, error, foto, Dir, guardar;
    [SerializeField] Sprite FotoPerfil, FotoPerfilC;
    [SerializeField] List<string> m_results = new List<string>();
    public GameObject Foto, FotoInicio, FotoRegistro,FotoRecorte;
    public Image MPFoto, InicioFoto, MPFotoRegistro, FotoAjustar;
    public bool FotoTomada, eliminarFoto;
    public int sexoint, fotot, añoint, mesint;
    public Dropdown mes;
    [SerializeField] Texture2D fotota, image2;
    [SerializeField] TextureScale ts;
    [SerializeField] byte[] bytes;
    public string[] split, Subir;
    [SerializeField] string NickNameSubir, MiPerfilSubir;
    [SerializeField] int sexoIntSubir, AnoSubir, MesSubir, offInt;
    public Texture2D image3, screenShot, senderImage, finalImage;
    [SerializeField] Texture2D screenCap;
    public TakePhotos takePhotos;
    [SerializeField]
    GameObject photoGuide;
    [SerializeField] Texture2D prevFoto;
    [SerializeField]bool isDeleted = false;

    void Awake()
    {
        MiPerfilSubir = PlayerPrefs.GetString("MiPerfilSubir");
        Subir = MiPerfilSubir.Split(new Char[] { ',' });
        if (Subir.Length != 0 && reg.id != 0 && Subir[0] != "")
        {
            NickNameSubir = Subir[0];
            Int32.TryParse(Subir[1], out MesSubir);
            Int32.TryParse(Subir[2], out AnoSubir);
            Int32.TryParse(Subir[3], out sexoIntSubir);
            RegistrarMP();
        }
    }
    // Use this for initialization
    void Start()
    {
        Dir = Application.persistentDataPath;
        guardar = PlayerPrefs.GetString("MP");
        
    }
    public void Offline()
    {
        NickName = split[0];
        Mes = split[1];
        MesN.text = split[1];
        Ano = split[2];
        AnoN.text = split[2];
        Int32.TryParse(split[3], out sexoint);
        if(split[3] == "0")
        {
            SexoM.isOn = true;
            SexoF.isOn = false;
        }
        if(split[3] == "1")
        {
            SexoM.isOn = false;
            SexoF.isOn = true;
        }
        
        
        if(File.Exists(Application.persistentDataPath + "/"+  "MiPerfil.jpg")){
            byte[] byteArray = File.ReadAllBytes(Application.persistentDataPath +"/" +  "MiPerfil.jpg");
            Texture2D texture = new Texture2D(1600, 1600);
            texture.LoadImage(byteArray);
            fotota = texture;
            fotot = 1;
            FotoPerfil = Sprite.Create(fotota, new Rect(0, 0, fotota.width, fotota.height), Vector2.zero);
            InicioFoto.sprite = FotoPerfil;
            //InicioFoto.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(FotoPerfil.texture.width, FotoPerfil.texture.height);
            InicioFoto.preserveAspect = true;
            MPFoto.sprite = FotoPerfil;
            MPFoto.preserveAspect = true;
            // MPFoto.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(FotoPerfil.texture.width, FotoPerfil.texture.height) / 1.2f;

            FotoRegistro.SetActive(true);
            Foto.SetActive(true);
            FotoTomada = true;
            FotoInicio.SetActive(true);
            //reg.casa.ReadCard();
            isDeleted = false;

        }
        else
        {
            isDeleted = true;
        }
        Ver();
    }
    // Update is called once per frame
    void Update()
    {
        //if (FotoTomada == true || fotot == 1)
        //{
        //    //Foto.SetActive(true);
            
        //    MPFoto.sprite = FotoPerfil;
        //    MPFotoRegistro.sprite = FotoPerfil;
        //}
        if (NickN.text != "")
        {
            NickInicio.text = NickName;
            //aclara.text = "<b>" + NickInicio.text + ",</b> hemos recibido tu mensaje \nresponderemos a tu aclaración \na tu correo electrónico:" + PlayerPrefs.GetString("email");
        }

        //if (SexoM.isOn == true)
        //{
        //    Bienv.text = "Bienvenido";
        //}
        //if (SexoF.isOn == true)
        //{
        //    Bienv.text = "Bienvenida";
        //}
        //if (SexoMR.isOn == true)
        //{
        //    Bienv.text = "Bienvenido";
        //}
        //if (SexoFR.isOn == true)
        //{
        //    Bienv.text = "Bienvenida";
        //}

       // if(!ob.cambiosMP)
       // FotoAjustar.transform.localPosition = Vector3.zero;

    }
    public void Click()
    {
        
            NickN.text = NickName;
            AnoN.text = Ano;
            MesN.text = Mes;
            if (Sexo == "Masculino")
                SexoM.isOn = true;
            if (Sexo == "Femenino")
                SexoF.isOn = true;
        if (Sexo == "No especificado")
            SexoNE.isOn = true;
        ob.Click("MiPerfil");
    }

    public void Guardar()//script guardado de datos personales
    {
        if (NickN.text != "")
        {
            NickName = NickN.text;
            Ano = AnoN.text;
            if (Ano != "Año(Opcional)")
            {
                añoint = Int32.Parse(Ano);
            }
            else if(Ano == "Año(Opcional)")
            {
                añoint = 9999;
                ob.DesError.text = "Se necesita especificar el año de nacimiento";
                ob.Click("Errors");
                return;
            }
            Mes = MesN.text;
            if (Mes == "Enero")
                mesint = 1;
            if (Mes == "Febrero")
                mesint = 2;
            if (Mes == "Marzo")
                mesint = 3;
            if (Mes == "Abril")
                mesint = 4;
            if (Mes == "Mayo")
                mesint = 5;
            if (Mes == "Junio")
                mesint = 6;
            if (Mes == "Julio")
                mesint = 7;
            if (Mes == "Agosto")
                mesint = 8;
            if (Mes == "Septiembre")
                mesint = 9;
            if (Mes == "Octubre")
                mesint = 10;
            if (Mes == "Noviembre")
                mesint = 11;
            if (Mes == "Diciembre")
                mesint = 12;
            if(Mes == "Mes(Opcional)")
            {
                mesint = 13;
                ob.DesError.text = "Se necesita especificar el Mes de nacimiento";
                ob.Click("Errors");
                return;
            }
            if (mesint == 0)
            {
                mesint = 13;
            }
            if (añoint == 0)
            {
                añoint = 13;
            }
            if (SexoM.isOn)
            {
                sexoint = 0;
                Sexo = "Masculino";
                SexoMR.isOn = true;
            }
            if (SexoF.isOn)
            {
                sexoint = 1;
                Sexo = "Femenino";
                SexoFR.isOn = true;
            }
            if (SexoNE.isOn)
            {
                sexoint = 2;
                Sexo = "No especificado";
                SexoNER.isOn = true;
            }
            X();
            ob.Click("Atras");
            if (FotoTomada == true)
            {
                FotoInicio.SetActive(true);
                InicioFoto.sprite = FotoPerfil;
                fotot = 1;
                foto = "1";
            }
            guardar =  NickName + "," + Mes + "," + Ano + "," + sexoint;
            PlayerPrefs.SetString("MP", guardar);
            NickNameSubir = NickName;
            MesSubir = mesint;
            AnoSubir = añoint;
            sexoIntSubir = sexoint;
            MiPerfilSubir = NickNameSubir + "," + MesSubir + "," + AnoSubir + "," + sexoIntSubir;
            PlayerPrefs.SetString("MiPerfilSubir", MiPerfilSubir);
            RegistrarMP();
        }
        else
        {
            Ano = AnoN.text;
            Mes = MesN.text;
            mesint = 13;
            añoint = 9999;
            ob.DesError.text = "Se necesita especificar el Nickname que se va a utilizar";
            ob.Click("Errors");
        }

        /*
        if (NickN.text != "")
        {
            NickName = NickN.text;
            Ano = AnoN.text;
        }
 else if((NickN.text == ""))
        {
            ob.DesError.text = "Se necesita especificar el Nickname que se va a utilizar";
            ob.Click("Errors");
            return;
        }
        if (Ano != "Año(Opcional)")
            {
                añoint = Int32.Parse(Ano);
            }
        else if(Ano == "Año(Opcional)")
            {
               añoint = 9999; 
               ob.DesError.text = "Se necesita especificar el año de nacimiento";
               ob.Click("Errors");
            return;
            }
         Mes = MesN.text;
         if(MesN.text!="")
         {
            if (Mes == "Enero")
                mesint = 1;
            if (Mes == "Febrero")
                mesint = 2;
            if (Mes == "Marzo")
                mesint = 3;
            if (Mes == "Abril")
                mesint = 4;
            if (Mes == "Mayo")
                mesint = 5;
            if (Mes == "Junio")
                mesint = 6;
            if (Mes == "Julio")
                mesint = 7;
            if (Mes == "Agosto")
                mesint = 8;
            if (Mes == "Septiembre")
                mesint = 9;
            if (Mes == "Octubre")
                mesint = 10;
            if (Mes == "Noviembre")
                mesint = 11;
            if (Mes == "Diciembre")
                mesint = 12;
            if(Mes == "Mes(Opcional)")
            {
               // mesint = 13;
                ob.DesError.text = "Se necesita especificar el Mes de nacimiento";
                ob.Click("Errors");
                return;
            }
          }
            if (SexoM.isOn)
            {
                sexoint = 0;
                Sexo = "Masculino";
                SexoMR.isOn = true;
            }
            if (SexoF.isOn)
            {
                sexoint = 1;
                Sexo = "Femenino";
                SexoFR.isOn = true;
            }
            if (SexoNE.isOn)
            {
                sexoint = 2;
                Sexo = "No especificado";
                SexoNER.isOn = true;
            }
            X();
            ob.Click("Atras");
            if (FotoTomada == true)
            {
                FotoInicio.SetActive(true);
                InicioFoto.sprite = FotoPerfil;
                fotot = 1;
                foto = "1";
            }
            
            guardar =  NickName + "," + Mes + "," + Ano + "," + sexoint;
            PlayerPrefs.SetString("MP", guardar);
            NickNameSubir = NickName;
            MesSubir = mesint;
            AnoSubir = añoint;
            sexoIntSubir = sexoint;
            MiPerfilSubir = NickNameSubir + "," + MesSubir + "," + AnoSubir + "," + sexoIntSubir;
            PlayerPrefs.SetString("MiPerfilSubir", MiPerfilSubir);
            RegistrarMP();

        */
    }

    public void GuardarVer()
    {
        if (NickNR.text != "")
        {
            NickName = NickNR.text;
            Ano = AnoNR.text;
            Mes = MesNR.text;
            if(Ano != "Año(Opcional)")
            {
                añoint = Int32.Parse(Ano);
            }
            else if (Ano == "Año(Opcional)")
            {
                añoint = 9999;
                ob.DesError.text = "Se necesita especificar el año de nacimiento";
                ob.Click("Errors");
                return;
            
            }
            if (Mes == "Enero")
                mesint = 1;
            if (Mes == "Febrero")
                mesint = 2;
            if (Mes == "Marzo")
                mesint = 3;
            if (Mes == "Abril")
                mesint = 4;
            if (Mes == "Mayo")
                mesint = 5;
            if (Mes == "Junio")
                mesint = 6;
            if (Mes == "Julio")
                mesint = 7;
            if (Mes == "Agosto")
                mesint = 8;
            if (Mes == "Septiembre")
                mesint = 9;
            if (Mes == "Octubre")
                mesint = 10;
            if (Mes == "Noviembre")
                mesint = 11;
            if (Mes == "Diciembre")
                mesint = 12;
            if (Mes == "Mes(Opcional)")
             {    mesint = 13;
            ob.DesError.text = "Se necesita especificar el Mes de nacimiento";
            ob.Click("Errors");
                return;
             }
            if (mesint == 0)
            {mesint = 13;
            }
            if (añoint == 0)
            {
                añoint = 13;
            }
            if (SexoMR.isOn)
            {
                sexoint = 0;
                Sexo = "Masculino";
                SexoM.isOn = true;
            }
            if (SexoFR.isOn)
            {
                sexoint = 1;
                Sexo = "Femenino";
                SexoF.isOn = true;
            }
            if (SexoNER.isOn)
            {
                sexoint = 2;
                Sexo = "No especificado";
                SexoNE.isOn = true;
            }
            NickInicio.text = NickName;
            registro = true;
            if (registro == true || reg.ingresa == true || ob.cambiosMP == true)
            {
                if (SexoMR.isOn == true)
                {
                    Bienv.text = "Bienvenido";
                }
                if (SexoFR.isOn == true)
                {
                    Bienv.text = "Bienvenida";
                }
                if (SexoNER.isOn == true)
                {
                    Bienv.text = "Bienvenid@";
                }
            }
            X();

            ob.Click("CompPerfil-Casa");
            if (FotoTomada == true)
            {
                FotoInicio.SetActive(true);
                InicioFoto.sprite = FotoPerfil;
                fotot = 1;
                foto = "1";
            }
            guardar = NickName + "," + Mes + "," + Ano + "," + sexoint;
            PlayerPrefs.SetString("MP", guardar);
            NickNameSubir = NickName;
            MesSubir = mesint;
            AnoSubir = añoint;
            sexoIntSubir = sexoint;
            MiPerfilSubir = NickNameSubir + "," + MesSubir + "," + AnoSubir + "," + sexoIntSubir;
            PlayerPrefs.SetString("MiPerfilSubir", MiPerfilSubir);
            RegistrarMP();
        }
        else
        {
            Ano = AnoN.text;
            Mes = MesN.text;
            mesint = 13;
            añoint = 9999;
            ob.DesError.text = "Se necesita especificar el Nickname que se va a utilizar";
            ob.Click("Errors");
        }/*

        if (NickN.text != "")
        {
            NickName = NickN.text;
            Ano = AnoN.text;
        }
 else if((NickN.text == ""))
        {
            ob.DesError.text = "Se necesita especificar el Nickname que se va a utilizar";
            ob.Click("Errors");
            return;
        }
        if (Ano != "Año(Opcional)")
            {
                añoint = Int32.Parse(Ano);
            }
        else if(Ano == "Año(Opcional)")
            {
               añoint = 9999; 
               ob.DesError.text = "Se necesita especificar el año de nacimiento";
               ob.Click("Errors");
            return;
            }
         Mes = MesN.text;
         if(MesN.text!="")
         {
            if (Mes == "Enero")
                mesint = 1;
            if (Mes == "Febrero")
                mesint = 2;
            if (Mes == "Marzo")
                mesint = 3;
            if (Mes == "Abril")
                mesint = 4;
            if (Mes == "Mayo")
                mesint = 5;
            if (Mes == "Junio")
                mesint = 6;
            if (Mes == "Julio")
                mesint = 7;
            if (Mes == "Agosto")
                mesint = 8;
            if (Mes == "Septiembre")
                mesint = 9;
            if (Mes == "Octubre")
                mesint = 10;
            if (Mes == "Noviembre")
                mesint = 11;
            if (Mes == "Diciembre")
                mesint = 12;
            if(Mes == "Mes(Opcional)")
            {
                // mesint = 13;
                ob.DesError.text = "Se necesita especificar el Mes de nacimiento";
                ob.Click("Errors");
                return;
            }
          }
            if (SexoM.isOn)
            {
                sexoint = 0;
                Sexo = "Masculino";
                SexoMR.isOn = true;
            }
            if (SexoF.isOn)
            {
                sexoint = 1;
                Sexo = "Femenino";
                SexoFR.isOn = true;
            }
            if (SexoNE.isOn)
            {
                sexoint = 2;
                Sexo = "No especificado";
                SexoNER.isOn = true;
            }
            X();
            ob.Click("Atras");
            if (FotoTomada == true)
            {
                FotoInicio.SetActive(true);
                InicioFoto.sprite = FotoPerfil;
                fotot = 1;
                foto = "1";
            }
            
            guardar =  NickName + "," + Mes + "," + Ano + "," + sexoint;
            PlayerPrefs.SetString("MP", guardar);
            NickNameSubir = NickName;
            MesSubir = mesint;
            AnoSubir = añoint;
            sexoIntSubir = sexoint;
            MiPerfilSubir = NickNameSubir + "," + MesSubir + "," + AnoSubir + "," + sexoIntSubir;
            PlayerPrefs.SetString("MiPerfilSubir", MiPerfilSubir);
            RegistrarMP();

               */
            }

    public void Ver()
    {
        NickN.text = NickName;
        AnoN.text = Ano;
        MesN.text = Mes;
        if (sexoint == 0)
        {
            SexoM.isOn = true;
            Bienv.text = "Bienvenido";
        }
        if (sexoint == 1)
        {
            SexoF.isOn = true;

            Bienv.text = "Bienvenida";
        }
        if (sexoint == 2)
        {
            SexoNE.isOn = true;

            Bienv.text = "Bienvenid@";
        }
        X();
    }
    public void X()
    {
        
        if (NickN.text != "")
        {
            NickInicio.text = NickN.text;
            //aclara.text = "<b>" + NickInicio.text + ",</b> hemos recibido tu mensaje \nresponderemos a tu aclaración \na tu correo electrónico:" + PlayerPrefs.GetString("email");
        }

        if (registro == false && reg.ingresa == false && ob.cambiosMP == false)
        {
            Bienv.text = "Bienvenido";
        }

            if (sexoint == 0)
            {
                Bienv.text = "Bienvenido";
            }
            if (sexoint == 1)
            {
                Bienv.text = "Bienvenida";
            }
        if (sexoint == 2)
        {
            Bienv.text = "Bienvenid@";
        }
        StartCoroutine(Starting());
        //    if(reg.ingresa == true)
        ////reg.casa.ReadCard();
    }

    IEnumerator Starting()
    {
        yield return new WaitForSeconds(10);
        reg.isStart = false;
    }

    public void TomarFoto()
    {
        if (Application.internetReachability != NetworkReachability.NotReachable) { 
        if (fotot == 0)
            {
                ob.ImagenGaleria.onClick.RemoveAllListeners();
                ob.ImagenCamara.onClick.RemoveAllListeners();
                ob.Click("Imagen");
                ob.ImagenCamara.onClick.AddListener(PickImageFromCamera);
                ob.ImagenGaleria.onClick.AddListener(PickImageFromAlbum);
            }
            else if (fotot == 1)
            {
                ob.ImagenCamaraEliminar.onClick.RemoveAllListeners();
                ob.ImagenGaleriaEliminar.onClick.RemoveAllListeners();
                ob.ImagenEliminarFoto.onClick.RemoveAllListeners();
                ob.Click("ImagenEliminar");
                ob.ImagenCamaraEliminar.onClick.AddListener(PickImageFromCamera);
                ob.ImagenGaleriaEliminar.onClick.AddListener(PickImageFromAlbum);
                ob.ImagenEliminarFoto.onClick.AddListener(EliminarFoto);
            }
    }
        else
        {
            ob.DesError.text = "Solo puedes modificar/eliminar la foto cuando estes conectado a internet";
            ob.Click("Errors");
        }
    }

    public void PickImageFromAlbum()
    {
        reg.StartLoad();
        StartCoroutine(Album());
    }

    IEnumerator Album()
    {
        yield return new WaitForSeconds(0.5f);
        if (fotot == 0)
            ob.Click("ImagenQuitar");
        else if (fotot == 1)
            ob.Click("ImagenEliminarQuitar");
        takePhotos.isFinished += TakePhoto;
#if UNITY_IOS
        // Set popover to last touch position
        NPBinding.UI.SetPopoverPointAtLastTouchPosition();
#else
        // Pick image
        //takePhotos.PickImage(2048);
#endif
     //   NPBinding.MediaLibrary.PickImage(eImageSource.ALBUM, 1, takePhotos.PickImageFinished);
    }

    public void PickImageFromCamera()
    {
        reg.StartLoad();
        StartCoroutine(Camera());
    }

    IEnumerator Camera()
    {
        yield return new WaitForSeconds(0.5f);
        if (fotot == 0)
            ob.Click("ImagenQuitar");
        else if (fotot == 1)
            ob.Click("ImagenEliminarQuitar");


        takePhotos.isFinished += TakePhoto;
#if UNITY_IOS
        // Set popover to last touch position
        NPBinding.UI.SetPopoverPointAtLastTouchPosition();
        NPBinding.MediaLibrary.PickImage(eImageSource.CAMERA, 1, takePhotos.PickImageFinished);
#else
        //wasPhoto = true;
        takePhotos.TakePicture(2048);
#endif

        // Pick image
        //NPBinding.MediaLibrary.PickImage(eImageSource.CAMERA, 1, takePhotos.PickImageFinished);
        //TakePhoto();
    }

    public void EliminarFoto()
    {
        Foto.SetActive(false);
        eliminarFoto = true;
        fotot = 0;
        FotoTomada = false;
        

        ob.Click("ImagenEliminarQuitar");
    }

    void TakePhoto()
    {
        FotoCambiada = true;
        eliminarFoto = false;
        //this.GetComponent<Renderer>().material.mainTexture = texture;
        Texture2D image3;
        image3 = takePhotos.photoImage;
        //error_text.text = "bytes not wrtiting";

        /* byte[] bytes;
         try
         {
         bytes = image3.EncodeToJPG();

         }catch(Exception E)
         {
             Debug.Log(E.ToString());
         }*/
        //error_text.text = "bytes did write";

        //TextureScale.Point(image3, image3.width, image3.height);

        //File.WriteAllBytes(Application.persistentDataPath + "MiPerfil.jpg", bytes);
        ////image2.Resize(image2.width / 2, image2.height / 2);

        FotoPerfilC = Sprite.Create(image3, new Rect((0), (0), (image3.width), (image3.height)), Vector2.zero);

        //FotoRegistro.SetActive(true);
        //Foto.SetActive(true);
        //FotoTomada = true;
        //MPFoto.sprite = FotoPerfilC;
        //MPFotoRegistro.sprite = FotoPerfilC;
        FotoAjustar.sprite = FotoPerfilC;
        FotoAjustar.transform.localPosition = Vector3.zero;
        //wasPhoto = false;
        ob.Click("AjustarFoto");


        if (takePhotos.isFinished != null)
            takePhotos.isFinished -= TakePhoto;

        //error_text.text = "photo finished";

        reg.EndLoad();
        System.GC.Collect();

    }


    /* public void PickImageFinished(ePickImageFinishReason _reason, Texture2D _image)
     {
         AddNewResult("Request to pick image from gallery finished. Reason for finish is " + _reason + ".");
         AppendResult(string.Format("Selected image is {0}.", (_image == null ? "NULL" : _image.ToString())));

         if(_image != null)
         {
             FotoCambiada = true;
             SetAllowsImageEditing();
             eliminarFoto = false;
             //this.GetComponent<Renderer>().material.mainTexture = texture;


             Texture2D image3 = _image;
             byte[] bytes = image3.EncodeToJPG();
             //File.WriteAllBytes(Application.persistentDataPath + "MiPerfil.jpg", bytes);
             ////image2.Resize(image2.width / 2, image2.height / 2);

             FotoPerfilC = Sprite.Create(image3, new Rect((0), (0), (image3.width), (image3.height)), Vector2.zero);
             ob.Click("AjustarFoto");
             //FotoRegistro.SetActive(true);
             //Foto.SetActive(true);
             //FotoTomada = true;
             //MPFoto.sprite = FotoPerfilC;
             //MPFotoRegistro.sprite = FotoPerfilC;
             FotoAjustar.sprite = FotoPerfilC;
         }

     }*/
    public void Guardad()
    {
        //Application.CaptureScreenshot(Application.persistentDataPath + "/Foto.png");
        
        StartCoroutine(GetFoto());
        //fotota = image3;
        //    fotot = 1;
        //    FotoPerfil = Sprite.Create(fotota, new Rect((fotota.width / 4), (fotota.height / 4), (fotota.width / 2), (fotota.height / 2)), Vector2.zero);
        //    InicioFoto.sprite = FotoPerfil;
        //    MPFoto.sprite = FotoPerfil;

        //    FotoRegistro.SetActive(true);
        //    Foto.SetActive(true);
        //    FotoTomada = true;
        //    FotoInicio.SetActive(true);
        //    reg.casa.ReadCard();
    }
    public void OnPostRender()
    {
        if (grood)
        {
            image3 = new Texture2D(600, 600);
            image3.ReadPixels(new Rect(0, 0, FotoRecorte.transform.position.y, FotoRecorte.transform.position.y), 0, 0);

            //image3.Apply();
            //FotoPerfilC = Sprite.Create(image3, new Rect((0), (0), (image3.width), (image3.height)), Vector2.zero);
            //Texture2D image3 = FotoRecorte.sprite.texture;
            byte[] bytes = image3.EncodeToJPG(50);
            //TextureScale.Point(image3, image3.width, image3.height);
        }
    }
   /* private void SetAllowsImageEditing()
    {
        NPBinding.MediaLibrary.SetAllowsImageEditing(true);
    }
   
    private static eImageSource GetBOTH()
    {
        return eImageSource.BOTH;
    }
    private static eImageSource GetCAMERA()
    {
        return eImageSource.CAMERA;
    }

    private static eImageSource GetALBUM()
    {
        return eImageSource.ALBUM;
    }
    protected void AppendResult(string _result)
    {
        m_results.Add(_result);
    }

    protected void AddNewResult(string _result)
    {
        m_results.Clear();
        m_results.Add(_result);
    }*/
    void RegistrarMP()
    {
        //reg.cargando = true;
        WWWForm Form = new WWWForm();
        Form.AddField("IdUsuario", reg.id);
        Form.AddField("NicknameUsuario", NickNameSubir);
        Form.AddField("mesUsuario", MesSubir);
        Form.AddField("anoUsuario", AnoSubir);
        
        Form.AddField("SexoUsuario", sexoIntSubir + 1);
        WebServices.ActMP(Form, Success, ErrorRegistrar);//editar perfil
    }
    public void ObtenerMP()
    {
        WWWForm Form = new WWWForm();
        Form.AddField("IdUsuario", reg.id);
        //Form.AddField("IdLocal", 0);
        WebServices.ObtMP(Form, SuccessObt, Error);
        ob.DesError.text = "Comprueba tu conexión a internet para utilizar la app";
    }
    void Success(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            //reg.cargando = false;
            reg.carga = false;
            error = ("" + ((MessageArg<string>)e).responseValue);
            Debug.Log("");
        }
        else if (e is MessageArg<DBMP>)
        {
            //print("guardado");
            //right = true;
            //register1 = 1;
            //PlayerPrefs.SetInt("Registrado", register1);
            //reg.cargando = false;
            DBMP dbmp = ((MessageArg<DBMP>)e).responseValue;
            //id = user.Id.Value;
            error = dbmp.Id.ToString();
            MiPerfilSubir = null;
            PlayerPrefs.SetString("MiPerfilSubir", MiPerfilSubir);
            if(FotoCambiada == true)
            {
            //print("foto cambiado");
                InicioFoto.sprite = FotoPerfilC;
                FotoCambiada = false;
                //byte[] byteArray = File.ReadAllBytes("Foto.jpg");
                //image3 = new Texture2D(Screen.width, Screen.height);
                //image3.LoadImage(byteArray);
                //FotoPerfilC = Sprite.Create(image3, new Rect(0, 0, FotoRecorte.transform.position.x, FotoRecorte.transform.position.y), Vector2.zero);
                ////ob.Click("AjustarFoto");
                //FotoRegistro.SetActive(true);
                //Foto.SetActive(true);
                //FotoTomada = true;
                //MPFoto.sprite = FotoPerfilC;
                //MPFotoRegistro.sprite = FotoPerfilC;
                ////image3 = FotoPerfilC.texture;
                //FotoPerfil = FotoPerfilC;

                //image3 = new Texture2D((int)FotoPerfilC.textureRect.height, (int)FotoPerfilC.textureRect.width);
                //var pixels = FotoPerfilC.texture.GetPixels(0,
                //                                        0,
                //                                        (int)FotoPerfilC.textureRect.height,
                //                                        (int)FotoPerfilC.textureRect.width);
                //image3.SetPixels(pixels);
                //image3.Apply();
                bytes = null;
                bytes = image3.EncodeToJPG(50);
                EnviarFoto();
                //File.WriteAllBytes(Application.persistentDataPath + "/" + "MiPerfil.jpg", bytes);
                InicioFoto.sprite = FotoPerfilC;
                isDeleted = false;

                ob.Click("Atras");
                //Ver();
            }
            else if(FotoCambiada == false && eliminarFoto == false)
            {
                //Ver();
                reg.carga = false;
            }
            else if(eliminarFoto == true)
            {
                eliminarFoto = false;
                FotoRegistro.SetActive(false);
                FotoInicio.SetActive(false);
                Foto.SetActive(false);
                reg.carga = false;
                EliminarFotoMP();
                isDeleted = true;
                if (File.Exists(Application.persistentDataPath + "/" + "MiPerfil.jpg"))
                {
                    File.Delete(Application.persistentDataPath + "/" + "MiPerfil.jpg");
                }
            }
            
            //ob.Click("TermYCond");
        }
    }
    void SuccessElimFoto(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            //reg.cargando = false;
            reg.carga = false;
            error = ("" + ((MessageArg<string>)e).responseValue);
            Debug.Log("");
        }
        else if (e is MessageArg<DBMP>)
        {
            //right = true;
            //register1 = 1;
            //PlayerPrefs.SetInt("Registrado", register1);
            //reg.cargando = false;
            //DBMP dbmp = ((MessageArg<DBMP>)e).responseValue;
            ////id = user.Id.Value;
            //error = dbmp.Id.ToString();
            //MiPerfilSubir = null;
            //PlayerPrefs.SetString("MiPerfilSubir", MiPerfilSubir);
            //if (FotoCambiada == true)
            //{

            //    FotoCambiada = false;
            //    EnviarFoto();
            //    //Ver();

            //}
            //else if (FotoCambiada == false && eliminarFoto == false)
            //{
            //    //Ver();
            //    reg.carga = false;
            //}


            //else if (eliminarFoto == true)
            //{
            //    eliminarFoto = false;
            //    FotoRegistro.SetActive(false);
            //    FotoInicio.SetActive(false);
            //    Foto.SetActive(false);
            //    reg.carga = false;
            //    EliminarFotoMP();
            //}

            //ob.Click("TermYCond");
        }
    }
    void SuccessObt(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            error = ("" + ((MessageArg<string>)e).responseValue);
            Debug.Log("");
        }
        else if (e is MessageArg<DBMP>)
        {
            //right = true;
            //register1 = 1;
            //PlayerPrefs.SetInt("Registrado", register1);
            DBMP dbmp = ((MessageArg<DBMP>)e).responseValue;
            NickName = dbmp.Nickname;
            foto = dbmp.Fotousuario;
            ob.Click("Inicios");
            if (dbmp.Ano != 9999)
            {
                Ano = dbmp.Ano.ToString();
            }
            else if (dbmp.Ano == 9999)
            {
                Ano = "Año(Opcional)";
            }
            if (dbmp.Mes == 1)
                Mes = "Enero";
            if (dbmp.Mes == 2)
                Mes = "Febrero";
            if (dbmp.Mes == 3)
                Mes = "Marzo";
            if (dbmp.Mes == 4)
                Mes = "Abril";
            if (dbmp.Mes == 5)
                Mes = "Mayo";
            if (dbmp.Mes == 6)
                Mes = "Junio";
            if (dbmp.Mes == 7)
                Mes = "Julio";
            if (dbmp.Mes == 8)
                Mes = "Agosto";
            if (dbmp.Mes == 9)
                Mes = "Septiembre";
            if (dbmp.Mes == 10)
                Mes = "Octubre";
            if (dbmp.Mes == 11)
                Mes = "Noviembre";
            if (dbmp.Mes == 12)
                Mes = "Diciembre";
            if (dbmp.Mes == 13)
                Mes = "Mes(Opcional)";
            sexoint = dbmp.Sexo - 1;
            error = dbmp.Id.ToString();
            guardar = NickName + "," + Mes + "," + Ano + "," + sexoint;
            PlayerPrefs.SetString("MP", guardar);
            ob.Click("Inicios");
            if (foto == "" || foto == null || foto == "null" || foto == " ")
            {
                //print("no Foto");
                //reg.casa.ReadCard();
                if (File.Exists(Application.persistentDataPath + "/" + "MiPerfil.jpg"))
                {
                    //print("delete");
                    File.Delete(Application.persistentDataPath + "/" + "MiPerfil.jpg");
                    Foto.SetActive(false);
                    FotoInicio.SetActive(false);
                    eliminarFoto = true;
                    fotot = 0;
                    FotoTomada = false;
                }
                Ver();
            }
            else if (foto != "" || foto != null || foto != " ")
            {
               // print("yes foto");
                StartCoroutine(Starts(foto));
                //reg.casa.ReadCard();
                //Ver();
            }
            //ob.Click("TermYCond");
        }
    }
    public void EliminarFotoMP()
    {
        WWWForm Form = new WWWForm();
        Form.AddField("IdUsuario", reg.id);
        //Form.AddField("IdLocal", 0);
        WebServices.EliminarMP(Form, SuccessElimFoto, Error);
        ob.DesError.text = "Comprueba tu conexión a internet para utilizar la app";
    }
    void ErrorRegistrar(System.Object Sender, EventArgs e)
    {
        //ob.Click("InicioQuitar");

        //ob.Click("ErrorQuitar");
        //ob.Click("IniciarSesionError");
        RegistrarMP();
        error = ("" + ((MessageArg<string>)e).responseValue);
    }
    void Error(System.Object Sender, EventArgs e)
    {
        //ob.Click("InicioQuitar");

        //ob.Click("ErrorQuitar");
        //ob.Click("IniciarSesionError");
        RegistrarMP();
        error = ("" + ((MessageArg<string>)e).responseValue);
    }


    IEnumerator Starts(string endDir)
    {
        // Start a download of the given URL
        string url = "https://payhome.azurewebsites.net/serv/";
        WWW www = new WWW(url + endDir);

        // Wait for download to complete
        yield return www;
        Texture2D texture = www.texture;
        //this.GetComponent<Renderer>().material.mainTexture = texture;
        byte[] bytes = texture.EncodeToJPG(50);
        File.WriteAllBytes(Application.persistentDataPath + "/" + "MiPerfil.jpg", bytes);
        // assign texture
        fotota = www.texture;
        fotot = 1;
        yield return new WaitForSeconds(1);
        FotoPerfil = Sprite.Create(fotota, new Rect(/*(fotota.width/2)*/0, /*(fotota.height/2)*/0, (fotota.width), (fotota.height)), Vector2.zero);
        FotoAjustar.sprite = FotoPerfil;

        InicioFoto.sprite = FotoPerfil;
        InicioFoto.preserveAspect = true;
        //InicioFoto.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(FotoPerfil.texture.width, FotoPerfil.texture.height);

        MPFoto.sprite = FotoPerfil;
        MPFoto.preserveAspect = true;
        //MPFoto.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(FotoPerfil.texture.width, FotoPerfil.texture.height)/1.2f;


        FotoRegistro.SetActive(true);
        Foto.SetActive(true);
        FotoTomada = true;
        FotoInicio.SetActive(true);
        //reg.casa.ReadCard();
        Ver();
        //oj.sprite = FotoPerfil;
        //reg.isStart = false;
    }
    public void EnviarFoto()
    {
        bytes = null;
        bytes = senderImage.EncodeToJPG(50);
        bytes = TextureScale.bytes;
        WWWForm Form = new WWWForm();
        Form.AddField("IdUsuario", reg.id);
        Form.AddBinaryData("archivoImagen", bytes);        
        WebServices.SubirFoto(Form, SuccessFoto, Error);
        
    }
    void SuccessFoto(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            //cargando = false;
            ////error = ("" + ((MessageArg<string>)e).responseValue);
            //EncabezadoErrorAclDudas.text = "Reporte de fallos";
            //ErrorAclDudas.onClick.AddListener(EnviarContacto);
            //ob.Click("Aclaraciones-Error");

            Debug.Log("");
        }
        else if (e is MessageArg<dbphoto>)
        {
            //right = true;
            //register1 = 1;
            //PlayerPrefs.SetInt("Registrado", register1);
            //cargando = false;
            dbphoto dbp = ((MessageArg<dbphoto>)e).responseValue;
            
            foto = dbp.FotoUsuario;
            isDeleted = false;

            //StartCoroutine(Starts(foto));
            //TextoAclaracionDudas.text = "<b>" + mp.NickInicio.text + ",</b> hemos recibido tu reporte de fallo, muchas gracias por reportarlo. Responderemos a tu correo electrónico cuando ya haya sido arreglado:";
            //EmailAclDudas.text = PlayerPrefs.GetString("email");
            //EncabezadoCorrectoAclDudas.text = "Reporte de fallo";
            //Reporte.text = "";
            //ob.Click("Aclaraciones-Correcto");
            //error = dudas.IdUsuario.ToString();

        }

    }

    IEnumerator GetFoto()
    {
        photoGuide.SetActive(false);
        yield return new WaitForEndOfFrame();

        screenCap = new Texture2D(Screen.width, Screen.height);
        screenCap.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        screenCap.Apply();
        byte[] bytesa = screenCap.EncodeToJPG(50);
        File.WriteAllBytes(Application.persistentDataPath + "/Foto.png", bytesa);
        yield return new WaitForEndOfFrame();
        byte[] byteArray = File.ReadAllBytes(Application.persistentDataPath + "/Foto.png");
        image3 = new Texture2D(Screen.width, Screen.height);
        image3.LoadImage(byteArray);
        FotoPerfilC = Sprite.Create(image3, new Rect(0, 0, FotoRecorte.transform.position.x, FotoRecorte.transform.position.y), Vector2.zero);
        //ob.Click("AjustarFoto");
        FotoRegistro.SetActive(true);
        Foto.SetActive(true);
        FotoTomada = true;

        MPFoto.sprite = FotoPerfilC;
        MPFoto.preserveAspect = true;
        //MPFoto.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(FotoPerfilC.texture.width, FotoPerfilC.texture.height) / 1.2f;

        MPFotoRegistro.sprite = FotoPerfilC;
        MPFotoRegistro.preserveAspect = true;
        //MPFotoRegistro.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(FotoPerfilC.texture.width, FotoPerfilC.texture.height);

        //image3 = FotoPerfilC.texture;
        FotoPerfil = FotoPerfilC;
        Texture2D image4 = new Texture2D((int)FotoPerfilC.textureRect.width, (int)FotoPerfilC.textureRect.height);
        var pixels = FotoPerfilC.texture.GetPixels(0,
                                                0,
                                                (int)FotoPerfilC.textureRect.width,
                                                (int)FotoPerfilC.textureRect.height);
        image4.SetPixels(pixels);
        image4.Apply();
        byte[] bytes2 = null;
        bytes2 = image4.EncodeToJPG(50);
        finalImage = image4;
        TextureScale.Point(image4, image4.width, image4.height);
        File.WriteAllBytes(Application.persistentDataPath + "/MiPerfil.jpg", bytes2);

        if (File.Exists(Application.persistentDataPath + "/" + "MiPerfil.jpg"))
        {
            byte[] byteArray2 = File.ReadAllBytes(Application.persistentDataPath + "/" + "MiPerfil.jpg");
            Texture2D texture = new Texture2D(1600, 1600);
            texture.LoadImage(byteArray2);
            fotota = texture;
            fotot = 1;
            FotoPerfil = Sprite.Create(fotota, new Rect(0, 0, fotota.width, fotota.height), Vector2.zero);

            var pixels2 = FotoPerfil.texture.GetPixels(0,
                                                0,
                                                (int)FotoPerfil.textureRect.width,
                                                (int)FotoPerfil.textureRect.height);
            image4.SetPixels(pixels2);
            image4.Apply();

            InicioFoto.sprite = FotoPerfil;
            InicioFoto.preserveAspect = true;
            //InicioFoto.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(FotoPerfil.texture.width, FotoPerfil.texture.height);
            MPFoto.sprite = FotoPerfil;
            MPFoto.preserveAspect = true;
            //MPFoto.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(FotoPerfil.texture.width, FotoPerfil.texture.height) / 1.2f;

            senderImage = image4;
        }
        ob.Click("Atras");
        photoGuide.SetActive(true);
    }

    public void Back()
    {
        if (isDeleted)
        {
            if (File.Exists(Application.persistentDataPath + "/" + "MiPerfil.jpg"))
            {
                File.Delete(Application.persistentDataPath + "/" + "MiPerfil.jpg");
            }
        }
        if (File.Exists(Application.persistentDataPath + "/" + "MiPerfil.jpg"))
        {
            Sprite spr = Sprite.Create(prevFoto, new Rect(0, 0, prevFoto.width, prevFoto.height), Vector2.zero);
            InicioFoto.sprite = spr;
            InicioFoto.preserveAspect = true;
            //InicioFoto.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(FotoPerfil.texture.width, FotoPerfil.texture.height);
            MPFoto.sprite = spr;
            MPFoto.preserveAspect = true;

            MPFoto.sprite = spr;
            MPFoto.preserveAspect = true;
            //MPFoto.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(FotoPerfilC.texture.width, FotoPerfilC.texture.height) / 1.2f;

            MPFotoRegistro.sprite = spr;
            MPFotoRegistro.preserveAspect = true;
        }
        FotoCambiada = false;
        FotoTomada = false;
        eliminarFoto = false;        
    }

    public void PrevFoto()
    {
        if(File.Exists(Application.persistentDataPath + "/"+  "MiPerfil.jpg"))
        {
            byte[] byteArray = File.ReadAllBytes(Application.persistentDataPath +"/" +  "MiPerfil.jpg");
            Texture2D texture = new Texture2D(1600, 1600);
            texture.LoadImage(byteArray);
            fotota = texture;
            prevFoto = fotota;
            fotot = 1;
            FotoPerfil = Sprite.Create(fotota, new Rect(0, 0, fotota.width, fotota.height), Vector2.zero);
            InicioFoto.sprite = FotoPerfil;
            //InicioFoto.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(FotoPerfil.texture.width, FotoPerfil.texture.height);
            InicioFoto.preserveAspect = true;
            MPFoto.sprite = FotoPerfil;
            MPFoto.preserveAspect = true;
            // MPFoto.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(FotoPerfil.texture.width, FotoPerfil.texture.height) / 1.2f;

            FotoRegistro.SetActive(true);
            Foto.SetActive(true);
            FotoTomada = true;
            FotoInicio.SetActive(true);
            //reg.casa.ReadCard();
            isDeleted = false;
        }
        else
        {
            Foto.SetActive(false);
            isDeleted = true;
        }
    }

}
