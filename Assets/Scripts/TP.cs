﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class TP : MonoBehaviour
{
    public ObjectManager ob;
    //public List<string> NNC, UbC, CodC = new List<string>();
    //public List<int> ID = new List<int>();
    public List<GameObject> CatPagos, CatPagosPer = new List<GameObject>(); /*Casass2, Historial = new List<GameObject>();*/
    public int i = 0, b = 0, idColor, idIcono, ids, c, h, codbar, posicion, y, id, pos, CatID, CatIDLocal;
    public GameObject categoria, categoriax, categoriaclone, categorias;
    public Vector2 G, G2;
    public InfoCategoria IP, IP2;
    public Guardar save;
    public float s = 0, d, pa, TimerRef;
    public ScrollRect scroll;
    public Text Categoria, EditarCat, HeaderCat;
    public AP ap;
    public Casas casa;
    public Color IcoColor, iconColorBack;
    public Image botonIcono, iconColor;
    public Sprite Ico, botonIconoBack;
    public GameObject Iconos, Colores, content, limsup, liminf, ToggleCB, TCB, Menu, Atras, MenuSuperior, BotonGuardar, BotonMod, mover, categoriat, Refrescar, circulo;
    public InputField NomCat;
    public List<Sprite> Icon = new List<Sprite>();
    public List<Color> Colo = new List<Color>();
    public List<string> NombreCategoria, NombreCategoriaPer = new List<string>();
    public Vector2 scrollOriginal, LimiteSuperior, LimiteInferior, PrimerTP, UltimoTP, posicioncontent, scrollinferior, contentActual, Ward, Ward2;
    public Toggle CB;
    public List<Sprite> IC, PagoPredeterminado = new List<Sprite>();
    public List<Color> CO = new List<Color>();
    public List<int> IDCatPagos, Posicion, IDCatPagosLocal, intColor, intIcon, CodBar, IDCatPagosPer, intColorPer, intIconPer, CodBarPer = new List<int>();
    public Swipe sw;
    public bool bajo, modificar, norepetido, RefBool;
    public string error, Offline, CatAgregarNE, CatModNE, CatEliminarNE, NoCat;
    public string[] Off, escribir, CatAgregarOffline, escribirCatAgregar, CatModOffline, escribirCatMod, CatElimOffline, escribirCatElim;
    public Registro reg;
    public List<DBCategoria> Categorias = new List<DBCategoria>();
    public List<int> IDCatPagosAgregarOffline, IDCatPagosLocalAgregarOffline, intColorAgregarOffline, intIconAgregarOffline,
        CodBarAgregarOffline, IDCatPagosModOffline, IDCatPagosLocalModOffline, intColorModOffline, intIconModOffline, CodBarModOffline, IDCatPagosElimOffline = new List<int>();
    public List<string> NombreCategoriaAgregarOffline, NombreCategoriaModOffline = new List<string>();
    public DBCategoria categoriate;
    int offlineInt;
    Char delimiter = ';';
    Char del = ',';
    // Use this for initialization
    void Awake()
    {
        for (int u = 0; u < 10; u++)
        {
            IP = CatPagos[u].GetComponent<InfoCategoria>();
            Icon.Add(IP.Icon.sprite);
            Colo.Add(IP.Icon.color);
            NombreCategoria.Add(IP.NombreCatPago);
        }
        Offl();
        //liminf = CatPagos[i-1];
        //LimiteSuperior = limsup.transform.position;
        //LimiteInferior = liminf.transform.position;
        //scrollOriginal = content.transform.localPosition;
    }
    public void Offl()
    {
        //posicion = 10;
        Offline = PlayerPrefs.GetString("Categorias");
        Off = Offline.Split(delimiter);
        if (Off[0] != "")
        {
            for (int gh = 0; gh < Off.Length; gh++)
            {
                if (Off.Length > IDCatPagosLocal.Count)
                {
                    escribir = null;
                    escribir = Off[gh].Split(del);
                    Int32.TryParse(escribir[0], out id);
                    IDCatPagosLocal.Add(id);
                    i = id;
                    Int32.TryParse(escribir[1], out CatID);
                    IDCatPagosPer.Add(CatID);
                    NombreCategoriaPer.Add(escribir[2]);
                    NoCat = escribir[2];
                    Int32.TryParse(escribir[3], out idColor);
                    intColorPer.Add(idColor);
                    IcoColor = CO[idColor];
                    Int32.TryParse(escribir[4], out idIcono);
                    intIconPer.Add(idIcono);
                    Ico = IC[idIcono];
                    Icon.Add(Ico);
                    Colo.Add(IcoColor);
                    Int32.TryParse(escribir[5], out codbar);
                    CodBarPer.Add(codbar);
                    Posicion.Add(posicion);
                    posicion++;
                    //i = categoriate.id;
                    X();

                }
            }
        }
        CatAgregarNE = PlayerPrefs.GetString("CategoriasAgregar");
        CatAgregarOffline = CatAgregarNE.Split(delimiter);
        for (int yu = 0; yu < CatAgregarOffline.Length; yu++)
        {
            if (CatAgregarOffline.Length > IDCatPagosLocalAgregarOffline.Count)
            {
                escribirCatAgregar = null;
            escribirCatAgregar = CatAgregarOffline[yu].Split(del);
            
                if (escribirCatAgregar[0] != "")
                {
                    Int32.TryParse(escribirCatAgregar[0], out offlineInt);
                    IDCatPagosLocalAgregarOffline.Add(offlineInt);
                    NombreCategoriaAgregarOffline.Add(escribirCatAgregar[1]);
                    Int32.TryParse(escribirCatAgregar[2], out offlineInt);
                    intColorAgregarOffline.Add(offlineInt);
                    Int32.TryParse(escribirCatAgregar[3], out offlineInt);
                    intIconAgregarOffline.Add(offlineInt);
                    Int32.TryParse(escribirCatAgregar[4], out offlineInt);
                    CodBarAgregarOffline.Add(offlineInt);
                }
                if (IDCatPagosLocalAgregarOffline.Count != 0)
                {
                    AgregarCategorias();
                }
            }
        }
        CatModNE = PlayerPrefs.GetString("CategoriasModificar");
        CatModOffline = CatModNE.Split(delimiter);
        for (int yu = 0; yu < CatModNE.Length; yu++)
        {
            if (CatModOffline.Length > IDCatPagosModOffline.Count)
            {
                escribirCatMod = null;
            escribirCatMod = CatModOffline[yu].Split(del);
            
                if (escribirCatMod[0] != "")
                {
                    Int32.TryParse(escribirCatMod[0], out offlineInt);
                    IDCatPagosModOffline.Add(offlineInt);
                    NombreCategoriaModOffline.Add(escribirCatMod[1]);
                    Int32.TryParse(escribirCatMod[2], out offlineInt);
                    intColorModOffline.Add(offlineInt);
                    Int32.TryParse(escribirCatMod[3], out offlineInt);
                    intIconModOffline.Add(offlineInt);
                    Int32.TryParse(escribirCatMod[4], out offlineInt);
                    CodBarModOffline.Add(offlineInt);
                }
                if (IDCatPagosModOffline.Count != 0)
                {
                    ModCategorias();
                }
            }
        }
        CatEliminarNE = PlayerPrefs.GetString("CategoriasEliminar");
        CatElimOffline = CatEliminarNE.Split(delimiter);
        if (CatElimOffline.Length > IDCatPagosElimOffline.Count)
        {
            for (int yo = 0; yo < CatElimOffline.Length; yo++)
        {
            
                if (CatElimOffline[yo] != "0")
                {
                    Int32.TryParse(CatElimOffline[yo], out offlineInt);
                    IDCatPagosElimOffline.Add(offlineInt);
                }
                if (IDCatPagosElimOffline.Count != 0)
                {
                    ElimCategorias();
                }
            }
        }
    }
    public void ModOff()
    {
        if(NomCat.text != "")
        {
            NombreCategoriaPer[ids] = NomCat.text;
            intColorPer[ids] = idColor;
            intIconPer[ids] = idIcono;
            CodBarPer[ids] = codbar;
            IP.codbar = codbar;
            IP.NombreCatPago = NomCat.text;
            IP.NCP.text = NomCat.text;
            IP.idColor = idColor;
            IP.color = CO[idColor];
            IP.idIcono = idIcono;
            IP.Icon.sprite = IC[idIcono];
            IP.IconColor.color = CO[idColor];
            IP.Icono = IC[idIcono];
            
            ob.Click("MisPagosHUB");
            
            for (int yh = 0; yh < CatPagosPer.Count; yh++)
            {
                if (yh == 0)
                    Offline = IDCatPagosLocal[yh] + "," + IDCatPagosPer[yh] + "," + NombreCategoriaPer[yh] + "," + intColorPer[yh] + "," + intIconPer[yh] + "," + CodBarPer[yh];
                else if (yh > 0)
                    Offline += ";" + IDCatPagosLocal[yh] + "," + IDCatPagosPer[yh] + "," + NombreCategoriaPer[yh] + "," + intColorPer[yh] + "," + intIconPer[yh] + "," + CodBarPer[yh];
            }
            PlayerPrefs.SetString("Categorias", Offline);
            if(CatID == 0)
            {
                for(int uo = 0; uo<IDCatPagosLocalAgregarOffline.Count; uo++)
                {
                    if(IDCatPagosLocalAgregarOffline[uo] == CatIDLocal)
                    {
                        NombreCategoriaAgregarOffline[uo] = NomCat.text;
                        intColorAgregarOffline[uo] = idColor + 1;
                        intIconAgregarOffline[uo] = idIcono + 1;
                        CodBarAgregarOffline[uo] = codbar + 1;
                    }
                }
                CatAgregarNE = null;
                for (int gh = 0; gh < NombreCategoriaAgregarOffline.Count; gh++)
                {
                    if (gh == 0)
                    {
                        CatAgregarNE = IDCatPagosLocalAgregarOffline[gh] + "," + NombreCategoriaAgregarOffline[gh] + "," + intColorAgregarOffline[gh] + "," + intIconAgregarOffline[gh] +
                            "," + CodBarAgregarOffline[gh];
                    }
                    else if (gh > 0)
                    {
                        CatAgregarNE += ";" + IDCatPagosLocalAgregarOffline[gh] + "," + NombreCategoriaAgregarOffline[gh] + "," + intColorAgregarOffline[gh] + "," + intIconAgregarOffline[gh] +
                            "," + CodBarAgregarOffline[gh];
                    }
                }
                PlayerPrefs.SetString("CategoriasAgregar", CatAgregarNE);
            }
            else if(CatID != 0)
            {
                IDCatPagosModOffline.Add(CatID);
                NombreCategoriaModOffline.Add(NomCat.text);
                intIconModOffline.Add(idIcono+1);
                intColorModOffline.Add(idColor+1);
                CodBarModOffline.Add(codbar+1);
                CatModNE = null;
                for(int gy = 0; gy<IDCatPagosModOffline.Count; gy++)
                {
                    if (gy == 0)
                    {
                        CatModNE = IDCatPagosModOffline[gy] + "," + NombreCategoriaModOffline[gy] + "," + intIconModOffline[gy] + "," + intColorModOffline[gy] + "," + CodBarModOffline[gy];
                    }
                    else if (gy > 0)
                    {
                        CatModNE += ";" + IDCatPagosModOffline[gy] + "," + NombreCategoriaModOffline[gy] + "," + intIconModOffline[gy] + "," + intColorModOffline[gy] + "," + CodBarModOffline[gy];
                    }
                }
                PlayerPrefs.SetString("CategoriasModificar", CatModNE);
                if(IDCatPagosModOffline.Count != 0)
                {
                    ModCategorias();
                }
            }
            
            
            CatID = 0;
            Quitar();
        }
        else if(NomCat.text == "")
        {


        }
    }
    // Update is called once per frame
    void Update()
    {
        if (CB.isOn == false)
        {
            codbar = 0;
            TCB.SetActive(false);
            ToggleCB.SetActive(true);
        }
        else
        {
            codbar = 1;
            TCB.SetActive(true);
            ToggleCB.SetActive(false);
        }


        if (reg.isStart&&(content.transform.localPosition.y < -5))
        {
            RefBool = true;
            Refrescar.SetActive(true);
        }
        else if (content.transform.localPosition.y > -5)
        {
            //RefBool = false;
            //Refrescar.SetActive(false);
        }
        if (RefBool == true)
        {
            TimerRef += Time.fixedUnscaledDeltaTime;
            pa += 8;
            Quaternion target = Quaternion.Euler(180, 180, pa);
            circulo.transform.rotation = Quaternion.Slerp(transform.rotation, target, pa);
        }
        else
        {
            TimerRef = 0;
        }
        if (TimerRef > 0.1 && IDCatPagosAgregarOffline.Count == 0 && IDCatPagosModOffline.Count == 0 && IDCatPagosElimOffline.Count == 0 && Application.internetReachability != NetworkReachability.NotReachable)
        {
            TimerRef = 0;
            RefBool = false;
            reg.carga = true;
            EliminarServ();
            ReadCard();
        }
    }
    public void Guardar()
    {
        if (NomCat.text != "")
        {
            i++;
            CatID = 0;
            content.transform.localPosition = scrollOriginal;
            NombreCategoriaPer.Add(NomCat.text);
            NoCat = NomCat.text;
            Icon.Add(Ico);
            Colo.Add(IcoColor);
            intColorPer.Add(idColor);
            intIconPer.Add(idIcono);

            IDCatPagosPer.Add(CatID);
            IDCatPagosLocal.Add(i);
            CodBarPer.Add(codbar);
            Posicion.Add(posicion);
            posicion++;
        
            NombreCategoriaAgregarOffline.Add(NomCat.text);
            NomCat.text = null;
            intColorAgregarOffline.Add(idColor + 1);
            intIconAgregarOffline.Add(idIcono + 1);
            IDCatPagosLocalAgregarOffline.Add(i);
            CodBarAgregarOffline.Add(codbar + 1);

            
            
            //ob.Click("Atras");
            
            Offline = null;
            for (int yh = 0; yh < NombreCategoriaPer.Count; yh++)
            {
                if (yh == 0)
                    Offline = IDCatPagosLocal[yh] + "," + IDCatPagosPer[yh] + "," + NombreCategoriaPer[yh] + "," + intColorPer[yh] + "," + intIconPer[yh] + "," + CodBarPer[yh];
                else if (yh > 0)
                    Offline += ";" + IDCatPagosLocal[yh] + "," + IDCatPagosPer[yh] + "," + NombreCategoriaPer[yh] + "," + intColorPer[yh] + "," + intIconPer[yh] + "," + CodBarPer[yh];
            }
            PlayerPrefs.SetString("Categorias", Offline);
            CatAgregarNE = null;
            for(int gh = 0; gh < NombreCategoriaAgregarOffline.Count; gh++)
            {
                if(gh == 0)
                {
                    CatAgregarNE = IDCatPagosLocalAgregarOffline[gh] + "," + NombreCategoriaAgregarOffline[gh] + "," + intColorAgregarOffline[gh] + "," + intIconAgregarOffline[gh] +
                        "," + CodBarAgregarOffline[gh];
                }
                else if (gh > 0)
                {
                    CatAgregarNE += ";" + IDCatPagosLocalAgregarOffline[gh] + "," + NombreCategoriaAgregarOffline[gh] + "," + intColorAgregarOffline[gh] + "," + intIconAgregarOffline[gh] +
                        "," + CodBarAgregarOffline[gh];
                }
            }
            PlayerPrefs.SetString("CategoriasAgregar", CatAgregarNE);
            if(IDCatPagosLocalAgregarOffline.Count != 0)
            {
                AgregarCategorias();
            }
            X();
            botonIcono.sprite = botonIconoBack;
            iconColor.color = iconColorBack;
            IcoColor = iconColorBack;
            Ico = botonIconoBack;
            Quitar();
        }
        else
        {
            ob.DesError.text = "Se debe introducir un nombre para agregar la categoria";
            ob.Click("Errors");
        }
    }

    public void X()
    {
        G = content.transform.position;

        //G2 = casax2.transform.position;

            b++;
            categoriaclone = Instantiate(categoria, G, Quaternion.identity, categorias.transform);
            //Instantiate(casa2, G2, Quaternion.identity, casas2.transform);
            //Instantiate(HistorialCasa, G2, Quaternion.identity, HistorialCasa.transform);
            //categoriaclone = GameObject.Find("TipoPagoX(Clone)");
            categoriaclone.name = "TipoPago" + i;
            //casaclone2 = GameObject.Find("CasaMHx(Clone)");
            //casaclone2.name = "CasaMH" + i;

            //HistorialClone = GameObject.Find("HistorialCasaX(Clone)");
            //HistorialClone.name = "HistorialCasa" + i;
            IP = categoriaclone.GetComponent<InfoCategoria>();
            //InfoMH = casaclone2.GetComponent<InfoCasaMH>();
            CatPagosPer.Add(categoriaclone);
            //Casass2.Add(casaclone2);
            //Historial.Add(HistorialClone);
            IP.NCP.text = NoCat;
            IP.Icono = Ico;
            IP.Icon.sprite = Ico;
            IP.color = IcoColor;
            IP.CatPagoID = CatID;
            IP.IconColor.color = IcoColor;
            IP.CatPagoIDLocal = i;
            IP.codbar = codbar;
            IP.idIcono = idIcono;
            IP.idColor = idColor;
            IP.personalizada = 1;
            IP.ob = ob;
            IP.ap = ap;
            IP.casas = casa;
            IP.NombreCatPago = NoCat;
        IP.posicion = Posicion[posicion-1];
            IP.tp = this;
            if (CB.isOn == true)
            {
                IP.CBB = true;
            }
            else
            {
                IP.CBB = false;
            }
            categoriaclone = null;
            //HistorialClone = null;
            //scroll.movementType = ScrollRect.MovementType.Elastic;
            //scroll.elasticity = 3.25f;

    }
    public void Quitar()
    {
        ob.Click("Atras");
        NomCat.text = null;
        botonIcono.sprite = botonIconoBack;
        iconColor.color = iconColorBack;
        IcoColor = iconColorBack;
        Ico = botonIconoBack;
        HeaderCat.text = "Agregar categoría";
        Menu.SetActive(true);
        Atras.SetActive(true);
        MenuSuperior.SetActive(true);
        BotonGuardar.SetActive(true);
        BotonMod.SetActive(false);
        IP.X = false;
        modificar = false;
    }

    public void Modificar()
    {
        if (NomCat.text != "")

        {
            //IP.NombreCatPago = NomCat.text;
            //IP.NCP.text = NomCat.text;
            //IP.Icono = Ico;
            //IP.color = IcoColor;
            //IP.Icono = botonIcono.sprite;
            //IP.color = iconColor.color;
            //IP.idColor = idColor;
            //IP.idIcono = idIcono;
            //Icon[ids] = Ico;
            //Colo[ids] = IcoColor;
            //IP.Icon.sprite = Ico;
            //IP.IconColor.color = IcoColor;
            //NombreCategoria[ids] = NomCat.text;

            ModCategorias();
            IP.X = false;
            modificar = false;
            Quitar();
        }
        else
        {
            ob.DesError.text = "Se debe introducir un nombre para agregar la categoria";
            ob.Click("Errors");
        }
    }
    public void Eliminar()
    {
        //Info.idCard = (i - 1);
        categoriat = CatPagosPer[pos - 10];
        c = pos-10;

        for (int d = pos; d < (posicion - 1); d++)
        {
            c++;
            Posicion[c] -= 1;
            mover = CatPagosPer[c];
            IP2 = mover.GetComponent<InfoCategoria>();
            IP2.posicion--;

        }
        //if(h == 0)
        //{
        //    fp.s = 0;
        //}

        //if (c == fp.i && fp.i == 0)
        //{
        //    fp.s = fp.tarjetax.transform.position.y;
        //    i++;
        //}

        Destroy(categoriat);
        //CatPagos.RemoveAt(pos);
        NombreCategoriaPer.RemoveAt(pos - 10);
        Icon.RemoveAt(pos);
        Colo.RemoveAt(pos);
        CatPagosPer.RemoveAt(pos - 10);
        IDCatPagosLocal.RemoveAt(pos - 10);
        IDCatPagosPer.RemoveAt(pos - 10);
        intColorPer.RemoveAt(pos - 10);
        intIconPer.RemoveAt(pos - 10);
        CodBarPer.RemoveAt(pos - 10);
        Posicion.RemoveAt(pos);

        posicion--;
        b--;
        Offline = null;
        for (int yh = 0; yh < CatPagosPer.Count; yh++)
            {
            if (yh == 0)
                Offline = IDCatPagosLocal[yh] + "," + IDCatPagosPer[yh] + "," + NombreCategoriaPer[yh] + "," + intColorPer[yh] + "," + intIconPer[yh] + "," + CodBarPer[yh];
            else if (yh > 0)
                Offline += ";" + IDCatPagosLocal[yh] + "," + IDCatPagosPer[yh] + "," + NombreCategoriaPer[yh] + "," + intColorPer[yh] + "," + intIconPer[yh] + "," + CodBarPer[yh];
        }
        PlayerPrefs.SetString("Categorias", Offline);

        if(CatID == 0)
        {
            for(int jui = 0; IDCatPagosLocalAgregarOffline.Count>jui; jui++)
            {
                if(IDCatPagosLocalAgregarOffline[jui] == CatIDLocal)
                {
                    IDCatPagosLocalAgregarOffline.RemoveAt(jui);
                    NombreCategoriaAgregarOffline.RemoveAt(jui);
                    intIconAgregarOffline.RemoveAt(jui);
                    intColorAgregarOffline.RemoveAt(jui);
                    CodBarAgregarOffline.RemoveAt(jui);
                }
            }
            CatAgregarNE = null;
            for (int gh = 0; gh < NombreCategoriaAgregarOffline.Count; gh++)
            {
                if (gh == 0)
                {
                    CatAgregarNE = IDCatPagosLocalAgregarOffline[gh] + "," + NombreCategoriaAgregarOffline[gh] + "," + intColorAgregarOffline[gh] + "," + intIconAgregarOffline[gh] +
                        "," + CodBarAgregarOffline[gh];
                }
                else if (gh > 0)
                {
                    CatAgregarNE += ";" + IDCatPagosLocalAgregarOffline[gh] + "," + NombreCategoriaAgregarOffline[gh] + "," + intColorAgregarOffline[gh] + "," + intIconAgregarOffline[gh] +
                        "," + CodBarAgregarOffline[gh];
                }
            }
            PlayerPrefs.SetString("CategoriasAgregar", CatAgregarNE);
        }
        else if(CatID != 0)
        {
            IDCatPagosElimOffline.Add(CatID);
            CatEliminarNE = null;
            for(int gyh = 0; IDCatPagosElimOffline.Count> gyh; gyh++)
            {
                if(gyh == 0)
                {
                    CatEliminarNE = IDCatPagosElimOffline[gyh] + ";";
                }
                else if(gyh != 0)
                {
                    CatEliminarNE += IDCatPagosElimOffline[gyh] + ";";
                }
            }
            PlayerPrefs.SetString("CategoriasEliminar", CatEliminarNE);
            if(IDCatPagosElimOffline.Count != 0)
            {
                ElimCategorias();
            }
        }
        //casa.NNC.RemoveAt(ids);
        //casa.UbC.RemoveAt(ids);
        //casa.CodC.RemoveAt(ids);
        //casa.Casass.RemoveAt(ids);
        //casa.ID.RemoveAt(ids);
        //casa.b--;

        //casa.i--;
        //c = 0;
        //fp.TCL[ids] = TCt;
        //fp.TCmL[ids] = TCm;
        //fp.NTL[ids] = NombTitu;
        //fp.ApL[ids] = Apell;
        //fp.ML[ids] = Mes;
        //fp.AnL[ids] = Ano;
        //fp.NickL[ids] = NickName;
        //fp.BancL[ids] = Banco;
        //fp.TipoL[ids] = Tipo;
        //fp.MarcaL[ids] = Marca;
        //TCL.Add(TCt);
        //TCmL.Add(TCm);
        //NTL.Add(NombTitu);
        //ApL.Add(Apell);
        //ML.Add(Mes);
        //AnL.Add(Ano);
        //NickL.Add(NickName);
        //BancL.Add(Banco);
        //TipoL.Add(Tipo);
        //MarcaL.Add(Marca);
        //ID.Add(i);
        //scroll.movementType = ScrollRect.MovementType.Elastic;
        //scroll.elasticity = 3.25f;
        //ob.Click("Atras");
    }
    public void AgregarCategorias()
    {
        if (NombreCategoriaAgregarOffline.Count != 0)
        {
            WWWForm Form = new WWWForm();
            Form.AddField("IdUsuario", reg.id);
            Form.AddField("NombreCategoria", NombreCategoriaAgregarOffline[0]);
            Form.AddField("ColorCategoria", intColorAgregarOffline[0]);
            Form.AddField("IconoCategoria", intIconAgregarOffline[0]);
            Form.AddField("CodigoBarras", CodBarAgregarOffline[0]);
            WebServices.AgregarCategoria(Form, SuccessAgregarCategoria, ErrorAgregarCategoria);
        }
        //reg.carga = true;
        //ob.DesError.text = "No se pudo agregar la categoría, checa tu conexión e intentalo de nuevo";
    }
    void SuccessAgregarCategoria(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            error = ("" + ((MessageArg<string>)e).responseValue);
            reg.carga = false;
            Debug.Log("");
        }
        else if (e is MessageArg<DBCategoria>)
        {
            //right = true;
            //register1 = 1;
            //PlayerPrefs.SetInt("Registrado", register1);
            DBCategoria categoria = ((MessageArg<DBCategoria>)e).responseValue;
            for(int op = 0; op<CatPagosPer.Count; op++)
            {
                if(CatPagosPer[op].GetComponent<InfoCategoria>().CatPagoIDLocal == IDCatPagosLocalAgregarOffline[0])
                {
                    CatPagosPer[op].GetComponent<InfoCategoria>().CatPagoID = categoria.IdCategoria;
                    IDCatPagosPer[op] = categoria.IdCategoria;
                }
            }
            IDCatPagosLocalAgregarOffline.RemoveAt(0);
            NombreCategoriaAgregarOffline.RemoveAt(0);
            intColorAgregarOffline.RemoveAt(0);
            intIconAgregarOffline.RemoveAt(0);
            CodBarAgregarOffline.RemoveAt(0);


            Offline = null;
            for (int yh = 0; yh < IDCatPagosPer.Count; yh++)
            {
                if (yh == 0)
                    Offline = IDCatPagosLocal[yh] + "," + IDCatPagosPer[yh] + "," + NombreCategoriaPer[yh] + "," + intColorPer[yh] + "," + intIconPer[yh] + "," + CodBarPer[yh];
                else if (yh > 0)
                    Offline += ";" + IDCatPagosLocal[yh] + "," + IDCatPagosPer[yh] + "," + NombreCategoriaPer[yh] + "," + intColorPer[yh] + "," + intIconPer[yh] + "," + CodBarPer[yh];
            }
            PlayerPrefs.SetString("Categorias", Offline);
            CatAgregarNE = null;
            for (int gh = 0; gh < NombreCategoriaAgregarOffline.Count; gh++)
            {
                if (gh == 0)
                {
                    CatAgregarNE = IDCatPagosLocalAgregarOffline[gh] + "," + NombreCategoriaAgregarOffline[gh] + "," + intColorAgregarOffline[gh] + "," + intIconAgregarOffline[gh] +
                        "," + CodBarAgregarOffline[gh];
                }
                else if (gh > 0)
                {
                    CatAgregarNE += ";" + IDCatPagosLocalAgregarOffline[gh] + "," + NombreCategoriaAgregarOffline[gh] + "," + intColorAgregarOffline[gh] + "," + intIconAgregarOffline[gh] +
                        "," + CodBarAgregarOffline[gh];
                }
            }
            PlayerPrefs.SetString("CategoriasAgregar", CatAgregarNE);
            if (IDCatPagosLocalAgregarOffline.Count != 0)
            {
                AgregarCategorias();
            }
            //EliminarServ();
            //ob.Click("Aviso");
            //ob.avisotexto.text = "Se agregó correctamente la categoría";
            //ReadCard();
            //Quitar();
            error = categoria.IdCategoria.ToString();
            //id = user.Id.Value;

        }
    }
    public void ModCategorias()
    {
        WWWForm Form = new WWWForm();
        Form.AddField("IdUsuario", reg.id);
        Form.AddField("IdCategoria", IDCatPagosModOffline[0]);
        Form.AddField("NombreCategoria", NombreCategoriaModOffline[0]);
        Form.AddField("ColorCategoria", intColorModOffline[0]);
        Form.AddField("IconoCategoria", intIconModOffline[0]);
        Form.AddField("CodigoBarras", CodBarModOffline[0]);
        
        WebServices.ModCategoria(Form, SuccessModCategoria, Error);
        //reg.carga = true;
        //ob.DesError.text = "No se pudo modificar la categoría, checa tu conexión e intentalo de nuevo";
    }
    void SuccessModCategoria(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            error = ("" + ((MessageArg<string>)e).responseValue);
            reg.carga = false;
            Debug.Log("");
        }
        else if (e is MessageArg<DBCategoria>)
        {
            //right = true;
            //register1 = 1;
            //PlayerPrefs.SetInt("Registrado", register1);
            DBCategoria categoria = ((MessageArg<DBCategoria>)e).responseValue;
            IDCatPagosModOffline.RemoveAt(0);
            NombreCategoriaModOffline.RemoveAt(0);
            intColorModOffline.RemoveAt(0);
            intIconModOffline.RemoveAt(0);
            CodBarModOffline.RemoveAt(0);
            CatModNE = null;
            for (int gy = 0; gy < IDCatPagosModOffline.Count; gy++)
            {
                if (gy == 0)
                {
                    CatModNE = IDCatPagosModOffline[gy] + "," + NombreCategoriaModOffline[gy] + "," + intIconAgregarOffline[gy] + "," + intColorModOffline[gy] + "," + CodBarModOffline[gy];
                }
                else if (gy > 0)
                {
                    CatModNE += ";" + IDCatPagosModOffline[gy] + "," + NombreCategoriaModOffline[gy] + "," + intIconAgregarOffline[gy] + "," + intColorModOffline[gy] + "," + CodBarModOffline[gy];
                }
            }
            PlayerPrefs.SetString("CategoriasModificar", CatModNE);
            if (IDCatPagosModOffline.Count != 0)
            {
                ModCategorias();
            }
            //EliminarServ();
            //ob.Click("Aviso");
            //ob.avisotexto.text = "Se modificó correctamente la categoría";
            //ReadCard();
            //Quitar();
            error = categoria.IdCategoria.ToString();
            //id = user.Id.Value;

        }
    }
    public void ElimCategorias()
    {
        if (IDCatPagosElimOffline.Count != 0)
        {
            WWWForm Form = new WWWForm();
            Form.AddField("IdUsuario", reg.id);
            Form.AddField("IdCategoria", IDCatPagosElimOffline[0]);
            WebServices.ElimCategoria(Form, SuccessElimCategoria, ErrorElimCat);
        }
        //reg.carga = true;
        //ob.DesError.text = "No se pudo eliminar la categoría, checa tu conexión e intentalo de nuevo";
    }
    void SuccessElimCategoria(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            error = ("" + ((MessageArg<string>)e).responseValue);
            if (IDCatPagosElimOffline[0] == 0)
            {
                IDCatPagosElimOffline.RemoveAt(0);
                CatEliminarNE = null;
                for (int gyh = 0; IDCatPagosElimOffline.Count > gyh; gyh++)
                {
                    if (gyh == 0)
                    {
                        CatEliminarNE = IDCatPagosElimOffline[gyh] + ";";
                    }
                    else if (gyh != 0)
                    {
                        CatEliminarNE += IDCatPagosElimOffline[gyh] + ";";
                    }
                }
                PlayerPrefs.SetString("CategoriasEliminar", CatEliminarNE);
            }
                ElimCategorias();
            reg.carga = false;
            //Debug.Log("");
        }
        else if (e is MessageArg<DBCategoria>)
        {
            //right = true;
            //register1 = 1;
            //PlayerPrefs.SetInt("Registrado", register1);
            DBCategoria categoria = ((MessageArg<DBCategoria>)e).responseValue;
            IDCatPagosElimOffline.RemoveAt(0);
            CatEliminarNE = null;
            for (int gyh = 0; IDCatPagosElimOffline.Count > gyh; gyh++)
            {
                if (gyh == 0)
                {
                    CatEliminarNE = IDCatPagosElimOffline[gyh] + ";";
                }
                else if (gyh != 0)
                {
                    CatEliminarNE += IDCatPagosElimOffline[gyh] + ";";
                }
            }
            PlayerPrefs.SetString("CategoriasEliminar", CatEliminarNE);
            if (IDCatPagosElimOffline.Count != 0)
            {
                ElimCategorias();
            }
            //ob.Click("Aviso");
            //ob.avisotexto.text = "Se eliminó correctamente la categoría";
            //EliminarServ();
            //ReadCard();
            //Quitar();
            error = categoria.IdCategoria.ToString();
            //id = user.Id.Value;

        }
    }
    void Error(System.Object Sender, EventArgs e)
    {
        error = ("" + ((MessageArg<string>)e).responseValue);
        reg.carga = false;
        //ob.Click("Errors");

    }
    void ErrorModCat(System.Object Sender, EventArgs e)
    {
        error = ("" + ((MessageArg<string>)e).responseValue);
        //reg.carga = false;
        //ob.Click("Errors");
        ModCategorias();

    }
    void ErrorElimCat(System.Object Sender, EventArgs e)
    {
        error = ("" + ((MessageArg<string>)e).responseValue);
        //reg.carga = false;
        //ob.Click("Errors");
        ElimCategorias();

    }
    void ErrorAgregarCategoria(System.Object Sender, EventArgs e)
    {
        error = ("" + ((MessageArg<string>)e).responseValue);
        //reg.carga = false;
        //ob.Click("Errors");
        AgregarCategorias();

    }
    public void ObtenerCategorias()
    {
        WWWForm Form = new WWWForm();
        Form.AddField("IdUsuario", reg.id);
        WebServices.ObtCategoria(Form, SuccessObtCategoria, Error);
        //ob.DesError.text = "No se pudo obtener las categorias, checa tu conexión e intentalo de nuevo";
    }
    void SuccessObtCategoria(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            error = ("" + ((MessageArg<string>)e).responseValue);
            Categorias.Clear();
            Debug.Log("");
        }
        else if (e is MessageArg<List<DBCategoria>>)
        {
            //right = true;
            //register1 = 1;
            //User user = ((MessageArg<User>)e).responseValue;
            //PlayerPrefs.SetInt("Registrado", register1);
            //User user = ((MessageArg<User>)e).responseValue;
            Categorias = ((MessageArg<List<DBCategoria>>)e).responseValue;
            StartCoroutine(DelayGetCategorias());

            //user.casas = ((MessageArg<List<DBCasate>>)e).responseValue;
            //= ((MessageArg<List<DBCasa>>)e).responseValue;
            //Casas = user.casas;
            //error = dbcasas.Id.ToString();
            //mp.ObtenerMP();
            //ingresa = true;
            //mp.Ver();

            //ob.Click("Inicios");

        }
    }
    public void ReadCard()
    {
        //reg.cargando = true;
        EliminarServ();
        ObtenerCategorias();
        //ob.Click("MisCasasHUB");
        //if (ID == save.id)
        //{

        //}
    }
    IEnumerator DelayGetCategorias()
    {
        //reg.cargando = true;
        if (reg.ingresa == false)
        {
            //reg.CargateMenor();
        }
        yield return new WaitForSeconds(.5f);
        if(reg.ingresa == true)
        ap.ben.ReadCardBeneficiario();
        for (y = 0; y < Categorias.Count; y++)
        {
            categoriate = Categorias[y];
            
            if (CatPagosPer.Count < Categorias.Count)
            {
                IDCatPagos.Add(categoriate.IdCategoria);
                IDCatPagosPer.Add(categoriate.IdCategoria);
                i++;
                NombreCategoriaPer.Add(categoriate.NombreCategoria);
                NoCat = categoriate.NombreCategoria;
                IDCatPagosLocal.Add(i);
                IcoColor = CO[categoriate.ColorCategoria - 1];
                Ico = IC[categoriate.IconoCategoria -1];
                Icon.Add(Ico);
                Colo.Add(IcoColor);
                idColor = categoriate.ColorCategoria - 1;
                intColorPer.Add(idColor);
                idIcono = categoriate.IconoCategoria - 1;
                intIconPer.Add(idIcono);
                codbar = categoriate.CodigoBarras - 1;
                CodBarPer.Add(codbar);
                CatID = categoriate.IdCategoria;
                Offline = null;
                for (int yh = 0; yh < IDCatPagosPer.Count; yh++)
                {
                    if (yh == 0)
                        Offline = IDCatPagosLocal[yh] + "," + IDCatPagosPer[yh] + "," + NombreCategoriaPer[yh] + "," + intColorPer[yh] + "," + intIconPer[yh] + "," + CodBarPer[yh];
                    else if (yh > 0)
                        Offline += ";" + IDCatPagosLocal[yh] + "," + IDCatPagosPer[yh] + "," + NombreCategoriaPer[yh] + "," + intColorPer[yh] + "," + intIconPer[yh] + "," + CodBarPer[yh];
                }
                PlayerPrefs.SetString("Categorias", Offline);


                //else {
                //    FotoTomada = true;
                //}
                Posicion.Add(posicion);
                posicion++;
                //i = categoriate.id;
                X();
            }

        }
        if(y == Categorias.Count)
        {
            //reg.cargando = false;
        }
    }
    public void EliminarServ()
    {
        c = CatPagosPer.Count;
        //Info.idCard = (i - 1);
        for (int yo = 0; yo < c; yo++)
        {

            ids = 0;
            categoriat = CatPagosPer[ids];
            //c = ids + 9;

            //for (int d = ids; d < (posicion - 1); d++)
            //{
            //    c++;
            //    Posicion[c] -= 1;
            //    mover = CatPagosPer[c-9];
            //    IP2 = mover.GetComponent<InfoCategoria>();
            //    IP2.posicion--;

            //}
            //if(h == 0)
            //{
            //    fp.s = 0;
            //}

            //if (c == fp.i && fp.i == 0)
            //{
            //    fp.s = fp.tarjetax.transform.position.y;
            //    i++;
            //}

            Destroy(categoriat);
            CatPagosPer.RemoveAt(ids);
            NombreCategoriaPer.RemoveAt(ids);
            Icon.RemoveAt(ids);
            Colo.RemoveAt(ids);
            IDCatPagosPer.RemoveAt(ids);
            IDCatPagosLocal.RemoveAt(ids);
            intIconPer.RemoveAt(ids);
            intColorPer.RemoveAt(ids);
            CodBarPer.RemoveAt(ids);
            Posicion.RemoveAt(ids);
            posicion--;
            b--;
            //ob.Click("Atras");
        }
    }
}
