﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TPBoton : MonoBehaviour {
    public TP tp;
    public int idBoton, idColor;
    public Image Children;
    public bool color, icono;
    public Sprite ico, boton;

	// Use this for initialization
	void Awake() {

        this.GetComponent<Button>().onClick.AddListener(this.Click);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void Click()
    {
        if (color == true)
        {
            tp.IcoColor = this.GetComponent<Image>().color;
            tp.iconColor.color = this.GetComponent<Image>().color;
            tp.idColor = idColor;
            
            tp.ob.Click("ElegirIconoColorQuitar");
        }
        if(icono == true)
        {
            //Children = GetComponentInChildren<Image>();
            ico = Children.sprite;
            tp.botonIcono.sprite = Children.sprite; 
            tp.Ico = ico;
            
            tp.idIcono = idBoton;
            tp.ob.Click("ElegirIconoColorQuitar");
        }
    }
}
