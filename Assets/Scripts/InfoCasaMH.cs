﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Globalization;
using System.IO;

public class InfoCasaMH : MonoBehaviour
{
    CultureInfo ci = new CultureInfo("es-mx");
    public int CasaID, es, os, idCat, idIcon, idCol, idPagCon, idPagEn, idPagEnCol, idPagEnIcono, CasaIDLocal;
    public string NickName, años, meses, dias, añosel, messel, añomessel, PagCon, PagEn, Cat, MontoPago, fecha;
    public Text NN, Casa, TotalHistorialTexto;
    public GameObject Historial, Año, Mes, PagoHistorial, PagoHistorialX, ContenidoAño, ContenidoMes, ContenidoPagos, AñoSel, MesSel;
    public ObjectManager ob;
    public int i, d, f, l, m, n, o, w, x, z, y;
    public List<string> AñoN, MesN, MesAñoN, MesP, MesAñoP, Pagos, FechaPago, PagadoCon, PagadoEn, Categoria, PrecioPago = new List<string>();
    public List<GameObject> Años, Meses, MesAño, PagoHistoriales = new List<GameObject>();
    public List<int> idCategoria, idIcono, idColor, idPagadoCon, idPagadoEn, idPagadoEnColor, idPagadoEnIcono, AñoCreado, MesCreado, PagoCreado = new List<int>();
    public List<string> NombreAño = new List<string>();
    public AP ap;
    public InfoAno IA, IA2;
    public InfoMes IM;
    public InfoPagoHistorial IPH;
    public bool añ, ms, pagado, mesaño;
    public Vector2 scrollOriginal, LimiteSuperior, LimiteInferior, PrimerTP, UltimoTP, posicioncontent, scrollinferior, contentActual;
    public Vector2 scrollOriginalMes, LimiteSuperiorMes, LimiteInferiorMes, PrimerTPMes, UltimoTPMes, posicioncontentMes, scrollinferiorMes, contentActualMes;
    public Vector2 scrollOriginalPagos, LimiteSuperiorPagos, LimiteInferiorPagos, PrimerTPPagos, UltimoTPPagos, posicioncontentPagos, scrollinferiorPagos, contentActualPagos;
    public GameObject content, limsup, liminf;
    public GameObject contentMes, limsupMes, liminfMes;
    public GameObject contentPagos, limsupPagos, liminfPagos;
    public bool bajo, bajoMes, bajoPagos, exacto, cambio;
    public float total;
    public bool FotoTomada;
    public int fotos, idFoto, posicion;
    public Sprite foto;
    public Image casapersonalizada;
    public GameObject casaper;
    public Casas casa;
    public string nombreCasa;

    // Use this for initialization
    void Start()
    {
        casa = GameObject.Find("AgregarMisCasas").GetComponent<Casas>();
        ContenidoAño = GameObject.Find("ContentAño");
        ContenidoMes = GameObject.Find("ContentMes");
        ContenidoPagos = GameObject.Find("ContentPagosHistorial");
        content = GameObject.Find("ContentAño");
        contentMes = GameObject.Find("ContentMes");
        contentPagos = GameObject.Find("ContentPagos");
        TotalHistorialTexto = GameObject.Find("TotalHistorialTexto").GetComponent<Text>();
        //StartCoroutine(Delay());
        /*if (File.Exists(Application.persistentDataPath + "/" + CasaID + ".jpg"))
        {
            byte[] bytes = File.ReadAllBytes(Application.persistentDataPath + "/" + CasaID + ".jpg");
            Texture2D fotota = new Texture2D(1600, 1600);
            fotota.LoadImage(bytes);
            foto = Sprite.Create(fotota, new Rect(/*(fotota.width / 4)/0, /*(fotota.height / 4)/0, (fotota.width // 2/), (fotota.height / 2/)), Vector2.zero);
            casaper.SetActive(true);
            casapersonalizada.sprite = foto;
        }*/
        System.GC.Collect();

    }
    IEnumerator Delay()
    {
        yield return new WaitForSeconds(1f);
        if (File.Exists(Application.persistentDataPath + "/" + CasaID + ".jpg"))
        {
            byte[] bytes = File.ReadAllBytes(Application.persistentDataPath + "/" + CasaID + ".jpg");
            Texture2D fotota = new Texture2D(1600, 1600);
            fotota.LoadImage(bytes);
            foto = Sprite.Create(fotota, new Rect(/*(fotota.width / 4)*/0, /*(fotota.height / 4)*/0, (fotota.width /*/ 2*/), (fotota.height /* 2*/)), Vector2.zero);
            casaper.SetActive(true);
            //print("hist casa");

            casapersonalizada.sprite = foto;
            casapersonalizada.preserveAspect = true;
            OrderImages(casapersonalizada);
        }
        System.GC.Collect();

    }

    // Update is called once per frame
    void Update()
    {
        if (File.Exists(Application.persistentDataPath + "/" + CasaID + ".jpg"))
        {
            casaper.SetActive(true);
            FotoTomada = true;
        }
        else
        {
            casaper.SetActive(false);
            FotoTomada = false;
        }
        if (cambio == true)
        {
            cambio = false;
            if (File.Exists(Application.persistentDataPath + "/" + CasaID + ".jpg"))
            {
                byte[] bytes = File.ReadAllBytes(Application.persistentDataPath + "/" + CasaID + ".jpg");
                Texture2D fotota = new Texture2D(1600, 1600);
                fotota.LoadImage(bytes);
                foto = Sprite.Create(fotota, new Rect(/*(fotota.width / 4)*/0, /*(fotota.height / 4)*/0, (fotota.width /*/ 2*/), (fotota.height /* 2*/)), Vector2.zero);
                casaper.SetActive(true);
                casapersonalizada.sprite = foto;
                casapersonalizada.preserveAspect = true;
                casapersonalizada.SetNativeSize();
                OrderImages(casapersonalizada);
            }
            System.GC.Collect();

        }
        if (z == 0)
        {
            if (z == 0)
            {
                total = 0;
                //TotalHistorialTexto.text = "$0.00";
            }
        }
        if (z > 0)
        {
            TotalHistorialTexto.text = total.ToString("C", ci);

        }

        años = DateTime.Now.Year.ToString();
        meses = DateTime.Now.Month.ToString();
        dias = DateTime.Now.Day.ToString();
        fecha = DateTime.Now.ToString("d/M/yyyy");
        //contentActual = content.transform.localPosition;
        //posicioncontent = content.transform.localPosition;

        //if (w > 1)
        //{
        //    PrimerTP = Años[0].transform.position;
        //    UltimoTP = Años[w - 1].transform.position;
        //    if (Años[0].transform.position.y < LimiteSuperior.y)
        //    {
        //        content.transform.localPosition = scrollOriginal;
        //    }
        //    if (Años[w - 1].transform.position.y < LimiteInferior.y)
        //    {
        //        bajo = true;
        //    }
        //    if (bajo == false)
        //    {
        //        content.transform.localPosition = scrollOriginal;
        //    }
        //    if (bajo == true && Años[w - 1].transform.position.y > LimiteInferior.y)
        //    {
        //        scrollinferior = new Vector2(contentActual.x, contentActual.y - 10);
        //        content.transform.localPosition = scrollinferior;
        //        bajo = false;
        //    }

        //}
        //if (w == 1)
        //{
        //    PrimerTP = Años[0].transform.position;
        //    UltimoTP = Años[0].transform.position;
        //    if (Años[0].transform.position.y < LimiteSuperior.y)
        //    {
        //        content.transform.localPosition = scrollOriginal;
        //    }
        //    if (Años[0].transform.position.y < LimiteInferior.y)
        //    {
        //        bajo = true;
        //    }
        //    if (bajo == false)
        //    {
        //        content.transform.localPosition = scrollOriginal;
        //    }
        //    if (bajo == true && Años[0].transform.position.y > LimiteInferior.y)
        //    {
        //        scrollinferior = new Vector2(contentActual.x, contentActual.y - 10);
        //        content.transform.localPosition = scrollinferior;
        //        bajo = false;
        //    }

        //}
        //if (w == 0)
        //{
        //    content.transform.position = scrollOriginal;
        //}
        //contentActualMes = contentMes.transform.localPosition;
        //posicioncontentMes = contentMes.transform.localPosition;

        //if (x > 1)
        //{
        //    PrimerTPMes = Meses[0].transform.position;
        //    UltimoTPMes = Meses[x - 1].transform.position;
        //    if (Meses[0].transform.position.y < LimiteSuperiorMes.y)
        //    {
        //        contentMes.transform.localPosition = scrollOriginalMes;
        //    }
        //    if (Meses[x - 1].transform.position.y < LimiteInferiorMes.y)
        //    {
        //        bajoMes = true;
        //    }
        //    if (bajoMes == false)
        //    {
        //        contentMes.transform.localPosition = scrollOriginalMes;
        //    }
        //    if (bajoMes == true && Meses[x - 1].transform.position.y > LimiteInferiorMes.y)
        //    {
        //        scrollinferiorMes = new Vector2(contentActualMes.x, contentActualMes.y - 10);
        //        contentMes.transform.localPosition = scrollinferiorMes;
        //        bajoMes = false;
        //    }

        //}
        //if (x == 1)
        //{
        //    PrimerTPMes = Meses[0].transform.position;
        //    UltimoTPMes = Meses[0].transform.position;
        //    if (Meses[0].transform.position.y < LimiteSuperiorMes.y)
        //    {
        //        contentMes.transform.localPosition = scrollOriginalMes;
        //    }
        //    if (Meses[0].transform.position.y < LimiteInferiorMes.y)
        //    {
        //        bajoMes = true;
        //    }
        //    if (bajoMes == false)
        //    {
        //        contentMes.transform.localPosition = scrollOriginalMes;
        //    }
        //    if (bajoMes == true && Meses[0].transform.position.y > LimiteInferiorMes.y)
        //    {
        //        scrollinferiorMes = new Vector2(contentActualMes.x, contentActualMes.y - 10);
        //        contentMes.transform.localPosition = scrollinferiorMes;
        //        bajoMes = false;
        //    }

        //}
        //if (x == 0)
        //{
        //    contentMes.transform.position = scrollOriginalMes;
        //}

        //contentActualPagos = contentPagos.transform.localPosition;
        //posicioncontentPagos = contentPagos.transform.localPosition;

        //if (z > 1)
        //{
        //    PrimerTPPagos = PagoHistoriales[0].transform.position;
        //    UltimoTPPagos = PagoHistoriales[z - 1].transform.position;
        //    if (PagoHistoriales[0].transform.position.y < LimiteSuperiorPagos.y)
        //    {
        //        contentPagos.transform.localPosition = scrollOriginalPagos;
        //    }
        //    if (PagoHistoriales[z - 1].transform.position.y < LimiteInferiorPagos.y)
        //    {
        //        bajoPagos = true;
        //    }
        //    if (bajoPagos == false)
        //    {
        //        contentPagos.transform.localPosition = scrollOriginalPagos;
        //    }
        //    if (bajoPagos == true && PagoHistoriales[z - 1].transform.position.y > LimiteInferiorPagos.y)
        //    {
        //        scrollinferiorPagos = new Vector2(contentActualPagos.x, contentActualPagos.y - 10);
        //        contentPagos.transform.localPosition = scrollinferiorPagos;
        //        bajoPagos = false;
        //    }

        //}
        //if (z == 1)
        //{
        //    PrimerTPPagos = PagoHistoriales[0].transform.position;
        //    UltimoTPPagos = PagoHistoriales[0].transform.position;
        //    if (PagoHistoriales[0].transform.position.y < LimiteSuperiorPagos.y)
        //    {
        //        contentPagos.transform.localPosition = scrollOriginalPagos;
        //    }
        //    if (PagoHistoriales[0].transform.position.y < LimiteInferiorPagos.y)
        //    {
        //        bajoPagos = true;
        //    }
        //    if (bajoPagos == false)
        //    {
        //        contentPagos.transform.localPosition = scrollOriginalPagos;
        //    }
        //    if (bajoPagos == true && PagoHistoriales[0].transform.position.y > LimiteInferiorPagos.y)
        //    {
        //        scrollinferiorPagos = new Vector2(contentActualPagos.x, contentActualPagos.y - 10);
        //        contentPagos.transform.localPosition = scrollinferiorPagos;
        //        bajoPagos = false;
        //    }

        //}
        //if (z == 0)
        //{
        //    contentPagos.transform.position = scrollOriginalPagos;
        //}
    }

    public void OrderImages(Image img)
    {
        if (img.sprite != null)
        {
            img.GetComponent<AspectRatioFitter>().aspectMode = AspectRatioFitter.AspectMode.EnvelopeParent;
            if (img.sprite.texture.width > img.sprite.texture.height)
            {
                img.GetComponent<AspectRatioFitter>().aspectRatio = 2f;
            }
            else if (img.sprite.texture.width < img.sprite.texture.height)
            {
                //casapersonalizada.GetComponent<AspectRatioFitter>().aspectMode = AspectRatioFitter.AspectMode.WidthControlsHeight;
                casapersonalizada.GetComponent<AspectRatioFitter>().aspectRatio = 0.001f;
            }
        }

    }

    public void Pago()
    {
        if (ap.pagarahora == false)
        {
            //print("saquese");
            //años = DateTime.Now.Year.ToString();
            //meses = DateTime.Now.Month.ToString();
            //dias = DateTime.Now.Day.ToString();
            //fecha = DateTime.Now.ToString("d/M/yyyy");
        }
        if (ap.pagoretrasado == true && ap.pagarahora == true)
        {
            años = ap.AñoP.text;
            //meses = ap.MesP.text;
            if (ap.MesP.text == "Enero")
            {
                meses = "1";
            }
            if (ap.MesP.text == "Febrero")
            {
                meses = "2";
            }
            if (ap.MesP.text == "Marzo")
            {
                meses = "3";
            }
            if (ap.MesP.text == "Abril")
            {
                meses = "4";
            }
            if (ap.MesP.text == "Mayo")
            {
                meses = "5";
            }
            if (ap.MesP.text == "Junio")
            {
                meses = "6";
            }
            if (ap.MesP.text == "Julio")
            {
                meses = "7";
            }
            if (ap.MesP.text == "Agosto")
            {
                meses = "8";
            }
            if (ap.MesP.text == "Septiembre")
            {
                meses = "9";
            }
            if (ap.MesP.text == "Octubre")
            {
                meses = "10";
            }
            if (ap.MesP.text == "Noviembre")
            {
                meses = "11";
            }
            if (ap.MesP.text == "Diciembre")
            {
                meses = "12";
            }
            dias = ap.DiaP.text;
            fecha = dias + "/" + meses + "/" + años;
        }
        d++;
        pagado = true;
        if (pagado == true)
        {

            MesP.Add(meses);
            MesAñoP.Add(años);
            PagadoCon.Add(PagCon);
            PagadoEn.Add(PagEn);
            Categoria.Add(Cat);
            idCategoria.Add(idCat);
            idIcono.Add(idIcon);
            idColor.Add(idCol);
            idPagadoEn.Add(idPagEn);
            idPagadoCon.Add(idPagCon);
            idPagadoEnColor.Add(idPagEnCol);
            idPagadoEnIcono.Add(idPagEnIcono);
            PrecioPago.Add(MontoPago);
            //print(fecha);
            FechaPago.Add(fecha);
            Pagos.Add(Cat);
            PagoCreado.Add(0);
            pagado = false;
        }
        if (l > 0)
        {
            for (i = 0; l > i; i++)
            {
                if (años == AñoN[i])
                {
                    es++;
                    n = i;
                }


            }
            if (l == i && es == 0)
            {
                AñoN.Add(años);
                AñoCreado.Add(0);
                l++;
                es = 0;
            }
            es = 0;
        }
        if (es == 0 && l == 0)
        {
            AñoN.Add(años);
            AñoCreado.Add(0);
            l++;
        }


        f++;
        if (m > 0)
        {
            for (i = 0; m > i; i++)
            {
                if (MesN[i] == meses && MesAñoN[i] == años)
                {
                    os++;

                    o = i;
                }

            }
            //os = 0;

        }

        if (m > 0 && os == 0)
        {
            mesaño = true;
        }

        if (mesaño == true)
        {
            MesN.Add(meses);
            MesAñoN.Add(años);
            m++;
            MesCreado.Add(0);
            mesaño = false;
            os = 0;
        }
        os = 0;
        if (m == 0)
        {
            MesN.Add(meses);
            MesAñoN.Add(años);
            m++;
            MesCreado.Add(0);
        }


    }
    public void ClickNombre()
    {
        for (i = 0; l > i; i++)
        {
            if (AñoCreado[i] == 0)
            {
                AñoSel = Instantiate(Año, ContenidoAño.transform.position, Quaternion.identity, ContenidoAño.transform);
                //AñoSel = GameObject.Find("AnoX(Clone)");
                AñoSel.name = AñoN[i];
                IA = AñoSel.GetComponent<InfoAno>();
                IA.CasaID = CasaID;
                ContenidoMes = IA.Content;
                IA.año.text = AñoN[i];
                IA.años = AñoN[i];
                IA.ICMH = this;
                Años.Add(AñoSel);
                IA.ob = ob;
                AñoCreado[i] = 1;
                w++;
                for (int o = 0; MesN.Count > o; o++)
                {
                    if (MesAñoN[o] == AñoN[i])
                    {

                        if (MesCreado[o] == 0)
                        {
                            MesSel = Instantiate(Mes, ContenidoMes.transform.position, Quaternion.identity, ContenidoMes.transform);
                            //MesSel = GameObject.Find("MesX(Clone)");
                            MesSel.name = MesN[o];
                            IM = MesSel.GetComponent<InfoMes>();
                            IM.CasaID = CasaID;
                            if (MesN[o] == "1")
                                IM.mes.text = "Enero";
                            if (MesN[o] == "2")
                                IM.mes.text = "Febrero";
                            if (MesN[o] == "3")
                                IM.mes.text = "Marzo";
                            if (MesN[o] == "4")
                                IM.mes.text = "Abril";
                            if (MesN[o] == "5")
                                IM.mes.text = "Mayo";
                            if (MesN[o] == "6")
                                IM.mes.text = "Junio";
                            if (MesN[o] == "7")
                                IM.mes.text = "Julio";
                            if (MesN[o] == "8")
                                IM.mes.text = "Agosto";
                            if (MesN[o] == "9")
                                IM.mes.text = "Septiembre";
                            if (MesN[o] == "10")
                                IM.mes.text = "Octubre";
                            if (MesN[o] == "11")
                                IM.mes.text = "Noviembre";
                            if (MesN[o] == "12")
                                IM.mes.text = "Diciembre";
                            //IM.mes.text = MesN[i];
                            IM.meses = MesN[o];
                            IM.años = AñoN[i];
                            IM.ICMH = this;
                            Meses.Add(MesSel);
                            IM.ob = ob;
                            x++;
                            MesCreado[o] = 1;
                        }
                    }
                }
            }
        }
    }
    public void ClickAño(string Añose)
    {
        for (i = 0; m > i; i++)
        {
            if (MesAñoN[i] == Añose)
            {
                if (MesCreado[i] == 0)
                {
                    MesSel = Instantiate(Mes, ContenidoMes.transform.position, Quaternion.identity, ContenidoMes.transform);
                    //MesSel = GameObject.Find("MesX(Clone)");
                    MesSel.name = MesN[i];
                    IM = MesSel.GetComponent<InfoMes>();
                    IM.CasaID = CasaID;
                    if (MesN[i] == "1")
                        IM.mes.text = "Enero";
                    if (MesN[i] == "2")
                        IM.mes.text = "Febrero";
                    if (MesN[i] == "3")
                        IM.mes.text = "Marzo";
                    if (MesN[i] == "4")
                        IM.mes.text = "Abril";
                    if (MesN[i] == "5")
                        IM.mes.text = "Mayo";
                    if (MesN[i] == "6")
                        IM.mes.text = "Junio";
                    if (MesN[i] == "7")
                        IM.mes.text = "Julio";
                    if (MesN[i] == "8")
                        IM.mes.text = "Agosto";
                    if (MesN[i] == "9")
                        IM.mes.text = "Septiembre";
                    if (MesN[i] == "10")
                        IM.mes.text = "Octubre";
                    if (MesN[i] == "11")
                        IM.mes.text = "Noviembre";
                    if (MesN[i] == "12")
                        IM.mes.text = "Diciembre";
                    //IM.mes.text = MesN[i];
                    IM.meses = MesN[i];
                    IM.años = añomessel;
                    IM.ICMH = this;
                    Meses.Add(MesSel);
                    IM.ob = ob;
                    x++;
                    MesCreado[i] = 1;
                }
            }
        }
    }
    public void ClickMes(string Meses)
    {
        for (int yu = 0; yu < MesP.Count; yu++)
        {
            if (MesP[yu] == messel)
            {
                ms = true;
            }
            //for (int i = 0; MesAñoP.Count > i; i++)
            //{


            if (MesAñoP[yu] == añomessel)
            {
                añ = true;
            }
            if (ms == true && añ == true)
            {
                if (PagoCreado[yu] == 0)
                {
                    ms = false;
                    añ = false;
                    PagoHistorial = Instantiate(PagoHistorialX, ContenidoPagos.transform.position, Quaternion.identity, ContenidoPagos.transform);
                    //    PagoHistorial = GameObject.Find("HistorialPagoX(Clone)");
                    PagoHistorial.transform.SetAsFirstSibling();
                    PagoHistorial.name = Categoria[yu];
                    IPH = PagoHistorial.GetComponent<InfoPagoHistorial>();
                    IPH.CasaID = CasaID;
                    IPH.CategoriaTexto = Pagos[yu];
                    //if (FechaPago[i] == "1")
                    //    IPH.FechaPagoTexto = "Enero";
                    //if (FechaPago[i] == "2")
                    //    IPH.FechaPagoTexto = "Febrero";
                    //if (FechaPago[i] == "3")
                    //    IPH.FechaPagoTexto = "Marzo";
                    //if (FechaPago[i] == "4")
                    //    IPH.FechaPagoTexto = "Abril";
                    //if (FechaPago[i] == "5")
                    //    IPH.FechaPagoTexto = "Mayo";
                    //if (FechaPago[i] == "6")
                    //    IPH.FechaPagoTexto = "Junio";
                    //if (FechaPago[i] == "7")
                    //    IPH.FechaPagoTexto = "Julio";
                    //if (FechaPago[i] == "8")
                    //    IPH.FechaPagoTexto = "Agosto";
                    //if (FechaPago[i] == "9")
                    //    IPH.FechaPagoTexto = "Septiembre";
                    //if (FechaPago[i] == "10")
                    //    IPH.FechaPagoTexto = "Octubre";
                    //if (FechaPago[i] == "11")
                    //    IPH.FechaPagoTexto = "Noviembre";
                    //if (FechaPago[i] == "12")
                    //    IPH.FechaPagoTexto = "Diciembre";
                   // print(FechaPago[yu]);
                    IPH.FechaPagoTexto = FechaPago[yu];
                    IPH.idCategoria = idCategoria[yu];
                    IPH.idColor = idColor[yu];
                    IPH.idColorPagadoEn = idPagadoEnColor[yu];
                    IPH.idIconCat = idIcono[yu];
                    IPH.idIconoPagadoEn = idPagadoEnIcono[yu];
                    IPH.PrecioPagoTexto = PrecioPago[yu];
                    //IPH.FechaPagoTexto = FechaPago[yu];
                    IPH.idPagadoEn = idPagadoEn[yu];
                    IPH.idPagadoCon = idPagadoCon[yu];
                    IPH.ICMH = this;
                    IPH.ap = ap;
                    PagoHistoriales.Insert(0, PagoHistorial);
                    PagoCreado[yu] = 1;

                    exacto = true;
                    z++;
                    //}
                }
            }
        }
    }
    public void Click()
    {
        casa.CasaAñosHistorial.text = NickName + ": Años";
        casa.CasaPagosHistorial.text = NickName + ": Pagos";
        ClickNombre();
        ob.Click("HistorialAño");
    }
    IEnumerator DelayCasa(string Añosese)
    {
        yield return true;
        //yield return new WaitForSecondsRealtime(0.1f);
        for (i = 0; m > i; i++)
        {
            if (MesAñoN[i] == Añosese)
            {
                if (MesCreado[i] == 0)
                {
                    MesSel = Instantiate(Mes, ContenidoMes.transform.position, Quaternion.identity, ContenidoMes.transform);
                    // MesSel = GameObject.Find("MesX(Clone)");
                    MesSel.name = MesN[i];
                    IM = MesSel.GetComponent<InfoMes>();
                    if (MesN[i] == "1")
                        IM.mes.text = "Enero";
                    if (MesN[i] == "2")
                        IM.mes.text = "Febrero";
                    if (MesN[i] == "3")
                        IM.mes.text = "Marzo";
                    if (MesN[i] == "4")
                        IM.mes.text = "Abril";
                    if (MesN[i] == "5")
                        IM.mes.text = "Mayo";
                    if (MesN[i] == "6")
                        IM.mes.text = "Junio";
                    if (MesN[i] == "7")
                        IM.mes.text = "Julio";
                    if (MesN[i] == "8")
                        IM.mes.text = "Agosto";
                    if (MesN[i] == "9")
                        IM.mes.text = "Septiembre";
                    if (MesN[i] == "10")
                        IM.mes.text = "Octubre";
                    if (MesN[i] == "11")
                        IM.mes.text = "Noviembre";
                    if (MesN[i] == "12")
                        IM.mes.text = "Diciembre";
                    //IM.mes.text = MesN[i];
                    IM.meses = MesN[i];
                    IM.años = Añosese;
                    IM.ICMH = this;
                    Meses.Add(MesSel);
                    IM.ob = ob;
                    x++;
                    MesCreado[i] = 1;
                }
            }
        }
    }
}
