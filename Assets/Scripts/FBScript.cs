﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;
using System;

public class FBScript : MonoBehaviour
{
    //public FormIS IS;
    //public GoogleAnalyticsV4 GA;
    public string username, lastname, birthday, id, dia, mes, año, email, gender;
    public long idInt;
    public Registro reg;
    public MP mp;
    void Awake()
    {
        if (!FB.IsInitialized)
        {
            // Initialize the Facebook SDK
            FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            FB.ActivateApp();
        }
    }
    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            // Signal an app activation App Event
            FB.ActivateApp();
            // Continue with Facebook SDK
            // ...
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }
    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else
        {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }
    }

    public void FBRegister()
    {
        var perms = new List<string>() { "public_profile", "email",  "user_birthday" };
        FB.LogInWithReadPermissions(perms, AuthCallback);
    }
    public void FBLogin()
    {
        var perms = new List<string>() { "public_profile", "email", "user_birthday" };
        FB.LogInWithReadPermissions(perms, AuthCallbackLogin);
    }
    private void AuthCallback(ILoginResult result)
    {
        if (result.Cancelled)
        {
            FB.LogOut();
        }
        //Debug.Log("FB error " + result.Error);
        //Debug.Log("FB cancelled " + result.Cancelled);
        if (FB.IsLoggedIn /*&& !result.Cancelled*/)
        {            
            //Debug.Log("registro FB");
            // AccessToken class will have session details
            var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
            // Print current access token's User ID
            //Debug.Log(aToken.UserId);
            // Print current access token's granted permissions
            /*foreach (string perm in aToken.Permissions)
            {
                Debug.Log(perm);

            }*/
            DealWithFBMenus(FB.IsLoggedIn);
        }
        else /*if (result.Cancelled)*/
        {
            FB.LogOut();
            mp.ob.DesError.text = "Error en el registro";
            mp.ob.Click("Errors");
            //Debug.Log("User cancelled login");
        }
        
    }
    private void AuthCallbackLogin(ILoginResult result)
    {
        if(result.Cancelled)
        {
            FB.LogOut();
        }
        //Debug.Log("FB error " + result.Error);
        //Debug.Log("FB cancelled " + result.Cancelled);
        if (FB.IsLoggedIn/* && !result.Cancelled*/)
        {
            //Debug.Log("Inicio FB");
            // AccessToken class will have session details
            var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
            // Print current access token's User ID
            //Debug.Log(aToken.UserId);
            // Print current access token's granted permissions
            /*foreach (string perm in aToken.Permissions)
            {
                Debug.Log(perm);

            }*/
            DealWithFBMenusLogin(FB.IsLoggedIn);
        }
        else /*if (result.Cancelled)*/
        {
            FB.LogOut();
            mp.ob.DesError.text = "Error al iniciar sesión.";
            mp.ob.Click("Errors");
            //Debug.Log("User cancelled login");
        }

    }

    public static void FBLogOut()
    {
        FB.LogOut();

    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    void DealWithFBMenus(bool isLoggedIn)
    {
        if (isLoggedIn)
        {
            FB.API("/me?fields=first_name", HttpMethod.GET, DisplayUsername);
            FB.API("/me?fields=last_name", HttpMethod.GET, DisplayLastName);
            FB.API("/me?fields=birthday", HttpMethod.GET, DisplayBirthDay);
            FB.API("/me?fields=id", HttpMethod.GET, DisplayId);
            FB.API("/me?fields=email", HttpMethod.GET, DisplayEmail);
            FB.API("/me?fields=gender", HttpMethod.GET, DisplayGender);
        }
        else
        {

        }
    }
    void DealWithFBMenusLogin(bool isLoggedIn)
    {
        if (isLoggedIn)
        {
            //FB.API("/me?fields=first_name", HttpMethod.GET, DisplayUsername);
            //FB.API("/me?fields=last_name", HttpMethod.GET, DisplayLastName);
            //FB.API("/me?fields=birthday", HttpMethod.GET, DisplayBirthDay);
            FB.API("/me?fields=id", HttpMethod.GET, DisplayIdLogin);
            FB.API("/me?fields=email", HttpMethod.GET, DisplayEmailLogin);
            //FB.API("/me?fields=gender", HttpMethod.GET, DisplayGender);
        }
        else
        {

        }
    }
    void DisplayLastName(IResult result)
    {
        if(result.Error == null)
        {
            lastname = "" + result.ResultDictionary["last_name"];

        }
        else
        {

        }
    }
    void DisplayUsername(IResult result)
    {
        if (result.Error == null)
        {
            username = "" + result.ResultDictionary["first_name"];
            mp.NickNR.text = username;
        }
        else
        {

        }
    }
    void DisplayBirthDay(IResult result)
    {
        if (result.Error == null)
        {
            birthday = "" + result.ResultDictionary["birthday"];
            string[] split = birthday.Split(new Char[] { '/' });
            dia = split[1];
            mes = split[0];
            año = split[2];
            mp.AnoNR.text = año;
            if(mes == "01")
            {
                mp.MesNR.text = "Enero";
                mp.mes.value = 1;
            }
            if (mes == "02")
            {
                mp.MesNR.text = "Febrero";
                mp.mes.value = 2;
            }
            if (mes == "03")
            {
                mp.MesNR.text = "Marzo";
                mp.mes.value = 3;
            }
            if (mes == "04")
            {
                mp.MesNR.text = "Abril";
                mp.mes.value = 4;
            }
            if (mes == "05")
            {
                mp.MesNR.text = "Mayo";
                mp.mes.value = 5;
            }
            if (mes == "06")
            {
                mp.MesNR.text = "Junio";
                mp.mes.value = 6;
            }
            if (mes == "07")
            {
                mp.MesNR.text = "Julio";
                mp.mes.value = 7;
            }
            if (mes == "08")
            {
                mp.MesNR.text = "Agosto";
                mp.mes.value = 8;
            }
            if (mes == "09")
            {
                mp.MesNR.text = "Septiembre";
                mp.mes.value = 9;
            }
            if (mes == "10")
            {
                mp.MesNR.text = "Octubre";
                mp.mes.value = 10;
            }
            if (mes == "11")
            {
                mp.MesNR.text = "Noviembre";
                mp.mes.value = 11;
            }
            if (mes == "12")
            {
                mp.MesNR.text = "Diciembre";
                mp.mes.value = 12;
            }

        }
        else
        {

        }
    }
    void DisplayId(IResult result)
    {
        if (result.Error == null)
        {
            id = "" + result.ResultDictionary["id"];
            idInt = Int64.Parse(id);
            reg.idFB = idInt;
            reg.CreateUserFB();
            //reg.CargateMenor();
        }
        else
        {

        }
    }
    void DisplayIdLogin(IResult result)
    {
        if (result.Error == null)
        {
            id = "" + result.ResultDictionary["id"];
            idInt = Int64.Parse(id);
            reg.idFB = idInt;
            reg.IniciarSesionFB();
            //reg.CargateMenor();
        }
        else
        {

        }
    }
    void DisplayEmail(IResult result)
    {
        if (result.Error == null)
        {
            email = "" + result.ResultDictionary["email"];
            reg.correoFB = email;
            PlayerPrefs.SetString("email", email);
            reg.emailusuario = PlayerPrefs.GetString("email");
            reg.email = email;
        }
        else
        {

        }
    }
    void DisplayEmailLogin(IResult result)
    {
        if (result.Error == null)
        {
            email = "" + result.ResultDictionary["email"];
            reg.correoFB = email;
            PlayerPrefs.SetString("email", email);
            reg.emailusuario = PlayerPrefs.GetString("email");
            reg.email = email;
        }
        else
        {

        }
    }
    void DisplayGender(IResult result)
    {
        if (result.Error == null)
        {
           gender = "" + result.ResultDictionary["gender"];
            if(gender == "male")
            {
                mp.SexoMR.isOn = true;
                mp.SexoFR.isOn = false;
            }
            else if(gender == "female")
            {
                mp.SexoMR.isOn = false;
                mp.SexoFR.isOn = true;
            }
        }
        else
        {

        }
    }
}