﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
public class InfoMes : MonoBehaviour {
    public int CasaID;
    public string meses, años;
    public AP ap;
    public Text mes;
    public InfoCasaMH ICMH;
    public ObjectManager ob;
    // Use this for initialization
    void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {

	}

    public void Click()
    {
        ICMH.messel = meses;
        ICMH.añomessel = años;
            //ICMH.añomessel = años;
        ICMH.ClickMes(meses);
        ob.Click("HistorialPagos");
    }
}
