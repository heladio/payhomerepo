﻿using UnityEngine;
using System.Collections;
using System;

public delegate void OnErrorDelegate(System.Object sender, EventArgs e);
public delegate void OnSuccessDelegate(System.Object sender, EventArgs e);
public delegate void OnProgressChangeDelegate(System.Object sender, EventArgs e);

public static class WebServices
{
	public static string baseUrl	= "https://payhome.azurewebsites.net/serv";

	public static string baseServer	= "";

	public static void RegisterUser(WWWForm wForm, OnSuccessDelegate onSuccessDelegate, OnErrorDelegate onErrorDelegate)
	{
		ParseUser parseUser = new ParseUser(baseUrl + "/registrarcuenta.php", wForm,
		                                    (int) NetworkScheduler.TYPE.TEXT);
		
		parseUser.success += new OnParseSuccessEventHandler(onSuccessDelegate);
		parseUser.failed += new OnParseErrorEventHandler(onErrorDelegate);
	}
    public static void LoginUser(WWWForm wForm, OnSuccessDelegate onSuccessDelegate, OnErrorDelegate onErrorDelegate)
    {
        ParseUser parseUser = new ParseUser(baseUrl + "/iniciarsesion.php", wForm,
                                            (int)NetworkScheduler.TYPE.TEXT);

        parseUser.success += new OnParseSuccessEventHandler(onSuccessDelegate);
        parseUser.failed += new OnParseErrorEventHandler(onErrorDelegate);
    }
    public static void LoginUserFB(WWWForm wForm, OnSuccessDelegate onSuccessDelegate, OnErrorDelegate onErrorDelegate)
    {
        ParseUser parseUser = new ParseUser(baseUrl + "/iniciarsesion_fb.php", wForm,
                                            (int)NetworkScheduler.TYPE.TEXT);

        parseUser.success += new OnParseSuccessEventHandler(onSuccessDelegate);
        parseUser.failed += new OnParseErrorEventHandler(onErrorDelegate);
    }
    public static void ActMP(WWWForm wForm, OnSuccessDelegate onSuccessDelegate, OnErrorDelegate onErrorDelegate)
    {
        ParseMP parseMP = new ParseMP(baseUrl + "/completareditarperfil.php", wForm,
                                            (int)NetworkScheduler.TYPE.TEXT);

        parseMP.success += new OnParseSuccessEventHandler(onSuccessDelegate);
        parseMP.failed += new OnParseErrorEventHandler(onErrorDelegate);
    }
    public static void ObtMP(WWWForm wForm, OnSuccessDelegate onSuccessDelegate, OnErrorDelegate onErrorDelegate)
    {
        ParseObtMP parseObtMP = new ParseObtMP(baseUrl + "/obtenerperfil.php", wForm,
                                            (int)NetworkScheduler.TYPE.TEXT);

        parseObtMP.success += new OnParseSuccessEventHandler(onSuccessDelegate);
        parseObtMP.failed += new OnParseErrorEventHandler(onErrorDelegate);
    }
    public static void EliminarMP(WWWForm wForm, OnSuccessDelegate onSuccessDelegate, OnErrorDelegate onErrorDelegate)
    {
        ParseObtMP parseObtMP = new ParseObtMP(baseUrl + "/eliminafotousuario.php", wForm,
                                            (int)NetworkScheduler.TYPE.TEXT);

        parseObtMP.success += new OnParseSuccessEventHandler(onSuccessDelegate);
        parseObtMP.failed += new OnParseErrorEventHandler(onErrorDelegate);
    }
    public static void ObtCasas(WWWForm wForm, OnSuccessDelegate onSuccessDelegate, OnErrorDelegate onErrorDelegate)
    {
        ParseCasa parseCasa = new ParseCasa(baseUrl + "/obtenercasas.php", wForm,
                                            (int)NetworkScheduler.TYPE.TEXT);

        parseCasa.success += new OnParseSuccessEventHandler(onSuccessDelegate);
        parseCasa.failed += new OnParseErrorEventHandler(onErrorDelegate);
    }
    public static void ModCasas(WWWForm wForm, OnSuccessDelegate onSuccessDelegate, OnErrorDelegate onErrorDelegate)
    {
        ParseModCasa parseModCasa = new ParseModCasa(baseUrl + "/actualizarcasa.php", wForm,
                                            (int)NetworkScheduler.TYPE.TEXT);

        parseModCasa.success += new OnParseSuccessEventHandler(onSuccessDelegate);
        parseModCasa.failed += new OnParseErrorEventHandler(onErrorDelegate);
    }
    public static void AgregarCasas(WWWForm wForm, OnSuccessDelegate onSuccessDelegate, OnErrorDelegate onErrorDelegate)
    {
        ParseModCasa parseModCasa = new ParseModCasa(baseUrl + "/agregarcasa.php", wForm,
                                            (int)NetworkScheduler.TYPE.TEXT);

        parseModCasa.success += new OnParseSuccessEventHandler(onSuccessDelegate);
        parseModCasa.failed += new OnParseErrorEventHandler(onErrorDelegate);
    }
    public static void ElimCasas(WWWForm wForm, OnSuccessDelegate onSuccessDelegate, OnErrorDelegate onErrorDelegate)
    {
        ParseModCasa parseModCasa = new ParseModCasa(baseUrl + "/eliminarcasa.php", wForm,
                                            (int)NetworkScheduler.TYPE.TEXT);

        parseModCasa.success += new OnParseSuccessEventHandler(onSuccessDelegate);
        parseModCasa.failed += new OnParseErrorEventHandler(onErrorDelegate);
    }

    public static void ElimCasasFoto(WWWForm wForm, OnSuccessDelegate onSuccessDelegate, OnErrorDelegate onErrorDelegate)
    {
        ParseModCasa parseModCasa = new ParseModCasa(baseUrl + "/eliminafotocasa.php", wForm,
                                            (int)NetworkScheduler.TYPE.TEXT);

        parseModCasa.success += new OnParseSuccessEventHandler(onSuccessDelegate);
        parseModCasa.failed += new OnParseErrorEventHandler(onErrorDelegate);
    }
    public static void ElimCategoria(WWWForm wForm, OnSuccessDelegate onSuccessDelegate, OnErrorDelegate onErrorDelegate)
    {
        ParseAgregarCategoria parseAgregarCategoria = new ParseAgregarCategoria(baseUrl + "/eliminarcategoria.php", wForm,
                                            (int)NetworkScheduler.TYPE.TEXT);

        parseAgregarCategoria.success += new OnParseSuccessEventHandler(onSuccessDelegate);
        parseAgregarCategoria.failed += new OnParseErrorEventHandler(onErrorDelegate);
    }
    public static void ObtCategoria(WWWForm wForm, OnSuccessDelegate onSuccessDelegate, OnErrorDelegate onErrorDelegate)
    {
        ParseCategoria parseCategoria = new ParseCategoria(baseUrl + "/obtenercategoria.php", wForm,
                                            (int)NetworkScheduler.TYPE.TEXT);

        parseCategoria.success += new OnParseSuccessEventHandler(onSuccessDelegate);
        parseCategoria.failed += new OnParseErrorEventHandler(onErrorDelegate);
    }
    public static void AgregarCategoria(WWWForm wForm, OnSuccessDelegate onSuccessDelegate, OnErrorDelegate onErrorDelegate)
    {
        ParseAgregarCategoria parseAgregarCategoria = new ParseAgregarCategoria(baseUrl + "/agregarcategoria.php", wForm,
                                            (int)NetworkScheduler.TYPE.TEXT);

        parseAgregarCategoria.success += new OnParseSuccessEventHandler(onSuccessDelegate);
        parseAgregarCategoria.failed += new OnParseErrorEventHandler(onErrorDelegate);
    }
    public static void ModCategoria(WWWForm wForm, OnSuccessDelegate onSuccessDelegate, OnErrorDelegate onErrorDelegate)
    {
        ParseAgregarCategoria parseAgregarCategoria = new ParseAgregarCategoria(baseUrl + "/editarcategoria.php", wForm,
                                            (int)NetworkScheduler.TYPE.TEXT);

        parseAgregarCategoria.success += new OnParseSuccessEventHandler(onSuccessDelegate);
        parseAgregarCategoria.failed += new OnParseErrorEventHandler(onErrorDelegate);
    }
    public static void ObtBenef(WWWForm wForm, OnSuccessDelegate onSuccessDelegate, OnErrorDelegate onErrorDelegate)
    {
        ParseBeneficiario parseBenef = new ParseBeneficiario(baseUrl + "/obtenerbeneficiario.php", wForm,
                                            (int)NetworkScheduler.TYPE.TEXT);

        parseBenef.success += new OnParseSuccessEventHandler(onSuccessDelegate);
        parseBenef.failed += new OnParseErrorEventHandler(onErrorDelegate);
    }
    public static void AgregarBenef(WWWForm wForm, OnSuccessDelegate onSuccessDelegate, OnErrorDelegate onErrorDelegate)
    {
        ParseAgregarModElimBenef parseAMEBenef = new ParseAgregarModElimBenef(baseUrl + "/agregarbeneficiario.php", wForm,
                                            (int)NetworkScheduler.TYPE.TEXT);

        parseAMEBenef.success += new OnParseSuccessEventHandler(onSuccessDelegate);
        parseAMEBenef.failed += new OnParseErrorEventHandler(onErrorDelegate);
    }
    public static void ModBenef(WWWForm wForm, OnSuccessDelegate onSuccessDelegate, OnErrorDelegate onErrorDelegate)
    {
        ParseAgregarModElimBenef parseAMEBenef = new ParseAgregarModElimBenef(baseUrl + "/editarbeneficiario.php", wForm,
                                            (int)NetworkScheduler.TYPE.TEXT);

        parseAMEBenef.success += new OnParseSuccessEventHandler(onSuccessDelegate);
        parseAMEBenef.failed += new OnParseErrorEventHandler(onErrorDelegate);
    }
    public static void ElimBenef(WWWForm wForm, OnSuccessDelegate onSuccessDelegate, OnErrorDelegate onErrorDelegate)
    {
        ParseAgregarModElimBenef parseAMEBenef = new ParseAgregarModElimBenef(baseUrl + "/eliminarbeneficiario.php", wForm,
                                            (int)NetworkScheduler.TYPE.TEXT);

        parseAMEBenef.success += new OnParseSuccessEventHandler(onSuccessDelegate);
        parseAMEBenef.failed += new OnParseErrorEventHandler(onErrorDelegate);
    }
    public static void AgregarPago(WWWForm wForm, OnSuccessDelegate onSuccessDelegate, OnErrorDelegate onErrorDelegate)
    {
        ParseAgrModElimPagos parseAMEPagos = new ParseAgrModElimPagos(baseUrl + "/agregarpago.php", wForm,
                                            (int)NetworkScheduler.TYPE.TEXT);

        parseAMEPagos.success += new OnParseSuccessEventHandler(onSuccessDelegate);
        parseAMEPagos.failed += new OnParseErrorEventHandler(onErrorDelegate);
    }
    public static void ElimPago(WWWForm wForm, OnSuccessDelegate onSuccessDelegate, OnErrorDelegate onErrorDelegate)
    {
        ParseAgrModElimPagos parseAMEPagos = new ParseAgrModElimPagos(baseUrl + "/eliminarpago.php", wForm,
                                            (int)NetworkScheduler.TYPE.TEXT);

        parseAMEPagos.success += new OnParseSuccessEventHandler(onSuccessDelegate);
        parseAMEPagos.failed += new OnParseErrorEventHandler(onErrorDelegate);
    }
    public static void ModPago(WWWForm wForm, OnSuccessDelegate onSuccessDelegate, OnErrorDelegate onErrorDelegate)
    {
        ParseAgrModElimPagos parseAMEPagos = new ParseAgrModElimPagos(baseUrl + "/actualizarpago.php", wForm,
                                            (int)NetworkScheduler.TYPE.TEXT);

        parseAMEPagos.success += new OnParseSuccessEventHandler(onSuccessDelegate);
        parseAMEPagos.failed += new OnParseErrorEventHandler(onErrorDelegate);
    }
    public static void ObtPago(WWWForm wForm, OnSuccessDelegate onSuccessDelegate, OnErrorDelegate onErrorDelegate)
    {
        ParseObtPago parseObtPagos = new ParseObtPago(baseUrl + "/obtenerpago.php", wForm,
                                            (int)NetworkScheduler.TYPE.TEXT);

        parseObtPagos.success += new OnParseSuccessEventHandler(onSuccessDelegate);
        parseObtPagos.failed += new OnParseErrorEventHandler(onErrorDelegate);
    }
    public static void AgregarPagoPasado(WWWForm wForm, OnSuccessDelegate onSuccessDelegate, OnErrorDelegate onErrorDelegate)
    {
        ParseAgrElimPagosPasado parseAEPagosPasado = new ParseAgrElimPagosPasado(baseUrl + "/agregarpagonorealizado.php", wForm,
                                            (int)NetworkScheduler.TYPE.TEXT);

        parseAEPagosPasado.success += new OnParseSuccessEventHandler(onSuccessDelegate);
        parseAEPagosPasado.failed += new OnParseErrorEventHandler(onErrorDelegate);
    }
    public static void ElimPagoPasado(WWWForm wForm, OnSuccessDelegate onSuccessDelegate, OnErrorDelegate onErrorDelegate)
    {
        ParseAgrElimPagosPasado parseAEPagosPasado = new ParseAgrElimPagosPasado(baseUrl + "/eliminarpagonorealizado.php", wForm,
                                            (int)NetworkScheduler.TYPE.TEXT);

        parseAEPagosPasado.success += new OnParseSuccessEventHandler(onSuccessDelegate);
        parseAEPagosPasado.failed += new OnParseErrorEventHandler(onErrorDelegate);
    }
    public static void ObtPagoPasado(WWWForm wForm, OnSuccessDelegate onSuccessDelegate, OnErrorDelegate onErrorDelegate)
    {
        ParseObtPagoPasado parseObtPagosPasado = new ParseObtPagoPasado(baseUrl + "/obtenerpagonorealizado.php", wForm,
                                            (int)NetworkScheduler.TYPE.TEXT);

        parseObtPagosPasado.success += new OnParseSuccessEventHandler(onSuccessDelegate);
        parseObtPagosPasado.failed += new OnParseErrorEventHandler(onErrorDelegate);
    }
    public static void AgregarHistorial(WWWForm wForm, OnSuccessDelegate onSuccessDelegate, OnErrorDelegate onErrorDelegate)
    {
        ParseAgregarHistorial parseAgregarHistorial= new ParseAgregarHistorial(baseUrl + "/agregarpagohistorial.php", wForm,
                                            (int)NetworkScheduler.TYPE.TEXT);

        parseAgregarHistorial.success += new OnParseSuccessEventHandler(onSuccessDelegate);
        parseAgregarHistorial.failed += new OnParseErrorEventHandler(onErrorDelegate);
    }
    public static void ObtHistorial(WWWForm wForm, OnSuccessDelegate onSuccessDelegate, OnErrorDelegate onErrorDelegate)
    {
        ParseObtHistorial parseObtHistorial = new ParseObtHistorial(baseUrl + "/obtenerpagohistorial.php", wForm,
                                            (int)NetworkScheduler.TYPE.TEXT);

        parseObtHistorial.success += new OnParseSuccessEventHandler(onSuccessDelegate);
        parseObtHistorial.failed += new OnParseErrorEventHandler(onErrorDelegate);
    }
    public static void ElimHistorial(WWWForm wForm, OnSuccessDelegate onSuccessDelegate, OnErrorDelegate onErrorDelegate)
    {
        ParseObtHistorial parseObtHistorial = new ParseObtHistorial(baseUrl + "/eliminarhistorial.php", wForm,
                                            (int)NetworkScheduler.TYPE.TEXT);

        parseObtHistorial.success += new OnParseSuccessEventHandler(onSuccessDelegate);
        parseObtHistorial.failed += new OnParseErrorEventHandler(onErrorDelegate);
    }
    public static void EnviarDudas(WWWForm wForm, OnSuccessDelegate onSuccessDelegate, OnErrorDelegate onErrorDelegate)
    {
        ParseAclDudas parseAclDudas = new ParseAclDudas(baseUrl + "/comentariosydudas.php", wForm,
                                            (int)NetworkScheduler.TYPE.TEXT);

        parseAclDudas.success += new OnParseSuccessEventHandler(onSuccessDelegate);
        parseAclDudas.failed += new OnParseErrorEventHandler(onErrorDelegate);
    }
    public static void EnviarAclaracion(WWWForm wForm, OnSuccessDelegate onSuccessDelegate, OnErrorDelegate onErrorDelegate)
    {
        ParseAclDudas parseAclDudas = new ParseAclDudas(baseUrl + "/enviaraclaracion.php", wForm,
                                            (int)NetworkScheduler.TYPE.TEXT);

        parseAclDudas.success += new OnParseSuccessEventHandler(onSuccessDelegate);
        parseAclDudas.failed += new OnParseErrorEventHandler(onErrorDelegate);
    }
    public static void EnviarContacto(WWWForm wForm, OnSuccessDelegate onSuccessDelegate, OnErrorDelegate onErrorDelegate)
    {
        ParseAclDudas parseAclDudas = new ParseAclDudas(baseUrl + "/pedircontacto.php", wForm,
                                            (int)NetworkScheduler.TYPE.TEXT);

        parseAclDudas.success += new OnParseSuccessEventHandler(onSuccessDelegate);
        parseAclDudas.failed += new OnParseErrorEventHandler(onErrorDelegate);
    }
    public static void RecuperarContrasena(WWWForm wForm, OnSuccessDelegate onSuccessDelegate, OnErrorDelegate onErrorDelegate)
    {
        ParseAclDudas parseAclDudas = new ParseAclDudas(baseUrl + "/recuperarcontrasena.php", wForm,
                                            (int)NetworkScheduler.TYPE.TEXT);

        parseAclDudas.success += new OnParseSuccessEventHandler(onSuccessDelegate);
        parseAclDudas.failed += new OnParseErrorEventHandler(onErrorDelegate);
    }

    public static void EnviarRecibo(WWWForm wForm, OnSuccessDelegate onSuccessDelegate, OnErrorDelegate onErrorDelegate)
    {
        ParseAclDudas parseAclDudas = new ParseAclDudas(baseUrl + "/serviciorecibodatos.php", wForm,
                                            (int)NetworkScheduler.TYPE.TEXT);

        parseAclDudas.success += new OnParseSuccessEventHandler(onSuccessDelegate);
        parseAclDudas.failed += new OnParseErrorEventHandler(onErrorDelegate);
    }
    public static void EnviarReporte(WWWForm wForm, OnSuccessDelegate onSuccessDelegate, OnErrorDelegate onErrorDelegate)
    {
        ParseAclDudas parseAclDudas = new ParseAclDudas(baseUrl + "/comentariosydudas.php", wForm,
                                            (int)NetworkScheduler.TYPE.TEXT);

        parseAclDudas.success += new OnParseSuccessEventHandler(onSuccessDelegate);
        parseAclDudas.failed += new OnParseErrorEventHandler(onErrorDelegate);
    }
    public static void RegisterUserFB(WWWForm wForm, OnSuccessDelegate onSuccessDelegate, OnErrorDelegate onErrorDelegate)
    {
        ParseUser parseUser = new ParseUser(baseUrl + "/registrarcuenta_fb.php", wForm,
                                            (int)NetworkScheduler.TYPE.TEXT);

        parseUser.success += new OnParseSuccessEventHandler(onSuccessDelegate);
        parseUser.failed += new OnParseErrorEventHandler(onErrorDelegate);
    }
    public static void SubirFoto(WWWForm wForm, OnSuccessDelegate onSuccessDelegate, OnErrorDelegate onErrorDelegate)
    {
        ParsePhoto parsePhoto = new ParsePhoto(baseUrl + "/subirfoto.php", wForm,
                                            (int)NetworkScheduler.TYPE.TEXT);

        parsePhoto.success += new OnParseSuccessEventHandler(onSuccessDelegate);
        parsePhoto.failed += new OnParseErrorEventHandler(onErrorDelegate);
    }
    public static void SubirFotoCasa(WWWForm wForm, OnSuccessDelegate onSuccessDelegate, OnErrorDelegate onErrorDelegate)
    {
        ParsePhoto parsePhoto = new ParsePhoto(baseUrl + "/subirfoto_casa.php", wForm,
                                            (int)NetworkScheduler.TYPE.TEXT);

        parsePhoto.success += new OnParseSuccessEventHandler(onSuccessDelegate);
        parsePhoto.failed += new OnParseErrorEventHandler(onErrorDelegate);
    }
    /*void success()
    {
        WWWForm form;
        form.AddField("email",);
    }
    loginUser(Form,Success,Error)
    void error()
    {

    }*/

}
