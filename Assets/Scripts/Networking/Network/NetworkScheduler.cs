﻿using UnityEngine;
using System.Collections;
using System;

public delegate void OnRequestSuccessEventHandler(object sender, EventArgs e);
public delegate void OnRequestFailedEventHandler(object sender, EventArgs e);
public delegate void OnRequestprogressChangeEventHandler(object sender, EventArgs e);

public class NetworkScheduler
{
	protected virtual void OnSuccess(EventArgs e)
	{
		if (success != null)
		{
			success(this, e);
		}
	}

	protected virtual void OnFailed(EventArgs e)
	{
		if (failed != null)
		{
			failed(this, e);
		}
	}

	protected virtual void OnProgressChange(EventArgs e)
	{
		if (progressChange != null)
		{
			progressChange(this, e);
		}
	}
	
	public WWW Request {
		get {
			return this.request;
		}
	}

	public NetworkScheduler(string url,WWWForm form, int type)
	{
		coroutineManager = Camera.main.GetComponent<CoroutineManager>();
		coroutineManager.StartChildCoroutine(DoGetRequest(url,form,type));
		//StartCoroutine(DoGetRequest(url,form,type));
	}

	public IEnumerator DoGetRequest(string url, WWWForm form, int type)
	{
		// AS
		//string keyValue = url.Substring(url.LastIndexOf("/") + 1);
		if(type == (int)TYPE.IMAGE)
		{
			Texture2D texture = null;
			yield return null;
			OnSuccess(new MessageArg<Texture2D>(texture));
		}
		else
		{
			if(form != null)
			{	//byte[] rawData = form.data;
				request = new WWW(url.Replace(" ","%20"),form);
			} else {
				request = new WWW(url.Replace(" ","%20"));
			}
			
			//Debug.Log("[esperando respuesta] de: " + url.Replace(" ","%20"));
			coroutineManager.StartChildCoroutine(SpyRequestProgress());
			yield return request;
			//		Debug.Log("[respuesta recibida]  de: " + url.Replace(" ","%20"));
			
			if (request.error != null)
			{ //si hay error
				OnFailed(new MessageArg<string>(request.error));
			}
			else
			{
				if(type == (int)TYPE.TEXT) {
					OnSuccess(new MessageArg<string>(request.text));
				} else if(type == (int)TYPE.IMAGE)
				{
//					if(!PageManager.DATABASE_IMAGE.ContainsKey(keyValue))
//					{
//						PageManager.DATABASE_IMAGE.Add(keyValue, request.texture);
//					}
//
//					OnSuccess(new MessageArg<Texture2D>(PageManager.DATABASE_IMAGE[keyValue]));

					OnSuccess(new MessageArg<string>("HI"));
				}
			}
		}
	}


	public IEnumerator SpyRequestProgress()
	{
		//spy progress while wait time < 15 seconds (300 x 0.5 = 15)
		for (int i=0; i < 300; i++)
		{
			OnProgressChange(new MessageArg<string>(decimal.Round((decimal)request.progress * 100,0).ToString() + "%"));
			if(request.progress == 1) break;
			yield return new WaitForSeconds(0.05f);
		}
	}

	WWW request;
	
	CoroutineManager coroutineManager;
	
	public enum TYPE { TEXT, IMAGE };

	public event OnRequestSuccessEventHandler success;
	public event OnRequestFailedEventHandler failed;
	public event OnRequestprogressChangeEventHandler progressChange;
}