﻿using UnityEngine;
using System.Collections;

public class CoroutineManager : MonoBehaviour {
	public void StartChildCoroutine(IEnumerator coroutineMethod) {
		StartCoroutine(coroutineMethod);
	}
}
