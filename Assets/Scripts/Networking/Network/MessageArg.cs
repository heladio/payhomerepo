﻿using UnityEngine;
using System.Collections;
using System;

public class MessageArg<T> : EventArgs
{
	public MessageArg (T responseValue)
	{
		this.responseValue = responseValue;
	}

	public T responseValue { get; set; }
}
