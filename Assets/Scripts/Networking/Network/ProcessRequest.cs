﻿using UnityEngine;
using System.Collections;
using System.Net.NetworkInformation;
using System;

public delegate void OnParseSuccessEventHandler(object sender, EventArgs e);
public delegate void OnParseErrorEventHandler(object sender, EventArgs e);
public delegate void OnParseProgressChangeEventHandler(object sender, EventArgs e);

public abstract class ProcessRequest
{
	protected virtual void OnSuccess(EventArgs e)
	{
		if (success != null)
		{
			success(this, e);
		}
	}

	protected virtual void OnFailed(EventArgs e)
	{
		if (failed != null)
		{
			failed(this, e);
		}
	}
	protected virtual void OnProgressChange(EventArgs e)
	{
		if (progressChange != null)
		{
			progressChange(this, e);
		}
	}

	public ProcessRequest(string url, WWWForm form, int type)
	{
		NetworkScheduler nScheduler = new NetworkScheduler(url, form, type);
//		Debug.Log("url petition= " + url);

		nScheduler.success += new OnRequestSuccessEventHandler(OnRequestSuccess);
		nScheduler.failed += new OnRequestFailedEventHandler(OnRequestFailed);
		nScheduler.progressChange += new OnRequestprogressChangeEventHandler(OnProgressChange);
	}

	private void OnRequestSuccess(object sender, EventArgs e)
	{
		proccessResponse(e);
	}

	private void OnRequestFailed(object sender, EventArgs e)
	{
		OnFailed(e);
	}

	private void OnProgressChange(object sender, EventArgs e)
	{
		OnProgressChange(e);
	}

	public abstract void proccessResponse(EventArgs responseObj);

	public event OnParseSuccessEventHandler success;

	public event OnParseErrorEventHandler failed;

	public event OnParseProgressChangeEventHandler progressChange;
}
