﻿using UnityEngine;
using System.Collections;
using System;
using SimpleJSON;
using System.Collections.Generic;

public class ParseModCasa : ProcessRequest
{
    public ParseModCasa(string url, WWWForm form, int type) : base(url, form, type) { }

    public override void proccessResponse(EventArgs responseObj)
    {
        try
        {
            MessageArg<string> response = (MessageArg<string>)responseObj;

            JSONNode casas = JSON.Parse(response.responseValue);

            string error = casas["error"].Value;

            if (string.IsNullOrEmpty(error))
            {
                //JSONNode nUser = root["user"];
                DBCasate dbcasas = new DBCasate();

                dbcasas.IdUsuario = casas["IdUsuario"].AsInt;
                dbcasas.IdCasa = casas/*["Casa"]*/["IdCasa"].AsInt;
                //dbcasas.NombreCasa = casas["Casa"]["NombreCasa"];
                //dbcasas.FotoCasa = casas["Casa"]["FotoCasa"];
                //dbcasas.UbicacionCasa = casas["Casa"]["UbicacionCasa"];
                //dbcasas.CodigoActivacion = casas["Casa"]["CodigoActivacion"];

                //currentUser.Name = nUser["name"].Value;
                //currentUser.Lastname = nUser["lastname"].Value;
                //currentUser.Email = nUser["mail"].Value;
                //currentUser.Status = nUser["status"].Value;
                //currentUser.RegisterDate = nUser["register_date"].Value;

                OnSuccess(new MessageArg<DBCasate>(dbcasas));
            }
            else
            {
                OnSuccess(new MessageArg<string>(error));
            }
        }
        catch (Exception e)
        {
            OnFailed(new MessageArg<string>(e.Message));
        }
    }
}
