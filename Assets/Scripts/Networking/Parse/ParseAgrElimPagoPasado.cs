﻿using UnityEngine;
using System.Collections;
using System;
using SimpleJSON;
using System.Collections.Generic;

public class ParseAgrElimPagosPasado : ProcessRequest
{	
	public ParseAgrElimPagosPasado(string url, WWWForm form, int type) : base (url, form, type) {}
	
	public override void proccessResponse(EventArgs responseObj)
	{
		try
		{
            MessageArg<string> response = (MessageArg<string>)responseObj;

            JSONNode root = JSON.Parse(response.responseValue);

            string error = root["error"].Value;
            //List<DBCasa> casass = new List<DBCasa>();
            if (string.IsNullOrEmpty(error))
            {
                //JSONNode nUser = root["user"];
                DBPagosPasado dbpagospasado = new DBPagosPasado();

                dbpagospasado.IdUsuario = root["IdUsuario"].AsInt;
                dbpagospasado.IdPagoPasado = root["IdPagoPasado"].AsInt;
                //dbcasas.NombreCasa = casas["Casa"]["NombreCasa"];
                //dbcasas.FotoCasa = casas["Casa"]["FotoCasa"];
                //dbcasas.UbicacionCasa = casas["Casa"]["UbicacionCasa"];
                //dbcasas.CodigoActivacion = casas["Casa"]["CodigoActivacion"];

                //currentUser.Name = nUser["name"].Value;
                //currentUser.Lastname = nUser["lastname"].Value;
                //currentUser.Email = nUser["mail"].Value;
                //currentUser.Status = nUser["status"].Value;
                //currentUser.RegisterDate = nUser["register_date"].Value;

                OnSuccess(new MessageArg<DBPagosPasado>(dbpagospasado));
            }

            else
            {
                OnSuccess(new MessageArg<string>(error));
            }
        }
        catch (Exception e)
        {
            OnFailed(new MessageArg<string>(e.Message));
        }
    }
}
