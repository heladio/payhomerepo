﻿using UnityEngine;
using System.Collections;
using System;
using SimpleJSON;
using System.Collections.Generic;

public class ParseObtPago : ProcessRequest
{	
	public ParseObtPago(string url, WWWForm form, int type) : base (url, form, type) {}
	
	public override void proccessResponse(EventArgs responseObj)
	{
		try
		{
			MessageArg<string> response = (MessageArg<string>) responseObj;

			JSONNode root = JSON.Parse(response.responseValue);
            
            string error = root["error"].Value;
            //List<DBCasa> casass = new List<DBCasa>();
            if (string.IsNullOrEmpty(error))
			{
                //JSONNode nUser = root["user"];
                //DBCasate dbcasas = new DBCasate();
                List<DBPagos> cuPagos = new List<DBPagos>();
				//currentCasa.IdCasa = root["IdUsuario"].AsInt;
                JSONArray pagos = root["Pagos"].AsArray;
                for (int b = 0; b < pagos.Count; b++)
                {
                    DBPagos dbpagos = new DBPagos();
                    dbpagos.IdPago = pagos[b]["Pago"]["IdPago"].AsInt;
                    dbpagos.NombrePago = pagos[b]["Pago"]["NombrePago"];
                    dbpagos.IconoPago = pagos[b]["Pago"]["IconoPago"].AsInt;
                    dbpagos.ColorPago = pagos[b]["Pago"]["ColorPago"].AsInt;
                    dbpagos.IdCasa = pagos[b]["Pago"]["IdCasa"].AsInt;
                    dbpagos.MontoPago = pagos[b]["Pago"]["MontoPago"];
                    dbpagos.FechaPago = pagos[b]["Pago"]["FechaPago"];
                    dbpagos.RecurrentePago = pagos[b]["Pago"]["RecurrentePago"].AsInt;
                    dbpagos.RepetirPago = pagos[b]["Pago"]["RepetirPago"].AsInt;
                    dbpagos.FechaRecordatorioPago = pagos[b]["Pago"]["FechaRecordatorioPago"].AsInt;
                    dbpagos.IdBeneficiario = pagos[b]["Pago"]["IdBeneficiario"].AsInt;
                    dbpagos.NotaPago = pagos[b]["Pago"]["NotaPago"];
                    dbpagos.ReciboPago = pagos[b]["Pago"]["ReciboPago"].AsInt;
                    dbpagos.CodigoPago = pagos[b]["Pago"]["CodigoPago"];
                    //currentCasa.IdCasa = casas[b]["IdCasa"].AsInt;
                    // currentCasa.NombreCasa = casas[b]["NombreCasa"];
                    // currentCasa.FotoCasa = casas[b]["FotoCasa"];
                    // currentCasa.UbicacionCasa = casas[b]["UbicacionCasa"];
                    // currentCasa.CodigoActivacion = casas[b]["CodigoActivacion"];

                    cuPagos.Add(dbpagos);
                    
                }
                //currentUser.Name = nUser["name"].Value;
                //currentUser.Lastname = nUser["lastname"].Value;
                //currentUser.Email = nUser["mail"].Value;
                //currentUser.Status = nUser["status"].Value;
                //currentUser.RegisterDate = nUser["register_date"].Value;

                OnSuccess(new MessageArg<List<DBPagos>>(cuPagos));
			}
			else
			{
				OnSuccess(new MessageArg<string>(error));
			}
		}
		catch(Exception e)
		{
			OnFailed(new MessageArg<string>(e.Message));	
		}
	}
}
