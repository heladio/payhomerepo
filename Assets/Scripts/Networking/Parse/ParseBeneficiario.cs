﻿using UnityEngine;
using System.Collections;
using System;
using SimpleJSON;
using System.Collections.Generic;

public class ParseBeneficiario : ProcessRequest
{	
	public ParseBeneficiario(string url, WWWForm form, int type) : base (url, form, type) {}
	
	public override void proccessResponse(EventArgs responseObj)
	{
		try
		{
			MessageArg<string> response = (MessageArg<string>) responseObj;

			JSONNode root = JSON.Parse(response.responseValue);
            
            string error = root["error"].Value;
            //List<DBCasa> casass = new List<DBCasa>();
            if (string.IsNullOrEmpty(error))
			{
                //JSONNode nUser = root["user"];
                //DBCasate dbcasas = new DBCasate();
                List<DBBeneficiario> cuBeneficiarios = new List<DBBeneficiario>();
				//currentCasa.IdCasa = root["IdUsuario"].AsInt;
                JSONArray beneficiarios = root["Beneficiarios"].AsArray;
                for (int b = 0; b < beneficiarios.Count; b++)
                {
                    DBBeneficiario dbbeneficiario = new DBBeneficiario();
                    dbbeneficiario.IdBeneficiario = beneficiarios[b]["Beneficiario"]["IdBeneficiario"].AsInt;
                    dbbeneficiario.NombreBeneficiario = beneficiarios[b]["Beneficiario"]["NombreBeneficiario"];
                    dbbeneficiario.ClabeBeneficiario = beneficiarios[b]["Beneficiario"]["CLABEBeneficiario"];
                    dbbeneficiario.ReferenciaBeneficiario = beneficiarios[b]["Beneficiario"]["ReferenciaBeneficiario"];
                    dbbeneficiario.EmailBeneficiario = beneficiarios[b]["Beneficiario"]["EmailBeneficiario"];
                    dbbeneficiario.TelefonoBeneficiario = beneficiarios[b]["Beneficiario"]["TelefonoBeneficiario"];
                    //currentCasa.IdCasa = casas[b]["IdCasa"].AsInt;
                    // currentCasa.NombreCasa = casas[b]["NombreCasa"];
                    // currentCasa.FotoCasa = casas[b]["FotoCasa"];
                    // currentCasa.UbicacionCasa = casas[b]["UbicacionCasa"];
                    // currentCasa.CodigoActivacion = casas[b]["CodigoActivacion"];

                    cuBeneficiarios.Add(dbbeneficiario);
                    
                }
                //currentUser.Name = nUser["name"].Value;
                //currentUser.Lastname = nUser["lastname"].Value;
                //currentUser.Email = nUser["mail"].Value;
                //currentUser.Status = nUser["status"].Value;
                //currentUser.RegisterDate = nUser["register_date"].Value;

                OnSuccess(new MessageArg<List<DBBeneficiario>>(cuBeneficiarios));
			}
			else
			{
				OnSuccess(new MessageArg<string>(error));
			}
		}
		catch(Exception e)
		{
			OnFailed(new MessageArg<string>(e.Message));	
		}
	}
}
