﻿using UnityEngine;
using System.Collections;
using System;
using SimpleJSON;
using System.Collections.Generic;

public class ParseCategoria : ProcessRequest
{	
	public ParseCategoria(string url, WWWForm form, int type) : base (url, form, type) {}
	
	public override void proccessResponse(EventArgs responseObj)
	{
		try
		{
			MessageArg<string> response = (MessageArg<string>) responseObj;

			JSONNode root = JSON.Parse(response.responseValue);
            
            string error = root["error"].Value;
            //List<DBCasa> casass = new List<DBCasa>();
            if (string.IsNullOrEmpty(error))
			{
                //JSONNode nUser = root["user"];
                //DBCasate dbcasas = new DBCasate();
                List<DBCategoria> cuCategoria = new List<DBCategoria>();
				//currentCasa.IdCasa = root["IdUsuario"].AsInt;
                JSONArray categorias = root["Categorias"].AsArray;
                for (int b = 0; b < categorias.Count; b++)
                {
                    DBCategoria dbcategoria = new DBCategoria();
                    dbcategoria.IdCategoria = categorias[b]["Categoria"]["IdCategoria"].AsInt;
                    dbcategoria.NombreCategoria = categorias[b]["Categoria"]["NombreCategoria"];
                    dbcategoria.IconoCategoria = categorias[b]["Categoria"]["IconoCategoria"].AsInt;
                    dbcategoria.ColorCategoria = categorias[b]["Categoria"]["ColorCategoria"].AsInt;
                    dbcategoria.CodigoBarras = categorias[b]["Categoria"]["CodigoBarras"].AsInt;
                    //currentCasa.IdCasa = casas[b]["IdCasa"].AsInt;
                    // currentCasa.NombreCasa = casas[b]["NombreCasa"];
                    // currentCasa.FotoCasa = casas[b]["FotoCasa"];
                    // currentCasa.UbicacionCasa = casas[b]["UbicacionCasa"];
                    // currentCasa.CodigoActivacion = casas[b]["CodigoActivacion"];

                    cuCategoria.Add(dbcategoria);
                    
                }
                //currentUser.Name = nUser["name"].Value;
                //currentUser.Lastname = nUser["lastname"].Value;
                //currentUser.Email = nUser["mail"].Value;
                //currentUser.Status = nUser["status"].Value;
                //currentUser.RegisterDate = nUser["register_date"].Value;

                OnSuccess(new MessageArg<List<DBCategoria>>(cuCategoria));
			}
			else
			{
				OnSuccess(new MessageArg<string>(error));
			}
		}
		catch(Exception e)
		{
			OnFailed(new MessageArg<string>(e.Message));	
		}
	}
}
