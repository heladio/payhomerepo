﻿using UnityEngine;
using System.Collections;
using System;
using SimpleJSON;
using System.Collections.Generic;

public class ParseCasa : ProcessRequest
{	
	public ParseCasa(string url, WWWForm form, int type) : base (url, form, type) {}
	
	public override void proccessResponse(EventArgs responseObj)
	{
		try
		{
			MessageArg<string> response = (MessageArg<string>) responseObj;

			JSONNode root = JSON.Parse(response.responseValue);
            
            string error = root["error"].Value;
            //List<DBCasa> casass = new List<DBCasa>();
            if (string.IsNullOrEmpty(error))
			{
                //JSONNode nUser = root["user"];
                //DBCasate dbcasas = new DBCasate();
                List<DBCasate> cuCasa = new List<DBCasate>();
				//currentCasa.IdCasa = root["IdUsuario"].AsInt;
                JSONArray casas = root["Casas"].AsArray;
                for (int b = 0; b < casas.Count; b++)
                {
                    DBCasate dbcasas = new DBCasate();
                    
                    dbcasas.IdCasa = casas[b]["Casa"]["IdCasa"].AsInt;
                    dbcasas.NombreCasa = casas[b]["Casa"]["NombreCasa"];
                    dbcasas.FotoCasa = casas[b]["Casa"]["FotoCasa"];
                    dbcasas.UbicacionCasa = casas[b]["Casa"]["UbicacionCasa"];
                    dbcasas.CodigoActivacion = casas[b]["Casa"]["CodigoActivacion"];
                    //currentCasa.IdCasa = casas[b]["IdCasa"].AsInt;
                    // currentCasa.NombreCasa = casas[b]["NombreCasa"];
                    // currentCasa.FotoCasa = casas[b]["FotoCasa"];
                    // currentCasa.UbicacionCasa = casas[b]["UbicacionCasa"];
                    // currentCasa.CodigoActivacion = casas[b]["CodigoActivacion"];

                    cuCasa.Add(dbcasas);
                    
                }
                //currentUser.Name = nUser["name"].Value;
                //currentUser.Lastname = nUser["lastname"].Value;
                //currentUser.Email = nUser["mail"].Value;
                //currentUser.Status = nUser["status"].Value;
                //currentUser.RegisterDate = nUser["register_date"].Value;

                OnSuccess(new MessageArg<List<DBCasate>>(cuCasa));
			}
			else
			{
				OnSuccess(new MessageArg<string>(error));
			}
		}
		catch(Exception e)
		{
			OnFailed(new MessageArg<string>(e.Message));	
		}
	}
}
