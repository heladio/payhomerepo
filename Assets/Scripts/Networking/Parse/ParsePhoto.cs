﻿using UnityEngine;
using System.Collections;
using System;
using SimpleJSON;


public class ParsePhoto : ProcessRequest
{	
	public ParsePhoto(string url, WWWForm form, int type) : base (url, form, type) {}
	
	public override void proccessResponse(EventArgs responseObj)
	{
		try
		{
			MessageArg<string> response = (MessageArg<string>) responseObj;

			JSONNode root = JSON.Parse(response.responseValue);

			string error = root["error"].Value;
            
            if (string.IsNullOrEmpty(error))
			{
				//JSONNode nUser = root["user"];
				dbphoto dbp = new dbphoto();

				dbp.Id = root["IdUsuario"].AsInt;
                dbp.FotoUsuario = root["FotoUsuario"];
                dbp.IdCasa = root["IdCasa"].AsInt;
                //currentUser.Name = nUser["name"].Value;
                //currentUser.Lastname = nUser["lastname"].Value;
                //currentUser.Email = nUser["mail"].Value;
                //currentUser.Status = nUser["status"].Value;
                //currentUser.RegisterDate = nUser["register_date"].Value;

                OnSuccess(new MessageArg<dbphoto>(dbp));
			}
			else
			{
				OnSuccess(new MessageArg<string>(error));
			}
		}
		catch(Exception e)
		{
			OnFailed(new MessageArg<string>(e.Message));	
		}
	}
}
