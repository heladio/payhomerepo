﻿using UnityEngine;
using System.Collections;
using System;
using SimpleJSON;
using System.Collections.Generic;

public class ParseObtHistorial : ProcessRequest
{	
	public ParseObtHistorial(string url, WWWForm form, int type) : base (url, form, type) {}
	
	public override void proccessResponse(EventArgs responseObj)
	{
		try
		{
			MessageArg<string> response = (MessageArg<string>) responseObj;

			JSONNode root = JSON.Parse(response.responseValue);
            
            string error = root["error"].Value;
            //List<DBCasa> casass = new List<DBCasa>();
            if (string.IsNullOrEmpty(error))
			{
                //JSONNode nUser = root["user"];
                //DBCasate dbcasas = new DBCasate();
                List<DBHistorial> cuHistorial = new List<DBHistorial>();
				//currentCasa.IdCasa = root["IdUsuario"].AsInt;
                JSONArray historiales = root["Historiales"].AsArray;
                for (int b = 0; b < historiales.Count; b++)
                {
                    DBHistorial dbhistoriales = new DBHistorial();
                    dbhistoriales.IdHistorial = historiales[b]["Historial"]["IdHistorial"].AsInt;
                    dbhistoriales.FechaHistorial = historiales[b]["Historial"]["FechaHistorial"];
                    dbhistoriales.IdPagadoCon = historiales[b]["Historial"]["IdPagadoCon"].AsInt;
                    dbhistoriales.IdPagadoEn = historiales[b]["Historial"]["IdPagadoEn"].AsInt;
                    dbhistoriales.IdCategoria = historiales[b]["Historial"]["IdCategoria"].AsInt;
                    dbhistoriales.IconoHistorial = historiales[b]["Historial"]["IconoHistorial"].AsInt;
                    dbhistoriales.ColorHistorial = historiales[b]["Historial"]["ColorHistorial"].AsInt;
                    dbhistoriales.NombreCategoriaHistorial = historiales[b]["Historial"]["NombreCategoriaHistorial"];
                    dbhistoriales.MontoPagoHistorial = historiales[b]["Historial"]["MontoPagoHistorial"];
                    dbhistoriales.IdCasaHistorial = historiales[b]["Historial"]["IdCasaHistorial"].AsInt;
                    dbhistoriales.MesHistorial = historiales[b]["Historial"]["MesHistorial"].AsInt;
                    dbhistoriales.AnoHistorial = historiales[b]["Historial"]["AnoHistorial"].AsInt;
                   
                    //currentCasa.IdCasa = casas[b]["IdCasa"].AsInt;
                    // currentCasa.NombreCasa = casas[b]["NombreCasa"];
                    // currentCasa.FotoCasa = casas[b]["FotoCasa"];
                    // currentCasa.UbicacionCasa = casas[b]["UbicacionCasa"];
                    // currentCasa.CodigoActivacion = casas[b]["CodigoActivacion"];

                    cuHistorial.Add(dbhistoriales);
                    
                }
                //currentUser.Name = nUser["name"].Value;
                //currentUser.Lastname = nUser["lastname"].Value;
                //currentUser.Email = nUser["mail"].Value;
                //currentUser.Status = nUser["status"].Value;
                //currentUser.RegisterDate = nUser["register_date"].Value;

                OnSuccess(new MessageArg<List<DBHistorial>>(cuHistorial));
			}
			else
			{
				OnSuccess(new MessageArg<string>(error));
			}
		}
		catch(Exception e)
		{
			OnFailed(new MessageArg<string>(e.Message));	
		}
	}
}
