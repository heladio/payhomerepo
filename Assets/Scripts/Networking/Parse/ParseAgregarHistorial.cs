﻿using UnityEngine;
using System.Collections;
using System;
using SimpleJSON;
using System.Collections.Generic;

public class ParseAgregarHistorial : ProcessRequest
{	
	public ParseAgregarHistorial(string url, WWWForm form, int type) : base (url, form, type) {}
	
	public override void proccessResponse(EventArgs responseObj)
	{
		try
		{
			MessageArg<string> response = (MessageArg<string>) responseObj;

			JSONNode root = JSON.Parse(response.responseValue);
            
            string error = root["error"].Value;
            //List<DBCasa> casass = new List<DBCasa>();
            if (string.IsNullOrEmpty(error))
            {
                //JSONNode nUser = root["user"];
                DBHistorial dbhistorial = new DBHistorial();

                dbhistorial.IdUsuario = root["IdUsuario"].AsInt;
                dbhistorial.IdHistorial = root["IdHistorial"].AsInt;
                //dbcasas.NombreCasa = casas["Casa"]["NombreCasa"];
                //dbcasas.FotoCa111sa = casas["Casa"]["FotoCasa"];
                //dbcasas.UbicacionCasa = casas["Casa"]["UbicacionCasa"];
                //dbcasas.CodigoActivacion = casas["Casa"]["CodigoActivacion"];

                //currentUser.Name = nUser["name"].Value;
                //currentUser.Lastname = nUser["lastname"].Value;
                //currentUser.Email = nUser["mail"].Value;
                //currentUser.Status = nUser["status"].Value;
                //currentUser.RegisterDate = nUser["register_date"].Value;

                OnSuccess(new MessageArg<DBHistorial>(dbhistorial));
            }

			else
			{
				OnSuccess(new MessageArg<string>(error));
			}
		}
		catch(Exception e)
		{
			OnFailed(new MessageArg<string>(e.Message));	
		}
	}
}
