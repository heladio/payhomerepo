﻿using UnityEngine;
using System.Collections;
using System;
using SimpleJSON;
using System.Collections.Generic;

public class ParseObtPagoPasado : ProcessRequest
{	
	public ParseObtPagoPasado(string url, WWWForm form, int type) : base (url, form, type) {}
	
	public override void proccessResponse(EventArgs responseObj)
	{
		try
		{
			MessageArg<string> response = (MessageArg<string>) responseObj;

			JSONNode root = JSON.Parse(response.responseValue);
            
            string error = root["error"].Value;
            //List<DBCasa> casass = new List<DBCasa>();
            if (string.IsNullOrEmpty(error))
			{
                //JSONNode nUser = root["user"];
                //DBCasate dbcasas = new DBCasate();
                List<DBPagosPasado> cuPagosPasados = new List<DBPagosPasado>();
				//currentCasa.IdCasa = root["IdUsuario"].AsInt;
                JSONArray pagospasado = root["PagosPasados"].AsArray;
                for (int b = 0; b < pagospasado.Count; b++)
                {
                    DBPagosPasado dbpagospasado = new DBPagosPasado();
                    dbpagospasado.IdPagoPasado = pagospasado[b]["PagoPasado"]["IdPagoPasado"].AsInt;
                    dbpagospasado.NombrePagoPasado = pagospasado[b]["PagoPasado"]["NombrePagoPasado"];
                    dbpagospasado.IconoPagoPasado = pagospasado[b]["PagoPasado"]["IconoPagoPasado"].AsInt;
                    dbpagospasado.ColorPagoPasado = pagospasado[b]["PagoPasado"]["ColorPagoPasado"].AsInt;
                    dbpagospasado.IdCasaPasado = pagospasado[b]["PagoPasado"]["IdCasaPasado"].AsInt;
                    dbpagospasado.MontoPagoPasado = pagospasado[b]["PagoPasado"]["MontoPagoPasado"];
                    dbpagospasado.FechaPagoPasado = pagospasado[b]["PagoPasado"]["FechaPagoPasado"];
                    dbpagospasado.RecurrentePagoPasado = pagospasado[b]["PagoPasado"]["RecurrentePagoPasado"].AsInt;
                    dbpagospasado.RepetirPagoPasado = pagospasado[b]["PagoPasado"]["RepetirPagoPasado"].AsInt;
                    dbpagospasado.FechaRecordatorioPagoPasado = pagospasado[b]["PagoPasado"]["FechaRecordatorioPagoPasado"].AsInt;
                    dbpagospasado.IdBeneficiarioPasado = pagospasado[b]["PagoPasado"]["IdBeneficiarioPasado"].AsInt;
                    dbpagospasado.NotaPagoPasado = pagospasado[b]["PagoPasado"]["NotaPagoPasado"];
                    dbpagospasado.ReciboPagoPasado = pagospasado[b]["PagoPasado"]["ReciboPagoPasado"].AsInt;
                    dbpagospasado.CodigoPagoPasado = pagospasado[b]["PagoPasado"]["CodigoPagoPasado"];
                    //currentCasa.IdCasa = casas[b]["IdCasa"].AsInt;
                    // currentCasa.NombreCasa = casas[b]["NombreCasa"];
                    // currentCasa.FotoCasa = casas[b]["FotoCasa"];
                    // currentCasa.UbicacionCasa = casas[b]["UbicacionCasa"];
                    // currentCasa.CodigoActivacion = casas[b]["CodigoActivacion"];

                    cuPagosPasados.Add(dbpagospasado);
                    
                }
                //currentUser.Name = nUser["name"].Value;
                //currentUser.Lastname = nUser["lastname"].Value;
                //currentUser.Email = nUser["mail"].Value;
                //currentUser.Status = nUser["status"].Value;
                //currentUser.RegisterDate = nUser["register_date"].Value;

                OnSuccess(new MessageArg<List<DBPagosPasado>>(cuPagosPasados));
			}
			else
			{
				OnSuccess(new MessageArg<string>(error));
			}
		}
		catch(Exception e)
		{
			OnFailed(new MessageArg<string>(e.Message));	
		}
	}
}
