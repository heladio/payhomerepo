﻿using UnityEngine;
using System.Collections;

public class DBMP
{
	private int? id;
	private string nickname;
	private string fotousuario;
	private int mes;
    private int ano;
    private int sexo;
	private string organization;
	private string gestId;
	private string image;
	private string thumb;
	private string groupName1;
	private string groupName2;
	private string type;
	private string status;
	private string registerDate;

	public DBMP()
	{
		id = null;
		nickname = "";
		mes = 0;
        ano = 0;
        sexo = 0;
		fotousuario = "";
		organization = "";
		gestId = "";
		image = "";
		thumb = "";
		groupName1 = "";
		groupName2 = "";
		type = "";
		status = "";
		registerDate = "";
	}

	public int? Id {
		get {
			return this.id;
		}
		set {
			id = value;
		}
	}

	public string Nickname {
		get {
			return this.nickname;
		}
		set {
			nickname = value;
		}
	}

	public string Fotousuario {
		get {
			return this.fotousuario;
		}
		set {
			fotousuario = value;
		}
	}

	public int Mes {
		get {
			return this.mes;
		}
		set {
			mes = value;
		}
	}
    public int Ano
    {
        get
        {
            return this.ano;
        }
        set
        {
            ano = value;
        }
    }

    public int Sexo {
		get {
			return this.sexo;
		}
		set {
			sexo = value;
		}
	}

	public string Organization {
		get {
			return this.organization;
		}
		set {
			organization = value;
		}
	}

	public string GestId {
		get {
			return this.gestId;
		}
		set {
			gestId = value;
		}
	}

	public string Image {
		get {
			return this.image;
		}
		set {
			image = value;
		}
	}

	public string Thumb {
		get {
			return this.thumb;
		}
		set {
			thumb = value;
		}
	}

	public string GroupName1 {
		get {
			return this.groupName1;
		}
		set {
			groupName1 = value;
		}
	}

	public string GroupName2 {
		get {
			return this.groupName2;
		}
		set {
			groupName2 = value;
		}
	}

	public string Type {
		get {
			return this.type;
		}
		set {
			type = value;
		}
	}

	public string Status{
		get{ return this.status;}
		set{ status = value;}
	}
	public string RegisterDate{
		get{ return this.registerDate;}
		set{ registerDate = value;}
	}
	/*
	public override string ToString ()
	{
		return string.Format ("[User: name={0}, lastname={1}, email={2}, level={3}, image={4}, group={5}, type={6}, id={7}]", name, lastname, email, level, image, group, type, id);
	}
	*/

}
