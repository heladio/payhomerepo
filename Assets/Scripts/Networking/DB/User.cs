﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class User
{
    public List<DBCasate> casas;
    private int? id;
	private string name;
	private string lastname;
	private string email;
	private string level;
	private string organization;
	private string gestId;
	private string image;
	private string thumb;
	private string groupName1;
	private string groupName2;
	private string type;
	private string status;
	private string registerDate;

	public User()
	{
		id = null;
		name = "";
		lastname = "";
		email = "";
		level = "";
		organization = "";
		gestId = "";
		image = "";
		thumb = "";
		groupName1 = "";
		groupName2 = "";
		type = "";
		status = "";
		registerDate = "";
	}

	public int? Id {
		get {
			return this.id;
		}
		set {
			id = value;
		}
	}

	public string Name {
		get {
			return this.name;
		}
		set {
			name = value;
		}
	}

	public string Lastname {
		get {
			return this.lastname;
		}
		set {
			lastname = value;
		}
	}

	public string Email {
		get {
			return this.email;
		}
		set {
			email = value;
		}
	}

	public string Level {
		get {
			return this.level;
		}
		set {
			level = value;
		}
	}

	public string Organization {
		get {
			return this.organization;
		}
		set {
			organization = value;
		}
	}

	public string GestId {
		get {
			return this.gestId;
		}
		set {
			gestId = value;
		}
	}

	public string Image {
		get {
			return this.image;
		}
		set {
			image = value;
		}
	}

	public string Thumb {
		get {
			return this.thumb;
		}
		set {
			thumb = value;
		}
	}

	public string GroupName1 {
		get {
			return this.groupName1;
		}
		set {
			groupName1 = value;
		}
	}

	public string GroupName2 {
		get {
			return this.groupName2;
		}
		set {
			groupName2 = value;
		}
	}

	public string Type {
		get {
			return this.type;
		}
		set {
			type = value;
		}
	}

	public string Status{
		get{ return this.status;}
		set{ status = value;}
	}
	public string RegisterDate{
		get{ return this.registerDate;}
		set{ registerDate = value;}
	}
	/*
	public override string ToString ()
	{
		return string.Format ("[User: name={0}, lastname={1}, email={2}, level={3}, image={4}, group={5}, type={6}, id={7}]", name, lastname, email, level, image, group, type, id);
	}
	*/

}
