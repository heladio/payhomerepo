﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class dbphoto
{
    public List<DBCasate> casas;
    private int id, idCasa;
	private string fotousuario;

	public dbphoto()
	{
		id = 0;
		fotousuario = "";
	}

	public int Id {
		get {
			return this.id;
		}
		set {
			id = value;
		}
	}
    public int IdCasa
    {
        get
        {
            return this.idCasa;
        }
        set
        {
            idCasa = value;
        }
    }

    public string FotoUsuario {
		get {
			return this.fotousuario;
		}
		set {
			fotousuario = value;
		}
	}
	/*
	public override string ToString ()
	{
		return string.Format ("[User: name={0}, lastname={1}, email={2}, level={3}, image={4}, group={5}, type={6}, id={7}]", name, lastname, email, level, image, group, type, id);
	}
	*/

}
