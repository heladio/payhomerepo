﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DBCasate
{
    //private List<ArrayList> casas = new List<ArrayList>();
    //public ArrayList casa;
    private int idCasa;
    private int idUsuario;
    private string nombreCasa;
	private string fotoCasa;
	private string ubicacionCasa;
	private string codigoActivacion;

	public DBCasate()
	{
        //casa.SetRange(5);
        //casa[0] = 0;
        //casa[1] = null;
        //casa[2] = null;
        //casa[3] = null;
        //casa[4] = null;
        idUsuario = 0;
		idCasa = 0;
		nombreCasa = "";
		fotoCasa = "";
		ubicacionCasa = "";
		codigoActivacion = "";
	}
    //public List<DBCasa> db
    //{
    //    get
    //    {
    //        return this.DB;
    //    }
    //    set
    //    {
    //        DB = value;
    //    }

    //}
    public int IdUsuario
    {
        get
        {
            return this.idUsuario;
        }
        set
        {
            idUsuario = value;
        }
    }
    public int IdCasa {
		get {
			return this.idCasa;
		}
		set {
			idCasa = value;
		}
	}

	public string NombreCasa {
		get {
			return this.nombreCasa;
		}
		set {
			nombreCasa = value;
		}
	}

	public string FotoCasa {
		get {
			return this.fotoCasa;
		}
		set {
			fotoCasa = value;
		}
	}

	public string UbicacionCasa {
		get {
			return this.ubicacionCasa;
		}
		set {
			ubicacionCasa = value;
		}
	}

	public string CodigoActivacion {
		get {
			return this.codigoActivacion;
		}
		set {
			codigoActivacion = value;
		}
	}

	
	/*
	public override string ToString ()
	{
		return string.Format ("[User: name={0}, lastname={1}, email={2}, level={3}, image={4}, group={5}, type={6}, id={7}]", name, lastname, email, level, image, group, type, id);
	}
	*/

}
