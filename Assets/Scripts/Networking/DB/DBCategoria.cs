﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DBCategoria
{
    //private List<ArrayList> casas = new List<ArrayList>();
    //public ArrayList casa;
    private int idUsuario;
    private int idCategoria;
	private string nombreCategoria;
	private int colorCategoria;
	private int iconoCategoria;
	private int codigoBarras;

	public DBCategoria()
	{
        //casa.SetRange(5);
        //casa[0] = 0;
        //casa[1] = null;
        //casa[2] = null;
        //casa[3] = null;
        //casa[4] = null;
        idUsuario = 0;
		idCategoria = 0;
		nombreCategoria = "";
		colorCategoria = 0;
		iconoCategoria = 0;
		codigoBarras = 0;
	}
    public int IdUsuario
    {
        get
        {
            return this.idUsuario;
        }
        set
        {
            idUsuario = value;
        }
    }
    public int IdCategoria {
		get {
			return this.idCategoria;
		}
		set {
			idCategoria = value;
		}
	}

    public string NombreCategoria {
        get{
            return this.nombreCategoria;
        }
        set{
            nombreCategoria = value;
        }
    }

    public int ColorCategoria {
		get {
			return this.colorCategoria;
		}
		set {
			colorCategoria = value;
		}
	}

	public int IconoCategoria {
		get {
			return this.iconoCategoria;
		}
		set {
			iconoCategoria = value;
		}
	}

	public int CodigoBarras {
		get {
			return this.codigoBarras;
		}
		set {
			codigoBarras = value;
		}
	}

	
	/*
	public override string ToString ()
	{
		return string.Format ("[User: name={0}, lastname={1}, email={2}, level={3}, image={4}, group={5}, type={6}, id={7}]", name, lastname, email, level, image, group, type, id);
	}
	*/

}
