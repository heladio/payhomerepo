﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DBBeneficiario
{
    //private List<ArrayList> casas = new List<ArrayList>();
    //public ArrayList casa;
    private int idUsuario;
    private int idBeneficiario;
    private string nombreBeneficiario;
	private string clabeBeneficiario;
	private string referenciaBeneficiario;
	private string emailBeneficiario;
    private string telefonoBeneficiario;

    public DBBeneficiario()
	{
        //casa.SetRange(5);
        //casa[0] = 0;
        //casa[1] = null;
        //casa[2] = null;
        //casa[3] = null;
        //casa[4] = null;
        idUsuario = 0;
		idBeneficiario = 0;
		nombreBeneficiario = "";
		clabeBeneficiario = "";
		referenciaBeneficiario = "";
		emailBeneficiario = "";
        telefonoBeneficiario = "";
    }
    //public List<DBCasa> db
    //{
    //    get
    //    {
    //        return this.DB;
    //    }
    //    set
    //    {
    //        DB = value;
    //    }

    //}
    public int IdUsuario
    {
        get
        {
            return this.idUsuario;
        }
        set
        {
            idUsuario = value;
        }
    }
    public int IdBeneficiario {
		get {
			return this.idBeneficiario;
		}
		set {
			idBeneficiario = value;
		}
	}
    public string NombreBeneficiario
    {
        get
        {
            return this.nombreBeneficiario;
        }
        set
        {
            nombreBeneficiario = value;
        }
    }

    public string ClabeBeneficiario {
		get {
			return this.clabeBeneficiario;
		}
		set {
			clabeBeneficiario = value;
		}
	}

	public string ReferenciaBeneficiario {
		get {
			return this.referenciaBeneficiario;
		}
		set {
			referenciaBeneficiario = value;
		}
	}

	public string EmailBeneficiario {
		get {
			return this.emailBeneficiario;
		}
		set {
			emailBeneficiario = value;
		}
	}

	public string TelefonoBeneficiario {
		get {
			return this.telefonoBeneficiario;
		}
		set {
			telefonoBeneficiario = value;
		}
	}

	
	/*
	public override string ToString ()
	{
		return string.Format ("[User: name={0}, lastname={1}, email={2}, level={3}, image={4}, group={5}, type={6}, id={7}]", name, lastname, email, level, image, group, type, id);
	}
	*/

}
