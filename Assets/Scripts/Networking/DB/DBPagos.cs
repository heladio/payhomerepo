﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class DBPagos
{
    //private List<ArrayList> casas = new List<ArrayList>();
    //public ArrayList casa;
    private int idUsuario;
    private int idPago;
	private string nombrePago;
	private int colorPago;
	private int iconoPago;
	private int idCasa;
    private string montoPago;
    private string fechaPago;
    private int recurrentePago;
    private int repetirPago;
    private int fechaRecordatorioPago;
    private int idBeneficiario;
    private string notaPago;
    private int reciboPago;
    private string codigoPago;

    public DBPagos()
	{
        //casa.SetRange(5);
        //casa[0] = 0;
        //casa[1] = null;
        //casa[2] = null;
        //casa[3] = null;
        //casa[4] = null;
        idUsuario = 0;
		idPago = 0;
		nombrePago = null;
		colorPago = 0;
		iconoPago = 0;
		idCasa = 0;
        montoPago = null;
        //fechaPago = 0;
        recurrentePago = 0;
        repetirPago = 0;
        fechaRecordatorioPago = 0;
        idBeneficiario = 0;
        notaPago = null;
        reciboPago = 0;
        codigoPago = null;
	}
    public int IdUsuario
    {
        get
        {
            return this.idUsuario;
        }
        set
        {
            idUsuario = value;
        }
    }
    public int IdPago {
		get {
			return this.idPago;
		}
		set {
			idPago = value;
		}
	}

    public string NombrePago {
        get{
            return this.nombrePago;
        }
        set{
            nombrePago = value;
        }
    }

    public int ColorPago {
		get {
			return this.colorPago;
		}
		set {
			colorPago = value;
		}
	}

	public int IconoPago {
		get {
			return this.iconoPago;
		}
		set {
			iconoPago = value;
		}
	}

	public int IdCasa {
		get {
			return this.idCasa;
		}
		set {
			idCasa = value;
		}
	}
    public string MontoPago
    {
        get
        {
            return this.montoPago;
        }
        set
        {
            montoPago = value;
        }
    }
    public string FechaPago
    {
        get
        {
            return this.fechaPago;
        }
        set
        {
            fechaPago = value;
        }
    }
    public int RecurrentePago
    {
        get
        {
            return this.recurrentePago;
        }
        set
        {
            recurrentePago = value;
        }
    }
    public int RepetirPago
    {
        get
        {
            return this.repetirPago;
        }
        set
        {
            repetirPago = value;
        }
    }
    public int FechaRecordatorioPago
    {
        get
        {
            return this.fechaRecordatorioPago;
        }
        set
        {
            fechaRecordatorioPago = value;
        }
    }
    public int IdBeneficiario
    {
        get
        {
            return this.idBeneficiario;
        }
        set
        {
            idBeneficiario = value;
        }
    }
    public string NotaPago
    {
        get
        {
            return this.notaPago;
        }
        set
        {
            notaPago = value;
        }
    }
    public int ReciboPago
    {
        get
        {
            return this.reciboPago;
        }
        set
        {
            reciboPago = value;
        }
    }
    public string CodigoPago
    {
        get
        {
            return this.codigoPago;
        }
        set
        {
            codigoPago = value;
        }
    }
    /*
	public override string ToString ()
	{
		return string.Format ("[User: name={0}, lastname={1}, email={2}, level={3}, image={4}, group={5}, type={6}, id={7}]", name, lastname, email, level, image, group, type, id);
	}
	*/

}
