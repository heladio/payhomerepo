﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class DBPagosPasado
{
    //private List<ArrayList> casas = new List<ArrayList>();
    //public ArrayList casa;
    private int idUsuario;
    private int idPagoPasado;
	private string nombrePagoPasado;
	private int colorPagoPasado;
	private int iconoPagoPasado;
	private int idCasaPasado;
    private string montoPagoPasado;
    private string fechaPagoPasado;
    private int recurrentePagoPasado;
    private int repetirPagoPasado;
    private int fechaRecordatorioPagoPasado;
    private int idBeneficiarioPasado;
    private string notaPagoPasado;
    private int reciboPagoPasado;
    private string codigoPagoPasado;

    public DBPagosPasado()
	{
        //casa.SetRange(5);
        //casa[0] = 0;
        //casa[1] = null;
        //casa[2] = null;
        //casa[3] = null;
        //casa[4] = null;
        idUsuario = 0;
		idPagoPasado = 0;
		nombrePagoPasado = null;
		colorPagoPasado = 0;
		iconoPagoPasado = 0;
		idCasaPasado = 0;
        montoPagoPasado = null;
        //fechaPago = 0;
        recurrentePagoPasado = 0;
        repetirPagoPasado = 0;
        fechaRecordatorioPagoPasado = 0;
        idBeneficiarioPasado = 0;
        notaPagoPasado = null;
        reciboPagoPasado = 0;
        codigoPagoPasado = null;
	}
    public int IdUsuario
    {
        get
        {
            return this.idUsuario;
        }
        set
        {
            idUsuario = value;
        }
    }
    public int IdPagoPasado {
		get {
			return this.idPagoPasado;
		}
		set {
			idPagoPasado = value;
		}
	}

    public string NombrePagoPasado {
        get{
            return this.nombrePagoPasado;
        }
        set{
            nombrePagoPasado = value;
        }
    }

    public int ColorPagoPasado {
		get {
			return this.colorPagoPasado;
		}
		set {
			colorPagoPasado = value;
		}
	}

	public int IconoPagoPasado {
		get {
			return this.iconoPagoPasado;
		}
		set {
			iconoPagoPasado = value;
		}
	}

	public int IdCasaPasado {
		get {
			return this.idCasaPasado;
		}
		set {
			idCasaPasado = value;
		}
	}
    public string MontoPagoPasado
    {
        get
        {
            return this.montoPagoPasado;
        }
        set
        {
            montoPagoPasado = value;
        }
    }
    public string FechaPagoPasado
    {
        get
        {
            return this.fechaPagoPasado;
        }
        set
        {
            fechaPagoPasado = value;
        }
    }
    public int RecurrentePagoPasado
    {
        get
        {
            return this.recurrentePagoPasado;
        }
        set
        {
            recurrentePagoPasado = value;
        }
    }
    public int RepetirPagoPasado
    {
        get
        {
            return this.repetirPagoPasado;
        }
        set
        {
            repetirPagoPasado = value;
        }
    }
    public int FechaRecordatorioPagoPasado
    {
        get
        {
            return this.fechaRecordatorioPagoPasado;
        }
        set
        {
            fechaRecordatorioPagoPasado = value;
        }
    }
    public int IdBeneficiarioPasado
    {
        get
        {
            return this.idBeneficiarioPasado;
        }
        set
        {
            idBeneficiarioPasado = value;
        }
    }
    public string NotaPagoPasado
    {
        get
        {
            return this.notaPagoPasado;
        }
        set
        {
            notaPagoPasado = value;
        }
    }
    public int ReciboPagoPasado
    {
        get
        {
            return this.reciboPagoPasado;
        }
        set
        {
            reciboPagoPasado = value;
        }
    }
    public string CodigoPagoPasado
    {
        get
        {
            return this.codigoPagoPasado;
        }
        set
        {
            codigoPagoPasado = value;
        }
    }
    /*
	public override string ToString ()
	{
		return string.Format ("[User: name={0}, lastname={1}, email={2}, level={3}, image={4}, group={5}, type={6}, id={7}]", name, lastname, email, level, image, group, type, id);
	}
	*/

}
