﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class DBHistorial
{
    //private List<ArrayList> casas = new List<ArrayList>();
    //public ArrayList casa;
    private int idUsuario;
    private int idHistorial;
	private string fechaHistorial;
    private int idPagadoCon;
    private int idPagadoEn;
    private int idCategoria;
    private int iconoHistorial;
    private int colorHistorial;
    private string nombreCategoriaHistorial;
    private string montoPagoHistorial;
    private int idCasaHistorial;
    private int mesHistorial;
    private int anoHistorial;

    public DBHistorial()
	{
        //casa.SetRange(5);
        //casa[0] = 0;
        //casa[1] = null;
        //casa[2] = null;
        //casa[3] = null;
        //casa[4] = null;
        idUsuario = 0;
        idHistorial = 0;
        fechaHistorial = null;
        idPagadoCon = 0;
        idPagadoEn = 0;
        idCategoria = 0;
        iconoHistorial = 0;
        colorHistorial = 0;
        nombreCategoriaHistorial = null;
        montoPagoHistorial = null;
        idCasaHistorial = 0;
        mesHistorial = 0;
        anoHistorial = 0;
	}
    public int IdUsuario
    {
        get
        {
            return this.idUsuario;
        }
        set
        {
            idUsuario = value;
        }
    }
    public int IdHistorial {
		get {
			return this.idHistorial;
		}
		set {
			idHistorial = value;
		}
	}

    public string FechaHistorial {
        get{
            return this.fechaHistorial;
        }
        set{
            fechaHistorial = value;
        }
    }

    public int IdPagadoCon {
		get {
			return this.idPagadoCon;
		}
		set {
			idPagadoCon = value;
		}
	}

	public int IdPagadoEn {
		get {
			return this.idPagadoEn;
		}
		set {
			idPagadoEn = value;
		}
	}

	public int IdCategoria {
		get {
			return this.idCategoria;
		}
		set {
			idCategoria = value;
		}
	}
    public int IconoHistorial
    {
        get
        {
            return this.iconoHistorial;
        }
        set
        {
            iconoHistorial = value;
        }
    }
    public int ColorHistorial
    {
        get
        {
            return this.colorHistorial;
        }
        set
        {
            colorHistorial = value;
        }
    }
    public string NombreCategoriaHistorial
    {
        get
        {
            return this.nombreCategoriaHistorial;
        }
        set
        {
            nombreCategoriaHistorial = value;
        }
    }
    public string MontoPagoHistorial
    {
        get
        {
            return this.montoPagoHistorial;
        }
        set
        {
            montoPagoHistorial = value;
        }
    }
    public int IdCasaHistorial
    {
        get
        {
            return this.idCasaHistorial;
        }
        set
        {
            idCasaHistorial = value;
        }
    }
    public int MesHistorial
    {
        get
        {
            return this.mesHistorial;
        }
        set
        {
            mesHistorial = value;
        }
    }
    public int AnoHistorial
    {
        get
        {
            return this.anoHistorial;
        }
        set
        {
            anoHistorial = value;
        }
    }
    
    /*
	public override string ToString ()
	{
		return string.Format ("[User: name={0}, lastname={1}, email={2}, level={3}, image={4}, group={5}, type={6}, id={7}]", name, lastname, email, level, image, group, type, id);
	}
	*/

}
