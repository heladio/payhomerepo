﻿using UnityEngine;
using System.Collections;
using System;

public class Test : MonoBehaviour {

	void Start () {
		WWWForm nf = new WWWForm();
		nf.AddField("name", "ale");
		nf.AddField("lastname", "azpi");
		WebServices.RegisterUser(nf, GoodUser, BadUser);
	}

	void GoodUser(System.Object sender, EventArgs e)
	{
		if(e is MessageArg<string>)
		{
			Debug.Log("Error="+((MessageArg<string>) e).responseValue);
		}
		else if(e is MessageArg<User>)
		{
			User user = ((MessageArg<User>) e).responseValue;
			//Debug.Log("User added="+user.Name);
		}

	}
	void BadUser(System.Object sender, EventArgs e)
	{
		string message = "";
		if(((MessageArg<string>) e) != null)
		{
			message = ((MessageArg<string>) e).responseValue;
		}
		else
		{
			message = e.ToString();
		}
		
		Debug.Log("Pepsico Connection Error= " + message);
	}
}
