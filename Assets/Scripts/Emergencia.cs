﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Emergencia : MonoBehaviour {

	// Use this for initialization
	public void Emergencias (string nombre)
    {
        switch (nombre)
        {
            case "InformacionGeneral":
                Application.OpenURL("tel://040");
                break;
            case "RadioPatrullas":
                Application.OpenURL("tel://066");
                break;
            case "ProteccionCivil":
                Application.OpenURL("tel://56832222");
                break;
            case "Emergencias":
                Application.OpenURL("tel://911");
                break;
            case "SistemaDeAguas":
                Application.OpenURL("tel://56543210");
                break;
            case "Locatel":
                Application.OpenURL("tel://56581111");
                break;
            case "Bomberos":
                Application.OpenURL("tel://068");
                break;
            case "CruzRoja":
                Application.OpenURL("tel://065");
                break;
            case "DenunciaAnonima":
                Application.OpenURL("tel://089");
                break;
            case "Policia":
                Application.OpenURL("tel://060");
                break;
            case "Setravi":
                Application.OpenURL("tel://52099913");
                break;
            case "Cazabaches":
                Application.OpenURL("tel://50622243");
                break;
            case "EmergenciasEnCarretera":
                Application.OpenURL("tel://074");
                break;
        }
    }
}
