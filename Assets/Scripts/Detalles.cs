﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Globalization;

public class Detalles : MonoBehaviour {
    CultureInfo ci = new CultureInfo("es-mx");
    public Text MontoText, PagadoEn, PagadoCon, FechaPago, Categoria, Casa;
    public Image IconoCat, ColorCat, IconoPagadoEn, ColorTextoPagadoEn;
    public int IdCasa, IdIconoCat, IdColorCat, IdPagadoCon, IdPagadoEn, idCategoria;
    public float MontoInt;
    public string MontoString, PagadoEnString, PagadoConString, FechaPagoString, CategoriaString, CasaString, mes, mesComp;
    public AP ap;
    public Casas casa;
    public bool leer;
    public Color colorback;
    public InfoCasaMH ICMH;
    public DateTime fecha;
    public TP tp;
    public bool NoP;
    // Use this for initialization

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (leer == true)
        {
            NoP = false;
            leer = false;
            for(int ui = 0; ui<10; ui++)
            {
                if(Categoria.text == tp.NombreCategoria[ui])
                {
                    NoP = true;
                }
            }
            if (NoP == true)
            {
                NoP = false;
                ColorCat.color =ap.tp.iconColorBack;
                IconoCat.sprite = ap.tp.PagoPredeterminado[IdIconoCat];
            }
            else 
            {
                IconoCat.sprite = ap.tp.IC[IdIconoCat];
                ColorCat.color = ap.tp.CO[IdColorCat];
            }
            
            IconoPagadoEn.sprite = ap.PagEnIcono[IdPagadoEn];
            MontoText.text = MontoInt.ToString("C", ci);
            PagadoEn.text = ap.PagEn[IdPagadoEn];
            PagadoEn.color = ap.PagEnColor[IdPagadoEn];
            PagadoCon.text = ap.PagCon[IdPagadoCon];
            Casa.text = ICMH.NickName;
            
            //fecha = DateTime.ParseExact(FechaPagoString, "d/M/yyyy", ci);
            //mesComp = fecha.Month.ToString();
            //if (mesComp == "1")
            //    mes = "Enero";
            //if (mesComp == "2")
            //    mes = "Febrero";
            //if (mesComp == "3")
            //    mes = "Marzo";
            //if (mesComp == "4")
            //    mes = "Abril";
            //if (mesComp == "5")
            //    mes = "Mayo";
            //if (mesComp == "6")
            //    mes = "Junio";
            //if (mesComp == "7")
            //    mes = "Julio";
            //if (mesComp == "8")
            //    mes = "Agosto";
            //if (mesComp == "9")
            //    mes = "Septiembre";
            //if (mesComp == "10")
            //    mes = "Octubre";
            //if (mesComp == "11")
            //    mes = "Noviembre";
            //if (mesComp == "12")
            //    mes = "Diciembre";
            //FechaPago.text = fecha.Day.ToString() + "/" + mes + "/" + fecha.Year.ToString();
            FechaPago.text = FechaPagoString;
        }
            }
}
