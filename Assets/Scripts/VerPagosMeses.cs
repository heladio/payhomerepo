﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

public class VerPagosMeses : MonoBehaviour {
    public AP ap;
    public InfoPago IP, IP2, IPPasado;
    public int mes, año, mes2, año2, mes3, año3, i, l, mesap, añoap,mesap2, añoap2, mesap3, añoap3, o, pa, acomodo, p;
    public float total, total2, total3, totalPasado;
    public List<GameObject> Mes, Mes2, Mes3, MesPasado = new List<GameObject>();
    public bool chequeo;
    public GameObject contenidoMes, contenidoMes2, contenidoMes3, SeleccionMes1, SeleccionMes2, SeleccionMes3Pantalla1, SeleccionMes3Pantalla2, contenidoMesPasado;
    public Text mestextoPantalla1, mes2textoPantalla1, mes3textoPantalla1, mestextoPantalla2, mes2textoPantalla2, mes3textoPantalla2, mestextoPantalla3, mes2textoPantalla3, mes3textoPantalla3, 
        totaltexto, total2texto, total3texto;
    public string messtring, añostring, mes2string, año2string, mes3string, año3string;
    CultureInfo ci = new CultureInfo("es-mx");
    ObjectManager ob;
    // Use this for initialization
    void Awake () {
        ob = GameObject.Find("ObjectManager").GetComponent<ObjectManager>();
	}

    // Update is called once per frame
    void Update() {

        //if (Mes.Count == 1)
        //{
        //    IP = ap.Mes[0].GetComponent<InfoPago>();
        //    total = float.Parse(IP.PrecioPago);
        //    totaltexto.text = total.ToString("C", ci);
        //}
        if (chequeo == true)
        {
            chequeo = false;
            X();
        }


        /*if (Mes.Count == 0)
        {
            total = 0;
            totaltexto.text = total.ToString("C", ci);
        }*/
        total = 0;

        for (int p = 0; ap.pagos.GetComponentsInChildren<InfoPago>().Length > p; p++)
        {
            if (ap.pagos.GetComponentsInChildren<InfoPago>()[p].gameObject != null)
            {
                IP2 = ap.pagos.GetComponentsInChildren<InfoPago>()[p].GetComponent<InfoPago>();
                if (IP2.gameObject.activeInHierarchy)
                {
                    total += float.Parse(IP2.PrecioPago);

                }
            }
        }
        totaltexto.text = total.ToString("C", ci);
    }
    IEnumerator DelayChequeo()
    {
        yield return new WaitForSeconds(1.5f);
        X();
    }
    public void X()
    {

        //if (chequeo == true)
        //{
        //    chequeo = false;
        Mes.Clear();
        Mes2.Clear();
        Mes3.Clear();

        SeleccionMes1.SetActive(false);
        SeleccionMes2.SetActive(false);

        SeleccionMes3Pantalla1.SetActive(false);
        SeleccionMes3Pantalla2.SetActive(false);

        total = 0;
        total2 = 0;
        total3 = 0;

        //pa = 0;

        InfoPago lastPago = null;
        int mon1 =0, mon2 =0, year1=0, year2=0;

        for (int po = 0; po < ap.PagosIndex.Count; po++)
        {
            IP = ap.PagosIndex[po].GetComponent<InfoPago>();
            if (IP.idCasa == ap.idCasa)
            {
                IP.acomodo = 0;
                if (lastPago == null)
                {
                    lastPago = IP;
                    mon1 = IP.MesNum;
                    year1 = IP.AñoNum;
                }
                else if (IP.AñoNum == lastPago.AñoNum && IP.MesNum > lastPago.MesNum)
                {
                    mon2 = IP.MesNum;
                    year2 = IP.AñoNum;
                }
                else if (IP.AñoNum > lastPago.AñoNum && mon2 == 0 && year2 == 0)
                {
                    mon2 = IP.MesNum;
                    year2 = IP.AñoNum;
                }
                else if (IP.AñoNum != year1 && IP.MesNum != mon1 && mon2 == 0 && year2 == 0)
                {
                    mon2 = IP.MesNum;
                    year2 = IP.AñoNum;
                }
            }
        }

        for (int po = 0; po < ap.PagosIndex.Count; po++)
        {
            IP = ap.PagosIndex[po].GetComponent<InfoPago>();
            if (IP.idCasa == ap.idCasa)
            {
                if (IP.AñoNum == year1 && IP.MesNum == mon1)
                {
                    IP.transform.SetParent(contenidoMes.transform);
                    total += float.Parse(IP.PrecioPago);
                    Mes.Add(IP.gameObject);
                    SeleccionMes1.SetActive(true);
                    mestextoPantalla1.text = IP.Mes + " " + IP.Año;
                    mestextoPantalla2.text = IP.Mes + " " + IP.Año;
                    mestextoPantalla3.text = IP.Mes + " " + IP.Año;

                }
                else if (IP.AñoNum == year2 && IP.MesNum == mon2)
                {
                    IP.transform.SetParent(contenidoMes2.transform);
                    total2 += float.Parse(IP.PrecioPago);
                    Mes2.Add(IP.gameObject);
                    SeleccionMes2.SetActive(true);

                    mes2textoPantalla1.text = IP.Mes + " " + IP.Año;
                    mes2textoPantalla2.text = IP.Mes + " " + IP.Año;
                    mes2textoPantalla3.text = IP.Mes + " " + IP.Año;
                }
                else
                {
                    IP.transform.SetParent(contenidoMes3.transform);
                    total3 += float.Parse(IP.PrecioPago);
                    Mes3.Add(IP.gameObject);
                    SeleccionMes3Pantalla1.SetActive(true);
                    SeleccionMes3Pantalla2.SetActive(true);
                }
            }
        }

        totaltexto.text = total.ToString("C", ci);
        total2texto.text = total2.ToString("C", ci);
        total3texto.text = total3.ToString("C", ci);

        /*i = 0;

        if (ap.PagosIndex.Count != 0 && pa < ap.PagosIndex.Count)
        {

            for (o = 0; (ap.PagosIndex.Count) > o; o++)
            {
                IP = ap.PagosIndex[o].GetComponent<InfoPago>();
                mes = IP.MesNum;
                año = IP.AñoNum;
                messtring = IP.Mes;
                añostring = IP.Año;
                if (i == 0 && (IP.nombreCasa == ap.nombreCasaMenu))
                {
                    print("first " + Mes2);
                    mesap = mes;
                    añoap = año;
                    Mes.Add(IP.gameObject);
                    i++;
                    pa++;
                    IP.acomodo = 1;
                    acomodo = IP.acomodo;
                    IP.ventana = 0;
                    ap.PagosIndex[o].transform.SetParent(contenidoMes.transform);
                    total = float.Parse(IP.PrecioPago);
                    SeleccionMes1.SetActive(true);
                    mestextoPantalla1.text = messtring + " " + añostring;
                    mestextoPantalla2.text = messtring + " " + añostring;
                    mestextoPantalla3.text = messtring + " " + añostring;
                    //ap.PagosIndex[o].transform.position = contenidoMes.transform.position;
                }
                for (int p = 0; ap.PagosIndex.Count > p; p++)
                {
                    IP2 = ap.PagosIndex[p].GetComponent<InfoPago>();
                    mes2 = IP2.MesNum;
                    año2 = IP2.AñoNum;
                    mes2string = IP2.Mes;
                    año2string = IP2.Año;
                    acomodo = IP2.acomodo;
                    if ((mes2 == mes && año2 == año) && i == 1 && acomodo == 0 && (IP2.nombreCasa == ap.nombreCasaMenu))
                    {
                        print("1 " + IP2.PrecioPago + " " + mes2);
                        Mes.Add(IP2.gameObject);
                        pa++;
                        IP2.acomodo = 1;
                        acomodo = IP2.acomodo;
                        IP2.ventana = 0;
                        ap.PagosIndex[p].transform.SetParent(contenidoMes.transform);
                        total += float.Parse(IP2.PrecioPago);
                    }
                    else if ((mes2 != mes || año2 != año) && i == 1 && acomodo == 0 && (IP2.nombreCasa == ap.nombreCasaMenu))
                    {
                        print("2 " + IP2.PrecioPago +" " + mes2);

                        mesap2 = mes2;
                        añoap2 = año2;
                        mes = mes2;
                        año = año2;
                        Mes2.Add(IP2.gameObject);
                        i++;
                        IP2.acomodo = 1;
                        acomodo = IP2.acomodo;
                        IP2.ventana = 1;
                        ap.PagosIndex[p].transform.SetParent(contenidoMes2.transform);
                        SeleccionMes2.SetActive(true);
                        mes2textoPantalla1.text = mes2string + " " + año2string;
                        mes2textoPantalla2.text = mes2string + " " + año2string;
                        mes2textoPantalla3.text = mes2string + " " + año2string;
                        total2 = float.Parse(IP2.PrecioPago);
                    }
                    else if ((mes2 == mes && año2 == año) && i == 2 && acomodo == 0 && (IP2.nombreCasa == ap.nombreCasaMenu))
                    {
                        print("3 " + IP2.PrecioPago + " " + mes2);

                        Mes2.Add(IP2.gameObject);
                        pa++;
                        IP2.acomodo = 1;
                        acomodo = IP2.acomodo;
                        IP2.ventana = 1;
                        ap.PagosIndex[p].transform.SetParent(contenidoMes2.transform);
                        total2 += float.Parse(IP2.PrecioPago);
                    }
                    else if ((mes2 != mes && año2 != año) && i == 2 && acomodo == 0 && (IP2.nombreCasa == ap.nombreCasaMenu))
                    {
                        print("4 " + IP2.PrecioPago + " " + mes2 + " "+año);
                        print(mes + " " + año);


                        mesap3 = mes2;
                        añoap3 = año2;
                        Mes3.Add(IP2.gameObject);
                        pa++;
                        i++;
                        IP2.acomodo = 1;
                        acomodo = IP2.acomodo;
                        IP2.ventana = 2;
                        ap.PagosIndex[p].transform.SetParent(contenidoMes3.transform);
                        SeleccionMes3Pantalla1.SetActive(true);
                        SeleccionMes3Pantalla2.SetActive(true);
                        total3 = float.Parse(IP2.PrecioPago);
                    }
                    else if ((mes2 >= mesap3 || año2 >= añoap3) && i == 3 && acomodo == 0 && (IP2.nombreCasa == ap.nombreCasaMenu)/*&& p <= ap.PagosIndex.Count)
                    {
                        print("5 " + IP2.PrecioPago + " " + mes2);

                        Mes3.Add(IP2.gameObject);

                        pa++;
                        IP2.acomodo = 1;
                        acomodo = IP2.acomodo;
                        IP2.ventana = 2;
                        ap.PagosIndex[p].transform.SetParent(contenidoMes3.transform);
                        total3 += float.Parse(IP2.PrecioPago);

                    }


                }
                if (Mes3.Count == 0 && ob.PantallaActual == "MisPagosPendientes3")
                {
                    if (Mes2.Count != 0)
                    {
                        ob.Click("MisPagos2");
                    }
                    else
                    {
                        ob.Click("MisPagos");
                    }
                }
                else if (Mes2.Count == 0 && ob.PantallaActual == "MisPagosPendientes2")
                {
                    ob.Click("MisPagos");
                }
                //if (i < 3)
                //{

                //}
            }

            totaltexto.text = total.ToString("C", ci);
            total2texto.text = total2.ToString("C", ci);
            total3texto.text = total3.ToString("C", ci);

            //pa = 0;
        }*/
    }
    public void PasadoX()
    {

        //if (chequeo == true)
        //{
        //    chequeo = false;
        MesPasado.Clear();
        totalPasado = 0;
        for (int po = 0; po < ap.PagosIndexPasado.Count; po++)
        {
            IP = ap.PagosIndexPasado[po].GetComponent<InfoPago>();
            ap.PagosIndexPasado[po].GetComponent<InfoPago>().acomodo = 0;
        }
        i = 0;

        if (ap.PagosIndexPasado.Count > 0 && pa <= ap.PagosIndexPasado.Count)
        {

            for (p = 0; (ap.PagosIndexPasado.Count) > p; p++)
            {
                IPPasado = ap.PagosIndexPasado[p].GetComponent<InfoPago>();
                if (IPPasado.idCasa == ap.idCasa)
                {
                    MesPasado.Add(IPPasado.gameObject);
                    //pa++;
                    ap.PagosIndexPasado[p].GetComponent<InfoPago>().acomodo = 1;
                    acomodo = ap.PagosIndexPasado[p].GetComponent<InfoPago>().acomodo;
                    ap.PagosIndex[p].transform.SetParent(contenidoMesPasado.transform);
                    totalPasado = float.Parse(IPPasado.PrecioPago);
                    //ap.PagosIndex[o].transform.position = contenidoMes.transform.position;
                }
                //totaltexto.text = total.ToString("C", ci);
                //total2texto.text = total2.ToString("C", ci);
                //total3texto.text = total3.ToString("C", ci);
                //pa = 0;
            }
        }
    }
    //}
}
