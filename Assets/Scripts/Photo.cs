﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VoxelBusters.Utility;
using VoxelBusters.Utility.UnityGUI.MENU;
using VoxelBusters.NativePlugins;
using System.IO;
using System;

public class Photo : MonoBehaviour {
    public int IdUsuario;
    public string Dir, error;
    public MediaLibrary ML;
    private List<string> m_results = new List<string>();
    public Sprite FotoPerfil;
    public Image oj;
    public byte[] bytes;
    public Texture2D foto, image2;


    public void Start()
    {
        Dir = Application.persistentDataPath;
    }

    public void UploadPhoto()
    {
        StartCoroutine(UploadFileCo("X", "https://payhome.azurewebsites.net/serv/subirfoto.php"));
    }
    IEnumerator UploadFileCo(string localFileName, string uploadURL)
    {
        WWW localFile = new WWW(Dir);
        yield return localFile;
        if (localFile.error == null)
            Debug.Log("Loaded file successfully");
        else
        {
            Debug.Log("Open file error: " + localFile.error);
            yield break; // stop the coroutine here
        }
        WWWForm postForm = new WWWForm();
        // version 1
        //postForm.AddBinaryData("theFile",localFile.bytes);
        // version 2
        postForm.AddBinaryData("archivoImagen", localFile.bytes, localFileName, "image/jpg");
        postForm.AddField("IdUsuario", IdUsuario);
        WWW upload = new WWW(uploadURL, postForm);
        yield return upload;
        if (upload.error == null)
            Debug.Log("upload done :" + upload.text);
        else
            Debug.Log("Error during upload: " + upload.error);
    }
    public void PickImageFromAlbum()
    {
        // Set popover to last touch position
        NPBinding.UI.SetPopoverPointAtLastTouchPosition();

        // Pick image
      //  NPBinding.MediaLibrary.PickImage(eImageSource.ALBUM, 1.0f, PickImageFinished);
    }

    public void PickImageFromCamera()
    {
        // Set popover to last touch position
        NPBinding.UI.SetPopoverPointAtLastTouchPosition();

        // Pick image
       // NPBinding.MediaLibrary.PickImage(eImageSource.CAMERA, 1.0f, PickImageFinished);
    }



    public void PickImageFinished(ePickImageFinishReason _reason, Texture2D _image)
    {
        AddNewResult("Request to pick image from gallery finished. Reason for finish is " + _reason + ".");
        AppendResult(string.Format("Selected image is {0}.", (_image == null ? "NULL" : _image.ToString())));

        if (_image != null)
        {
            SetAllowsImageEditing();
            FotoPerfil = Sprite.Create(_image, new Rect((_image.width / 4), (_image.height / 4), (_image.width / 2), (_image.height / 2)), Vector2.zero);
            oj.sprite = FotoPerfil;
            image2 = _image;
            TextureScale.Point(image2, image2.width / 8, image2.height / 8);
            //_image.EncodeToPNG();
            //bytes = _image.EncodeToPNG();
            //File.WriteAllBytes(Dir + "/imageMP.png", bytes);
            //_image = FillInClear(_image);
            //FotoRegistro.SetActive(true);
            //Foto.SetActive(true);
            //FotoTomada = true;
        }

    }
    private void SetAllowsImageEditing()
    {
     //   NPBinding.MediaLibrary.SetAllowsImageEditing(true);
    }

    private static eImageSource GetBOTH()
    {
        return eImageSource.BOTH;
    }
    private static eImageSource GetCAMERA()
    {
        return eImageSource.CAMERA;
    }

    private static eImageSource GetALBUM()
    {
        return eImageSource.ALBUM;
    }
    protected void AppendResult(string _result)
    {
        m_results.Add(_result);
    }

    protected void AddNewResult(string _result)
    {
        m_results.Clear();
        m_results.Add(_result);
    }
    public void EnviarFoto()
    {
        WWWForm Form = new WWWForm();
        Form.AddField("IdUsuario", "14");
        Form.AddBinaryData("archivoImagen", bytes);
        WebServices.SubirFoto(Form, SuccessFoto, Error);
    }
    void SuccessFoto(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            //cargando = false;
            ////error = ("" + ((MessageArg<string>)e).responseValue);
            //EncabezadoErrorAclDudas.text = "Reporte de fallos";
            //ErrorAclDudas.onClick.AddListener(EnviarContacto);
            //ob.Click("Aclaraciones-Error");

            Debug.Log("");
        }
        else if (e is MessageArg<DBAclDudas>)
        {
            //right = true;
            //register1 = 1;
            //PlayerPrefs.SetInt("Registrado", register1);
            //cargando = false;
            DBAclDudas dudas = ((MessageArg<DBAclDudas>)e).responseValue;
            //TextoAclaracionDudas.text = "<b>" + mp.NickInicio.text + ",</b> hemos recibido tu reporte de fallo, muchas gracias por reportarlo. Responderemos a tu correo electrónico cuando ya haya sido arreglado:";
            //EmailAclDudas.text = PlayerPrefs.GetString("email");
            //EncabezadoCorrectoAclDudas.text = "Reporte de fallo";
            //Reporte.text = "";
            //ob.Click("Aclaraciones-Correcto");
            //error = dudas.IdUsuario.ToString();

        }
    }
    void Error(System.Object Sender, EventArgs e)
    {
        error = ("" + ((MessageArg<string>)e).responseValue);
        //ob.Click("Error");


    }
    public void DownloadFoto()
    {
        StartCoroutine(Starts("uploads/14_1509383928.png"));
        //WWW www = new WWW(url);
        
    }
    IEnumerator Starts(string endDir)
    {
        // Start a download of the given URL
        string url = "https://payhome.azurewebsites.net/serv/";
        WWW www = new WWW(url + endDir);

        // Wait for download to complete
        yield return www;

        // assign texture
        foto = www.texture;
        FotoPerfil = Sprite.Create(foto, new Rect((foto.width / 4), (foto.height / 4), (foto.width / 2), (foto.height / 2)), Vector2.zero);

        oj.sprite = FotoPerfil;
    }
}

    