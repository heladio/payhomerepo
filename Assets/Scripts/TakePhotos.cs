﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VoxelBusters.Utility;
using VoxelBusters.Utility.UnityGUI.MENU;
using VoxelBusters.NativePlugins;
using System;
using System.IO;

public class TakePhotos : MonoBehaviour {

    public delegate void Finished();
    public Finished isFinished;
    private Texture2D _photoImage;
    public Texture2D photoImage { get { return _photoImage; } }
    [SerializeField]
    Registro reg;

    private List<string> m_results = new List<string>();

    private void SetAllowsImageEditing()
    {
   //     NPBinding.MediaLibrary.SetAllowsImageEditing(true);
    }    

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void TakePicture(int maxSize)
    {
        NativeCamera.Permission permission = NativeCamera.TakePicture((path) =>
        {
            Debug.Log("Image path: " + path);
            if (path != null)
            {
                // Create a Texture2D from the captured image
                Texture2D texture = NativeCamera.LoadImageAtPath(path, maxSize, false);

                if (texture == null)
                {
                    Debug.Log("Couldn't load texture from " + path);
                    //error_text.text = "could not load texture" + path;
                    return;
                }
                else
                {
                   // error_text.text = "load texture" + path;
                    _photoImage = texture;
                    NativeGallery.SaveImageToGallery(texture, "PayHome", "Perfil {0}.jpg");

                }
                isFinished();
            }
            else
            {
                Debug.Log("No path");
                reg.EndLoad();
            }
        }, maxSize);

        Debug.Log("Permission result: " + permission);
    }

    public void PickImage(int maxSize)
    {
        NativeGallery.Permission permission = NativeGallery.GetImageFromGallery((path) =>
        {
            Debug.Log("Image path: " + path);
            if (path != null)
            {
                // Create Texture from selected image
                Texture2D texture = NativeGallery.LoadImageAtPath(path, maxSize);
                if (texture == null)
                {
                    Debug.Log("Couldn't load texture from " + path);
                    return;
                }
                else
                {
                    _photoImage = texture;
                }
                isFinished();                
            }
            else
            {
                Debug.Log("No path");
                reg.EndLoad();

            }
        }, "Selecciona una imagen", "image/jpg", maxSize);

        Debug.Log("Permission result: " + permission);
    }

    public void PickImageFinished(ePickImageFinishReason _reason, Texture2D _image)
    {
        //AddNewResult("Request to pick image from gallery finished. Reason for finish is " + _reason + ".");
        // AppendResult(string.Format("Selected image is {0}.", (_image == null ? "NULL" : _image.ToString())));
        if (_image != null)
        {
            //SetAllowsImageEditing();
            _photoImage = _image;

            if (isFinished!=null)
            {
                isFinished();
            }
        }
        else
        {
            reg.EndLoad();

        }

    }
}
