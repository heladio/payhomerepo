﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

public class Swipe : MonoBehaviour {
    CultureInfo ci = new CultureInfo("es-mx");
    public float Toquex, ToqueGuar, ToqueDown, aspectw, aspecth, aspect, aspectg, aspectc, aspecti, initialprimero;
    public List<Image> H = new List<Image>();
    public Sprite PS, PL;
    public int distance, h = 0;
    public decimal g;
    public bool SwipeHecho, SwipeDer, SwipeIzq, Me, distances, cambioPago;
    public Vector2 P1, PGuard, P2Guard, P3Guard, P2, P3, ORV;
    public GameObject primero, segundo, tercero;
    public AP ap;
    public InfoPago ip;
    public float todo, suma;
    public Text to;
	// Use this for initialization
	void Start () {
        
        PGuard = primero.transform.position;
        P2Guard = segundo.transform.position;
        P3Guard = tercero.transform.position;
        initialprimero = primero.transform.position.x;
	}
	
	// Update is called once per frame
	void Update () {
        if (cambioPago == true)
        {
            cambioPago = false;
            todo = 0;
            for (int u = 0; u < ap.Pagos.Count; u++)
            {
                ip = ap.Pagos[u].GetComponent<InfoPago>();
                float.TryParse(ip.PrecioPago, out suma);
                todo += suma;
            }
            if (ap.Pagos.Count <= 0)
            {
                todo = 0;
            }
            to.text = todo.ToString("C", ci);
        }
        /*for(int i =0;i<ap.Pagos.Count;++i)
        {
        Debug.Log(ap.Pagos[i]);

        }*/
        P1 = primero.transform.position;
        P2 = segundo.transform.position;
        P3 = tercero.transform.position;
        Toquex = Input.mousePosition.x;
        if (Input.GetMouseButtonDown(0))
        {
            SwipeHecho = true;
            ToqueDown = Input.mousePosition.x;
        }
        if (Input.GetMouseButtonUp(0))
        {
            SwipeHecho = false;
            Me = false;
            SwipeDer = false;
            SwipeIzq = false;
            distance = 0;
            distances = false;
        }
        aspecth = Screen.height;
        aspectw = Screen.width;
        aspect = (aspectw / aspecth);
        aspectg = (float)(Math.Round((double)aspect, 1));
        aspectc = (float)(Math.Round((double)aspect, 2));
        aspecti = (float)(Math.Round((double)aspect, 3));

        if (h == 0)
        {
            H[0].sprite = PS;
            H[1].sprite = PL;
            H[2].sprite = PL;
        }
        if (h == 1)
        {
            H[0].sprite = PL;
            H[1].sprite = PS;
            H[2].sprite = PL;
        }
        if (h == 2)
        {
            H[0].sprite = PL;
            H[1].sprite = PL;
            H[2].sprite = PS;
        }
    }

    public void OnMouseDrag()
    {
        ToqueGuar = Input.mousePosition.x;

        if (Toquex < ToqueGuar && SwipeHecho == true && SwipeIzq == false)
        {
            SwipeIzq = true;
            SwipeDer = false;
            distance = 0;
        }

        if (Toquex > ToqueGuar && SwipeHecho == true && SwipeDer == false)
        {
            SwipeIzq = false;
            SwipeDer = true;
            distance = 0;
        }
        if (SwipeIzq == true)
        {
            distance++;
        }

        if (SwipeDer == true)
        {
            distance++;
        }

        if (distance >= 2)
        {
            distances = true;
            distance = 0;
        }
        else
        {
            distances = false;
        }

        if (h <= 0 && SwipeDer == true && SwipeHecho == true)
        {

            iTween.MoveTo(primero, new Vector2((-Screen.height * 0.35f), primero.transform.position.y), 0.5f);
            iTween.MoveTo(segundo, new Vector2((-Screen.height * 0.35f), segundo.transform.position.y), 0.5f);
            //iTween.MoveTo(tercero, new Vector2((Screen.height * 1.5f), tercero.transform.position.y), 0.5f);
            h = 1;
            SwipeDer = false;
            SwipeHecho = false;
        }
        if (h == 1 && SwipeDer == true && SwipeHecho == true)
        {

            //iTween.MoveTo(primero, new Vector2((-Screen.height * 1f), primero.transform.position.y), 0.5f);
            iTween.MoveTo(segundo, new Vector2((-Screen.height * 1.05f), segundo.transform.position.y), 0.5f);
            iTween.MoveTo(tercero, new Vector2((-Screen.height * 0.35f), tercero.transform.position.y), 0.5f);
            h = 2;
        }

        if (h >= 2 && SwipeIzq == true && SwipeHecho == true)
        {

            //iTween.MoveTo(primero, new Vector2((-Screen.height * 1f), primero.transform.position.y), 0.5f);
            iTween.MoveTo(segundo, new Vector2((-Screen.height * 0.35f), segundo.transform.position.y), 0.5f);
            iTween.MoveTo(tercero, new Vector2((Screen.height * 0.35f), tercero.transform.position.y), 0.5f);
            h = 1;
            SwipeDer = false;
            SwipeHecho = false;
        }
        if (h == 1 && SwipeIzq == true && SwipeHecho == true)
        {

            iTween.MoveTo(primero, new Vector2(initialprimero, primero.transform.position.y), 0.5f);
            iTween.MoveTo(segundo, new Vector2((Screen.height * 0.35f), segundo.transform.position.y), 0.5f);
            //iTween.MoveTo(tercero, new Vector2((Screen.height * 1f), tercero.transform.position.y), 0.5f);
            h = 0;
        }


        //if (h <= 0 && SwipeDer == true && SwipeHecho == true && aspecti == 0.562f)
        //{
            
        //    iTween.MoveTo(primero, new Vector2(primero.transform.position.x + (-Screen.height * 0.635f), primero.transform.position.y), 0.5f);
        //    iTween.MoveTo(segundo, new Vector2(segundo.transform.position.x + (-Screen.height*0.635f), segundo.transform.position.y), 0.5f);
        //    iTween.MoveTo(tercero, new Vector2(tercero.transform.position.x + (-Screen.height * 0.635f), tercero.transform.position.y), 0.5f);
        //    h = 1;
        //    SwipeDer = false;
        //    SwipeHecho = false;
        //}
        //if (h == 1 && SwipeDer == true && SwipeHecho == true && aspecti == 0.562f)
        //{

        //    iTween.MoveTo(primero, new Vector2(primero.transform.position.x + (-Screen.height * 0.635f), primero.transform.position.y), 0.5f);
        //    iTween.MoveTo(segundo, new Vector2(segundo.transform.position.x + (-Screen.height * 0.635f), segundo.transform.position.y), 0.5f);
        //    iTween.MoveTo(tercero, new Vector2(tercero.transform.position.x + (-Screen.height * 0.635f), tercero.transform.position.y), 0.5f);
        //    h = 2;
        //}

        //if (h >= 2 && SwipeIzq == true && SwipeHecho == true && aspecti == 0.562f)
        //{

        //    iTween.MoveTo(primero, new Vector2(primero.transform.position.x + (Screen.height * 0.635f), primero.transform.position.y), 0.5f);
        //    iTween.MoveTo(segundo, new Vector2(segundo.transform.position.x + (Screen.height * 0.635f), segundo.transform.position.y), 0.5f);
        //    iTween.MoveTo(tercero, new Vector2(tercero.transform.position.x + (Screen.height * 0.635f), tercero.transform.position.y), 0.5f);
        //    h = 1;
        //    SwipeDer = false;
        //    SwipeHecho = false;
        //}
        //if (h == 1 && SwipeIzq == true && SwipeHecho == true && aspecti == 0.562f)
        //{

        //    iTween.MoveTo(primero, new Vector2(primero.transform.position.x + (Screen.height * 0.635f), primero.transform.position.y), 0.5f);
        //    iTween.MoveTo(segundo, new Vector2(segundo.transform.position.x + (Screen.height * 0.635f), segundo.transform.position.y), 0.5f);
        //    iTween.MoveTo(tercero, new Vector2(tercero.transform.position.x + (Screen.height * 0.635f), tercero.transform.position.y), 0.5f);
        //    h = 0;
        //}

        //if (h <= 0 && SwipeDer == true && SwipeHecho == true && (aspect == 0.5))
        //{

        //    iTween.MoveTo(primero, new Vector2(primero.transform.position.x + (-Screen.height * 0.6f), primero.transform.position.y), 0.5f);
        //    iTween.MoveTo(segundo, new Vector2(segundo.transform.position.x + (-Screen.height * 0.6f), segundo.transform.position.y), 0.5f);
        //    iTween.MoveTo(tercero, new Vector2(tercero.transform.position.x + (-Screen.height * 0.6f), tercero.transform.position.y), 0.5f);
        //    h = 1;
        //    SwipeDer = false;
        //    SwipeHecho = false;
        //}
        //if (h == 1 && SwipeDer == true && SwipeHecho == true && (aspect == 0.5))
        //{

        //    iTween.MoveTo(primero, new Vector2(primero.transform.position.x + (-Screen.height * 0.6f), primero.transform.position.y), 0.5f);
        //    iTween.MoveTo(segundo, new Vector2(segundo.transform.position.x + (-Screen.height * 0.6f), segundo.transform.position.y), 0.5f);
        //    iTween.MoveTo(tercero, new Vector2(tercero.transform.position.x + (-Screen.height * 0.6f), tercero.transform.position.y), 0.5f);
        //    h = 2;
        //}

        //if (h >= 2 && SwipeIzq == true && SwipeHecho == true && (aspect == 0.5))
        //{

        //    iTween.MoveTo(primero, new Vector2(primero.transform.position.x + (Screen.height * 0.6f), primero.transform.position.y), 0.5f);
        //    iTween.MoveTo(segundo, new Vector2(segundo.transform.position.x + (Screen.height * 0.6f), segundo.transform.position.y), 0.5f);
        //    iTween.MoveTo(tercero, new Vector2(tercero.transform.position.x + (Screen.height * 0.6f), tercero.transform.position.y), 0.5f);
        //    h = 1;
        //    SwipeDer = false;
        //    SwipeHecho = false;
        //}
        //if (h == 1 && SwipeIzq == true && SwipeHecho == true && (aspect == 0.5))
        //{

        //    iTween.MoveTo(primero, new Vector2(primero.transform.position.x + (Screen.height * 0.6f), primero.transform.position.y), 0.5f);
        //    iTween.MoveTo(segundo, new Vector2(segundo.transform.position.x + (Screen.height * 0.6f), segundo.transform.position.y), 0.5f);
        //    iTween.MoveTo(tercero, new Vector2(tercero.transform.position.x + (Screen.height * 0.6f), tercero.transform.position.y), 0.5f);
        //    h = 0;
        //}
        //if (h <= 0 && SwipeDer == true && SwipeHecho == true && (aspectc == 0.75))
        //{

        //    iTween.MoveTo(primero, new Vector2(primero.transform.position.x + (-Screen.height * 0.74f), primero.transform.position.y), 0.5f);
        //    iTween.MoveTo(segundo, new Vector2(segundo.transform.position.x + (-Screen.height * 0.74f), segundo.transform.position.y), 0.5f);
        //    iTween.MoveTo(tercero, new Vector2(tercero.transform.position.x + (-Screen.height * 0.74f), tercero.transform.position.y), 0.5f);
        //    h = 1;
        //    SwipeDer = false;
        //    SwipeHecho = false;
        //}
        //if (h == 1 && SwipeDer == true && SwipeHecho == true && (aspectc == 0.75))
        //{

        //    iTween.MoveTo(primero, new Vector2(primero.transform.position.x + (-Screen.height * 0.74f), primero.transform.position.y), 0.5f);
        //    iTween.MoveTo(segundo, new Vector2(segundo.transform.position.x + (-Screen.height * 0.74f), segundo.transform.position.y), 0.5f);
        //    iTween.MoveTo(tercero, new Vector2(tercero.transform.position.x + (-Screen.height * 0.74f), tercero.transform.position.y), 0.5f);
        //    h = 2;
        //}

        //if (h >= 2 && SwipeIzq == true && SwipeHecho == true && (aspectc == 0.75))
        //{

        //    iTween.MoveTo(primero, new Vector2(primero.transform.position.x + (Screen.height * 0.74f), primero.transform.position.y), 0.5f);
        //    iTween.MoveTo(segundo, new Vector2(segundo.transform.position.x + (Screen.height * 0.74f), segundo.transform.position.y), 0.5f);
        //    iTween.MoveTo(tercero, new Vector2(tercero.transform.position.x + (Screen.height * 0.74f), tercero.transform.position.y), 0.5f);
        //    h = 1;
        //    SwipeDer = false;
        //    SwipeHecho = false;
        //}
        //if (h == 1 && SwipeIzq == true && SwipeHecho == true && (aspectc == 0.75))
        //{

        //    iTween.MoveTo(primero, new Vector2(primero.transform.position.x + (Screen.height * 0.74f), primero.transform.position.y), 0.5f);
        //    iTween.MoveTo(segundo, new Vector2(segundo.transform.position.x + (Screen.height * 0.74f), segundo.transform.position.y), 0.5f);
        //    iTween.MoveTo(tercero, new Vector2(tercero.transform.position.x + (Screen.height * 0.74f), tercero.transform.position.y), 0.5f);
        //    h = 0;
        //}

        //if (h <= 0 && SwipeDer == true && SwipeHecho == true && (aspectg == 0.6f))
        //{

        //    iTween.MoveTo(primero, new Vector2(primero.transform.position.x + (-Screen.height * 0.7f), primero.transform.position.y), 0.5f);
        //    iTween.MoveTo(segundo, new Vector2(segundo.transform.position.x + (-Screen.height * 0.7f), segundo.transform.position.y), 0.5f);
        //    iTween.MoveTo(tercero, new Vector2(tercero.transform.position.x + (-Screen.height * 0.7f), tercero.transform.position.y), 0.5f);
        //    h = 1;
        //    SwipeDer = false;
        //    SwipeHecho = false;
        //}
        //if (h == 1 && SwipeDer == true && SwipeHecho == true && (aspectg == 0.6f))
        //{

        //    iTween.MoveTo(primero, new Vector2(primero.transform.position.x + (-Screen.height * 0.7f), primero.transform.position.y), 0.5f);
        //    iTween.MoveTo(segundo, new Vector2(segundo.transform.position.x + (-Screen.height * 0.7f), segundo.transform.position.y), 0.5f);
        //    iTween.MoveTo(tercero, new Vector2(tercero.transform.position.x + (-Screen.height * 0.7f), tercero.transform.position.y), 0.5f);
        //    h = 2;
        //}

        //if (h >= 2 && SwipeIzq == true && SwipeHecho == true && (aspectg == 0.6f))
        //{

        //    iTween.MoveTo(primero, new Vector2(primero.transform.position.x + (Screen.height * 0.7f), primero.transform.position.y), 0.5f);
        //    iTween.MoveTo(segundo, new Vector2(segundo.transform.position.x + (Screen.height * 0.7f), segundo.transform.position.y), 0.5f);
        //    iTween.MoveTo(tercero, new Vector2(tercero.transform.position.x + (Screen.height * 0.7f), tercero.transform.position.y), 0.5f);
        //    h = 1;
        //    SwipeDer = false;
        //    SwipeHecho = false;
        //}
        //if (h == 1 && SwipeIzq == true && SwipeHecho == true && (aspectg == 0.6f))
        //{

        //    iTween.MoveTo(primero, new Vector2(primero.transform.position.x + (Screen.height * 0.7f), primero.transform.position.y), 0.5f);
        //    iTween.MoveTo(segundo, new Vector2(segundo.transform.position.x + (Screen.height * 0.7f), segundo.transform.position.y), 0.5f);
        //    iTween.MoveTo(tercero, new Vector2(tercero.transform.position.x + (Screen.height * 0.7f), tercero.transform.position.y), 0.5f);
        //    h = 0;
        //}

        //if (h <= 0 && SwipeDer == true && SwipeHecho == true && (aspect == 0.4864865f))
        //{

        //    iTween.MoveTo(primero, new Vector2(primero.transform.position.x + (-Screen.height * 0.59f), primero.transform.position.y), 0.5f);
        //    iTween.MoveTo(segundo, new Vector2(segundo.transform.position.x + (-Screen.height * 0.59f), segundo.transform.position.y), 0.5f);
        //    iTween.MoveTo(tercero, new Vector2(tercero.transform.position.x + (-Screen.height * 0.59f), tercero.transform.position.y), 0.5f);
        //    h = 1;
        //    SwipeDer = false;
        //    SwipeHecho = false;
        //}
        //if (h == 1 && SwipeDer == true && SwipeHecho == true && (aspect == 0.4864865f))
        //{

        //    iTween.MoveTo(primero, new Vector2(primero.transform.position.x + (-Screen.height * 0.59f), primero.transform.position.y), 0.5f);
        //    iTween.MoveTo(segundo, new Vector2(segundo.transform.position.x + (-Screen.height * 0.59f), segundo.transform.position.y), 0.5f);
        //    iTween.MoveTo(tercero, new Vector2(tercero.transform.position.x + (-Screen.height * 0.59f), tercero.transform.position.y), 0.5f);
        //    h = 2;
        //}

        //if (h >= 2 && SwipeIzq == true && SwipeHecho == true && (aspect == 0.4864865f))
        //{

        //    iTween.MoveTo(primero, new Vector2(primero.transform.position.x + (Screen.height * 0.59f), primero.transform.position.y), 0.5f);
        //    iTween.MoveTo(segundo, new Vector2(segundo.transform.position.x + (Screen.height * 0.59f), segundo.transform.position.y), 0.5f);
        //    iTween.MoveTo(tercero, new Vector2(tercero.transform.position.x + (Screen.height * 0.59f), tercero.transform.position.y), 0.5f);
        //    h = 1;
        //    SwipeDer = false;
        //    SwipeHecho = false;
        //}
        //if (h == 1 && SwipeIzq == true && SwipeHecho == true && (aspect == 0.4864865f))
        //{

        //    iTween.MoveTo(primero, new Vector2(primero.transform.position.x + (Screen.height * 0.59f), primero.transform.position.y), 0.5f);
        //    iTween.MoveTo(segundo, new Vector2(segundo.transform.position.x + (Screen.height * 0.59f), segundo.transform.position.y), 0.5f);
        //    iTween.MoveTo(tercero, new Vector2(tercero.transform.position.x + (Screen.height * 0.59f), tercero.transform.position.y), 0.5f);
        //    h = 0;
        //}
    }
}
