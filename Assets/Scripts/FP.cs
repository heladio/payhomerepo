﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FP : MonoBehaviour {
    public InputField TC, NT, Ap, Nick, TCR, NTR, ApR, NickR;
    public Text Banc, Tip, M, An, BancR, TipR, MR, AnR;
    public string TCt, TCm, NombTitu, Apell, Mes, Ano, NickName, Banco, Tipo, Marca, MarcaR, TCtR, TCmR, NombTituR, ApellR, MesR, AnoR, NickNameR, BancoR, TipoR;
    public ObjectManager ob;
    public List<string> TCL, TCmL, NTL, ApL, ML, AnL, NickL, BancL, TipoL, MarcaL = new List<string>();
    public List<int> ID = new List<int>();
    public List<GameObject> Tarjs = new List<GameObject>();
    public int i = 0, b = 0;
    public Toggle Visa, MasterCard, American, VisaR, MasterCardR, AmericanR;
    public GameObject tarjeta, tarjetas, tarjetax, tarjetaclone;
    public Vector2 G;
    public InfoCard Info;
    public Guardar save;
    public float s = 0, d;
    public ScrollRect scroll;
    public bool re, inmod;
	// Use this for initialization
	void Start () {
        //if (i > b)
        //{
        //    b++;
        //    Instantiate(tarjeta, new Vector2(0, 0), Quaternion.identity, tarjetas.transform);
        //}
	}
	
	// Update is called once per frame
	void Update () {
        G = tarjetax.transform.position;
        if (b == 0 && i == 1)
        {
            
            b++;
            Instantiate(tarjeta, G, Quaternion.identity, tarjetas.transform);
            tarjetaclone = GameObject.Find("Tarjetax(Clone)");
            tarjetaclone.name = "Tarjeta" + (i-1);
            Info = tarjetaclone.GetComponent<InfoCard>();
            Tarjs.Add(tarjetaclone);

            Info.Titular.text = NickName;
            Info.NumTerminacion.text = TCm;
            Info.TipoTarjeta.text = Tipo;
            Info.Titu = NombTitu;
            Info.Ap = Apell;
            Info.NT = TCt;
            Info.Ano = Ano;
            Info.Mes = Mes;
            Info.Banco = Banco;
            Info.Tipo = Tipo;
            Info.Marca = Marca;
            Info.NN = NickName;
            Info.idCard = (i-1);
            Info.ob = ob;
            tarjetaclone = null;
            scroll.movementType = ScrollRect.MovementType.Elastic;
            scroll.elasticity = 3.25f;
        }
        else if (i > b && b>0)
        {
            
            
            b++;
            scroll.movementType = ScrollRect.MovementType.Elastic;
            scroll.elasticity = 0;
            Instantiate(tarjeta, new Vector2(G.x, s), Quaternion.identity, tarjetas.transform);
            tarjetaclone = GameObject.Find("Tarjetax(Clone)");
            tarjetaclone.name = "Tarjeta" + (i-1);
            if (b >= 1)
            {
                d = G.y * 0.18f;
                tarjetaclone.transform.position = new Vector2((G.x), Tarjs[i - 2].transform.position.y - d);
                //s = s + G.y - d;
            }
            else if (b > 1)
            {
                //s = s - d;
            }
            Info = tarjetaclone.GetComponent<InfoCard>();
            Tarjs.Add(tarjetaclone);
            Info.Titular.text = NickName;
            Info.NumTerminacion.text = TCm;
            Info.TipoTarjeta.text = Tipo;
            Info.Titu = NombTitu;
            Info.Ap = Apell;
            Info.NT = TCt;
            Info.Ano = Ano;
            Info.Mes = Mes;
            Info.Banco = Banco;
            Info.Tipo = Tipo;
            Info.Marca = Marca;
            Info.NN = NickName;
            Info.idCard = (i-1);
            Info.ob = ob;
            tarjetaclone = null;
            scroll.movementType = ScrollRect.MovementType.Elastic;
            scroll.elasticity = 3.25f;
            
        }
            TCt = TC.text;
            NombTitu = NT.text;
            Apell = Ap.text;
            Mes = M.text;
            Ano = An.text;
            NickName = Nick.text;
            Banco = Banc.text;
            Tipo = Tip.text;
            if (Visa.isOn == true)
            {
                Marca = "Visa";
            }
            if (MasterCard.isOn == true)
            {
                Marca = "MasterCard";
            }
            if (American.isOn == true)
            {
                Marca = "American Express";
            }

            TCtR = TCR.text;
            NombTituR = NTR.text;
            ApellR = ApR.text;
            MesR = MR.text;
            AnoR = AnR.text;
            NickNameR = NickR.text;
            BancoR = BancR.text;
            TipoR = TipR.text;
            if (VisaR.isOn == true)
            {
                MarcaR = "Visa";
            }
            if (MasterCardR.isOn == true)
            {
                MarcaR = "MasterCard";
            }
            if (AmericanR.isOn == true)
            {
                MarcaR = "American Express";
            }

        if (TCt.Length == 16)
        {
            TCm = TCt.Substring(12, 4);
            Debug.Log(TCt.Length);
        }
        if (TCtR.Length == 16)
        {
            TCmR = TCtR.Substring(12, 4);
            Debug.Log(TCt.Length);
        }



    }

    public void ReadCard()
    {
        
        //if (ID == save.id)
        //{

        //}
    }

    public void Guardar()
    {
        if (TCt != "" && NombTitu != "" && Apell != "" && Mes != "Mes" && Ano != "Año" && NickName != "" && Banco != "Banco" && Tipo != "Tipo de Tarjeta")
        {
            scroll.movementType = ScrollRect.MovementType.Elastic;
            scroll.elasticity = 0;
            TCL.Add(TCt);
            TCmL.Add(TCm);
            NTL.Add(NombTitu);
            ApL.Add(Apell);
            ML.Add(Mes);
            AnL.Add(Ano);
            NickL.Add(NickName);
            BancL.Add(Banco);
            TipoL.Add(Tipo);
            MarcaL.Add(Marca);
            ID.Add(i);
            i++;
            TC.text = null;
            NT.text = null;
            Ap.text = null;
            M.text = "Mes";
            An.text = "Año";
            Nick.text = null;
            Visa.isOn = true;
            MasterCard.isOn = false;
            American.isOn = false;
            Banc.text = "Banco";
            Tip.text = "Tipo de Tarjeta";
            inmod = true;
            //TCm = null;
            scroll.movementType = ScrollRect.MovementType.Elastic;
            scroll.elasticity = 3.25f;
            ob.Click("Atras");
        }
        else
        {
            ob.Click("ErrorTarjeta");
        }
    }
    public void GuardarVer()
    {
        if (TCtR != "" && NombTituR != "" && ApellR != "" && MesR != "Mes" && AnoR != "Año" && NickNameR != "" && BancoR != "Banco" && TipoR != "Tipo de Tarjeta")
        {
            scroll.movementType = ScrollRect.MovementType.Elastic;
            scroll.elasticity = 0;
            TCL.Add(TCtR);
            TCmL.Add(TCmR);
            NTL.Add(NombTituR);
            ApL.Add(ApellR);
            ML.Add(MesR);
            AnL.Add(AnoR);
            NickL.Add(NickNameR);
            BancL.Add(BancoR);
            TipoL.Add(TipoR);
            MarcaL.Add(MarcaR);
            ID.Add(i);
            i++;
            TCR.text = null;
            NTR.text = null;
            ApR.text = null;
            MR.text = null;
            AnR.text = null;
            NickR.text = null;
            VisaR.isOn = true;
            MasterCardR.isOn = false;
            AmericanR.isOn = false;
            BancR.text = "Banco";
            TipR.text = "Tipo de Tarjeta";
            TCt = TCtR;
            NombTitu = NombTituR;
            Apell = ApellR;
            Mes = MesR;
            Ano = AnoR;
            NickName = NickNameR;
            Banco = BancoR;
            Tipo = TipoR;
            Marca = MarcaR;
            TCm = TCmR;
            re = true;
            //TCm = null;
            scroll.movementType = ScrollRect.MovementType.Elastic;
            scroll.elasticity = 3.25f;

            ob.Click("Inicios");
        }
        else
        {
            ob.Click("ErrorTarjeta");
        }
    }

        public void Omitir()
    {
            TCR.text = null;
            NTR.text = null;
            ApR.text = null;
            MR.text = null;
            AnR.text = null;
            NickR.text = null;
            VisaR.isOn = true;
            MasterCardR.isOn = false;
            AmericanR.isOn = false;
            BancR.text = "Banco";
            TipR.text = "Tipo de Tarjeta";
            TCmR = null;
            //TCm = null;
            ob.Click("Inicios");
    }
    public void GuardarX()
    {
        if (TCt != "" && NombTitu != "" && Apell != "" && Mes != "Mes" && Ano != "Año" && NickName != "" && Banco != "Banco" && Tipo != "Tipo de Tarjeta")
        {
            scroll.movementType = ScrollRect.MovementType.Elastic;
            scroll.elasticity = 0;
            TCL.Add(TCt);
            TCmL.Add(TCm);
            NTL.Add(NombTitu);
            ApL.Add(Apell);
            ML.Add(Mes);
            AnL.Add(Ano);
            NickL.Add(NickName);
            BancL.Add(Banco);
            TipoL.Add(Tipo);
            MarcaL.Add(Marca);
            ID.Add(i);
            i++;
            TC.text = null;
            NT.text = null;
            Ap.text = null;
            M.text = "Mes";
            An.text = "Año";
            Nick.text = null;
            Visa.isOn = true;
            MasterCard.isOn = false;
            American.isOn = false;
            Banc.text = "Banco";
            Tip.text = "Tipo de Tarjeta";
            inmod = true;
            //TCm = null;
            scroll.movementType = ScrollRect.MovementType.Elastic;
            scroll.elasticity = 3.25f;
            Carga();
            ob.Click("Inicios");
        }
    }
    public void Carga()
    {
        G = tarjetax.transform.position;
        if (b == 0 && i == 1)
        {

            b++;
            Instantiate(tarjeta, G, Quaternion.identity, tarjetas.transform);
            tarjetaclone = GameObject.Find("Tarjetax(Clone)");
            tarjetaclone.name = "Tarjeta" + (i - 1);
            Info = tarjetaclone.GetComponent<InfoCard>();
            Tarjs.Add(tarjetaclone);

            Info.Titular.text = NickName;
            Info.NumTerminacion.text = TCm;
            Info.TipoTarjeta.text = Tipo;
            Info.Titu = NombTitu;
            Info.Ap = Apell;
            Info.NT = TCt;
            Info.Ano = Ano;
            Info.Mes = Mes;
            Info.Banco = Banco;
            Info.Tipo = Tipo;
            Info.Marca = Marca;
            Info.NN = NickName;
            Info.idCard = (i - 1);
            Info.ob = ob;
            tarjetaclone = null;
            scroll.movementType = ScrollRect.MovementType.Elastic;
            scroll.elasticity = 3.25f;
        }
        else if (i > b && b > 0)
        {


            b++;
            scroll.movementType = ScrollRect.MovementType.Elastic;
            scroll.elasticity = 0;
            Instantiate(tarjeta, new Vector2(G.x, s), Quaternion.identity, tarjetas.transform);
            tarjetaclone = GameObject.Find("Tarjetax(Clone)");
            tarjetaclone.name = "Tarjeta" + (i - 1);
            if (b >= 1)
            {
                d = G.y * 0.18f;
                tarjetaclone.transform.position = new Vector2((G.x), Tarjs[i - 2].transform.position.y - d);
                //s = s + G.y - d;
            }
            else if (b > 1)
            {
                //s = s - d;
            }
            Info = tarjetaclone.GetComponent<InfoCard>();
            Tarjs.Add(tarjetaclone);
            Info.Titular.text = NickName;
            Info.NumTerminacion.text = TCm;
            Info.TipoTarjeta.text = Tipo;
            Info.Titu = NombTitu;
            Info.Ap = Apell;
            Info.NT = TCt;
            Info.Ano = Ano;
            Info.Mes = Mes;
            Info.Banco = Banco;
            Info.Tipo = Tipo;
            Info.Marca = Marca;
            Info.NN = NickName;
            Info.idCard = (i - 1);
            Info.ob = ob;
            tarjetaclone = null;
            scroll.movementType = ScrollRect.MovementType.Elastic;
            scroll.elasticity = 3.25f;

        }
        TCt = TC.text;
        NombTitu = NT.text;
        Apell = Ap.text;
        Mes = M.text;
        Ano = An.text;
        NickName = Nick.text;
        Banco = Banc.text;
        Tipo = Tip.text;
        if (Visa.isOn == true)
        {
            Marca = "Visa";
        }
        if (MasterCard.isOn == true)
        {
            Marca = "MasterCard";
        }
        if (American.isOn == true)
        {
            Marca = "American Express";
        }

        TCtR = TCR.text;
        NombTituR = NTR.text;
        ApellR = ApR.text;
        MesR = MR.text;
        AnoR = AnR.text;
        NickNameR = NickR.text;
        BancoR = BancR.text;
        TipoR = TipR.text;
        if (VisaR.isOn == true)
        {
            MarcaR = "Visa";
        }
        if (MasterCardR.isOn == true)
        {
            MarcaR = "MasterCard";
        }
        if (AmericanR.isOn == true)
        {
            MarcaR = "American Express";
        }

        if (TCt.Length == 16)
        {
            TCm = TCt.Substring(12, 4);
            Debug.Log(TCt.Length);
        }
        if (TCtR.Length == 16)
        {
            TCmR = TCtR.Substring(12, 4);
            Debug.Log(TCt.Length);
        }
    }
}
