﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoCard : MonoBehaviour {
    public Text Titular, TipoTarjeta, NumTerminacion;
    public GameObject Card, OM;
    public int idCard;
    public string Titu, Ap, Mes, Ano, NN, Banco, Tipo, Marca, NT;
    public ObjectManager ob;
    public MFP mfp;
    
	// Use this for initialization
	void Start () {
        Card = this.gameObject;
        this.GetComponent<Button>().onClick.AddListener(ReadCard);
        OM = GameObject.Find("ModFormaPago");
        mfp = OM.GetComponent<MFP>();

    }
	
	// Update is called once per frame
	void Update () {
		
	}
    public void ReadCard()
    {
        mfp.Info = this;
        mfp.tarjeta = this.gameObject;
        mfp.NT.text = Titu;
        mfp.TC.text = NT;
        mfp.Ap.text = Ap;
        mfp.M.text = Mes;
        mfp.An.text = Ano;
        mfp.Nick.text = NN;
        mfp.Banc.text = Banco;
        mfp.Tip.text = Tipo;
        mfp.ids = idCard;
        if(Marca == "Visa")
        {
            mfp.Visa.isOn = true;
            mfp.MasterCard.isOn = false;
            mfp.American.isOn = false;
        }
        if (Marca == "MasterCard")
        {
            mfp.Visa.isOn = false;
            mfp.MasterCard.isOn = true;
            mfp.American.isOn = false;
        }
        if (Marca == "American Express")
        {
            mfp.Visa.isOn = false;
            mfp.MasterCard.isOn = false;
            mfp.American.isOn = true;
        }
        //Apell.text = Ap;
        //M.text = Mes;
        //An.text = Ano;
        //Ban.text = Banco;
        //Tip.text = Tipo;
        //Mar.text = Marca;
        //NumTarj.text = NT;
        ob.Click("ModFormaPago");
    }
} 
