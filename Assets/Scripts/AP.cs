﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class AP : MonoBehaviour
{
    readonly CultureInfo ci = new CultureInfo("es-mx");
    [SerializeField] ObjectManager ob;
    public GameObject pago, pagox, pagoclone, pagos, pagoPasado, pagoPasadoX, pagoPasadoClone, pagosPasados, contenidos, contenidosPasados;

    public int i, o, z, w, v, idCasa, idBen, idIcono, idColor, b, CadaCuando, AlertaAntes, idCat, RecPago, PagPer, PagPred, RecordPago, codbar, theMonth, theDay, theYear, dias,
        meses, años, ids, c, h, f, a, g, checkpasado, PagoID, idPagoEn, idPagoEnIcono, idPagoEnColor, idPagoCon, Tot, TotPasado, posicion, posicionPasado, posicionCasa, pos, zz, zzz,
        index, ca, modo, diasc, mesesc, añosc, indexPasado, indexbackPasado, indexback2Pasado, index2Pasado, mm, mmm, zo, zoo, idCasaBack;
    Vector2 G, G2;
    InfoCasa ICa;
    
    public InfoPago IP, IP2, IP3;
    [HideInInspector]
    public InfoCasaPago ICP;
    [HideInInspector]
    public InfoCategoria IC;
    [HideInInspector]
    public InfoBeneficiario IB;
    public Casas casa;
    public Beneficiario ben;
    public TP tp;
    float s = 0, d, e, p = 0, m, n;
    [SerializeField] ScrollRect scroll;
    public InputField PrecioPago, NotaPago, CsB;
    public Text Categoria, Casa, RepetirCadaTexto, AlertasTexto, DiaP, MesP, AñoP, Fecha, Beneficiario, NomCat, Repeticion, Total, TotalPasado, botonTxt, fondoTexto,
        fondoTexto2, fondoTexto3, NotaTexto;
    float Deposito;
    public Color IcoColor, iconColorBack;
    public Image botonIcono, iconColor;
    Sprite Ico, botonIconoBack;
    public GameObject Iconos, Colores, content, limsup, liminf, RPD, RPA, Benef, CB, RPD2, RPA2, BG, BPG, Pagost, mover, moverPasado, contentPasado, limsupPasado, liminfPasado;
    
    public List<int> IdPago, IdCasa, CadaCuandos, AlertasAntes, idBeneficiario, idCategoria, idColors, idIconos, ReciboPagos, PagoPersonalizado, RecordatorioPagos, IdPPred = new List<int>();
    
    public List<string> Dia, Mes, Año, Nota, PrecioPagos, CodBarras, nombreCasas, fechaComparacion = new List<string>();
    
    public List<int> IdPagoPasado, IdCasaPasado, CadaCuandosPasado, AlertasAntesPasado, idBeneficiarioPasado, idCategoriaPasado, idColorsPasado,
        idIconosPasado, ReciboPagosPasado, PagoPersonalizadoPasado, RecordatorioPagosPasado, IdPPredPasado, Checarpasado, Posicion, PosicionPasado, IdPagoLocal, IdPagoPasadoLocal = new List<int>();
    [HideInInspector]
    public List<string> DiaPasado, MesPasado, AñoPasado, NotaPasado, PrecioPagosPasado, CodBarrasPasado = new List<string>();
    
    List<Sprite> Icon, PP = new List<Sprite>();
    
    List<Color> Colo = new List<Color>();
    
    public List<string> NombreCategoria, NombreCategoriaPasado = new List<string>();
    
    public List<GameObject> Pagos, PagosPasado, PagosIndex, PagosIndexPasado = new List<GameObject>();
    
    public bool fechaActual, Enero, Febrero, Marzo, Abril, Mayo, Junio, Julio, Agosto, Septiembre, Octubre, Noviembre, Diciembre, Ahora, AhoraBack, change, load,
        loadPasado, changePasado, Historial, norepetido, casaencontrada, norepetidopasado, modp, encontrado, mismafecha;
    
    public Toggle PagoEventual, ReciboPago, ReciboPago2;
    public Toggle PagoRecurrente;
    public Casas casas;
    Vector2 scrollOriginal, LimiteSuperior, LimiteInferior, PrimerTP, UltimoTP, posicioncontent, scrollinferior, contentActual, Ward, Ward2, scrollOriginalPasado,
        LimiteSuperiorPasado, LimiteInferiorPasado, PrimerTPPasado, UltimoTPPasado, posicioncontentPasado, scrollinferiorPasado, contentActualPasado;
    
    public Swipe sw;
    public Fecha Date;
    public GameObject GuardarAgregar, GuardarEditar, botonph;
    
    public bool fec, pagoretrasado, pagarahora, editar, past, pagado, pagardentro, bajo, bajoPasado, cambio, cambioPasado, depo, repcada, fechabool, casatexto, alertabool, fechaeditar, eliminar,
        pp, pp2, casaHistorial, pagar, acomodo, obtenido, entrar, agregar, modificar;
    
    float precio;
    [SerializeField]
    DateTime date, dt, FechaComp, FechaComp2, FechaCompPasado, FechaComp2Pasado;
    
    List<DateTime> fechasComparation = new List<DateTime>();
    
    public string fecha, fechas, precios, pagadocon, pagadoen, nombcasa, pagorecurrenteventual, mesnum, NombreCategoriaString, errorMonto, errorCasa, errorRepetir,
        errorAlerta, errorFecha, meshoy, mesprox, error, fechaHistorial, precioHistorial, fondoString, DiaPago, MesPago, AñoPago, fechasHistorial, fechaMod, CuerpoMens, diaback, mesback,
        añoback, nombreCasaMenu, nombrePago;
    [SerializeField]
    InfoCasaMH ICMH;
    [HideInInspector]
    public List<string> PagCon, PagEn = new List<string>();
    [HideInInspector]
    public List<Sprite> PagEnIcono = new List<Sprite>();
    [HideInInspector]
    public List<Color> PagEnColor = new List<Color>();
    [HideInInspector]
    public float apagar, tick;
    
    float totalPasado;
    [SerializeField] TestNotification TN;
    
    int index1, index2, indexback, indexback2, IdCasaHistorial, mesHistorial, añoHistorial, y, ups, gh;
    [SerializeField]
    VerPagosMeses VPM;
    [SerializeField]
    SwipeVentana SP;
    [SerializeField]
    GameObject ErrorPrecio, ErrorProgPago, ErrorFechaPago, ErrorRepetCada, ErrorRecordPago;
    
    List<DBPagos> dbpagos = new List<DBPagos>();
    [SerializeField]
    DBPagos pagate;
    
    List<DBPagosPasado> dbpagospasado = new List<DBPagosPasado>();
    [SerializeField]
    DBPagosPasado pasadote;
    
    List<DBHistorial> dbhistorial = new List<DBHistorial>();
    [SerializeField]
    DBHistorial historialote;
    [SerializeField]
    Registro reg;
    [SerializeField]
    InfoCategoria ICS;
    public GameObject Refrescar1, Refrescar2, Refrescar3, RefrescarPasado, content2, content3, contentP, circulo1, circulo2, circulo3, circuloP, circuloCasa, circuloaños, circulopagos,
        contentaños, RefrescarCasas, RefrescarAños, RefrescarPagos;
    [HideInInspector]
    public bool RefBool, Ref2Bool, Ref3Bool, RefPasadoBool, RefBoolHist, check, agregando;
    //[HideInInspector]
    //float TimerRef, TimerRef2, TimerRef3, TimerRefPasado, pa, TimerRefHistorial;
    string[] separators = { "/" };
    [HideInInspector]
    public string Offline, OfflinePasado, nombreCasa, NoEnviadoPagosAgregar, offlineString, ModificarPagosNE, AgregarPagoPasadoNE, EliminarPagoPasadoNE, EliminarPagoNE;
    
    string[] Off, escribir, OffPasado, escribirPasado, escribirPagosNoEnviado, OffAgregar, OffMod, escribirPagosMod, escribirPagosPasadosAgregar, OffPasadoAgregar, escribirPagoPasadoEliminar,
        OffPasadoEliminar, OffPagoEliminar, OffHistorial, OffHistorialAgregar, escribirHistorial, escribirHistorialAgregar;
    [HideInInspector]
    public List<int> IdPagoLocalOffline, IdPagoOffline, IdCasaOffline, IdBeneficiarioOffline, CadaCuandosOffline, AlertasAntesOffline, idCategoriaOffline, ReciboPagosOffline, RecordatorioPagosOffline,
        PagoPersonalizadoOffline, idColorsOffline, idIconosOffline, IdPPredOffline, ChecarpasadoOffline = new List<int>();
    [HideInInspector]
    public List<string> PrecioPagosOffline, CodBarrasOffline, DiaOffline, MesOffline, AñoOffline, NotaOffline, NombreCategoriaOffline, nombreCasasOffline, fechaOffline = new List<string>();
    [HideInInspector]
    public List<int> IdPagoLocalModOffline, IdPagoModOffline, IdCasaModOffline, IdBeneficiarioModOffline, CadaCuandosModOffline, AlertasAntesModOffline, idCategoriaModOffline,
        ReciboPagosModOffline, RecordatorioPagosModOffline, PagoPersonalizadoModOffline, idColorsModOffline, idIconosModOffline, IdPPredModOffline, ChecarpasadoModOffline = new List<int>();
    [HideInInspector]
    public List<string> PrecioPagosModOffline, CodBarrasModOffline, DiaModOffline, MesModOffline, AñoModOffline, NotaModOffline, NombreCategoriaModOffline,
        nombreCasasModOffline, fechaModOffline = new List<string>();
    [HideInInspector]
    public List<int> IdPagoLocalPasadoOffline, IdPagoPasadoOffline, IdCasaPasadoOffline, IdBeneficiarioPasadoOffline, CadaCuandosPasadoOffline, AlertasAntesPasadoOffline,
        idCategoriaPasadoOffline, ReciboPagosPasadoOffline, RecordatorioPagosPasadoOffline,
        PagoPersonalizadoPasadoOffline, idColorsPasadoOffline, idIconosPasadoOffline, IdPPredPasadoOffline, ChecarpasadoPasadoOffline = new List<int>();
    [HideInInspector]
    public List<string> PrecioPagosPasadoOffline, CodBarrasPasadoOffline, DiaPasadoOffline, MesPasadoOffline, AñoPasadoOffline, NotaPasadoOffline, NombreCategoriaPasadoOffline,
        nombreCasasPasadoOffline, fechaPasadoOffline = new List<string>();
    [HideInInspector]
    public List<int> EliminarPasadoOffline, EliminarPagoOffline = new List<int>();
    [HideInInspector]
    public List<string> NombreCatOff, precioHistorialOff, fechaHistorialOff, nombreCasasHist = new List<string>();
    [HideInInspector]
    public List<int> idCatOff, idColorOff, idIconoOff, idPagoEnOff, idPagoConOff, idPagoEnColorOff, idPagoEnIconoOff, idCasaHistorialOff, mesHistorialOff, añoHistorialOff = new List<int>();
    [HideInInspector]
    public string AgregarHistorialNE, HistorialOff;

    [HideInInspector]
    public List<string> NombreCatAgregarHist, precioHistorialAgregarHist, fechaHistorialAgregarHist, nombreCasasAgregarHist = new List<string>();
    [HideInInspector]
    public List<int> idCatAgregarHist, idColorAgregarHist, idIconoAgregarHist, idPagoEnAgregarHist, idPagoConAgregarHist, idPagoEnColorAgregarHist, idPagoEnIconoAgregarHist, idCasaHistorialAgregarHist,
        mesHistorialAgregarHist, añoHistorialAgregarHist = new List<int>();
    
    DateTime ModFecha;
    readonly Char delimiter = ';';
    readonly Char del = ',';
    [HideInInspector]
    public int offlineInt, IdPagoLocalB;
    bool sameDate = false;
    bool isGetHist = false;
    bool isGetPagos = false;

    public int lastRecordarPago;
    public int lastCadaCuando;
    public string lastRepetirCadaCuandoText;
    public string lastNotas;
    public string lastAlertaAntesText;
    public int lastAlertasAntes;

    public void OfflPago()
    {
        Offline = PlayerPrefs.GetString("Pagos");
        Off = Offline.Split(delimiter);

        for (int gh = 0; gh < Off.Length; gh++)
        {
            escribir = null;
            escribir = Off[gh].Split(del);
            if (escribir[0] != "")
            {
                Int32.TryParse(escribir[0], out i);
                IdPagoLocal.Insert(0,i);

                Int32.TryParse(escribir[1], out zo);
                IdPago.Add(zo);
                Int32.TryParse(escribir[2], out idCasa);
                IdCasa.Add(idCasa);
                Int32.TryParse(escribir[3], out idBen);
                idBeneficiario.Add(idBen);
                Int32.TryParse(escribir[4], out CadaCuando);
                CadaCuandos.Add(CadaCuando);
                Int32.TryParse(escribir[5], out AlertaAntes);
                AlertasAntes.Add(AlertaAntes);
                Int32.TryParse(escribir[6], out idCat);
                idCategoria.Add(idCat);
                CodBarras.Add(escribir[7]);
                PrecioPagos.Add(escribir[8]);
                Dia.Add(escribir[9]);
                Mes.Add(escribir[10]);
                Año.Add(escribir[11]);
                Nota.Add(escribir[12]);
                Int32.TryParse(escribir[13], out RecPago);
                ReciboPagos.Add(RecPago);
                Int32.TryParse(escribir[14], out RecordPago);
                RecordatorioPagos.Add(RecordPago);
                Int32.TryParse(escribir[15], out idColor);
                idColors.Add(idColor);
                Int32.TryParse(escribir[16], out idIcono);
                idIconos.Add(idIcono);
                Int32.TryParse(escribir[17], out PagPred);
                IdPPred.Add(PagPred);
                Int32.TryParse(escribir[18], out checkpasado);
                Checarpasado.Add(checkpasado);
                NombreCategoria.Add(escribir[19]);
                Int32.TryParse(escribir[20], out PagPer);
                PagoPersonalizado.Add(PagPer);
                //nombreCasas.Add(escribir[21]);
                Posicion.Insert(0,posicion);
                posicion++;
                if (gh == Off.Length - 1)
                {

                    //reg.cargando = false;
                    entrar = true;
                    acomodo = true;
                    obtenido = true;
                    load = true;
                    //Acomodo();
                    sw.cambioPago = true;
                }
                //X();
            }

        }
        NoEnviadoPagosAgregar = PlayerPrefs.GetString("PagosAgregar");
        OffAgregar = NoEnviadoPagosAgregar.Split(delimiter);
        for (int huj = 0; huj < OffAgregar.Length; huj++)
        {
            escribirPagosNoEnviado = null;
            escribirPagosNoEnviado = OffAgregar[huj].Split(del);
            if (escribirPagosNoEnviado[0] != "")
            {
                Int32.TryParse(escribirPagosNoEnviado[0], out offlineInt);
                IdPagoLocalOffline.Add(offlineInt);
                NombreCategoriaOffline.Add(escribirPagosNoEnviado[1]);
                Int32.TryParse(escribirPagosNoEnviado[2], out offlineInt);
                idColorsOffline.Add(offlineInt);
                Int32.TryParse(escribirPagosNoEnviado[3], out offlineInt);
                idIconosOffline.Add(offlineInt);
                Int32.TryParse(escribirPagosNoEnviado[4], out offlineInt);
                IdCasaOffline.Add(offlineInt);
                PrecioPagosOffline.Add(escribirPagosNoEnviado[5]);
                fechaOffline.Add(escribirPagosNoEnviado[6]);
                Int32.TryParse(escribirPagosNoEnviado[7], out offlineInt);
                RecordatorioPagosOffline.Add(offlineInt);
                Int32.TryParse(escribirPagosNoEnviado[8], out offlineInt);
                CadaCuandosOffline.Add(offlineInt);
                Int32.TryParse(escribirPagosNoEnviado[9], out offlineInt);
                AlertasAntesOffline.Add(offlineInt);
                Int32.TryParse(escribirPagosNoEnviado[10], out offlineInt);
                IdBeneficiarioOffline.Add(offlineInt);
                NotaOffline.Add(escribirPagosNoEnviado[11]);
                Int32.TryParse(escribirPagosNoEnviado[12], out offlineInt);
                ReciboPagosOffline.Add(offlineInt);
                CodBarrasOffline.Add(escribirPagosNoEnviado[13]);
                nombreCasasOffline.Add(escribirPagosNoEnviado[14]);
            }
        }
        ModificarPagosNE = PlayerPrefs.GetString("PagosModificar");
        OffMod = ModificarPagosNE.Split(delimiter);
        for (int huk = 0; huk < OffMod.Length; huk++)
        {
            escribirPagosMod = null;
            escribirPagosMod = OffMod[huk].Split(del);
            if (escribirPagosMod[0] != "")
            {
                Int32.TryParse(escribirPagosMod[0], out offlineInt);
                IdPagoModOffline.Add(offlineInt);
                NombreCategoriaModOffline.Add(escribirPagosMod[1]);
                Int32.TryParse(escribirPagosMod[2], out offlineInt);
                idColorsModOffline.Add(offlineInt);
                Int32.TryParse(escribirPagosMod[3], out offlineInt);
                idIconosModOffline.Add(offlineInt);
                Int32.TryParse(escribirPagosMod[4], out offlineInt);
                IdCasaModOffline.Add(offlineInt);
                PrecioPagosModOffline.Add(escribirPagosMod[5]);
                fechaModOffline.Add(escribirPagosMod[6]);
                Int32.TryParse(escribirPagosMod[7], out offlineInt);
                RecordatorioPagosModOffline.Add(offlineInt);
                Int32.TryParse(escribirPagosMod[8], out offlineInt);
                CadaCuandosModOffline.Add(offlineInt);
                Int32.TryParse(escribirPagosMod[9], out offlineInt);
                AlertasAntesModOffline.Add(offlineInt);
                Int32.TryParse(escribirPagosMod[10], out offlineInt);
                IdBeneficiarioModOffline.Add(offlineInt);
                NotaModOffline.Add(escribirPagosMod[11]);
                Int32.TryParse(escribirPagosMod[12], out offlineInt);
                ReciboPagosModOffline.Add(offlineInt);
                CodBarrasModOffline.Add(escribirPagosMod[13]);
            }
        }
        if (IdPagoModOffline.Count > 0)
        {
            ModPago();
        }
        if (IdPagoLocalOffline.Count > 0 && agregando == false)
        {
            if (IdCasaOffline[0] != 0)
            {
                agregando = true;
                AgregarPago();
            }
        }
        EliminarPagoNE = PlayerPrefs.GetString("PagosEliminar");
        OffPagoEliminar = EliminarPagoNE.Split(delimiter);
        for (int uj = 0; uj < OffPagoEliminar.Length; uj++)
        {
            Int32.TryParse(OffPagoEliminar[uj], out offlineInt);
            if (offlineInt != 0)
            {
                EliminarPagoOffline.Add(offlineInt);
            }
        }
        if (EliminarPagoOffline.Count != 0)
        {
            ElimPago();
        }
    }
    public void OfflPagoPasado()
    {
        OfflinePasado = PlayerPrefs.GetString("PagosPasado");
        OffPasado = OfflinePasado.Split(delimiter);
        for (int gj = 0; gj < OffPasado.Length; gj++)
        {
            escribirPasado = null;
            escribirPasado = OffPasado[gj].Split(del);
            if (escribirPasado[0] != "")
            {
                Int32.TryParse(escribirPasado[0], out f);
                IdPagoPasadoLocal.Add(f);
                Int32.TryParse(escribirPasado[1], out zoo);
                IdPagoPasado.Add(zoo);
                Int32.TryParse(escribirPasado[2], out idCasa);
                IdCasaPasado.Add(idCasa);
                Int32.TryParse(escribirPasado[3], out idBen);
                idBeneficiarioPasado.Add(idBen);
                Int32.TryParse(escribirPasado[4], out idCat);
                idCategoriaPasado.Add(idCat);
                CodBarrasPasado.Add(escribirPasado[5]);
                PrecioPagosPasado.Add(escribirPasado[6]);
                DiaPasado.Add(escribirPasado[7]);
                MesPasado.Add(escribirPasado[8]);
                AñoPasado.Add(escribirPasado[9]);
                NotaPasado.Add(escribirPasado[10]);
                Int32.TryParse(escribirPasado[11], out RecPago);
                ReciboPagosPasado.Add(RecPago);
                Int32.TryParse(escribirPasado[12], out idColor);
                idColorsPasado.Add(idColor);
                Int32.TryParse(escribirPasado[13], out idIcono);
                idIconosPasado.Add(idIcono);
                Int32.TryParse(escribirPasado[14], out PagPred);
                IdPPredPasado.Add(PagPred);
                Int32.TryParse(escribirPasado[15], out checkpasado);
                Checarpasado.Add(checkpasado);
                NombreCategoriaPasado.Add(escribirPasado[16]);
                Int32.TryParse(escribirPasado[17], out PagPer);
                PagoPersonalizadoPasado.Add(PagPer);
                PosicionPasado.Add(posicionPasado);
                posicionPasado++;
                PasadoX();
            }
        }
        AgregarPagoPasadoNE = PlayerPrefs.GetString("PagosPasadoAgregar");
        OffPasadoAgregar = AgregarPagoPasadoNE.Split(delimiter);
        for (int gj = 0; gj < OffPasadoAgregar.Length; gj++)
        {
            escribirPagosPasadosAgregar = null;
            escribirPagosPasadosAgregar = OffPasadoAgregar[gj].Split(del);
            if (escribirPagosPasadosAgregar[0] != "")
            {
                Int32.TryParse(escribirPagosPasadosAgregar[0], out offlineInt);
                IdPagoLocalPasadoOffline.Add(offlineInt);
                NombreCategoriaPasadoOffline.Add(escribirPagosPasadosAgregar[1]);
                Int32.TryParse(escribirPagosPasadosAgregar[2], out offlineInt);
                idColorsPasadoOffline.Add(offlineInt);
                Int32.TryParse(escribirPagosPasadosAgregar[3], out offlineInt);
                idIconosPasadoOffline.Add(offlineInt);
                Int32.TryParse(escribirPagosPasadosAgregar[4], out offlineInt);
                IdCasaPasadoOffline.Add(offlineInt);
                PrecioPagosPasadoOffline.Add(escribirPagosPasadosAgregar[5]);
                fechaPasadoOffline.Add(escribirPagosPasadosAgregar[6]);
                Int32.TryParse(escribirPagosPasadosAgregar[7], out offlineInt);
                RecordatorioPagosPasadoOffline.Add(offlineInt);
                Int32.TryParse(escribirPagosPasadosAgregar[8], out offlineInt);
                CadaCuandosPasadoOffline.Add(offlineInt);
                Int32.TryParse(escribirPagosPasadosAgregar[9], out offlineInt);
                AlertasAntesPasadoOffline.Add(offlineInt);
                Int32.TryParse(escribirPagosPasadosAgregar[10], out offlineInt);
                IdBeneficiarioPasadoOffline.Add(offlineInt);
                NotaPasadoOffline.Add(escribirPagosPasadosAgregar[11]);
                Int32.TryParse(escribirPagosPasadosAgregar[12], out offlineInt);
                ReciboPagosPasadoOffline.Add(offlineInt);
                CodBarrasPasadoOffline.Add(escribirPagosPasadosAgregar[13]);
                AgregarPagoPasado();
            }
        }
        EliminarPagoPasadoNE = PlayerPrefs.GetString("PagosPasadoEliminar");
        OffPasadoEliminar = EliminarPagoPasadoNE.Split(delimiter);
        for (int uj = 0; uj < OffPasadoEliminar.Length; uj++)
        {
            Int32.TryParse(OffPasadoEliminar[uj], out offlineInt);
            if (offlineInt != 0)
            {
                EliminarPasadoOffline.Add(offlineInt);
            }
        }
        if (EliminarPasadoOffline.Count != 0)
        {
            ElimPagoPasado();
        }
    }

    public void OfflineHistorial()
    {
        HistorialOff = PlayerPrefs.GetString("Historial");
        OffHistorial = HistorialOff.Split(delimiter);
        for (int iuo = 0; iuo < OffHistorial.Length; iuo++)
        {
            escribirHistorial = null;
            escribirHistorial = OffHistorial[iuo].Split(del);
            if (escribirHistorial[0] != "")
            {
               
                NombreCatOff.Add(escribirHistorial[0]);
                NombreCategoriaString = escribirHistorial[0];
                Int32.TryParse(escribirHistorial[1], out offlineInt);
                idCat = offlineInt;
                idCatOff.Add(offlineInt);
                Int32.TryParse(escribirHistorial[2], out offlineInt);
                idColor = offlineInt;
                idColorOff.Add(offlineInt);
                Int32.TryParse(escribirHistorial[3], out offlineInt);
                idIcono = offlineInt;
                idIconoOff.Add(offlineInt);
                Int32.TryParse(escribirHistorial[4], out offlineInt);
                idPagoEn = offlineInt;
                idPagoEnOff.Add(offlineInt);
                Int32.TryParse(escribirHistorial[5], out offlineInt);
                idPagoCon = offlineInt;
                idPagoConOff.Add(offlineInt);
                precioHistorialOff.Add(escribirHistorial[6]);
                precioHistorial = escribirHistorial[6];
                fechaHistorialOff.Add(escribirHistorial[7]);
                fechaHistorial = escribirHistorial[7];
                Int32.TryParse(escribirHistorial[8], out offlineInt);
                idPagoEnColor = offlineInt;
                idPagoEnColorOff.Add(offlineInt);
                Int32.TryParse(escribirHistorial[9], out offlineInt);
                idPagoEnIcono = offlineInt;
                idPagoEnIconoOff.Add(offlineInt);
                Int32.TryParse(escribirHistorial[10], out offlineInt);
                mesHistorial = offlineInt;
                mesHistorialOff.Add(offlineInt);
                Int32.TryParse(escribirHistorial[11], out offlineInt);
                añoHistorial = offlineInt;
                añoHistorialOff.Add(offlineInt);
                Int32.TryParse(escribirHistorial[12], out offlineInt);
                IdCasaHistorial = offlineInt;
                idCasaHistorialOff.Add(offlineInt);
                nombreCasasHist.Add(escribirHistorial[13]);
                nombreCasa = escribirHistorial[13];
                HistorialX();
            }
        }
        AgregarHistorialNE = PlayerPrefs.GetString("HistorialAgregar");
        OffHistorialAgregar = AgregarHistorialNE.Split(delimiter);
        for (int ujk = 0; ujk < OffHistorialAgregar.Length; ujk++)
        {
            escribirHistorialAgregar = null;
            escribirHistorialAgregar = OffHistorialAgregar[ujk].Split(del);
            if (escribirHistorialAgregar[0] != "")
            {
                NombreCatAgregarHist.Add(escribirHistorialAgregar[0]);
                Int32.TryParse(escribirHistorialAgregar[1], out offlineInt);
                idCatAgregarHist.Add(offlineInt);
                Int32.TryParse(escribirHistorialAgregar[2], out offlineInt);
                idColorAgregarHist.Add(offlineInt);
                Int32.TryParse(escribirHistorialAgregar[3], out offlineInt);
                idIconoAgregarHist.Add(offlineInt);
                Int32.TryParse(escribirHistorialAgregar[4], out offlineInt);
                idPagoEnAgregarHist.Add(offlineInt);
                Int32.TryParse(escribirHistorialAgregar[5], out offlineInt);
                idPagoConAgregarHist.Add(offlineInt);
                precioHistorialAgregarHist.Add(escribirHistorialAgregar[6]);
                fechaHistorialAgregarHist.Add(escribirHistorialAgregar[7]);
                Int32.TryParse(escribirHistorialAgregar[8], out offlineInt);
                idPagoEnColorAgregarHist.Add(offlineInt);
                Int32.TryParse(escribirHistorialAgregar[9], out offlineInt);
                idPagoEnIconoAgregarHist.Add(offlineInt);
                Int32.TryParse(escribirHistorialAgregar[10], out offlineInt);
                mesHistorialAgregarHist.Add(offlineInt);
                Int32.TryParse(escribirHistorialAgregar[11], out offlineInt);
                añoHistorialAgregarHist.Add(offlineInt);
                Int32.TryParse(escribirHistorialAgregar[12], out offlineInt);
                idCasaHistorialAgregarHist.Add(offlineInt);
                nombreCasasAgregarHist.Add(escribirHistorialAgregar[13]);
                if (idCasaHistorialAgregarHist[0] != 0)
                {
                    AgregarHistorial();
                }
            }
        }
    }

    void Awake()
    {
       
        DiaP.text = DateTime.Now.Day.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if (IdCasaOffline.Count != 0 && agregando == false)
        {

            if (IdCasaOffline[0] != 0)
            {
                agregando = true;
                AgregarPago();
            }
        }
        if (IdCasaModOffline.Count != 0 && agregando == false)
        {
            agregando = true;
            ModPago();
        }
        if (EliminarPagoOffline.Count != 0 && agregando == false)
        {
            agregando = true;
            ElimPago();
            if (EliminarPasadoOffline.Count != 0)
                ElimPagoPasado();
        }
        

        bool isOnPagos = ob.Atras.Any(item => item == ob.MisPagos);

        if(!isOnPagos&&!reg.isStart)
        {
            for (int i = 0; i < Pagos.Count; i++)
            {
                Pagos[i].SetActive(false);
            }

            for (int i = 0; i < PagosPasado.Count; i++)
            {
                PagosPasado[i].SetActive(false);
            }
        }

        if (isOnPagos && !reg.isStart)
        {
            for (int i = 0; i < Pagos.Count; i++)
            {
                if (Pagos[i].GetComponent<InfoPago>().idCasa == idCasa)
                {
                    Pagos[i].SetActive(true);
                }
            }

            for (int i = 0; i < PagosPasado.Count; i++)
            {
                if (PagosPasado[i].GetComponent<InfoPago>().idCasa == idCasa)
                {
                    PagosPasado[i].SetActive(true);
                }
            }
        }

        fondoTexto.text = fondoString;
        fondoTexto2.text = fondoString;
        fondoTexto3.text = fondoString;
        
        if (PrecioPago.text == "")
        {
            depo = true;
            errorMonto = "\n-Asignar el monto del pago/gasto";
        }
        else
        {
            depo = false;
            errorMonto = "";
        }

        if (Casa.text == "Mis casas")
        {
            casatexto = true;
            errorCasa = "\n-Asignar una casa al pago/gasto";
        }
        else
        {
            casatexto = false;
            errorCasa = "";
        }

        if (RepetirCadaTexto.text == "Repetir cada")
        {
            repcada = true;
            errorRepetir = "\n-Asignar cada cuando se va a repetir el pago/gasto";
        }
        else
        {
            repcada = false;
            errorRepetir = "";
        }

        if (AlertasTexto.text == "Alerta de recordatorio")
        {
            alertabool = true;
            errorAlerta = "\n-Asignar la alerta de recordatorio al pago/gasto";
        }
        else
        {
            alertabool = false;
            errorAlerta = "";
        }

        if (Fecha.text == "Fecha de pago")
        {
            fechabool = true;
            errorFecha = "\n-Asignar la fecha de pago/gasto";
        }
        else
        {
            fechabool = false;
            errorFecha = "";
        }


        if (PrecioPago.text != "")
        {
            apagar = float.Parse(PrecioPago.text);
        }
        Categoria.text = NombreCategoriaString;
        

        //aquí se pone el total de pagos pendientes
            totalPasado = 0;
        for (int i = 0; i < PagosPasado.Count; i++)
        {
            if(PagosPasado[i].activeInHierarchy)
            {
                totalPasado += float.Parse(PagosPasado[i].GetComponent<InfoPago>().PrecioPago);
            }
        }
            TotalPasado.text = totalPasado.ToString("C", ci);

        theYear = System.DateTime.Now.Year;
        theDay = System.DateTime.Now.Day;
        theMonth = System.DateTime.Now.Month;

        if (ReciboPago2.isOn == false)
        {
            RecPago = 0;
            RPA2.SetActive(false);
            RPD2.SetActive(true);
        }
        else
        {
            RecPago = 1;
            RPA2.SetActive(true);
            RPD2.SetActive(false);
        }
        
        if (ReciboPago.isOn == false)
        {
            pagardentro = false;
            RPA.SetActive(false);
            RPD.SetActive(true);
        }
        else
        {
            pagardentro = true;
            AhoraBack = true;
            RPA.SetActive(true);
            RPD.SetActive(false);
        }

        if (pagoretrasado == true)
        {
            botonTxt.text = "Marcar como pagado:";
            //PagoRecurrente.isOn = false;
            //PagoEventual.isOn = true;
            CadaCuando = 0;
            RepetirCadaTexto.text = "Repetir cada";
            AlertasTexto.text = "Alerta de recordatorio";
            AlertaAntes = 0;
            checkpasado = 1;
        }
        else
        {
            botonTxt.text = "Pagar ahora:";
            checkpasado = 0;
        }
      
    }

    public void PagoUnicoAceptar()
    {
        ob.Click("PagoUnicoQuitar");
        ReciboPago.isOn = true;
        pagardentro = true;
        DiaP.text = DateTime.Now.Day.ToString();
        if (DateTime.Now.Month == 1 || DateTime.Now.Month == 01)
            MesP.text = "Enero";
        else if (DateTime.Now.Month == 2 || DateTime.Now.Month == 02)
            MesP.text = "Febrero";
        else if (DateTime.Now.Month == 3 || DateTime.Now.Month == 03)
            MesP.text = "Marzo";
        else if (DateTime.Now.Month == 4 || DateTime.Now.Month == 04)
            MesP.text = "Abril";
        else if (DateTime.Now.Month == 5 || DateTime.Now.Month == 05)
            MesP.text = "Mayo";
        else if (DateTime.Now.Month == 6 || DateTime.Now.Month == 06)
            MesP.text = "Junio";
        else if (DateTime.Now.Month == 7 || DateTime.Now.Month == 07)
            MesP.text = "Julio";
        else if (DateTime.Now.Month == 8 || DateTime.Now.Month == 08)
            MesP.text = "Agosto";
        else if (DateTime.Now.Month == 9 || DateTime.Now.Month == 09)
            MesP.text = "Septiembre";
        else if (DateTime.Now.Month == 10)
            MesP.text = "Octubre";
        else if (DateTime.Now.Month == 11)
            MesP.text = "Noviembre";
        else if (DateTime.Now.Month == 12)
            MesP.text = "Diciembre";
        AñoP.text = DateTime.Now.Year.ToString();

        ob.Click("OpcionesMetPago");
    }

    public void Guardar()
    {
        //Debug.Log("Guardar Pago");
        /*print("---------------------");
        print(AlertasTexto.text);
        print(pagardentro);
        print(pagoretrasado);
        print("fecha: " + Fecha.text);
        print("casa: " + Casa.text);
        print("precio: " + PrecioPago.text);
        print("same date: " + sameDate);
        print("i: " + i);
        print("m: " + m);
        print("record pago: " + RecordPago);
        print("---------------------");*/

        if (i > m)
        {
            m = i;
        }
        if (f > n)
        {
            n = f;
        }

        if (PrecioPago.text != "" && Casa.text != "Mis casas" && (Fecha.text == "Fecha de pago" || sameDate == true) && AlertasTexto.text == "Alerta de recordatorio" && pagoretrasado == false && (pagardentro == false||pagardentro==true) && RecordPago == 0)
        {
            //print("1");
            ob.Click("PagoUnico");
        }
        else if (PrecioPago.text != "" && Casa.text != "Mis casas" && Fecha.text != "Fecha de pago" && AlertasTexto.text != "Alerta de recordatorio" && pagoretrasado == false && pagardentro == false && RecordPago == 1 && RepetirCadaTexto.text != "Repetir cada")
        {
            //print("2");

            i++;
            IdPagoLocal.Insert(0, i);

            IdPago.Add(0);
            IdCasa.Add(idCasa);
            idBeneficiario.Add(idBen);
            CadaCuandos.Add(CadaCuando);
            AlertasAntes.Add(AlertaAntes);
            idCategoria.Add(idCat);
            CodBarras.Add(CsB.text);
            PrecioPagos.Add(PrecioPago.text);
            Dia.Add(DiaP.text);
            Mes.Add(MesP.text);
            Año.Add(AñoP.text);
            Nota.Add(NotaPago.text);
            ReciboPagos.Add(RecPago);
            NombreCategoria.Add(NombreCategoriaString);
            RecordatorioPagos.Add(RecordPago);
            PagoPersonalizado.Add(PagPer);
            idColors.Add(idColor);
            idIconos.Add(idIcono);
            IdPPred.Add(PagPred);
            Checarpasado.Add(checkpasado);
            //print("Aqui agrego 1 " + nombreCasaMenu);
            nombreCasas.Add(nombreCasaMenu);

            IdPagoLocalOffline.Add(i);
            NombreCategoriaOffline.Add(NombreCategoriaString);
            idColorsOffline.Add(idColor);
            idIconosOffline.Add(idIcono);
            IdCasaOffline.Add(idCasa);
            PrecioPagosOffline.Add(PrecioPago.text);

            fechaOffline.Add(DiaP.text + "/" + MesP.text + "/" + AñoP.text);

            RecordatorioPagosOffline.Add(RecordPago);
            CadaCuandosOffline.Add(CadaCuando);
            AlertasAntesOffline.Add(AlertaAntes);
            IdBeneficiarioOffline.Add(idBen);
            NotaOffline.Add(NotaPago.text);
            ReciboPagosOffline.Add(RecPago);
            CodBarrasOffline.Add(CsB.text);
            nombreCasasOffline.Add(nombreCasaMenu);


            Posicion.Insert(0,posicion);
            posicion++;
            agregar = true;
            X();
            //ob.Click("MisPagos");
            if (idCasa != 0)
            {
                agregando = true;
                AgregarPago();
            }
            if (nombreCasaMenu != "" && nombreCasaMenu != null && ob.PantallaActual != "MisPagosPendientes")
                ob.Click("MisPagos");
            Offline = null;
            for (int yh = 0; yh < Pagos.Count; yh++)
            {
                if (yh == 0)
                    Offline = IdPagoLocal[yh] + "," + IdPago[yh] + "," + IdCasa[yh] + "," + idBeneficiario[yh] + "," + CadaCuandos[yh] + "," + AlertasAntes[yh] + "," + idCategoria[yh] + "," + CodBarras[yh] + ","
                        + PrecioPagos[yh] + "," + Dia[yh] + "," + Mes[yh] + "," + Año[yh] + "," + Nota[yh] + "," + ReciboPagos[yh] + "," + RecordatorioPagos[yh] + "," + idColors[yh] + "," +
                        idIconos[yh] + "," + IdPPred[yh] + "," + Checarpasado[yh] + "," + NombreCategoria[yh] + "," + PagoPersonalizado[yh] + "," + nombreCasas[yh];
                else if (yh > 0)
                    Offline += ";" + IdPagoLocal[yh] + "," + IdPago[yh] + "," + IdCasa[yh] + "," + idBeneficiario[yh] + "," + CadaCuandos[yh] + "," + AlertasAntes[yh] + "," + idCategoria[yh] + "," + CodBarras[yh] + ","
                        + PrecioPagos[yh] + "," + Dia[yh] + "," + Mes[yh] + "," + Año[yh] + "," + Nota[yh] + "," + ReciboPagos[yh] + "," + RecordatorioPagos[yh] + "," + idColors[yh] + "," +
                        idIconos[yh] + "," + IdPPred[yh] + "," + Checarpasado[yh] + "," + NombreCategoria[yh] + "," + PagoPersonalizado[yh] + "," + nombreCasas[yh];
            }
            PlayerPrefs.SetString("Pagos", Offline);

            NoEnviadoPagosAgregar = null;
            for (int gii = 0; IdPagoLocalOffline.Count > gii; gii++)
            {
                if (gii == 0)
                {
                    NoEnviadoPagosAgregar = IdPagoLocalOffline[gii] + "," + NombreCategoriaOffline[gii] + "," + idColorsOffline[gii] + "," + idIconosOffline[gii] + "," + IdCasaOffline[gii] + "," +
                        PrecioPagosOffline[gii] + "," + fechaOffline[gii] + "," + RecordatorioPagosOffline[gii] + "," + CadaCuandosOffline[gii] + "," + AlertasAntesOffline[gii] + "," + IdBeneficiarioOffline[gii] +
                        "," + NotaOffline[gii] + "," + ReciboPagosOffline[gii] + "," + CodBarrasOffline[gii] + "," + nombreCasasOffline[gii];
                }
                else if (gii > 0)
                {
                    NoEnviadoPagosAgregar += ";" + IdPagoLocalOffline[gii] + "," + NombreCategoriaOffline[gii] + "," + idColorsOffline[gii] + "," + idIconosOffline[gii] + "," + IdCasaOffline[gii] + "," +
                        PrecioPagosOffline[gii] + "," + fechaOffline[gii] + "," + RecordatorioPagosOffline[gii] + "," + CadaCuandosOffline[gii] + "," + AlertasAntesOffline[gii] + "," + IdBeneficiarioOffline[gii] +
                        "," + NotaOffline[gii] + "," + ReciboPagosOffline[gii] + "," + CodBarrasOffline[gii] + "," + nombreCasasOffline[gii];
                }
            }
            PlayerPrefs.SetString("PagosAgregar", NoEnviadoPagosAgregar);
            ob.Atras.Clear();
            ob.i = 0;
            ob.Atras.Add(ob.CasaPagosHUB);
            ob.i++;
            ob.Atras.Add(ob.MisPagos);
            ob.i++;

        }
        else if ((PrecioPago.text == "" || Casa.text == "Mis casas" || Fecha.text == "Fecha de pago" || AlertasTexto.text == "Alerta de recordatorio" || RepetirCadaTexto.text == "Repetir cada") && pagoretrasado == false && pagardentro == false && i == m && RecordPago == 1)
        {
            //print("3");

            ob.DesError.text = "Faltan los siguientes datos para terminar con el pago:\n";
            if (depo == true)
            {
                ob.DesError.text = ob.DesError.text + errorMonto;
                ErrorFechaPago.SetActive(true);
            }
            if (casatexto == true)
                ob.DesError.text = ob.DesError.text + errorCasa;
            if (fechabool == true)
            {
                ob.DesError.text = ob.DesError.text + errorFecha;
                ErrorProgPago.SetActive(true);
                ErrorFechaPago.SetActive(true);
            }
            if (alertabool == true)
            {
                ob.DesError.text = ob.DesError.text + errorAlerta;
                ErrorProgPago.SetActive(true);
                ErrorRecordPago.SetActive(true);
            }
            if (repcada == true)
            {
                ob.DesError.text = ob.DesError.text + errorRepetir;
                ErrorProgPago.SetActive(true);
                ErrorRepetCada.SetActive(true);
            }
            ob.Click("Errors");
        }
        else if (PrecioPago.text != "" && Casa.text != "Mis casas" && Fecha.text != "Fecha de pago" && AlertasTexto.text != "Alerta de recordatorio" && pagoretrasado == false && pagardentro == false && RecordPago == 0)
        {
            //print("4");

            i++;
            IdPagoLocal.Insert(0,i);

            IdPago.Add(0);
            IdCasa.Add(idCasa);
            idBeneficiario.Add(idBen);
            CadaCuandos.Add(CadaCuando);
            AlertasAntes.Add(AlertaAntes);
            idCategoria.Add(idCat);
            CodBarras.Add(CsB.text);
            PrecioPagos.Add(PrecioPago.text);
            Dia.Add(DiaP.text);
            Mes.Add(MesP.text);
            Año.Add(AñoP.text);
            Nota.Add(NotaPago.text);
            ReciboPagos.Add(RecPago);
            NombreCategoria.Add(NombreCategoriaString);
            RecordatorioPagos.Add(RecordPago);
            PagoPersonalizado.Add(PagPer);
            idColors.Add(idColor);
            idIconos.Add(idIcono);
            IdPPred.Add(PagPred);
            Checarpasado.Add(checkpasado);
            //print("Aqui agrego 2 " + nombreCasaMenu);
            nombreCasas.Add(nombreCasaMenu);

            IdPagoLocalOffline.Add(i);
            NombreCategoriaOffline.Add(NombreCategoriaString);
            idColorsOffline.Add(idColor);
            idIconosOffline.Add(idIcono);
            IdCasaOffline.Add(idCasa);
            PrecioPagosOffline.Add(PrecioPago.text);
            fechaOffline.Add(DiaP.text + "/" + MesP.text + "/" + AñoP.text);
            RecordatorioPagosOffline.Add(RecordPago);
            CadaCuandosOffline.Add(CadaCuando);
            AlertasAntesOffline.Add(AlertaAntes);
            IdBeneficiarioOffline.Add(idBen);
            NotaOffline.Add(NotaPago.text);
            ReciboPagosOffline.Add(RecPago);
            CodBarrasOffline.Add(CsB.text);
            nombreCasasOffline.Add(nombreCasaMenu);

            Posicion.Insert(0,posicion);
            posicion++;
            agregar = true;
            X();
            if (idCasa != 0)
            {
                agregando = true;
                AgregarPago();
            }
            //ob.Click("MisPagos");
            //ob.Click("CasaPagosHUB");
            if (nombreCasaMenu != "" && nombreCasaMenu != null && ob.PantallaActual != "MisPagosPendientes")
                ob.Click("MisPagos");

            for (int yh = 0; yh < Pagos.Count; yh++)
            {
                if (yh == 0)
                    Offline = IdPagoLocal[yh] + "," + IdPago[yh] + "," + IdCasa[yh] + "," + idBeneficiario[yh] + "," + CadaCuandos[yh] + "," + AlertasAntes[yh] + "," + idCategoria[yh] + "," + CodBarras[yh] + ","
                        + PrecioPagos[yh] + "," + Dia[yh] + "," + Mes[yh] + "," + Año[yh] + "," + Nota[yh] + "," + ReciboPagos[yh] + "," + RecordatorioPagos[yh] + "," + idColors[yh] + "," +
                        idIconos[yh] + "," + IdPPred[yh] + "," + Checarpasado[yh] + "," + NombreCategoria[yh] + "," + PagoPersonalizado[yh] + "," + nombreCasas[yh];
                else if (yh > 0)
                    Offline += ";" + IdPagoLocal[yh] + "," + IdPago[yh] + "," + IdCasa[yh] + "," + idBeneficiario[yh] + "," + CadaCuandos[yh] + "," + AlertasAntes[yh] + "," + idCategoria[yh] + "," + CodBarras[yh] + ","
                        + PrecioPagos[yh] + "," + Dia[yh] + "," + Mes[yh] + "," + Año[yh] + "," + Nota[yh] + "," + ReciboPagos[yh] + "," + RecordatorioPagos[yh] + "," + idColors[yh] + "," +
                        idIconos[yh] + "," + IdPPred[yh] + "," + Checarpasado[yh] + "," + NombreCategoria[yh] + "," + PagoPersonalizado[yh] + "," + nombreCasas[yh];
            }
            PlayerPrefs.SetString("Pagos", Offline);

            NoEnviadoPagosAgregar = null;
            for (int gii = 0; IdPagoLocalOffline.Count > gii; gii++)
            {
                if (gii == 0)
                {
                    NoEnviadoPagosAgregar = IdPagoLocalOffline[gii] + "," + NombreCategoriaOffline[gii] + "," + idColorsOffline[gii] + "," + idIconosOffline[gii] + "," + IdCasaOffline[gii] + "," +
                        PrecioPagosOffline[gii] + "," + fechaOffline[gii] + "," + RecordatorioPagosOffline[gii] + "," + CadaCuandosOffline[gii] + "," + AlertasAntesOffline[gii] + "," + IdBeneficiarioOffline[gii] +
                        "," + NotaOffline[gii] + "," + ReciboPagosOffline[gii] + "," + CodBarrasOffline[gii] + "," + nombreCasasOffline[gii];
                }
                else if (gii > 0)
                {
                    NoEnviadoPagosAgregar += ";" + IdPagoLocalOffline[gii] + "," + NombreCategoriaOffline[gii] + "," + idColorsOffline[gii] + "," + idIconosOffline[gii] + "," + IdCasaOffline[gii] + "," +
                        PrecioPagosOffline[gii] + "," + fechaOffline[gii] + "," + RecordatorioPagosOffline[gii] + "," + CadaCuandosOffline[gii] + "," + AlertasAntesOffline[gii] + "," + IdBeneficiarioOffline[gii] +
                        "," + NotaOffline[gii] + "," + ReciboPagosOffline[gii] + "," + CodBarrasOffline[gii] + "," + nombreCasasOffline[gii];
                }
            }
            PlayerPrefs.SetString("PagosAgregar", NoEnviadoPagosAgregar);
            ob.Atras.Clear();
            ob.i = 0;
            ob.Atras.Add(ob.CasaPagosHUB);
            ob.i++;
            ob.Atras.Add(ob.MisPagos);
            ob.i++;
            //AgregarPago();
        }
        else if ((PrecioPago.text == "" || Casa.text == "Mis casas" || Fecha.text == "Fecha de pago" || AlertasTexto.text == "Alerta de recordatorio") && pagoretrasado == false && pagardentro == false && i == m && RecordPago == 0)
        {
            //print("5");

            ob.DesError.text = "Faltan los siguientes datos para terminar con el pago:\n";
            if (depo == true)
            {
                ob.DesError.text = ob.DesError.text + errorMonto;
                ErrorFechaPago.SetActive(true);
            }
            if (casatexto == true)
                ob.DesError.text = ob.DesError.text + errorCasa;
            if (fechabool == true)
            {
                ob.DesError.text = ob.DesError.text + errorFecha;
                ErrorProgPago.SetActive(true);
                ErrorFechaPago.SetActive(true);
            }
            if (alertabool == true)
            {
                ob.DesError.text = ob.DesError.text + errorAlerta;
                ErrorProgPago.SetActive(true);
                ErrorRecordPago.SetActive(true);
            }
            if (repcada == true)
            {
                ob.DesError.text = ob.DesError.text + errorRepetir;
                ErrorProgPago.SetActive(true);
                ErrorRepetCada.SetActive(true);
            }
            ob.Click("Errors");
        }
        /*else if (PrecioPago.text != "" && Casa.text != "Mis casas" && Fecha.text != "Fecha de pago" && AlertasTexto.text != "Alerta de recordatorio" && pagoretrasado == false && pagardentro != true && RecordPago == 1 && RepetirCadaTexto.text != "Repetir cada")
        {
            print("Hello there");
            ob.DesError.text = "No puedes pagar ahora un pago recurrente\n";
            ob.Click("Errors");
            ReciboPago.isOn = false;
        }*/
        else if (PrecioPago.text != "" && Casa.text != "Mis casas" && Fecha.text != "Fecha de pago" && AlertasTexto.text != "Alerta de recordatorio" && pagoretrasado == false && pagardentro == true && RecordPago == 1 && RepetirCadaTexto.text != "Repetir cada")
        {
            //print("6");

            MesesC();
            fechaMod = meses + "/" + DiaP.text + "/" + AñoP.text;
            DateTime.TryParse(fechaMod, out ModFecha);
            if (CadaCuando == 1)
            {
                ModFecha = ModFecha.AddDays(7);
            }
            else if (CadaCuando == 2)
            {
                ModFecha = ModFecha.AddDays(15);
            }
            else if (CadaCuando == 3)
            {
                ModFecha = ModFecha.AddDays(21);
            }
            else if (CadaCuando == 4)
            {
                ModFecha = ModFecha.AddDays(30);
            }
            else if (CadaCuando == 5)
            {
                ModFecha = ModFecha.AddDays(60);
            }
            else if (CadaCuando == 6)
            {
                ModFecha = ModFecha.AddDays(120);
            }
            else if (CadaCuando == 7)
            {
                ModFecha = ModFecha.AddDays(180);
            }
            else if (CadaCuando == 8)
            {
                ModFecha = ModFecha.AddDays(365);
            }

            if (ModFecha.Month == 1)
                mesback = "Enero";
            else if (ModFecha.Month == 2)
                mesback = "Febrero";
            else if (ModFecha.Month == 3)
                mesback = "Marzo";
            else if (ModFecha.Month == 4)
                mesback = "Abril";
            else if (ModFecha.Month == 5)
                mesback = "Mayo";
            else if (ModFecha.Month == 6)
                mesback = "Junio";
            else if (ModFecha.Month == 7)
                mesback = "Julio";
            else if (ModFecha.Month == 8)
                mesback = "Agosto";
            else if (ModFecha.Month == 9)
                mesback = "Septiembre";
            else if (ModFecha.Month == 10)
                mesback = "Octubre";
            else if (ModFecha.Month == 11)
                mesback = "Noviembre";
            else if (ModFecha.Month == 12)
                mesback = "Diciembre";

            i++;
            IdPagoLocal.Insert(0,i);
            IdPagoLocalB = i;
            IdPago.Add(0);
            IdCasa.Add(idCasa);
            idBeneficiario.Add(idBen);
            CadaCuandos.Add(CadaCuando);
            AlertasAntes.Add(AlertaAntes);
            idCategoria.Add(idCat);
            CodBarras.Add(CsB.text);
            PrecioPagos.Add(PrecioPago.text);

            Dia.Add(DiaP.text);
            //Dia.Add(ModFecha.Day.ToString("d"));

            Mes.Add(MesP.text);
            //Mes.Add(mesback);

            Año.Add(AñoP.text);
            //Año.Add(ModFecha.Year.ToString());

            Nota.Add(NotaPago.text);
            ReciboPagos.Add(RecPago);
            NombreCategoria.Add(NombreCategoriaString);
            RecordatorioPagos.Add(RecordPago);
            PagoPersonalizado.Add(PagPer);
            idColors.Add(idColor);
            idIconos.Add(idIcono);
            IdPPred.Add(PagPred);
            Checarpasado.Add(checkpasado);
            //print("Aqui agrego 3 " + nombreCasaMenu);
            nombreCasas.Add(nombreCasaMenu);

            IdPagoLocalOffline.Add(i);
            NombreCategoriaOffline.Add(NombreCategoriaString);
            idColorsOffline.Add(idColor);
            idIconosOffline.Add(idIcono);
            IdCasaOffline.Add(idCasa);
            PrecioPagosOffline.Add(PrecioPago.text);

            fechaOffline.Add(DiaP.text + "/" + MesP.text + "/" + AñoP.text);
            //fechaOffline.Add(ModFecha.Day.ToString("d") + "/" + mesback + "/" + ModFecha.Year.ToString());

            RecordatorioPagosOffline.Add(RecordPago);
            CadaCuandosOffline.Add(CadaCuando);
            AlertasAntesOffline.Add(AlertaAntes);
            IdBeneficiarioOffline.Add(idBen);
            NotaOffline.Add(NotaPago.text);
            ReciboPagosOffline.Add(RecPago);
            CodBarrasOffline.Add(CsB.text);
            nombreCasasOffline.Add(nombreCasaMenu);

            Posicion.Insert(0,posicion);
            posicion++;
            agregar = true;
            X();
            if (idCasa != 0)
            {
                agregando = true;
                AgregarPago();
            }
            //AgregarPago();
            ob.CancelarPago.SetActive(true);
            ob.Click("OpcionesMetPago");

            Offline = null;
            for (int yh = 0; yh < Pagos.Count; yh++)
            {
                if (yh == 0)
                    Offline = IdPagoLocal[yh] + "," + IdPago[yh] + "," + IdCasa[yh] + "," + idBeneficiario[yh] + "," + CadaCuandos[yh] + "," + AlertasAntes[yh] + "," + idCategoria[yh] + "," + CodBarras[yh] + ","
                        + PrecioPagos[yh] + "," + Dia[yh] + "," + Mes[yh] + "," + Año[yh] + "," + Nota[yh] + "," + ReciboPagos[yh] + "," + RecordatorioPagos[yh] + "," + idColors[yh] + "," +
                        idIconos[yh] + "," + IdPPred[yh] + "," + Checarpasado[yh] + "," + NombreCategoria[yh] + "," + PagoPersonalizado[yh] + "," + nombreCasas[yh];
                else if (yh > 0)
                    Offline += ";" + IdPagoLocal[yh] + "," + 
                        IdPago[yh] + "," + IdCasa[yh] + "," + 
                        idBeneficiario[yh] + "," + 
                        CadaCuandos[yh] + "," + 
                        AlertasAntes[yh] + "," + idCategoria[yh] + "," + CodBarras[yh] + ","
                        + PrecioPagos[yh] + "," + Dia[yh] + "," + Mes[yh] + "," + Año[yh] + "," + Nota[yh] + "," + ReciboPagos[yh] + "," + RecordatorioPagos[yh] + "," + idColors[yh] + "," +
                        idIconos[yh] + "," + IdPPred[yh] + "," + Checarpasado[yh] + "," + NombreCategoria[yh] + "," + PagoPersonalizado[yh] + "," + nombreCasas[yh];
            }
            PlayerPrefs.SetString("Pagos", Offline);

            NoEnviadoPagosAgregar = null;
            for (int gii = 0; IdPagoLocalOffline.Count > gii; gii++)
            {
                if (gii == 0)
                {
                    NoEnviadoPagosAgregar = IdPagoLocalOffline[gii] + "," + NombreCategoriaOffline[gii] + "," + idColorsOffline[gii] + "," + idIconosOffline[gii] + "," + IdCasaOffline[gii] + "," +
                        PrecioPagosOffline[gii] + "," + fechaOffline[gii] + "," + RecordatorioPagosOffline[gii] + "," + CadaCuandosOffline[gii] + "," + AlertasAntesOffline[gii] + "," + IdBeneficiarioOffline[gii] +
                        "," + NotaOffline[gii] + "," + ReciboPagosOffline[gii] + "," + CodBarrasOffline[gii] + "," + nombreCasasOffline[gii];
                }
                else if (gii > 0)
                {
                    NoEnviadoPagosAgregar += ";" + IdPagoLocalOffline[gii] + "," + NombreCategoriaOffline[gii] + "," + idColorsOffline[gii] + "," + idIconosOffline[gii] + "," + IdCasaOffline[gii] + "," +
                        PrecioPagosOffline[gii] + "," + fechaOffline[gii] + "," + RecordatorioPagosOffline[gii] + "," + CadaCuandosOffline[gii] + "," + AlertasAntesOffline[gii] + "," + IdBeneficiarioOffline[gii] +
                        "," + NotaOffline[gii] + "," + ReciboPagosOffline[gii] + "," + CodBarrasOffline[gii] + "," + nombreCasasOffline[gii];
                }
            }
            PlayerPrefs.SetString("PagosAgregar", NoEnviadoPagosAgregar);
            ob.Atras.Clear();
            ob.i = 0;
            ob.Atras.Add(ob.CasaPagosHUB);
            ob.i++;
            ob.Atras.Add(ob.MisPagos);
            ob.i++;
            //AgregarPago();

        }
        else if ((PrecioPago.text == "" || Casa.text == "Mis casas" || Fecha.text == "Fecha de pago" || RepetirCadaTexto.text == "Repetir cada" || AlertasTexto.text == "Alerta de recordatorio") && pagoretrasado == false && f == n && pagardentro == true && RecordPago == 1)
        {
            //print("7");

            ob.DesError.text = "Faltan los siguientes datos para terminar con el pago:\n";
            if (depo == true)
            {
                ob.DesError.text = ob.DesError.text + errorMonto;
                ErrorFechaPago.SetActive(true);
            }
            if (casatexto == true)
                ob.DesError.text = ob.DesError.text + errorCasa;
            if (fechabool == true)
            {
                ob.DesError.text = ob.DesError.text + errorFecha;
                ErrorProgPago.SetActive(true);
                ErrorFechaPago.SetActive(true);
            }
            if (alertabool == true)
            {
                ob.DesError.text = ob.DesError.text + errorAlerta;
                ErrorProgPago.SetActive(true);
                ErrorRecordPago.SetActive(true);
            }
            if (repcada == true)
            {
                ob.DesError.text = ob.DesError.text + errorRepetir;
                ErrorProgPago.SetActive(true);
                ErrorRepetCada.SetActive(true);
            }
            ob.Click("Errors");
        }
        //if (PrecioPago.text != "" && Casa.text != "Mis casas" && Fecha.text != "Fecha de pago" && AlertasTexto.text != "Alerta de recordatorio" && RepetirCadaTexto.text != "Repetir cada" && pagoretrasado == false && pagardentro == true && RecordPago == 1)
        //{
        //    IdPago.Add(i);
        //    IdCasa.Add(idCasa);
        //    idBeneficiario.Add(idBen);
        //    CadaCuandos.Add(CadaCuando);
        //    AlertasAntes.Add(AlertaAntes);
        //    idCategoria.Add(idCat);
        //    CodBarras.Add(CsB.text);
        //    PrecioPagos.Add(PrecioPago.text);
        //    Dia.Add(DiaP.text);
        //    Mes.Add(MesP.text);
        //    Año.Add(AñoP.text);
        //    Nota.Add(NotaPago.text);
        //    ReciboPagos.Add(RecPago);
        //    NombreCategoria.Add(NombreCategoriaString);
        //    RecordatorioPagos.Add(RecordPago);
        //    PagoPersonalizado.Add(PagPer);
        //    idColors.Add(idColor);
        //    idIconos.Add(idIcono);
        //    IdPPred.Add(PagPred);
        //    Checarpasado.Add(checkpasado);
        //    Posicion.Add(posicion);
        //    //NomCat.text = null;
        //    //Icon.Add(Ico);
        //    //Colo.Add(IcoColor);
        //    posicion++;
        //    i++;
        //    X();
        //    //botonIcono.sprite = botonIconoBack;
        //    //iconColor.color = iconColorBack;
        //    //ob.Click("MisPagos");
        //    ob.CancelarPago.SetActive(false);
        //    ob.Click("OpcionesMetPago");
        //}
        //else if ((PrecioPago.text == "" || Casa.text == "Mis casas" || Fecha.text == "Fecha de pago" || AlertasTexto.text == "Alerta de recordatorio" || RepetirCadaTexto.text == "Repetir cada") && pagoretrasado == false && pagardentro == true && i == m && RecordPago == 1)
        //{

        //    ob.DesError.text = "Faltan los siguientes datos para terminar con el pago:";
        //    if (depo == true)
        //        ob.DesError.text = ob.DesError.text + errorMonto;
        //    if (casatexto == true)
        //        ob.DesError.text = ob.DesError.text + errorCasa;
        //    if (fechabool == true)
        //        ob.DesError.text = ob.DesError.text + errorFecha;
        //    if (alertabool == true)
        //        ob.DesError.text = ob.DesError.text + errorAlerta;
        //    ob.Click("Errors");
        //}
       /* else if (PrecioPago.text != "" && Casa.text != "Mis casas" && Fecha.text != "Fecha de pago" && pagoretrasado == true && pagardentro == true)
        {
            print("8");
            ob.Click("PagoUnico");
            /*GuardarPasado();
            //AgregarPagoPasado();
            ob.Click("MisPagosVencidos");
            botonIcono.sprite = botonIconoBack;
            iconColor.color = iconColorBack;
            //ob.Click("MisPagosVencidos");
            //ob.CancelarPago.SetActive(false);
            ob.Click("OpcionesMetPago");*/
        //}
        else if (PrecioPago.text != "" && Casa.text != "Mis casas" && Fecha.text != "Fecha de pago" && pagoretrasado == true && pagardentro == false)
        {
            //print("8.1");

            GuardarPasado();
            //AgregarPagoPasado();
            ob.Click("MisPagosVencidos");
            botonIcono.sprite = botonIconoBack;
            iconColor.color = iconColorBack;
            //ob.Click("MisPagosVencidos");
            //ob.CancelarPago.SetActive(false);
            //ob.Click("OpcionesMetPago");
        }
        else if ((PrecioPago.text == "" || Casa.text == "Mis casas" || Fecha.text == "Fecha de pago") && pagoretrasado == true && pagardentro == false && m == i)
        {
            //print("9");

            ob.DesError.text = "Faltan los siguientes datos para terminar con el pago:\n";
            if (depo == true)
            {
                ob.DesError.text = ob.DesError.text + errorMonto;
                ErrorFechaPago.SetActive(true);
            }
            if (casatexto == true)
                ob.DesError.text = ob.DesError.text + errorCasa;
            if (fechabool == true)
            {
                ob.DesError.text = ob.DesError.text + errorFecha;
                ErrorProgPago.SetActive(true);
                ErrorFechaPago.SetActive(true);
            }
            ob.Click("Errors");
        }
        else if (PrecioPago.text != "" && Casa.text != "Mis casas" && Fecha.text != "Fecha de pago" && pagoretrasado == true && pagardentro == true && RecordPago == 0)
        {
            //print("10");

            ob.Click("OpcionesMetPago");
        }
        else if ((PrecioPago.text == "" || Casa.text == "Mis casas" || Fecha.text == "Fecha de pago") && pagoretrasado == true && pagardentro == true)
        {
            //print("11");

            ob.DesError.text = "Faltan los siguientes datos para terminar con el pago:\n";
            if (depo == true)
            {
                ob.DesError.text = ob.DesError.text + errorMonto;
                ErrorFechaPago.SetActive(true);
            }
            if (casatexto == true)
                ob.DesError.text = ob.DesError.text + errorCasa;
            if (fechabool == true)
            {
                ob.DesError.text = ob.DesError.text + errorFecha;
                ErrorProgPago.SetActive(true);
                ErrorFechaPago.SetActive(true);
            }
            ob.Click("Errors");
        }
        else if (PrecioPago.text != "" && Casa.text != "Mis casas" && Fecha.text != "Fecha de pago" && (AlertasTexto.text != "Alerta de recordatorio"|| AlertasTexto.text == "Alerta de recordatorio") && pagoretrasado == false && pagardentro == true && RecordPago == 0)
        {
            //print("12");

            ob.Click("OpcionesMetPago");
        }
        else if ((PrecioPago.text == "" || Casa.text == "Mis casas" || Fecha.text == "Fecha de pago" || AlertasTexto.text == "Alerta de recordatorio") && pagoretrasado == false && pagardentro == true && RecordPago == 0)
        {
            //print("13");

            ob.DesError.text = "Faltan los siguientes datos para terminar con el pago:\n";
            if (depo == true)
            {
                ob.DesError.text = ob.DesError.text + errorMonto;
                ErrorFechaPago.SetActive(true);
            }
            if (casatexto == true)
                ob.DesError.text = ob.DesError.text + errorCasa;
            if (fechabool == true)
            {
                ob.DesError.text = ob.DesError.text + errorFecha;
                ErrorProgPago.SetActive(true);
                ErrorFechaPago.SetActive(true);
            }
            if (alertabool == true)
            {
                ob.DesError.text = ob.DesError.text + errorAlerta;
                ErrorProgPago.SetActive(true);
                ErrorRecordPago.SetActive(true);
            }
            if (repcada == true)
            {
                ob.DesError.text = ob.DesError.text + errorRepetir;
                ErrorProgPago.SetActive(true);
                ErrorRepetCada.SetActive(true);
            }
            ob.Click("Errors");
        }
        else
        {
            print("!!!");
        }
        if (i > m)
        {
            m = i;
        }
        if (f > n)
        {
            n = f;
        }

    }

    void AlgoPagos()
    {
        for (int yh = 0; yh < IdPagoLocal.Count; yh++)
        {
           // print(yh);
            /*if (yh == 0)
            {
                Offline = IdPagoLocal[yh] + "," + IdPago[yh] + "," + IdCasa[yh] + "," + idBeneficiario[yh] + "," + CadaCuandos[yh] + "," + AlertasAntes[yh] + "," + idCategoria[yh] + "," + CodBarras[yh] + ","
                    + PrecioPagos[yh] + "," + Dia[yh] + "," + Mes[yh] + "," + Año[yh] + "," + Nota[yh] + "," + ReciboPagos[yh] + "," + RecordatorioPagos[yh] + "," + idColors[yh] + "," +
                    idIconos[yh] + "," + IdPPred[yh] + "," + Checarpasado[yh] + "," + NombreCategoria[yh] + "," + PagoPersonalizado[yh] + "," + nombreCasas[yh];
            }
            else if (yh > 0)*/
            //{
                Offline += IdPagoLocal[yh] + "," +
                    IdPago[yh] + "," +
                    IdCasa[yh] + "," +
                    idBeneficiario[yh] + "," +
                    CadaCuandos[yh] + "," +
                    AlertasAntes[yh] + "," +
                    idCategoria[yh] + "," +
                    CodBarras[yh] + "," +
                    PrecioPagos[yh] + "," +
                    Dia[yh] + "," +
                    Mes[yh] + "," +
                    Año[yh] + "," +
                    Nota[yh] + "," +
                    ReciboPagos[yh] + "," +
                    RecordatorioPagos[yh] + "," +
                    idColors[yh] + "," +
                    idIconos[yh] + "," +
                    IdPPred[yh] + "," +
                    Checarpasado[yh] + "," +
                    NombreCategoria[yh] + "," +
                    PagoPersonalizado[yh] + "," +
                    nombreCasas[yh] + ";";
            //}
        }
    }

    public void X()
    {

        G = pago.transform.position;

        //G2 = casax2.transform.position;
        //if (b == 0 && i == 1)
        //{
        b++;
        pagoclone = Instantiate(pago, G, Quaternion.identity, pagos.transform);
        pagoclone.transform.SetAsFirstSibling();
        //Instantiate(casa2, G2, Quaternion.identity, casas2.transform);
        //Instantiate(HistorialCasa, G2, Quaternion.identity, HistorialCasa.transform);
        //pagoclone = GameObject.Find("PagoX(Clone)");
        pagoclone.name = "Pago" + i;
        //casaclone2 = GameObject.Find("CasaMHx(Clone)");
        //casaclone2.name = "CasaMH" + i;
        pagoclone.transform.SetParent(pagos.transform);
        pagoclone.transform.SetAsFirstSibling();
        pagoclone.SetActive(false);
        //HistorialClone = GameObject.Find("HistorialCasaX(Clone)");
        //HistorialClone.name = "HistorialCasa" + i;
        IP = pagoclone.GetComponent<InfoPago>();
        //InfoMH = casaclone2.GetComponent<InfoCasaMH>();
        Pagos.Insert(0, pagoclone);
        //Pagos.Add(pagoclone);
        //PagosIndex.Add(pagoclone);
        //Casass2.Add(casaclone2);
        //Historial.Add(HistorialClone);
        /*else
        {
            if (RecordPago == 1 && RepetirCadaTexto.text != "Repetir cada")
            {
                posx = posx - 1;
            }
        }*/

        IP.NombCat.text = NombreCategoria[NombreCategoria.Count-1];
        IP.NombreCatPago = NombreCategoria[NombreCategoria.Count - 1];
        IP.idCat = idCategoria[idCategoria.Count - 1];
        precio = float.Parse(PrecioPagos[PrecioPagos.Count - 1]);
        precios = precio.ToString("C", ci);
        IP.TotalPago.text = precios;
        IP.PrecioPago = PrecioPagos[PrecioPagos.Count - 1];
        IP.FechaPago.text = Dia[Dia.Count - 1] + "/" + Mes[Mes.Count - 1] + "/" + Año[Año.Count - 1];
        IP.Dia = Dia[Dia.Count - 1];
        IP.Mes = Mes[Mes.Count - 1];
        IP.Año = Año[Año.Count - 1];
        IP.Nota = Nota[Nota.Count - 1];
        IP.idColor = idColors[idColors.Count - 1];
        IP.idIcono = idIconos[idIconos.Count-1];
        IP.Personalizado = PagoPersonalizado[PagoPersonalizado.Count - 1];
        IP.idCasa = IdCasa[IdCasa.Count - 1];

        bool casaencontrada = false;

        for (int lol = 0; lol < casa.Casass.Count; lol++)
        {
            if (idCasa == casa.Casass[lol].GetComponent<InfoCasa>().idCasa)
            {

                IP.Casa.text = casa.Casass[lol].GetComponent<InfoCasa>().NickNameC;
                IP.nombreCasa = casa.Casass[lol].GetComponent<InfoCasa>().NickNameC;
                //IP.NCP.text = casa.Casass[lol].GetComponent<InfoCasa>().NickNameC;
                IP.Casa.text = casa.Casass[lol].GetComponent<InfoCasa>().NickNameC;
                nombreCasas.Add(casa.Casass[lol].GetComponent<InfoCasa>().NickNameC);
                casaencontrada = true;

            }
        }

        if (casaencontrada == false)
        {
            nombreCasas.Add("No encontrada");
        }
        //IP.Casa.text = ICP.NickName;
        if (nombreCasas.Count != 0)
        {

            IP.idBen = idBeneficiario[idBeneficiario.Count - 1];
            IP.CadaCuando = CadaCuandos[CadaCuandos.Count - 1];
            IP.AlertaAntes = AlertasAntes[AlertasAntes.Count - 1];
            IP.RecordPago = RecordatorioPagos[RecordatorioPagos.Count - 1];
            IP.nombreCasa = nombreCasas[nombreCasas.Count - 1];
            IP.RecPago = ReciboPagos[ReciboPagos.Count - 1];
            IP.idPPred = IdPPred[IdPPred.Count - 1];
            IP.CodBarras = CodBarras[CodBarras.Count - 1];
            IP.PagoID = IdPago[IdPago.Count - 1];
            IP.PagoIDLocal = i;
            IP.pasados = 0;
            IP.posicion = Posicion[Posicion.Count-1];

            IP.ob = ob;
            IP.tp = tp;
            IP.ap = this;
            IP.ben = ben;
            IP.Date = Date;
            IP.SP = SP;
            if (IP.CadaCuando == 1)
                IP.TipoRecurrencia.text = "Semanal";
            else if (IP.CadaCuando == 2)
                IP.TipoRecurrencia.text = "Cada quince días";
            else if (IP.CadaCuando == 3)
                IP.TipoRecurrencia.text = "Cada tres semanas";
            else if (IP.CadaCuando == 4)
                IP.TipoRecurrencia.text = "Mensual";
            else if (IP.CadaCuando == 5)
                IP.TipoRecurrencia.text = "Bimestral";
            else if (IP.CadaCuando == 6)
                IP.TipoRecurrencia.text = "Cuatrimestral";
            else if (IP.CadaCuando == 7)
                IP.TipoRecurrencia.text = "Semestral";
            else if (IP.CadaCuando == 8)
                IP.TipoRecurrencia.text = "Anual";
            else if (IP.CadaCuando == 9)
                IP.TipoRecurrencia.text = "Sólo una vez";
            if (RecordPago == 0)
            {
                IP.Recurrencia.text = "Pago: ";

            }

            else if (RecordPago == 1)
            {
                IP.Recurrencia.text = "Pago:";
            }
            //IP.modificado = true;


            //pagarahora = false;
            if (pagardentro == true)
            {
                //ReciboPago.isOn = true;
            }
            else
            {

                PrecioPago.text = null;
                //Casa.text = "Mis casas";
                Fecha.text = "Fecha de pago";
                pagardentro = false;
                ReciboPago.isOn = false;
                CadaCuando = 9;
                AlertaAntes = 0;
                PagPer = 0;
                PagPred = 0;
                RecordPago = 0;
                DiaP.text = DateTime.Now.Day.ToString();
                Date.DiaP.value = DateTime.Now.Day - 1;
                if (DateTime.Now.Month == 1)
                {
                    MesP.text = "Enero";
                    Date.MesP.value = 0;
                }
                else if (DateTime.Now.Month == 2)
                {
                    MesP.text = "Febrero";
                    Date.MesP.value = 1;
                }
                else if (DateTime.Now.Month == 3)
                {
                    MesP.text = "Marzo";
                    Date.MesP.value = 2;
                }
                else if (DateTime.Now.Month == 4)
                {
                    MesP.text = "Abril";
                    Date.MesP.value = 3;
                }
                else if (DateTime.Now.Month == 5)
                {
                    MesP.text = "Mayo";
                    Date.MesP.value = 4;
                }
                else if (DateTime.Now.Month == 6)
                {
                    MesP.text = "Junio";
                    Date.MesP.value = 5;
                }
                else if (DateTime.Now.Month == 7)
                {
                    MesP.text = "Julio";
                    Date.MesP.value = 6;
                }
                else if (DateTime.Now.Month == 8)
                {
                    MesP.text = "Agosto";
                    Date.MesP.value = 7;
                }
                else if (DateTime.Now.Month == 9)
                {
                    MesP.text = "Septiembre";
                    Date.MesP.value = 8;
                }
                else if (DateTime.Now.Month == 10)
                {
                    MesP.text = "Octubre";
                    Date.MesP.value = 9;
                }
                else if (DateTime.Now.Month == 11)
                {
                    MesP.text = "Noviembre";
                    Date.MesP.value = 10;
                }
                else if (DateTime.Now.Month == 12)
                {
                    MesP.text = "Diciembre";
                    Date.MesP.value = 11;
                }
                AñoP.text = DateTime.Now.Year.ToString();
                Date.AñoP.value = 0;

                //PagoRecurrente.isOn = true;
                RecordPago = 1;
                RepetirCadaTexto.text = "Repetir cada: Sólo una vez";
                AlertasTexto.text = "Alerta de recordatorio";
                NotaPago.text = null;
                Beneficiario.text = "Beneficiario";
                CsB.text = null;
            }
            ReciboPago2.isOn = true;
            //    IP.Icon.sprite = botonIcono.sprite;
            //    IP.IconColor.color = iconColor.color;
            ErrorPrecio.SetActive(false);
            ErrorFechaPago.SetActive(false);
            ErrorProgPago.SetActive(false);
            ErrorRecordPago.SetActive(false);
            ErrorRepetCada.SetActive(false);
          //  IP.TN = TN;
            IP.reg = reg;
            if (nombreCasaMenu != "" && nombreCasaMenu != null && ob.PantallaActual != "MisPagosPendientes")
                ob.Click("MisPagos");
            load = true;
            //acomodo = true;
            //obtenido = true;
            //StartCoroutine(AcomodoSeconds());
            //    if (pagardentro == false)
            //    {
            //        IP = null;
            //    }
            //cambio = true;
            //change = true;
            //VPM.chequeo = true;
            pagoclone = null;
            Offline = null;
            AlgoPagos();
            PlayerPrefs.SetString("Pagos", Offline);
            if (entrar == true || agregar == true)
            {

                //reg.cargando = false;
                agregar = false;
                entrar = false;
                acomodo = true;
                obtenido = true;
                load = true;
                StartCoroutine(AcomodoSeconds());
                sw.cambioPago = true;
            }
        }
    }
    public void RepetirCada(string Cada)
    {
        switch (Cada)
        {
            case "Semanal":
                CadaCuando = 1;
                RecordPago = 1;
                RepetirCadaTexto.text = "Repetir cada: Semanal";
                ob.Click("RepetirCadaQuitar");
                break;
            case "Quincenal":
                CadaCuando = 2;
                RecordPago = 1;
                RepetirCadaTexto.text = "Repetir cada: Cada quince días";
                ob.Click("RepetirCadaQuitar");
                break;
            case "Trisemanal":
                CadaCuando = 3;
                RecordPago = 1;
                RepetirCadaTexto.text = "Repetir cada: Cada tres semanas";
                ob.Click("RepetirCadaQuitar");
                break;
            case "Mensual":
                CadaCuando = 4;
                RecordPago = 1;
                RepetirCadaTexto.text = "Repetir cada: Mensual";
                ob.Click("RepetirCadaQuitar");
                break;
            case "Bimestral":
                CadaCuando = 5;
                RecordPago = 1;
                RepetirCadaTexto.text = "Repetir cada: Bimestral";
                ob.Click("RepetirCadaQuitar");
                break;
            case "Cuatrimestral":
                CadaCuando = 6;
                RecordPago = 1;
                RepetirCadaTexto.text = "Repetir cada: Cuatrimestral";
                ob.Click("RepetirCadaQuitar");
                break;
            case "Semestral":
                CadaCuando = 7;
                RecordPago = 1;
                RepetirCadaTexto.text = "Repetir cada: Semestral";
                ob.Click("RepetirCadaQuitar");
                break;
            case "Anual":
                CadaCuando = 8;
                RecordPago = 1;
                RepetirCadaTexto.text = "Repetir cada: Anual";
                ob.Click("RepetirCadaQuitar");
                break;
            case "Nunca":
                CadaCuando = 9;
                RecordPago = 0;
                RepetirCadaTexto.text = "Repetir cada: Sólo una vez";
                ob.Click("RepetirCadaQuitar");
                break;
            default:
                CadaCuando = 9;
                RecordPago = 0;
                RepetirCadaTexto.text = "Repetir cada: Sólo una vez";
                ob.Click("RepetirCadaQuitar");
                break;
        }
    }
    public void Alertas(string Cada)
    {
        switch (Cada)
        {
            case "1dia":
                AlertaAntes = 1;
                AlertasTexto.text = "Alerta de recordatorio: 1 día antes de la fecha de vencimiento";
                ob.Click("AlertasQuitar");
                break;
            case "2dias":
                AlertaAntes = 2;
                AlertasTexto.text = "Alerta de recordatorio: Durante 2 días antes de la fecha de vencimiento";
                ob.Click("AlertasQuitar");
                break;
            case "3dias":
                AlertaAntes = 3;
                AlertasTexto.text = "Alerta de recordatorio: Durante 3 días antes de la fecha de vencimiento";
                ob.Click("AlertasQuitar");
                break;
            default:
                break;
        }
    }
    public void FechaP()
    {
        diasc = Int32.Parse(DiaP.text);
        añosc = Int32.Parse(AñoP.text);
        //mesesc = Int32.Parse(MesP.text);
        MesesC();

        if (añosc > DateTime.Now.Year && modo == 1)
        {
            //print("1");
            sameDate = false;


            Fecha.text = "Fecha de pago: " + DiaP.text + "/" + MesP.text + "/" + AñoP.text;
            dias = Int32.Parse(DiaP.text);
            años = Int32.Parse(AñoP.text);
            meses = GetMonthNum(MesP.text);
            //RecordPago = 1;
            //meses = Int32.Parse(MesP.text);
            // MesesC();
            ob.Click("FechaPagoQuitar");
            fechaeditar = false;
        }
        else if (diasc < DateTime.Now.Day && mesesc <= DateTime.Now.Month && modo == 1)
        {
            //print("2");
            sameDate = false;


            ob.DesError.text = "No se puede cambiar la fecha a una anterior a la del día de hoy porque estas editando un pago, si requieres un pago no realizado, debes crear uno nuevo y asignarle esta fecha";
            ob.Click("Errors");
            fechaeditar = true;

        }
        else if(añosc < DateTime.Now.Year || 
            (añosc == DateTime.Now.Year && mesesc < DateTime.Now.Month) || 
            ((añosc == DateTime.Now.Year && mesesc == DateTime.Now.Month && diasc < DateTime.Now.Day)))
        {
            //print("3");
            sameDate = false;


            ob.Click("ErrorDia");
            //pagoretrasado = true;

            fechaeditar = false;

        }
        /*else if (diasc < DateTime.Now.Day && mesesc <= DateTime.Now.Month && añosc <= DateTime.Now.Year && modo == 0)
        {
            print("3");

            ob.Click("ErrorDia");
            //pagoretrasado = true;

            fechaeditar = false;
        }*/
        else if ((diasc == theDay && mesesc == theMonth && añosc == theYear /*&& modo == 0*/) /* && editar == false*/)
        {
            //print("4");

            sameDate = true;

            pagoretrasado = false;
            Fecha.text = "Fecha de pago: " + DiaP.text + "/" + MesP.text + "/" + AñoP.text;
            dias = Int32.Parse(DiaP.text);
            años = Int32.Parse(AñoP.text);
            meses = GetMonthNum(MesP.text);
            //RecordPago = 1;
            //meses = Int32.Parse(MesP.text);
            // MesesC();
            ob.Click("FechaPagoQuitar");
            fechaeditar = false;
        }
        else if ((diasc > theDay && mesesc >= theMonth && añosc >= theYear /*&& modo == 0*/) /* && editar == false*/)
        {
            //print("5");

            sameDate = false;

            pagoretrasado = false;
            Fecha.text = "Fecha de pago: " + DiaP.text + "/" + MesP.text + "/" + AñoP.text;
            dias = Int32.Parse(DiaP.text);
            años = Int32.Parse(AñoP.text);
            meses = GetMonthNum(MesP.text);
            //RecordPago = 1;
            //meses = Int32.Parse(MesP.text);
            // MesesC();
            ob.Click("FechaPagoQuitar");
            fechaeditar = false;
        }
        else if ((diasc <= theDay && mesesc >= theMonth && añosc >= theYear) /* && editar == false*/)
        {
            //print("6");
            sameDate = false;

            pagoretrasado = false;
            Fecha.text = "Fecha de pago: " + DiaP.text + "/" + MesP.text + "/" + AñoP.text;
            dias = Int32.Parse(DiaP.text);
            años = Int32.Parse(AñoP.text);
            meses = GetMonthNum(MesP.text);
            //meses = Int32.Parse(MesP.text);
            // MesesC();
            ob.Click("FechaPagoQuitar");
            fechaeditar = false;

        }
        else if ((mesesc < theMonth && añosc > theYear) /* && editar == false*/)
        {
            //print("7");
            sameDate = false;

            pagoretrasado = false;
            Fecha.text = "Fecha de pago: " + DiaP.text + "/" + MesP.text + "/" + AñoP.text;
            dias = Int32.Parse(DiaP.text);
            años = Int32.Parse(AñoP.text);
            meses = GetMonthNum(MesP.text);
            //meses = Int32.Parse(MesP.text);
            //  MesesC();
            ob.Click("FechaPagoQuitar");
            fechaeditar = false;
        }
        else
        {
            print("8");
        }

        //if ((dias < theDay && meses <= theMonth && años <= theYear) && editar == true)
        //{
        //    ob.DesError.text = "La fecha es menor a la de hoy, corrígelo e intentalo de nuevo";
        //    ob.Click("Errors");
        //}
        //else if ((dias >= theDay || meses >= theMonth || años >= theYear) && editar == true)
        //{
        //    Fecha.text = "Fecha de pago: " + DiaP.text + "/" + MesP.text + "/" + AñoP.text;
        //    ob.Click("FechaPagoQuitar");
        //}
    }
    public void QuitarFecha()
    {
        if (meses == 0)
        {
            dias = theDay;
            meses = theMonth;
            años = theYear;
        }

        fechaeditar = false;

        for (int i = 0; i < Date.AnoP.Count; i++)
        {
            if (Date.AnoP[i] == años)
            {
                Date.AñoP.value = i;
                break;
            }
        }
        Date.MesP.value = meses - 1;
        Date.DiaP.value = dias - 1;

        AñoP.text = años + "";
        MesP.text = GetMonth(meses);
        DiaP.text = dias + "";

        ob.Click("FechaPagoQuitar");
    }
    public int GetMonthNum(string month)
    {
        switch (month)
        {
            case "Enero":
                return 1;
            case "Febrero":
                return 2;
            case "Marzo":
                return 3;
            case "Abril":
                return 4;
            case "Mayo":
                return 5;
            case "Junio":
                return 6;
            case "Julio":
                return 7;
            case "Agosto":
                return 8;
            case "Septiembre":
                return 9;
            case "Octubre":
                return 10;
            case "Noviembre":
                return 11;
            case "Diciembre":
                return 12;
        }
        return 0;
    }
    public string GetMonth(int month)
    {
        switch (month)
        {
            case 1:
                return "Enero";
            case 2:
                return "Febrero";
            case 3:
                return "Marzo";
            case 4:
                return "Abril";
            case 5:
                return "Mayo";
            case 6:
                return "Junio";
            case 7:
                return "Julio";
            case 8:
                return "Agosto";
            case 9:
                return "Septiembre";
            case 10:
                return "Octubre";
            case 11:
                return "Noviembre";
            case 12:
                return "Diciembre";
        }
        return "";
    }
    public void FechaAnterior()
    {
        if (modo == 0)
        {
            Fecha.text = "Fecha de pago: " + DiaP.text + "/" + MesP.text + "/" + AñoP.text;
            dias = Int32.Parse(DiaP.text);
            años = Int32.Parse(AñoP.text);
            meses = GetMonthNum(MesP.text);
            //meses = Int32.Parse(MesP.text);
            // MesesC();
            RecordPago = 0;
            pagoretrasado = true;
            ob.Click("ErrorDiaQuitar");
            ob.Click("FechaPagoQuitar");
        }
    }
    public void Modificar()
    {
        if (PrecioPago.text != "" && Casa.text != "Mis casas" && Fecha.text != "Fecha de pago" && AlertasTexto.text != "Alerta de recordatorio" && fechaeditar == false)
        {
            PagoID = IP.PagoID;
            IP.idCasa = idCasa;
            IP.idIcono = idIcono;
            IP.idColor = idColor;
            IP.CadaCuando = CadaCuando;
            IP.AlertaAntes = AlertaAntes;
            IP.idCat = idCat;
            IP.idBen = idBen;
            IP.RecPago = RecPago;
            IP.idPPred = PagPred;
            IP.Personalizado = PagPer;
            IP.RecordPago = RecordPago;
            IP.Dia = DiaP.text;
            IP.Mes = MesP.text;
            IP.Año = AñoP.text;
            IP.NombreCatPago = Categoria.text;
            IP.PrecioPago = PrecioPago.text;
            IP.CodBarras = CsB.text;
            IP.Nota = NotaPago.text;
            IP.pasados = checkpasado;
            //if(idCat < 10)
            //{
            //    botonIcono.sprite = tp.Icon[idCat];

            //}
            //else if (idCat > 9)
            //{
            //    botonIcono.sprite = tp.Icon[idIcono];
            //    iconColor.color = tp.Colo[idColor];
            //}
            if (IP.CadaCuando == 1)
                IP.TipoRecurrencia.text = "Semanal";
            else if (IP.CadaCuando == 2)
                IP.TipoRecurrencia.text = "Cada quince días";
            else if (IP.CadaCuando == 3)
                IP.TipoRecurrencia.text = "Cada tres semanas";
            else if (IP.CadaCuando == 4)
                IP.TipoRecurrencia.text = "Mensual";
            else if (IP.CadaCuando == 5)
                IP.TipoRecurrencia.text = "Bimestral";
            else if (IP.CadaCuando == 6)
                IP.TipoRecurrencia.text = "Cuatrimestral";
            else if (IP.CadaCuando == 7)
                IP.TipoRecurrencia.text = "Semestral";
            else if (IP.CadaCuando == 8)
                IP.TipoRecurrencia.text = "Anual";
            else if (IP.CadaCuando == 9)
                IP.TipoRecurrencia.text = "Sólo una vez";
            if (RecordPago == 0)
            {
                IP.Recurrencia.text = "Pago: ";
            }
            else if (RecordPago == 1)
            {
                IP.Recurrencia.text = "Pago: ";
            }
            IP.NombCat.text = Categoria.text;
            precio = float.Parse(PrecioPago.text);
            precios = precio.ToString("C", ci);
            IP.TotalPago.text = precios;
            //IP.PrecioPago = PrecioPagos[i - 1];
            IP.FechaPago.text = DiaP.text + "/" + MesP.text + "/" + AñoP.text;
            if (MesP.text == "Enero")
            {
                IP.MesNum = 1;
            }
            else if (MesP.text == "Febrero")
            {
                IP.MesNum = 2;
            }
            else if (MesP.text == "Marzo")
            {
                IP.MesNum = 3;
            }
            else if (MesP.text == "Abril")
            {
                IP.MesNum = 4;
            }
            else if (MesP.text == "Mayo")
            {
                IP.MesNum = 5;
            }
            else if (MesP.text == "Junio")
            {
                IP.MesNum = 6;
            }
            else if (MesP.text == "Julio")
            {
                IP.MesNum = 7;
            }
            else if (MesP.text == "Agosto")
            {
                IP.MesNum = 8;
            }
            else if (MesP.text == "Septiembre")
            {
                IP.MesNum = 9;
            }
            else if (MesP.text == "Octubre")
            {
                IP.MesNum = 10;
            }
            else if (MesP.text == "Noviembre")
            {
                IP.MesNum = 11;
            }
            else if (MesP.text == "Diciembre")
            {
                IP.MesNum = 12;
            }
            IP.Casa.text = ICP.NickName;
            IP.modificado = true;
            ids = IP.posicion;
            IdCasa[IdCasa.Count-1] = idCasa;
            idBeneficiario[ids] = idBen;
            CadaCuandos[ids] = CadaCuando;
            AlertasAntes[ids] = AlertaAntes;
            idCategoria[ids] = idCat;
            CodBarras[ids] = CsB.text;
            PrecioPagos[ids] = PrecioPago.text;
            Dia[ids] = DiaP.text;
            Mes[ids] = MesP.text;
            Año[ids] = AñoP.text;
            Nota[ids] = NotaPago.text;
            ReciboPagos[ids] = RecPago;
            RecordatorioPagos[ids] = RecordPago;
            NombreCategoria[ids] = NombreCategoriaString;
            idColors[ids] = idColor;
            idIconos[ids] = idIcono;
            IdPPred[ids] = PagPred;
            //Checarpasado[ids] = checkpasado;
            //IdCasa.Add(idCasa);
            //idBeneficiario.Add(idBen);
            //CadaCuandos.Add(CadaCuando);
            //AlertasAntes.Add(AlertaAntes);
            //idCategoria.Add(idCat);
            //CodBarras.Add(CsB.text);
            //PrecioPagos.Add(PrecioPago.text);
            //Dia.Add(DiaP.text);
            //Mes.Add(MesP.text);
            //Año.Add(AñoP.text);
            //Nota.Add(NotaPago.text);
            //ReciboPagos.Add(RecPago);
            //NombreCategoria.Add(NomCat.text);
            //RecordatorioPagos.Add(RecordPago);
            //PagoPersonalizado.Add(PagPer);
            //idColors.Add(idColor);
            //idIconos.Add(idIcono);
            //IdPPred.Add(PagPred);

            //ob.Click("CasaPagosHUB");
            if (nombreCasaMenu != "" && nombreCasaMenu != null && ob.PantallaActual != "MisPagosPendientes")
                ob.Click("MisPagos");

            for (int yh = 0; yh < Pagos.Count; yh++)
            {
                if (yh == 0)
                    Offline = IdPagoLocal[yh] + "," + 
                        IdPago[yh] + "," + 
                        IdCasa[yh] + "," + 
                        idBeneficiario[yh] + "," + CadaCuandos[yh] + "," + AlertasAntes[yh] + "," + idCategoria[yh] + "," + CodBarras[yh] + ","
                        + PrecioPagos[yh] + "," + Dia[yh] + "," + Mes[yh] + "," + Año[yh] + "," + Nota[yh] + "," + ReciboPagos[yh] + "," + RecordatorioPagos[yh] + "," + idColors[yh] + "," +
                        idIconos[yh] + "," + IdPPred[yh] + "," + Checarpasado[yh] + "," + NombreCategoria[yh] + "," + PagoPersonalizado[yh] + "," + nombreCasas[yh];
                else if (yh > 0)
                    Offline += ";" + IdPagoLocal[yh] + "," + IdPago[yh] + "," + IdCasa[yh] + "," + idBeneficiario[yh] + "," + CadaCuandos[yh] + "," + AlertasAntes[yh] + "," + idCategoria[yh] + "," + CodBarras[yh] + ","
                        + PrecioPagos[yh] + "," + Dia[yh] + "," + Mes[yh] + "," + Año[yh] + "," + Nota[yh] + "," + ReciboPagos[yh] + "," + RecordatorioPagos[yh] + "," + idColors[yh] + "," +
                        idIconos[yh] + "," 
                        + IdPPred[yh] + "," 
                        + Checarpasado[yh] + "," 
                        + NombreCategoria[yh] + "," 
                        + PagoPersonalizado[yh] + "," 
                        + nombreCasas[yh];
            }
            PlayerPrefs.SetString("Pagos", Offline);
            if (PagoID != 0)
            {
                IdPagoModOffline.Add(PagoID);
                NombreCategoriaModOffline.Add(NombreCategoriaString);
                idColorsModOffline.Add(idColor);
                idIconosModOffline.Add(idIcono);
                IdCasaModOffline.Add(idCasa);
                PrecioPagosModOffline.Add(PrecioPago.text);
                fechaModOffline.Add(DiaP.text + "/" + MesP.text + "/" + AñoP.text);
                RecordatorioPagosModOffline.Add(RecordPago);
                CadaCuandosModOffline.Add(CadaCuando);
                AlertasAntesModOffline.Add(AlertaAntes);
                IdBeneficiarioModOffline.Add(idBen);
                NotaModOffline.Add(NotaPago.text);
                ReciboPagosModOffline.Add(RecPago);
                CodBarrasModOffline.Add(CsB.text);
                ModificarPagosNE = null;
                for (int gii = 0; IdPagoModOffline.Count > gii; gii++)
                {
                    if (gii == 0)
                    {
                        ModificarPagosNE = IdPagoModOffline[gii] + "," + NombreCategoriaModOffline[gii] + "," + idColorsModOffline[gii] + "," + idIconosModOffline[gii] + "," + IdCasaModOffline[gii] + "," +
                            PrecioPagosModOffline[gii] + "," + fechaModOffline[gii] + "," + RecordatorioPagosModOffline[gii] + "," + CadaCuandosModOffline[gii] + "," + AlertasAntesModOffline[gii] + "," + IdBeneficiarioModOffline[gii] +
                            "," + NotaModOffline[gii] + "," + ReciboPagosModOffline[gii] + "," + CodBarrasModOffline[gii];
                    }
                    else if (gii > 0)
                    {
                        ModificarPagosNE += ";" + IdPagoModOffline[gii] + "," + NombreCategoriaModOffline[gii] + "," + idColorsModOffline[gii] + "," + idIconosModOffline[gii] + "," + IdCasaModOffline[gii] + "," +
                            PrecioPagosModOffline[gii] + "," + fechaModOffline[gii] + "," + RecordatorioPagosModOffline[gii] + "," + CadaCuandosModOffline[gii] + "," + AlertasAntesModOffline[gii] + "," + IdBeneficiarioModOffline[gii] +
                            "," + NotaModOffline[gii] + "," + ReciboPagosModOffline[gii] + "," + CodBarrasModOffline[gii];
                    }
                }
                PlayerPrefs.SetString("PagosModificar", ModificarPagosNE);
                ModPago();
            }
            else if (PagoID == 0)
            {
                for (int huj = 0; huj < IdPagoOffline.Count; huj++)
                {
                    if (IdPagoOffline[huj] == IdPagoLocalB)
                    {
                        PrecioPagosOffline[huj] = PrecioPago.text;
                        fechaOffline[huj] = DiaP.text + "/" + MesP.text + "/" + AñoP.text;
                        RecordatorioPagosOffline[huj] = RecordPago;
                        CadaCuandosOffline[huj] = CadaCuando;
                        AlertasAntesOffline[huj] = AlertaAntes;
                        IdBeneficiarioOffline[huj] = idBen;
                        NotaOffline[huj] = NotaPago.text;
                        ReciboPagosOffline[huj] = RecPago;
                        CodBarrasOffline[huj] = CsB.text;
                    }

                }
                NoEnviadoPagosAgregar = null;
                for (int gii = 0; IdPagoLocalOffline.Count > gii; gii++)
                {
                    if (gii == 0)
                    {
                        NoEnviadoPagosAgregar = IdPagoLocalOffline[gii] + "," + NombreCategoriaOffline[gii] + "," + idColorsOffline[gii] + "," + idIconosOffline[gii] + "," + IdCasaOffline[gii] + "," +
                            PrecioPagosOffline[gii] + "," + fechaOffline[gii] + "," + RecordatorioPagosOffline[gii] + "," + CadaCuandosOffline[gii] + "," + AlertasAntesOffline[gii] + "," + IdBeneficiarioOffline[gii] +
                            "," + NotaOffline[gii] + "," + ReciboPagosOffline[gii] + "," + CodBarrasOffline[gii] + "," + nombreCasasOffline[gii];
                    }
                    else if (gii > 0)
                    {
                        NoEnviadoPagosAgregar += ";" + IdPagoLocalOffline[gii] + "," + NombreCategoriaOffline[gii] + "," + idColorsOffline[gii] + "," + idIconosOffline[gii] + "," + IdCasaOffline[gii] + "," +
                            PrecioPagosOffline[gii] + "," + fechaOffline[gii] + "," + RecordatorioPagosOffline[gii] + "," + CadaCuandosOffline[gii] + "," + AlertasAntesOffline[gii] + "," + IdBeneficiarioOffline[gii] +
                            "," + NotaOffline[gii] + "," + ReciboPagosOffline[gii] + "," + CodBarrasOffline[gii] + "," + nombreCasasOffline[gii];
                    }
                }
                PlayerPrefs.SetString("PagosAgregar", NoEnviadoPagosAgregar);
            }
            IP.X = false;
            IP.modificado = true;
            cambio = true;
            change = true;
            //VPM.chequeo = true;
            Quitar();
            //reg.cargando = false;
            acomodo = true;
            obtenido = true;
            load = true;
            StartCoroutine(AcomodoSeconds());
            sw.cambioPago = true;
        }
        else
        {
            ob.DesError.text = "Los datos no son los necesarios para poder agregar este pago";
            ob.Click("Errors");
        }
        ob.Atras.Clear();
        ob.i = 0;
        ob.Atras.Add(ob.CasaPagosHUB);
        ob.i++;
        ob.Atras.Add(ob.MisPagos);
        ob.i++;
    }
    public void Quitar()
    {
        //ob.Click("MisPagos");
        PrecioPago.text = null;
        //Casa.text = "Mis casas";
        Fecha.text = "Fecha de pago";
        //PagoRecurrente.isOn = true;
        RecordPago = 1;
        RepetirCadaTexto.text = "Repetir cada: Sólo una vez";
        AlertasTexto.text = "Alerta de recordatorio";
        NotaPago.text = null;
        Beneficiario.text = "Beneficiario";
        CadaCuando = 9;
        AlertaAntes = 0;
        PagPer = 0;
        PagPred = 0;
        RecordPago = 0;
        idBen = 0;
        DiaP.text = DateTime.Now.Day.ToString();
        Date.DiaP.value = DateTime.Now.Day - 1;
        if (DateTime.Now.Month == 1)
        {
            MesP.text = "Enero";
            Date.MesP.value = 0;
        }
        else if (DateTime.Now.Month == 2)
        {
            MesP.text = "Febrero";
            Date.MesP.value = 1;
        }
        else if (DateTime.Now.Month == 3)
        {
            MesP.text = "Marzo";
            Date.MesP.value = 2;
        }
        else if (DateTime.Now.Month == 4)
        {
            MesP.text = "Abril";
            Date.MesP.value = 3;
        }
        else if (DateTime.Now.Month == 5)
        {
            MesP.text = "Mayo";
            Date.MesP.value = 4;
        }
        else if (DateTime.Now.Month == 6)
        {
            MesP.text = "Junio";
            Date.MesP.value = 5;
        }
        else if (DateTime.Now.Month == 7)
        {
            MesP.text = "Julio";
            Date.MesP.value = 6;
        }
        else if (DateTime.Now.Month == 8)
        {
            MesP.text = "Agosto";
            Date.MesP.value = 7;
        }
        else if (DateTime.Now.Month == 9)
        {
            MesP.text = "Septiembre";
            Date.MesP.value = 8;
        }
        else if (DateTime.Now.Month == 10)
        {
            MesP.text = "Octubre";
            Date.MesP.value = 9;
        }
        else if (DateTime.Now.Month == 11)
        {
            MesP.text = "Noviembre";
            Date.MesP.value = 10;
        }
        else if (DateTime.Now.Month == 12)
        {
            MesP.text = "Diciembre";
            Date.MesP.value = 11;
        }
        //MesP.text = DateTime.Now.Month.ToString();
        AñoP.text = DateTime.Now.Year.ToString();
        Date.AñoP.value = 0;
        CsB.text = null;
        if (pagardentro == true)
        {
            ReciboPago.isOn = true;
        }
        else
        {
            ReciboPago.isOn = false;
        }
        ReciboPago2.isOn = true;
        ErrorPrecio.SetActive(false);
        ErrorFechaPago.SetActive(false);
        ErrorProgPago.SetActive(false);
        ErrorRecordPago.SetActive(false);
        ErrorRepetCada.SetActive(false);
        //IP.modificado = true;
        IP = null;
        GuardarAgregar.SetActive(true);
        GuardarEditar.SetActive(false);
        ob.Pagos = false;
        modo = 0;
        fechaeditar = false;
        pagoretrasado = false;
        //IB = null;
        editar = false;
    }
    public void QuitarM()
    {
        modp = false;
        botonph.SetActive(true);
        if (nombreCasaMenu != "" && nombreCasaMenu != null && ob.PantallaActual != "MisPagosPendientes")
            ob.Click("MisPagos");
        PrecioPago.text = null;
        //Casa.text = "Mis casas";
        Fecha.text = "Fecha de pago";
        //PagoRecurrente.isOn = true;
        RecordPago = 0;
        RepetirCadaTexto.text = "Repetir cada: Sólo una vez";
        AlertasTexto.text = "Alerta de recordatorio";
        NotaPago.text = null;
        Beneficiario.text = "Beneficiario";
        CadaCuando = 9;
        AlertaAntes = 0;
        PagPer = 0;
        PagPred = 0;
        //RecordPago = 0;
        idBen = 0;
        DiaP.text = DateTime.Now.Day.ToString();
        Date.DiaP.value = DateTime.Now.Day - 1;
        if (DateTime.Now.Month == 1)
        {
            MesP.text = "Enero";
            Date.MesP.value = 0;
        }
        if (DateTime.Now.Month == 2)
        {
            MesP.text = "Febrero";
            Date.MesP.value = 1;
        }
        if (DateTime.Now.Month == 3)
        {
            MesP.text = "Marzo";
            Date.MesP.value = 2;
        }
        if (DateTime.Now.Month == 4)
        {
            MesP.text = "Abril";
            Date.MesP.value = 3;
        }
        if (DateTime.Now.Month == 5)
        {
            MesP.text = "Mayo";
            Date.MesP.value = 4;
        }
        if (DateTime.Now.Month == 6)
        {
            MesP.text = "Junio";
            Date.MesP.value = 5;
        }
        if (DateTime.Now.Month == 7)
        {
            MesP.text = "Julio";
            Date.MesP.value = 6;
        }
        if (DateTime.Now.Month == 8)
        {
            MesP.text = "Agosto";
            Date.MesP.value = 7;
        }
        if (DateTime.Now.Month == 9)
        {
            MesP.text = "Septiembre";
            Date.MesP.value = 8;
        }
        if (DateTime.Now.Month == 10)
        {
            MesP.text = "Octubre";
            Date.MesP.value = 9;
        }
        if (DateTime.Now.Month == 11)
        {
            MesP.text = "Noviembre";
            Date.MesP.value = 10;
        }
        if (DateTime.Now.Month == 12)
        {
            MesP.text = "Diciembre";
            Date.MesP.value = 11;
        }
        //MesP.text = DateTime.Now.Month.ToString();
        AñoP.text = DateTime.Now.Year.ToString();
        Date.AñoP.value = 0;
        CsB.text = null;
        if (pagardentro == true)
        {
            ReciboPago.isOn = true;
        }
        else
        {
            ReciboPago.isOn = false;
        }
        ReciboPago2.isOn = true;
        ErrorPrecio.SetActive(false);
        ErrorFechaPago.SetActive(false);
        ErrorProgPago.SetActive(false);
        ErrorRecordPago.SetActive(false);
        ErrorRepetCada.SetActive(false);
        //IP.modificado = true;
        IP = null;
        GuardarAgregar.SetActive(true);
        GuardarEditar.SetActive(false);
        ob.Pagos = false;
        modo = 0;
        fechaeditar = false;
        //IB = null;
        ob.Atras.Clear();
        ob.i = 0;
        ob.Atras.Add(ob.CasaPagosHUB);
        ob.i++;
        ob.Atras.Add(ob.MisPagos);
        ob.i++;
        editar = false;
    }
    public void Eliminar()
    {

        if (eliminar == true)
        {
        //print("eliminar");

            eliminar = false;
            //ids = IP.posicion;
            //c = ids;
            //ca = index;
            for (int findPago = 0; findPago < Pagos.Count; findPago++)
            {
                if (Pagos[findPago].name == nombrePago)
                {
                    Pagost = Pagos[findPago];
                    ids = findPago;
                    Pagos.RemoveAt(findPago);
                }
            }
            //Pagost = Pagos[ids];
            //for (int d = ids; d < (posicion - 1); d++)
            //{
            //    c++;
            //    Posicion[c] -= 1;
            //    mover = Pagos[c];
            //    IP2 = mover.GetComponent<InfoPago>();
            //    IP2.posicion--;
            //    Ward = Ward2;

            //}
            //for (int po = index; po < (index - 1); po++)
            //{
            //    //ca++;
            //    mover = PagosIndex[ca];
            //    IP2 = mover.GetComponent<InfoPago>();
            //    //IP2.index--;

            //}
            //IP2 = null;
            Destroy(Pagost);

            //PagosIndex.RemoveAt(index);
            /*for (int uii = 0; uii < PagosIndex.Count; uii++)
            {
                //if(IdPagoLocalB == PagosIndex[uii].GetComponent<InfoPago>().PagoIDLocal)
                //{
                //    PagosIndex.RemoveAt(uii);
                //}
            }*/

            IdCasa.RemoveAt(ids);
            idBeneficiario.RemoveAt(ids);
            CadaCuandos.RemoveAt(ids);
            AlertasAntes.RemoveAt(ids);
            idCategoria.RemoveAt(ids);
            CodBarras.RemoveAt(ids);
            PrecioPagos.RemoveAt(ids);
            Dia.RemoveAt(ids);
            Mes.RemoveAt(ids);
            Año.RemoveAt(ids);
            Nota.RemoveAt(ids);
            ReciboPagos.RemoveAt(ids);
            RecordatorioPagos.RemoveAt(ids);
            NombreCategoria.RemoveAt(ids);
            idColors.RemoveAt(ids);
            idIconos.RemoveAt(ids);
            IdPPred.RemoveAt(ids);
            IdPago.RemoveAt(ids);
            IdPagoLocal.RemoveAt(ids);
            //Checarpasado.RemoveAt(ids);
            PagoPersonalizado.RemoveAt(ids);
            
            Posicion.RemoveAt(ids);
            posicion--;
            cambio = true;
            //VPM.chequeo = true;
            //if (IP != null)
            //{
            //    for (int juju = 0; juju < PagosIndex.Count; juju++)
            //    {
            //        if (PagosIndex[juju].name == IP.name)
            //        {
            //            PagosIndex.RemoveAt(juju);
            //        }
            //    }
            //}
            b--;
            Pagost = null;
            ob.Pagos = false;
            load = true;
            Offline = null;
            if (IdPagoLocal.Count != 0)
            {
                AlgoPagos();
            }
            PlayerPrefs.SetString("Pagos", Offline);
            if (PagoID == 0 && IdPagoLocalOffline.Count != 0)
            {
                for (int jok = 0; jok < IdPagoLocalOffline.Count; jok++)
                {
                    if (IdPagoLocalOffline[jok] == IdPagoLocalB)
                    {
                        IdPagoLocalOffline.RemoveAt(jok);
                        NombreCategoriaOffline.RemoveAt(jok);
                        idColorsOffline.RemoveAt(jok);
                        idIconosOffline.RemoveAt(jok);
                        IdCasaOffline.RemoveAt(jok);
                        PrecioPagosOffline.RemoveAt(jok);
                        fechaOffline.RemoveAt(jok);
                        RecordatorioPagosOffline.RemoveAt(jok);
                        CadaCuandosOffline.RemoveAt(jok);
                        AlertasAntesOffline.RemoveAt(jok);
                        IdBeneficiarioOffline.RemoveAt(jok);
                        NotaOffline.RemoveAt(jok);
                        ReciboPagosOffline.RemoveAt(jok);
                        CodBarrasOffline.RemoveAt(jok);
                        nombreCasasOffline.RemoveAt(jok);
                    }
                }
            }
            else if (PagoID != 0)
            {
                EliminarPagoOffline.Add(PagoID);
                EliminarPagoNE = null;
                for (int yu = 0; yu < EliminarPagoOffline.Count; yu++)
                {
                    if (yu == 0)
                    {
                        EliminarPagoNE = EliminarPagoOffline[yu] + ";";
                    }
                    else if (yu > 0)
                    {
                        EliminarPagoNE += EliminarPagoOffline[yu] + ";";
                    }
                }
                PlayerPrefs.SetString("PagosEliminar", EliminarPagoNE);
                ElimPago();
            }
            //ob.Click("CasaPagosHUB");

            //reg.cargando = false;
            acomodo = true;
            obtenido = true;
            StartCoroutine(AcomodoSeconds());
            sw.cambioPago = true;


        }
        //lo hace arriba
        //Quitar();
    }
    public void EliminarServ()
    {
        c = Pagos.Count;
        VPM.Mes.Clear();
        VPM.Mes2.Clear();
        VPM.Mes3.Clear();
        for (int yu = 0; yu < c; yu++)
        {
            //ids = IP.posicion;
            ids = 0;
            //c = ids;
            //ca = index;
            Pagost = Pagos[ids];
            //for (int d = ids; d < (posicion - 1); d++)
            //{
            //    c++;
            //    Posicion[c] -= 1;
            //    mover = Pagos[c];
            //    IP2 = mover.GetComponent<InfoPago>();
            //    IP2.posicion--;
            //    Ward = Ward2;

            //}
            //for (int po = index; po < (index - 1); po++)
            //{
            //    ca++;
            //    mover = PagosIndex[ca];
            //    IP2 = mover.GetComponent<InfoPago>();
            //    IP2.index--;

            //}
            IP2 = null;
            Destroy(Pagost);

            Pagos.RemoveAt(ids);
            //PagosIndex.RemoveAt(ids);
            IdCasa.RemoveAt(ids);
            idBeneficiario.RemoveAt(ids);
            CadaCuandos.RemoveAt(ids);
            AlertasAntes.RemoveAt(ids);
            idCategoria.RemoveAt(ids);
            CodBarras.RemoveAt(ids);
            PrecioPagos.RemoveAt(ids);
            Dia.RemoveAt(ids);
            Mes.RemoveAt(ids);
            Año.RemoveAt(ids);
            Nota.RemoveAt(ids);
            IdPagoLocal.RemoveAt(ids);
            if (nombreCasas.Count != 0)
                nombreCasas.RemoveAt(ids);
            ReciboPagos.RemoveAt(ids);
            RecordatorioPagos.RemoveAt(ids);
            NombreCategoria.RemoveAt(ids);
            idColors.RemoveAt(ids);
            idIconos.RemoveAt(ids);
            IdPPred.RemoveAt(ids);
            IdPago.RemoveAt(ids);
            //print("1");
            Checarpasado.Clear();
            PagoPersonalizado.RemoveAt(ids);
            Posicion.RemoveAt(ids);
            posicion--;
            cambio = true;
            //VPM.chequeo = true;

            b--;
            Pagost = null;
            ob.Pagos = false;
            load = true;
            acomodo = true;
            StartCoroutine(DelayAcomodo());
        }
    }
    public void EliminarPasado()
    {
        eliminar = false;
        /*if(ids!=IP.idCasa)
        {
            ids = IP.idCasa;
        }*/
        //print("ids: "+ids);
        Pagost = PagosPasado[ids];
        c = ids;

        for (int e = ids; e < (posicionPasado - 1); e++)
        {
            c++;
            PosicionPasado[c] -= 1;
            mover = PagosPasado[c];
            //IP4 = mover.GetComponent<InfoPago>();
            //IP4.posicionPasado--;

        }
        for (int po = indexPasado; po < (indexPasado - 1); po++)
        {
            ca++;
            mover = PagosIndexPasado[ca];
            IP2 = mover.GetComponent<InfoPago>();
            IP2.index--;

        }
        //if (h > 0)
        //{
        //    p += (e);
        //}
        //if(h == 0)
        //{
        //    fp.s = 0;
        //}

        //if (c == fp.i && fp.i == 0)
        //{
        //    fp.s = fp.tarjetax.transform.position.y;
        //    i++;
        //}

        PagosPasado.RemoveAt(ids);
        IdCasaPasado.RemoveAt(ids);
        idBeneficiarioPasado.RemoveAt(ids);
        CodBarrasPasado.RemoveAt(ids);
        PrecioPagosPasado.RemoveAt(ids);
        DiaPasado.RemoveAt(ids);
        MesPasado.RemoveAt(ids);
        AñoPasado.RemoveAt(ids);
        NotaPasado.RemoveAt(ids);
        ReciboPagosPasado.RemoveAt(ids);
        IdPagoPasado.RemoveAt(ids);
        IdPagoPasadoLocal.RemoveAt(ids);
        PagoPersonalizadoPasado.RemoveAt(ids);
        idCategoriaPasado.RemoveAt(ids);
        NombreCategoriaPasado.RemoveAt(ids);
        idColorsPasado.RemoveAt(ids);
        idIconosPasado.RemoveAt(ids);
        IdPPredPasado.RemoveAt(ids);
        Checarpasado.RemoveAt(ids);
        //if (RecordPago == 0 && i > 0)
        //{
        //    i--;
        //}
        posicionPasado--;
        cambioPasado = true;
        Destroy(Pagost);
        OfflinePasado = null;
        for (int yh = 0; yh < PagosPasado.Count; yh++)
        {
            if (yh == 0)
                OfflinePasado = IdPagoPasadoLocal[yh] + "," + IdPagoPasado[yh] + "," + IdCasaPasado[yh] + "," + idBeneficiarioPasado[yh] + "," + idCategoriaPasado[yh] + "," + CodBarrasPasado[yh] + ","
                    + PrecioPagosPasado[yh] + "," + DiaPasado[yh] + "," + MesPasado[yh] + "," + AñoPasado[yh] + "," + NotaPasado[yh] + "," + ReciboPagosPasado[yh] + "," + idColorsPasado[yh] + "," +
                    idIconosPasado[yh] + "," + IdPPredPasado[yh] + "," + Checarpasado[yh] + "," + NombreCategoriaPasado[yh] + "," + PagoPersonalizadoPasado[yh];
            else if (yh > 0)
                OfflinePasado += ";" + IdPagoPasadoLocal[yh] + "," + IdPagoPasado[yh] + "," + IdCasaPasado[yh] + "," + idBeneficiarioPasado[yh] + "," + idCategoriaPasado[yh] + "," + CodBarrasPasado[yh] + ","
                    + PrecioPagosPasado[yh] + "," + DiaPasado[yh] + "," + MesPasado[yh] + "," + AñoPasado[yh] + "," + NotaPasado[yh] + "," + ReciboPagosPasado[yh] + "," + idColorsPasado[yh] + "," +
                    idIconosPasado[yh] + "," + IdPPredPasado[yh] + "," + Checarpasado[yh] + "," + NombreCategoriaPasado[yh] + "," + PagoPersonalizadoPasado[yh];
        }
        PlayerPrefs.SetString("PagosPasado", OfflinePasado);
        if (PagoID == 0)
        {
            for (int hu = 0; hu < IdPagoLocalPasadoOffline.Count; hu++)
            {
                if (IdPagoLocalPasadoOffline[hu] == IdPagoLocalB)
                {
                    IdPagoLocalPasadoOffline.RemoveAt(hu);
                    NombreCategoriaPasadoOffline.RemoveAt(hu);
                    idColorsPasadoOffline.RemoveAt(hu);
                    idIconosPasadoOffline.RemoveAt(hu);
                    IdCasaPasadoOffline.RemoveAt(hu);
                    PrecioPagosPasadoOffline.RemoveAt(hu);
                    fechaPasadoOffline.RemoveAt(hu);
                    RecordatorioPagosPasadoOffline.RemoveAt(hu);
                    CadaCuandosPasadoOffline.RemoveAt(hu);
                    AlertasAntesPasadoOffline.RemoveAt(hu);
                    IdBeneficiarioPasadoOffline.RemoveAt(hu);
                    NotaPasadoOffline.RemoveAt(hu);
                    ReciboPagosPasadoOffline.RemoveAt(hu);
                    CodBarrasPasadoOffline.RemoveAt(hu);
                }
            }
            AgregarPagoPasadoNE = null;
            for (int hu = 0; hu < IdPagoLocalPasadoOffline.Count; hu++)
            {
                if (hu == 0)
                {
                    AgregarPagoPasadoNE = IdPagoLocalPasadoOffline[hu] + "," + NombreCategoriaPasadoOffline[hu] + "," + idColorsPasadoOffline[hu] + "," + idIconosPasadoOffline[hu] +
                        "," + IdCasaPasadoOffline[hu] + "," + PrecioPagosPasadoOffline[hu] + "," + fechaPasadoOffline[hu] + "," + RecordatorioPagosPasadoOffline[hu] + "," +
                        CadaCuandosPasadoOffline[hu] + "," + AlertasAntesPasadoOffline[hu] + "," + IdBeneficiarioPasadoOffline[hu] + "," + NotaPasadoOffline[hu] + "," +
                        ReciboPagosPasadoOffline[hu] + "," + CodBarrasPasadoOffline[hu];
                }
                else if (hu > 0)
                {
                    AgregarPagoPasadoNE += ";" + IdPagoLocalPasadoOffline[hu] + "," + NombreCategoriaPasadoOffline[hu] + "," + idColorsPasadoOffline[hu] + "," + idIconosPasadoOffline[hu] +
                        "," + IdCasaPasadoOffline[hu] + "," + PrecioPagosPasadoOffline[hu] + "," + fechaPasadoOffline[hu] + "," + RecordatorioPagosPasadoOffline[hu] + "," +
                        CadaCuandosPasadoOffline[hu] + "," + AlertasAntesPasadoOffline[hu] + "," + IdBeneficiarioPasadoOffline[hu] + "," + NotaPasadoOffline[hu] + "," +
                        ReciboPagosPasadoOffline[hu] + "," + CodBarrasPasadoOffline[hu];
                }
            }
            PlayerPrefs.SetString("PagosPasadoAgregar", AgregarPagoPasadoNE);
        }
        else if (PagoID != 0)
        {
            EliminarPasadoOffline.Add(PagoID);
            EliminarPagoPasadoNE = null;
            for (int count = 0; count < EliminarPasadoOffline.Count; count++)
            {
                if (count == 0)
                {
                    EliminarPagoPasadoNE = EliminarPasadoOffline[count] + ";";
                }
                else if (count != 0)
                {
                    EliminarPagoPasadoNE += EliminarPasadoOffline[count] + ";";
                }
            }
            PlayerPrefs.SetString("PagosPasadoEliminar", EliminarPagoPasadoNE);
            ElimPagoPasado();
        }
        //ob.Click("CasaPagosHUB");
    }
    public void EliminarPasadoServ()
    {
        //c = ;
        for (int yup = 0; yup < PagosPasado.Count; yup++)
        {

            Destroy(PagosPasado[yup]);
        }

        PagosPasado.Clear();
        IdCasaPasado.Clear();
        idBeneficiarioPasado.Clear();
        CodBarrasPasado.Clear();
        PrecioPagosPasado.Clear();
        DiaPasado.Clear();
        MesPasado.Clear();
        AñoPasado.Clear();
        NotaPasado.Clear();
        ReciboPagosPasado.Clear();
        IdPagoPasado.Clear();
        IdPagoPasadoLocal.Clear();
        PosicionPasado.Clear();
        PagoPersonalizadoPasado.Clear();
        idCategoriaPasado.Clear();
        NombreCategoriaPasado.Clear();
        idColorsPasado.Clear();
        idIconosPasado.Clear();
        IdPPredPasado.Clear();
        //print("2");
        Checarpasado.Clear();
        //if (RecordPago == 0 && i > 0)
        //{
        //    i--;
        //}
        posicionPasado = 0;
        cambioPasado = true;
        OfflinePasado = null;
        for (int yh = 0; yh < PagosPasado.Count; yh++)
        {
            if (yh == 0)
                OfflinePasado = IdPagoPasadoLocal[yh] + "," + IdPagoPasado[yh] + "," + IdCasaPasado[yh] + "," + idBeneficiarioPasado[yh] + "," + idCategoriaPasado[yh] + "," + CodBarrasPasado[yh] + ","
                    + PrecioPagosPasado[yh] + "," + DiaPasado[yh] + "," + MesPasado[yh] + "," + AñoPasado[yh] + "," + NotaPasado[yh] + "," + ReciboPagosPasado[yh] + "," + idColorsPasado[yh] + "," +
                    idIconosPasado[yh] + "," + IdPPredPasado[yh] + "," + Checarpasado[yh] + "," + NombreCategoriaPasado[yh] + "," + PagoPersonalizadoPasado[yh];
            else if (yh > 0)
                OfflinePasado += ";" + IdPagoPasadoLocal[yh] + "," + IdPagoPasado[yh] + "," + IdCasaPasado[yh] + "," + idBeneficiarioPasado[yh] + "," + idCategoriaPasado[yh] + "," + CodBarrasPasado[yh] + ","
                    + PrecioPagosPasado[yh] + "," + DiaPasado[yh] + "," + MesPasado[yh] + "," + AñoPasado[yh] + "," + NotaPasado[yh] + "," + ReciboPagosPasado[yh] + "," + idColorsPasado[yh] + "," +
                    idIconosPasado[yh] + "," + IdPPredPasado[yh] + "," + Checarpasado[yh] + "," + NombreCategoriaPasado[yh] + "," + PagoPersonalizadoPasado[yh];
        }
        PlayerPrefs.SetString("PagosPasado", OfflinePasado);
    }
    public void Pasado()
    {
        //GuardarPasado();
    }
    public void GuardarPasado()
    {
        if (PrecioPago.text != "" && Casa.text != "Mis casas" && Fecha.text != "Fecha de pago")
        {
            f++;
            IdPagoPasadoLocal.Add(f);
            IdPagoPasado.Add(0);
            IdCasaPasado.Add(idCasa);
            idBeneficiarioPasado.Add(idBen);
            //CadaCuandosPasado.Add(CadaCuando);
            //AlertasAntesPasado.Add(AlertaAntes);
            idCategoriaPasado.Add(idCat);
            CodBarrasPasado.Add(CsB.text);
            PrecioPagosPasado.Add(PrecioPago.text);
            DiaPasado.Add(DiaP.text);
            MesPasado.Add(MesP.text);
            AñoPasado.Add(AñoP.text);
            NotaPasado.Add(NotaPago.text);
            ReciboPagosPasado.Add(RecPago);
            NombreCategoriaPasado.Add(NombreCategoriaString);
            //RecordatorioPagosPasado.Add(RecordPago);
            PagoPersonalizadoPasado.Add(PagPer);
            idColorsPasado.Add(idColor);
            idIconosPasado.Add(idIcono);
            IdPPredPasado.Add(PagPred);
            Checarpasado.Add(checkpasado);
            PosicionPasado.Add(posicionPasado);
            //NomCat.text = null;
            //Icon.Add(Ico);
            //Colo.Add(IcoColor);
            IdPagoLocalPasadoOffline.Add(f);
            NombreCategoriaPasadoOffline.Add(NombreCategoriaString);
            idColorsPasadoOffline.Add(idColor);
            idIconosPasadoOffline.Add(idColor);
            IdCasaPasadoOffline.Add(idCasa);
            PrecioPagosPasadoOffline.Add(PrecioPago.text);
            fechaPasadoOffline.Add(DiaP.text + "/" + MesP.text + "/" + AñoP.text);
            RecordatorioPagosPasadoOffline.Add(0);
            CadaCuandosPasadoOffline.Add(0);
            AlertasAntesPasadoOffline.Add(0);
            IdBeneficiarioPasadoOffline.Add(idBen);
            NotaPasadoOffline.Add(NotaPago.text);
            ReciboPagosPasadoOffline.Add(RecPago);
            CodBarrasPasadoOffline.Add(CsB.text);
            posicionPasado++;
            PasadoX();
            ob.Pagos = false;
            //ob.Click("CasaPagosHUB");
            ob.Click("MisPagosVencidos");

            OfflinePasado = null;
            for (int yh = 0; yh < PagosPasado.Count; yh++)
            {
                if (yh == 0)
                    OfflinePasado = IdPagoPasadoLocal[yh] + "," + IdPagoPasado[yh] + "," + IdCasaPasado[yh] + "," + idBeneficiarioPasado[yh] + "," + idCategoriaPasado[yh] + "," + CodBarrasPasado[yh] + ","
                        + PrecioPagosPasado[yh] + "," + DiaPasado[yh] + "," + MesPasado[yh] + "," + AñoPasado[yh] + "," + NotaPasado[yh] + "," + ReciboPagosPasado[yh] + "," + idColorsPasado[yh] + "," +
                        idIconosPasado[yh] + "," + IdPPredPasado[yh] + "," + Checarpasado[yh] + "," + NombreCategoriaPasado[yh] + "," + PagoPersonalizadoPasado[yh];
                else if (yh > 0)
                    OfflinePasado += ";" + IdPagoPasadoLocal[yh] + "," + IdPagoPasado[yh] + "," + IdCasaPasado[yh] + "," + idBeneficiarioPasado[yh] + "," + idCategoriaPasado[yh] + "," + CodBarrasPasado[yh] + ","
                        + PrecioPagosPasado[yh] + "," + DiaPasado[yh] + "," + MesPasado[yh] + "," + AñoPasado[yh] + "," + NotaPasado[yh] + "," + ReciboPagosPasado[yh] + "," + idColorsPasado[yh] + "," +
                        idIconosPasado[yh] + "," + IdPPredPasado[yh] + "," + Checarpasado[yh] + "," + NombreCategoriaPasado[yh] + "," + PagoPersonalizadoPasado[yh];
            }
            PlayerPrefs.SetString("PagosPasado", OfflinePasado);
            AgregarPagoPasadoNE = null;
            for (int hu = 0; hu < IdPagoLocalPasadoOffline.Count; hu++)
            {
                if (hu == 0)
                {
                    AgregarPagoPasadoNE = IdPagoLocalPasadoOffline[hu] + "," + NombreCategoriaPasadoOffline[hu] + "," + idColorsPasadoOffline[hu] + "," + idIconosPasadoOffline[hu] +
                        "," + IdCasaPasadoOffline[hu] + "," + PrecioPagosPasadoOffline[hu] + "," + fechaPasadoOffline[hu] + "," + RecordatorioPagosPasadoOffline[hu] + "," +
                        CadaCuandosPasadoOffline[hu] + "," + AlertasAntesPasadoOffline[hu] + "," + IdBeneficiarioPasadoOffline[hu] + "," + NotaPasadoOffline[hu] + "," +
                        ReciboPagosPasadoOffline[hu] + "," + CodBarrasPasadoOffline[hu];
                }
                else if (hu > 0)
                {
                    AgregarPagoPasadoNE += ";" + IdPagoLocalPasadoOffline[hu] + "," + NombreCategoriaPasadoOffline[hu] + "," + idColorsPasadoOffline[hu] + "," + idIconosPasadoOffline[hu] +
                        "," + IdCasaPasadoOffline[hu] + "," + PrecioPagosPasadoOffline[hu] + "," + fechaPasadoOffline[hu] + "," + RecordatorioPagosPasadoOffline[hu] + "," +
                        CadaCuandosPasadoOffline[hu] + "," + AlertasAntesPasadoOffline[hu] + "," + IdBeneficiarioPasadoOffline[hu] + "," + NotaPasadoOffline[hu] + "," +
                        ReciboPagosPasadoOffline[hu] + "," + CodBarrasPasadoOffline[hu];
                }
            }
            PlayerPrefs.SetString("PagosPasadoAgregar", AgregarPagoPasadoNE);
            AgregarPagoPasado();

            //botonIcono.sprite = botonIconoBack;
            //iconColor.color = iconColorBack;
            //ob.Click("AtrasPago");
        }
        else
        {
            ob.DesError.text = "Los datos no son los necesarios para poder agregar este pago";
            ob.Click("Errors");
        }
    }

    public void PasadoX()
    {
        G2 = contentPasado.transform.position;

        //G2 = casax2.transform.position;
        //if (a == 0 && f == 1)
        //{
        //    a++;
        pagoPasadoClone = Instantiate(pagoPasado, G2, Quaternion.identity, pagosPasados.transform);
        pagoPasadoClone.transform.SetAsFirstSibling();
        //Instantiate(casa2, G2, Quaternion.identity, casas2.transform);
        //Instantiate(HistorialCasa, G2, Quaternion.identity, HistorialCasa.transform);
        //pagoPasadoClone = GameObject.Find("PagoVencidoX(Clone)");
        pagoPasadoClone.name = "PagoVencido" + f;

        pagoPasadoClone.transform.SetParent(pagosPasados.transform);
        pagoPasadoClone.transform.SetAsFirstSibling();
        //casaclone2 = GameObject.Find("CasaMHx(Clone)");
        //casaclone2.name = "CasaMH" + i;
        pagoPasadoClone.SetActive(false);
        //HistorialClone = GameObject.Find("HistorialCasaX(Clone)");
        //HistorialClone.name = "HistorialCasa" + i;
        IP3 = pagoPasadoClone.GetComponent<InfoPago>();
        //InfoMH = casaclone2.GetComponent<InfoCasaMH>();
        PagosPasado.Insert(0, pagoPasadoClone);
        //Casass2.Add(casaclone2);
        //Historial.Add(HistorialClone);
        IP3.NombCat.text = NombreCategoriaPasado[posicionPasado - 1];
        IP3.NombreCatPago = NombreCategoriaPasado[posicionPasado - 1];
        precio = float.Parse(PrecioPagosPasado[posicionPasado - 1]);
        precios = precio.ToString("C", ci);
        IP3.TotalPago.text = precios;
        IP3.PrecioPago = PrecioPagosPasado[posicionPasado - 1];
        IP3.FechaPago.text = DiaPasado[posicionPasado - 1] + "/" + MesPasado[posicionPasado - 1] + "/" + AñoPasado[posicionPasado - 1];
        IP3.Dia = DiaPasado[posicionPasado - 1];
        IP3.Mes = MesPasado[posicionPasado - 1];
        IP3.Año = AñoPasado[posicionPasado - 1];
        IP3.Nota = NotaPasado[posicionPasado - 1];
        IP3.idColor = idColorsPasado[posicionPasado - 1];
        IP3.idIcono = idIconosPasado[posicionPasado - 1];
        IP3.Personalizado = PagoPersonalizadoPasado[posicionPasado - 1];
        IP3.idCasa = IdCasaPasado[posicionPasado - 1];
        IP3.idBen = idBeneficiarioPasado[posicionPasado - 1];
        IP3.idCat = idCategoriaPasado[posicionPasado - 1];
        IP3.RecPago = ReciboPagosPasado[posicionPasado - 1];
        IP3.idPPred = IdPPredPasado[posicionPasado - 1];
        IP3.CodBarras = CodBarrasPasado[posicionPasado - 1];
        //IP3.posicionPasado = PosicionPasado[posicionPasado - 1];
        IP3.PagoID = zoo;
        IP3.PagoIDLocal = f;
        //IP3.PagoID = f;
        IP3.ob = ob;
        IP3.tp = tp;
        IP3.ap = this;
        IP3.Icon.sprite = botonIcono.sprite;
        IP3.IconColor.color = iconColor.color;
        IP3.Date = Date;
        IP3.Casa.text = nombcasa;
        IP3.Recurrencia.text = pagorecurrenteventual;
        IP3.pasados = checkpasado;
        PrecioPago.text = null;
        //Casa.text = "Mis casas";
        Fecha.text = "Fecha de pago";
        //PagoRecurrente.isOn = true;
        RecordPago = 1;
        for (int x = 0; x < casa.Casass.Count; x++)
        {
            if (casa.Casass[x].GetComponent<InfoCasa>().idCasa == idCasa)
            {
                IP3.Casa.text = casa.Casass[x].GetComponent<InfoCasa>().NickNameC;
            }
        }
        RepetirCadaTexto.text = "Repetir cada: Sólo una vez";
        AlertasTexto.text = "Alerta de recordatorio";
        NotaPago.text = null;
        Beneficiario.text = "Beneficiario";
        CsB.text = null;
        ReciboPago.isOn = false;
        ReciboPago2.isOn = true;
        pagarahora = false;
        pagardentro = false;
        IP3.pasados = 1;
        IP3 = null;
        pagoPasadoClone = null;
        cambioPasado = true;

        //IP3.TN = TN;
        //IP3.ap = this;
        //HistorialClone = null;
        //scroll.movementType = ScrollRect.MovementType.Elastic;
        //scroll.elasticity = 3.25f;
        //ob.Click("MisPagos");
        //QuitarM();
        Quitar();

    }

    void GuardarHistorial()
    {
        HistorialOff = null;

        //print("guardando historial");
        for (int qw = 0; qw < NombreCatOff.Count; qw++)
        {
            /* for (int i = 0; i < casasIP.Count; i++)
             {
                 if (casasIP[i].CasaID == idCasaHistorialOff[qw])
                 {*/
            if (qw == 0)
            {
                HistorialOff = NombreCatOff[qw] + "," + idCatOff[qw] + "," + idColorOff[qw] + "," + idIconoOff[qw] + "," + idPagoEnOff[qw] + "," + idPagoConOff[qw] + "," + precioHistorialOff[qw] + "," +
                    fechaHistorialOff[qw] + "," + idPagoEnColorOff[qw] + "," + idPagoEnIconoOff[qw] + "," + mesHistorialOff[qw] + "," + añoHistorialOff[qw] + "," + idCasaHistorialOff[qw] + "," + nombreCasasHist[qw];
            }
            else if (qw != 0)
            {
                HistorialOff += ";" + NombreCatOff[qw] + "," + idCatOff[qw] + "," + idColorOff[qw] + "," + idIconoOff[qw] + "," + idPagoEnOff[qw] + "," + idPagoConOff[qw] + "," + precioHistorialOff[qw] + "," +
                    fechaHistorialOff[qw] + "," + idPagoEnColorOff[qw] + "," + idPagoEnIconoOff[qw] + "," + mesHistorialOff[qw] + "," + añoHistorialOff[qw] + "," + idCasaHistorialOff[qw] + "," + nombreCasasHist[qw];
            }
            //}
            //}
        }

        PlayerPrefs.SetString("Historial", HistorialOff);
    }

    void AgregarHist()
    {
        AgregarHistorialNE = null;


        for (int qw = 0; qw < NombreCatAgregarHist.Count; qw++)
        {
            /*for (int i = 0; i < casasIP.Count; i++)
            {
                if (casasIP[i].CasaID == idCasaHistorialOff[qw])
                {*/
            if (qw == 0)
            {
                AgregarHistorialNE = NombreCatAgregarHist[qw] + "," + idCatAgregarHist[qw] + "," + idColorAgregarHist[qw] + "," + idIconoAgregarHist[qw] + "," + idPagoEnAgregarHist[qw] + "," +
                    idPagoConAgregarHist[qw] + "," + precioHistorialAgregarHist[qw] + "," + fechaHistorialAgregarHist[qw] + "," + idPagoEnColorAgregarHist[qw] + "," +
                    idPagoEnIconoAgregarHist[qw] + "," + mesHistorialAgregarHist[qw] + "," + añoHistorialAgregarHist[qw] + "," + idCasaHistorialAgregarHist[qw] + "," + nombreCasasAgregarHist[qw];
            }
            else if (qw != 0)
            {
                AgregarHistorialNE += ";" + NombreCatAgregarHist[qw] + "," + idCatAgregarHist[qw] + "," + idColorAgregarHist[qw] + "," + idIconoAgregarHist[qw] + "," + idPagoEnAgregarHist[qw] + "," +
                    idPagoConAgregarHist[qw] + "," + precioHistorialAgregarHist[qw] + "," + fechaHistorialAgregarHist[qw] + "," + idPagoEnColorAgregarHist[qw] + "," +
                    idPagoEnIconoAgregarHist[qw] + "," + mesHistorialAgregarHist[qw] + "," + añoHistorialAgregarHist[qw] + "," + idCasaHistorialAgregarHist[qw] + "," + nombreCasasAgregarHist[qw];
            }
            //}
            //}
        }
        PlayerPrefs.SetString("HistorialAgregar", AgregarHistorialNE);
    }

    void MesHoy()
    {
        if (DateTime.Now.Month.ToString() == "1")
            meshoy = "Enero";
        if (DateTime.Now.Month.ToString() == "2")
            meshoy = "Febrero";
        if (DateTime.Now.Month.ToString() == "3")
            meshoy = "Marzo";
        if (DateTime.Now.Month.ToString() == "4")
            meshoy = "Abril";
        if (DateTime.Now.Month.ToString() == "5")
            meshoy = "Mayo";
        if (DateTime.Now.Month.ToString() == "6")
            meshoy = "Junio";
        if (DateTime.Now.Month.ToString() == "7")
            meshoy = "Julio";
        if (DateTime.Now.Month.ToString() == "8")
            meshoy = "Agosto";
        if (DateTime.Now.Month.ToString() == "9")
            meshoy = "Septiembre";
        if (DateTime.Now.Month.ToString() == "10")
            meshoy = "Octubre";
        if (DateTime.Now.Month.ToString() == "11")
            meshoy = "Noviembre";
        if (DateTime.Now.Month.ToString() == "12")
            meshoy = "Diciembre";
    }

    void MesProx()
    {
        if (IP.date.Month.ToString() == "1")
            mesprox = "Enero";
        if (IP.date.Month.ToString() == "2")
            mesprox = "Febrero";
        if (IP.date.Month.ToString() == "3")
            mesprox = "Marzo";
        if (IP.date.Month.ToString() == "4")
            mesprox = "Abril";
        if (IP.date.Month.ToString() == "5")
            mesprox = "Mayo";
        if (IP.date.Month.ToString() == "6")
            mesprox = "Junio";
        if (IP.date.Month.ToString() == "7")
            mesprox = "Julio";
        if (IP.date.Month.ToString() == "8")
            mesprox = "Agosto";
        if (IP.date.Month.ToString() == "9")
            mesprox = "Septiembre";
        if (IP.date.Month.ToString() == "10")
            mesprox = "Octubre";
        if (IP.date.Month.ToString() == "11")
            mesprox = "Noviembre";
        if (IP.date.Month.ToString() == "12")
            mesprox = "Diciembre";
    }

    void MesesC()
    {
        if (MesP.text == "Enero")
            mesesc = 1;
        if (MesP.text == "Febrero")
            mesesc = 2;
        if (MesP.text == "Marzo")
            mesesc = 3;
        if (MesP.text == "Abril")
            mesesc = 4;
        if (MesP.text == "Mayo")
            mesesc = 5;
        if (MesP.text == "Junio")
            mesesc = 6;
        if (MesP.text == "Julio")
            mesesc = 7;
        if (MesP.text == "Agosto")
            mesesc = 8;
        if (MesP.text == "Septiembre")
            mesesc = 9;
        if (MesP.text == "Octubre")
            mesesc = 10;
        if (MesP.text == "Noviembre")
            mesesc = 11;
        if (MesP.text == "Diciembre")
            mesesc = 12;
    }

    public void PagoEn(string Donde)
    {
        switch (Donde)
        {
            case "PagoConCredito":
                pagadocon = "Pagado con Crédito";
                idPagoCon = 0;
                ob.Click("OpcionesMetPagoQuitar");
                ob.BancaEnLinea.SetActive(true);
                ob.Click("OpcionesPagoEn");
                break;
            case "PagoConDebito":
                idPagoCon = 1;
                pagadocon = "Pagado con Débito";
                ob.BancaEnLinea.SetActive(true);
                ob.Click("OpcionesMetPagoQuitar");
                ob.Click("OpcionesPagoEn");
                break;
            case "PagoConEfectivo":
                idPagoCon = 2;
                pagadocon = "Pagado con Efectivo";
                ob.BancaEnLinea.SetActive(false);
                ob.Click("OpcionesMetPagoQuitar");
                ob.Click("OpcionesPagoEn");
                break;

            case "PagoConTransferencia":
                sameDate = false;

                #region Transferencia
                idPagoCon = 3;
                idPagoEn = 0;
                pagadoen = "Banca en línea";
                ob.BancaEnLinea.SetActive(true);
                pagadocon = "Transferencia Bancaria";
                ob.Click("OpcionesMetPagoQuitar");
                //ob.Click("CasaPagosHUB");
                pagarahora = true;
                pagado = true;
                if (RecordPago == 0 && pagardentro == true)
                {
                    if (RecordPago == 0 && pagoretrasado == true && pagado == true && pagarahora == true)
                    {
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        fechasHistorial = DiaP.text + "/" + MesP.text + "/" + AñoP.text;
                        MesesC();
                        añosc = Int32.Parse(AñoP.text);



                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        HistorialX();

                        //GuardarHistorial();

                        NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);
                        //AgregarHist();
                        //ICMH.ap = this;
                        //ICMH.Cat = NombreCategoriaString;
                        //ICMH.idCat = idCat;
                        //ICMH.idCol = idColor;
                        //ICMH.idIcon = idIcono;
                        //ICMH.idPagEn = idPagoEn;
                        //ICMH.idPagCon = idPagoCon;
                        //ICMH.idPagEnCol = idPagoEnColor;
                        //ICMH.idPagEnIcono = idPagoEnIcono;
                        //ICMH.MontoPago = PrecioPago.text;
                        //ICMH.Pago();

                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }
                        if (ReciboPago.isOn == true)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + NombreCategoriaString + " asigando a la casa " + ICa.NickNameC +
                               " con un monto de " + apagar.ToString("C", ci) + " con la fecha de " + DiaP.text + "/" + MesP.text + "/" + AñoP.text;
                            EnviarRecibo();
                        }
                        ob.Click("Pagado");

                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + NombreCategoriaString + ", a la casa: " + ICa.NickNameC + ", con un monto de: " + apagar.ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DiaP.text + "/" + MesP.text + "/" + AñoP.text + ", como es un pago vencido, no se agregara a su lista de pagos vencidos";
                        pagarahora = false;
                        pagardentro = false;

                        pagado = false;
                        AhoraBack = false;
                        ReciboPago.isOn = false;

                    }
                    if (RecordPago == 0 && pagoretrasado == false && pagado == true && pagarahora == true)
                    {
                        //ICMH = casas.Casass2[posicionCasa].GetComponent<InfoCasaMH>();
                        //ICMH.ap = this;
                        //ICMH.Cat = NombreCategoriaString;
                        //ICMH.idCat = idCat;
                        //ICMH.idCol = idColor;
                        //ICMH.idIcon = idIcono;
                        //ICMH.idPagEn = idPagoEn;
                        //ICMH.idPagCon = idPagoCon;
                        //ICMH.idPagEnCol = idPagoEnColor;
                        //ICMH.idPagEnIcono = idPagoEnIcono;
                        //ICMH.MontoPago = PrecioPago.text;
                        //ICMH.Pago();
                        pagado = false;
                        pagarahora = false;
                        pagardentro = false;
                        AhoraBack = false;
                        ReciboPago.isOn = false;
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        MesHoy();
                        fechasHistorial = DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString();
                        mesesc = DateTime.Now.Month;
                        añosc = DateTime.Now.Year;
                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        HistorialX();

                        //GuardarHistorial();

                        NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);

                        //
                        Eliminar();

                        //AgregarHist();


                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }
                        if (ReciboPago.isOn == true)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + NombreCategoriaString + " asigando a la casa " + ICa.NickNameC +
                               " con un monto de " + apagar.ToString("C", ci) + " con la fecha de " + DateTime.Now.Day.ToString("d") + "/" + meshoy + DateTime.Now.Year.ToString();
                            EnviarRecibo();
                        }
                        ob.Click("Pagado");
                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + NombreCategoriaString + ", a la casa: " + ICa.NickNameC + ", con un monto de: " + apagar.ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString() + ", como es un pago del tipo eventual, no se agregara a su lista de pagos programados";

                    }
                }
                if (RecordPago == 1 && pagardentro == true && pagoretrasado == false)
                {
                    if (RecordPago == 1 && pagado == true && pagarahora == true)
                    {
                        //ICMH = casas.Casass2[posicionCasa].GetComponent<InfoCasaMH>();
                        //ICMH.ap = this;
                        //ICMH.Cat = NombreCategoriaString;
                        //ICMH.idCat = idCat;
                        //ICMH.idCol = idColor;
                        //ICMH.idIcon = idIcono;
                        //ICMH.idPagEn = idPagoEn;
                        //ICMH.idPagCon = idPagoCon;
                        //ICMH.idPagEnCol = idPagoEnColor;
                        //ICMH.idPagEnIcono = idPagoEnIcono;
                        //ICMH.MontoPago = PrecioPago.text;
                        //ICMH.Pago();
                        pagado = false;
                        pagarahora = false;
                        pagardentro = false;
                        AhoraBack = false;
                        ReciboPago.isOn = false;
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        MesHoy();
                        for (int hello = 0; hello < Pagos.Count; hello++)
                        {
                            if (Pagos[hello].GetComponent<InfoPago>().PagoIDLocal == IdPagoLocalB)
                            {
                                IP = Pagos[hello].GetComponent<InfoPago>();
                            }
                        }
                        MesProx();
                        fechasHistorial = DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString();
                        fechaMod = IP.date.Day.ToString("d") + "/" + mesprox + "/" + IP.date.Year.ToString();
                        pagar = true;

                        CadaCuando = IP.CadaCuando;
                        PrecioPago.text = IP.PrecioPago;
                        AlertaAntes = IP.AlertaAntes;
                        idBen = IP.idBen;
                        NotaPago.text = IP.Nota;
                        idColor = IP.idColor;
                        idIcono = IP.idIcono;
                        RecPago = IP.RecPago;
                        RecordPago = IP.RecordPago;
                        if (IP.PagoID == 0)
                        {
                            for (int uh = 0; uh < Pagos.Count; uh++)
                            {
                                if (IP.PagoIDLocal == IdPagoLocalOffline[uh])
                                {
                                    fechaOffline[uh] = fechaMod;
                                }
                            }
                            NoEnviadoPagosAgregar = null;
                            for (int gii = 0; IdPagoLocalOffline.Count > gii; gii++)
                            {
                                if (gii == 0)
                                {
                                    NoEnviadoPagosAgregar = IdPagoLocalOffline[gii] + "," + NombreCategoriaOffline[gii] + "," + idColorsOffline[gii] + "," + idIconosOffline[gii] + "," + IdCasaOffline[gii] + "," +
                                        PrecioPagosOffline[gii] + "," + fechaOffline[gii] + "," + RecordatorioPagosOffline[gii] + "," + CadaCuandosOffline[gii] + "," + AlertasAntesOffline[gii] + "," + IdBeneficiarioOffline[gii] +
                                        "," + NotaOffline[gii] + "," + ReciboPagosOffline[gii] + "," + CodBarrasOffline[gii] + "," + nombreCasasOffline[gii];
                                }
                                else if (gii > 0)
                                {
                                    NoEnviadoPagosAgregar += ";" + IdPagoLocalOffline[gii] + "," + NombreCategoriaOffline[gii] + "," + idColorsOffline[gii] + "," + idIconosOffline[gii] + "," + IdCasaOffline[gii] + "," +
                                        PrecioPagosOffline[gii] + "," + fechaOffline[gii] + "," + RecordatorioPagosOffline[gii] + "," + CadaCuandosOffline[gii] + "," + AlertasAntesOffline[gii] + "," + IdBeneficiarioOffline[gii] +
                                        "," + NotaOffline[gii] + "," + ReciboPagosOffline[gii] + "," + CodBarrasOffline[gii] + "," + nombreCasasOffline[gii];
                                }
                            }
                            PlayerPrefs.SetString("PagosAgregar", NoEnviadoPagosAgregar);

                        }


                        if (IP.CodBarras != "")
                        {
                            codbar = Int32.Parse(IP.CodBarras);
                        }
                        ids = IP.PagoID;
                        IP2 = IP;
                        mesesc = DateTime.Now.Month;
                        añosc = DateTime.Now.Year;
                        Historial = true;
                        if (IP.RecPago == 1)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + IP.NombreCatPago + " asigando a la casa " + ICa.NickNameC + " con un monto de " + float.Parse(IP.PrecioPago).ToString("C", ci) + " con la fecha de " +
                                fechasHistorial + " y su próxima fecha de pago es el " + fechaMod;
                            EnviarRecibo();
                        }
                        //Modificar();

                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);

                        //GuardarHistorial();

                        NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        //HistorialX();

                        //AgregarHist();

                        HistorialX();
                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }
                        ob.Click("Pagado");
                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + IP2.NombreCatPago + ", a la casa: " + ICa.NickNameC + ", con un monto de: " + float.Parse(IP2.PrecioPago).ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString() + ", su próximo pago será el: " + IP2.dateproximopago.Day.ToString("d") + "/" + mesprox + "/" + IP2.dateproximopago.Year.ToString();
                        ob.Click("OpcionesPagoEnQuitar");

                    }
                }
                if (pagarahora == true)
                {
                    if (AhoraBack == true && past == false)
                    {
                        for (int fy = 0; Pagos.Count > fy; fy++)
                        {
                            if (Pagos[fy].GetComponent<InfoPago>().PagoID == ids)
                            {
                                IP = Pagos[fy].GetComponent<InfoPago>();
                            }
                        }
                        //IP = Pagos[posicion].GetComponent<InfoPago>();
                    }
                    if (AhoraBack == true && past == true)
                    {
                        for (int fy = 0; PagosPasado.Count > fy; fy++)
                        {
                            if (PagosPasado[fy].GetComponent<InfoPago>().PagoID == ids)
                            {
                                IP = PagosPasado[fy].GetComponent<InfoPago>();
                            }
                        }
                        //IP = PagosPasado[posicion].GetComponent<InfoPago>();
                    }

                    if (IP.RecordPago == 1 && pagado == true && past == false)
                    {
                        //ICMH = casas.Casass2[posicionCasa].GetComponent<InfoCasaMH>();
                        //ICMH.ap = this;
                        //ICMH.Cat = IP.NombreCatPago;
                        //ICMH.idCat = IP.idCat;
                        //ICMH.idCol = IP.idColor;
                        //ICMH.idIcon = IP.idIcono;
                        //ICMH.idPagEn = idPagoEn;
                        //ICMH.idPagCon = idPagoCon;
                        //ICMH.idPagEnCol = idPagoEnColor;
                        //ICMH.idPagEnIcono = idPagoEnIcono;
                        //ICMH.MontoPago = IP.PrecioPago;
                        //ICMH.Pago();
                        IP.pagado = true;
                        pagarahora = false;
                        pagardentro = false;
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        MesHoy();

                        if (IP.dateproximopago.Month.ToString() == "1")
                            mesprox = "Enero";
                        if (IP.dateproximopago.Month.ToString() == "2")
                            mesprox = "Febrero";
                        if (IP.dateproximopago.Month.ToString() == "3")
                            mesprox = "Marzo";
                        if (IP.dateproximopago.Month.ToString() == "4")
                            mesprox = "Abril";
                        if (IP.dateproximopago.Month.ToString() == "5")
                            mesprox = "Mayo";
                        if (IP.dateproximopago.Month.ToString() == "6")
                            mesprox = "Junio";
                        if (IP.dateproximopago.Month.ToString() == "7")
                            mesprox = "Julio";
                        if (IP.dateproximopago.Month.ToString() == "8")
                            mesprox = "Agosto";
                        if (IP.dateproximopago.Month.ToString() == "9")
                            mesprox = "Septiembre";
                        if (IP.dateproximopago.Month.ToString() == "10")
                            mesprox = "Octubre";
                        if (IP.dateproximopago.Month.ToString() == "11")
                            mesprox = "Noviembre";
                        if (IP.dateproximopago.Month.ToString() == "12")
                            mesprox = "Diciembre";

                        fechasHistorial = DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString();
                        fechaMod = IP.dateproximopago.Day.ToString("d") + "/" + mesprox + "/" + IP.dateproximopago.Year.ToString();
                        ids = IP.PagoID;
                        pagar = true;
                        CadaCuando = IP.CadaCuando;
                        AlertaAntes = IP.AlertaAntes;
                        PrecioPago.text = IP.PrecioPago;
                        Categoria.text = IP.NombreCatPago;
                        idIcono = IP.idIcono;
                        idColor = IP.idColor;
                        NombreCategoriaString = IP.NombreCatPago;
                        mesesc = IP.MesNum;
                        añosc = IP.AñoNum;
                        Historial = true;


                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        HistorialX();

                        //GuardarHistorial();

                        NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);

                        //AgregarHist();

                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }


                        if (IP.RecPago == 1)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + IP.NombreCatPago + " asigando a la casa " + IP.nombreCasa + " con un monto de " + float.Parse(IP.PrecioPago).ToString("C", ci) + " con la fecha de " +
                                fechasHistorial + " y su próxima fecha de pago es el " + fechaMod;
                            EnviarRecibo();
                        }
                        ob.Click("Pagado");
                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + IP.NombreCatPago + ", a la casa: " + IP.nombreCasa + ", con un monto de: " + float.Parse(IP.PrecioPago).ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString() + ", su próximo pago será el: " + IP.dateproximopago.Day.ToString("d") + "/" + mesprox + "/" + IP.dateproximopago.Year.ToString();
                        ICMH = null;
                        pagado = false;
                        AhoraBack = false;

                    }
                    if (IP.RecordPago == 0 && past == false && pagado == true)
                    {
                        //ICMH = casas.Casass2[posicionCasa].GetComponent<InfoCasaMH>();
                        //ICMH.ap = this;
                        //ICMH.Cat = IP.NombreCatPago;
                        //ICMH.idCat = IP.idCat;
                        //ICMH.idCol = IP.idColor;
                        //ICMH.idIcon = IP.idIcono;
                        //ICMH.idPagEn = idPagoEn;
                        //ICMH.idPagCon = idPagoCon;
                        //ICMH.idPagEnCol = idPagoEnColor;
                        //ICMH.idPagEnIcono = idPagoEnIcono;
                        //ICMH.MontoPago = IP.PrecioPago;
                        //ICMH.Pago();
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        MesHoy();

                        fechasHistorial = DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString();
                        pagar = true;
                        CadaCuando = IP.CadaCuando;
                        AlertaAntes = IP.AlertaAntes;
                        PrecioPago.text = IP.PrecioPago;
                        Categoria.text = IP.NombreCatPago;
                        idIcono = IP.idIcono;
                        idColor = IP.idColor;
                        NombreCategoriaString = IP.NombreCatPago;
                        mesesc = IP.MesNum;
                        añosc = IP.AñoNum;

                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        HistorialX();

                        //GuardarHistorial();

                        NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);

                        //AgregarHist();


                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }
                        ob.Click("Pagado");
                        if (IP.RecPago == 1)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + NombreCategoriaString + " asigando a la casa " + nombreCasaMenu +
                               " con un monto de: " + apagar.ToString("C", ci) + " con la fecha de " + DateTime.Now.Day.ToString("d") + "/" + meshoy + DateTime.Now.Year.ToString();
                            EnviarRecibo();
                        }
                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + NombreCategoriaString + ", a la casa: " + nombreCasaMenu + ", con un monto de " + apagar.ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString() + ", como es un pago del tipo eventual, no se agregara a su lista de pagos programados";
                        IP.Eliminar();

                        pagarahora = false;
                        pagardentro = false;
                        pagado = false;

                    }

                    if (past == true && pagado == true)
                    {
                        ////ICMH = casas.Casass2[posicionCasa].GetComponent<InfoCasaMH>();
                        ////ICMH.ap = this;
                        ////ICMH.Cat = IP.NombreCatPago;
                        ////ICMH.idCat = IP.idCat;
                        ////ICMH.idCol = IP.idColor;
                        ////ICMH.idIcon = IP.idIcono;
                        ////ICMH.idPagEn = idPagoEn;
                        ////ICMH.idPagCon = idPagoCon;
                        ////ICMH.idPagEnCol = idPagoEnColor;
                        ////ICMH.idPagEnIcono = idPagoEnIcono;
                        ////ICMH.MontoPago = IP.PrecioPago;
                        ////ICMH.Pago();
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        MesHoy();
                        //ids = IP.PagoID;
                        pagar = true;
                        CadaCuando = IP.CadaCuando;
                        AlertaAntes = IP.AlertaAntes;
                        PrecioPago.text = IP.PrecioPago;
                        Categoria.text = IP.NombreCatPago;
                        fechasHistorial = DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString();
                        idIcono = IP.idIcono;
                        idColor = IP.idColor;
                        NombreCategoriaString = IP.NombreCatPago;
                        mesesc = IP.MesNum;
                        añosc = IP.AñoNum;

                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        HistorialX();

                        //GuardarHistorial();

                        if (NombreCategoriaString != "")
                            NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);

                        //AgregarHist();

                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }
                        ob.Click("Pagado");
                        if (IP.RecPago == 1)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + NombreCategoriaString + " asigando a la casa " + IP.nombreCasa +
                               " con un monto de " + float.Parse(IP.PrecioPago).ToString("C", ci) + " con la fecha de " + DateTime.Now.Day.ToString("d") + "/" + meshoy + DateTime.Now.Year.ToString();
                            EnviarRecibo();
                        }
                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + IP.NombreCatPago + ", a la casa: " + IP.nombreCasa + ", con un monto de: " + float.Parse(IP.PrecioPago).ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString() + ", como es un pago vencido, se eliminará de su lista de pagos vencidos";
                        IP.EliminarPasado();
                        //EliminarServ();
                        //ObtenerPagos();
                        past = false;
                        pagado = false;
                        pagarahora = false;
                        pagardentro = false;
                        AhoraBack = false;

                    }
                }
                IP = null;
                Quitar();
                QuitarM();
                #endregion
                break;

            case "BancaLinea":
                sameDate = false;

                #region bancaLinea
                idPagoEn = 0;
                idPagoEnIcono = 0;
                idPagoEnColor = 0;
                pagadoen = "Banca en línea";
                ob.Click("OpcionesPagoEnQuitar");
                //ob.Click("CasaPagosHUB");
                pagarahora = true;
                pagado = true;
                if (RecordPago == 0 && pagardentro == true)
                {
                    if (RecordPago == 0 && pagoretrasado == true && pagado == true && pagarahora == true)
                    {
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        fechasHistorial = DiaP.text + "/" + MesP.text + "/" + AñoP.text;
                        MesesC();
                        añosc = Int32.Parse(AñoP.text);



                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        HistorialX();

                        //GuardarHistorial();

                        NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);

                        //AgregarHist();

                        //ICMH.ap = this;
                        //ICMH.Cat = NombreCategoriaString;
                        //ICMH.idCat = idCat;
                        //ICMH.idCol = idColor;
                        //ICMH.idIcon = idIcono;
                        //ICMH.idPagEn = idPagoEn;
                        //ICMH.idPagCon = idPagoCon;
                        //ICMH.idPagEnCol = idPagoEnColor;
                        //ICMH.idPagEnIcono = idPagoEnIcono;
                        //ICMH.MontoPago = PrecioPago.text;
                        //ICMH.Pago();

                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }
                        if (ReciboPago.isOn == true)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + NombreCategoriaString + " asigando a la casa " + ICa.NickNameC +
                               " con un monto de " + apagar.ToString("C", ci) + " con la fecha de " + DiaP.text + "/" + MesP.text + "/" + AñoP.text;
                            EnviarRecibo();
                        }
                        ob.Click("Pagado");

                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + NombreCategoriaString + ", a la casa: " + ICa.NickNameC + ", con un monto de: " + apagar.ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DiaP.text + "/" + MesP.text + "/" + AñoP.text + ", como es un pago vencido, no se agregara a su lista de pagos vencidos";
                        pagarahora = false;
                        pagardentro = false;

                        pagado = false;
                        AhoraBack = false;
                        ReciboPago.isOn = false;

                    }
                    if (RecordPago == 0 && pagoretrasado == false && pagado == true && pagarahora == true)
                    {
                        //ICMH = casas.Casass2[posicionCasa].GetComponent<InfoCasaMH>();
                        //ICMH.ap = this;
                        //ICMH.Cat = NombreCategoriaString;
                        //ICMH.idCat = idCat;
                        //ICMH.idCol = idColor;
                        //ICMH.idIcon = idIcono;
                        //ICMH.idPagEn = idPagoEn;
                        //ICMH.idPagCon = idPagoCon;
                        //ICMH.idPagEnCol = idPagoEnColor;
                        //ICMH.idPagEnIcono = idPagoEnIcono;
                        //ICMH.MontoPago = PrecioPago.text;
                        //ICMH.Pago();
                        pagado = false;
                        pagarahora = false;
                        pagardentro = false;
                        AhoraBack = false;
                        ReciboPago.isOn = false;
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        MesHoy();
                        fechasHistorial = DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString();
                        mesesc = DateTime.Now.Month;
                        añosc = DateTime.Now.Year;
                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        HistorialX();

                        //GuardarHistorial();

                        NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);

                        Eliminar();

                        //AgregarHist();


                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }
                        if (ReciboPago.isOn == true)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + NombreCategoriaString + " asigando a la casa " + ICa.NickNameC +
                               " con un monto de " + apagar.ToString("C", ci) + " con la fecha de " + DateTime.Now.Day.ToString("d") + "/" + meshoy + DateTime.Now.Year.ToString();
                            EnviarRecibo();
                        }
                        ob.Click("Pagado");
                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + NombreCategoriaString + ", a la casa: " + ICa.NickNameC + ", con un monto de: " + apagar.ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString() + ", como es un pago del tipo eventual, no se agregara a su lista de pagos programados";

                    }
                }
                if (RecordPago == 1 && pagardentro == true && pagoretrasado == false)
                {
                    if (RecordPago == 1 && pagado == true && pagarahora == true)
                    {
                        //ICMH = casas.Casass2[posicionCasa].GetComponent<InfoCasaMH>();
                        //ICMH.ap = this;
                        //ICMH.Cat = NombreCategoriaString;
                        //ICMH.idCat = idCat;
                        //ICMH.idCol = idColor;
                        //ICMH.idIcon = idIcono;
                        //ICMH.idPagEn = idPagoEn;
                        //ICMH.idPagCon = idPagoCon;
                        //ICMH.idPagEnCol = idPagoEnColor;
                        //ICMH.idPagEnIcono = idPagoEnIcono;
                        //ICMH.MontoPago = PrecioPago.text;
                        //ICMH.Pago();
                        pagado = false;
                        pagarahora = false;
                        pagardentro = false;
                        AhoraBack = false;
                        ReciboPago.isOn = false;
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        MesHoy();
                        for (int hello = 0; hello < Pagos.Count; hello++)
                        {
                            if (Pagos[hello].GetComponent<InfoPago>().PagoIDLocal == IdPagoLocalB)
                            {
                                IP = Pagos[hello].GetComponent<InfoPago>();
                            }
                        }
                        MesProx();
                        fechasHistorial = DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString();
                        fechaMod = IP.date.Day.ToString("d") + "/" + mesprox + "/" + IP.date.Year.ToString();
                        pagar = true;

                        CadaCuando = IP.CadaCuando;
                        PrecioPago.text = IP.PrecioPago;
                        AlertaAntes = IP.AlertaAntes;
                        idBen = IP.idBen;
                        NotaPago.text = IP.Nota;
                        idColor = IP.idColor;
                        idIcono = IP.idIcono;
                        RecPago = IP.RecPago;
                        RecordPago = IP.RecordPago;
                        if (IP.PagoID == 0)
                        {
                            for (int uh = 0; uh < Pagos.Count; uh++)
                            {
                                if (IP.PagoIDLocal == IdPagoLocalOffline[uh])
                                {
                                    fechaOffline[uh] = fechaMod;
                                }
                            }
                            NoEnviadoPagosAgregar = null;
                            for (int gii = 0; IdPagoLocalOffline.Count > gii; gii++)
                            {
                                if (gii == 0)
                                {
                                    NoEnviadoPagosAgregar = IdPagoLocalOffline[gii] + "," + NombreCategoriaOffline[gii] + "," + idColorsOffline[gii] + "," + idIconosOffline[gii] + "," + IdCasaOffline[gii] + "," +
                                        PrecioPagosOffline[gii] + "," + fechaOffline[gii] + "," + RecordatorioPagosOffline[gii] + "," + CadaCuandosOffline[gii] + "," + AlertasAntesOffline[gii] + "," + IdBeneficiarioOffline[gii] +
                                        "," + NotaOffline[gii] + "," + ReciboPagosOffline[gii] + "," + CodBarrasOffline[gii] + "," + nombreCasasOffline[gii];
                                }
                                else if (gii > 0)
                                {
                                    NoEnviadoPagosAgregar += ";" + IdPagoLocalOffline[gii] + "," + NombreCategoriaOffline[gii] + "," + idColorsOffline[gii] + "," + idIconosOffline[gii] + "," + IdCasaOffline[gii] + "," +
                                        PrecioPagosOffline[gii] + "," + fechaOffline[gii] + "," + RecordatorioPagosOffline[gii] + "," + CadaCuandosOffline[gii] + "," + AlertasAntesOffline[gii] + "," + IdBeneficiarioOffline[gii] +
                                        "," + NotaOffline[gii] + "," + ReciboPagosOffline[gii] + "," + CodBarrasOffline[gii] + "," + nombreCasasOffline[gii];
                                }
                            }
                            PlayerPrefs.SetString("PagosAgregar", NoEnviadoPagosAgregar);

                        }


                        if (IP.CodBarras != "")
                        {
                            codbar = Int32.Parse(IP.CodBarras);
                        }
                        ids = IP.PagoID;
                        IP2 = IP;
                        mesesc = DateTime.Now.Month;
                        añosc = DateTime.Now.Year;
                        Historial = true;
                        if (IP.RecPago == 1)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + IP.NombreCatPago + " asigando a la casa " + ICa.NickNameC + " con un monto de " + float.Parse(IP.PrecioPago).ToString("C", ci) + " con la fecha de " +
                                fechasHistorial + " y su próxima fecha de pago es el " + fechaMod;
                            EnviarRecibo();
                        }
                        //Modificar();

                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);

                        //GuardarHistorial();

                        NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                       // HistorialX();

                        //AgregarHist();

                        HistorialX();
                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }
                        ob.Click("Pagado");
                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + IP2.NombreCatPago + ", a la casa: " + ICa.NickNameC + ", con un monto de: " + float.Parse(IP2.PrecioPago).ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString() + ", su próximo pago será el: " + IP2.dateproximopago.Day.ToString("d") + "/" + mesprox + "/" + IP2.dateproximopago.Year.ToString();
                        ob.Click("OpcionesPagoEnQuitar");

                    }
                }
                if (pagarahora == true)
                {
                    if (AhoraBack == true && past == false)
                    {
                        for (int fy = 0; Pagos.Count > fy; fy++)
                        {
                            if (Pagos[fy].GetComponent<InfoPago>().PagoID == ids)
                            {
                                IP = Pagos[fy].GetComponent<InfoPago>();
                            }
                        }
                        //IP = Pagos[posicion].GetComponent<InfoPago>();
                    }
                    if (AhoraBack == true && past == true)
                    {
                        for (int fy = 0; PagosPasado.Count > fy; fy++)
                        {
                            if (PagosPasado[fy].GetComponent<InfoPago>().PagoID == ids)
                            {
                                IP = PagosPasado[fy].GetComponent<InfoPago>();
                            }
                        }
                        //IP = PagosPasado[posicion].GetComponent<InfoPago>();
                    }

                    if (IP.RecordPago == 1 && pagado == true && past == false)
                    {
                        //ICMH = casas.Casass2[posicionCasa].GetComponent<InfoCasaMH>();
                        //ICMH.ap = this;
                        //ICMH.Cat = IP.NombreCatPago;
                        //ICMH.idCat = IP.idCat;
                        //ICMH.idCol = IP.idColor;
                        //ICMH.idIcon = IP.idIcono;
                        //ICMH.idPagEn = idPagoEn;
                        //ICMH.idPagCon = idPagoCon;
                        //ICMH.idPagEnCol = idPagoEnColor;
                        //ICMH.idPagEnIcono = idPagoEnIcono;
                        //ICMH.MontoPago = IP.PrecioPago;
                        //ICMH.Pago();
                        IP.pagado = true;
                        pagarahora = false;
                        pagardentro = false;
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        MesHoy();

                        if (IP.dateproximopago.Month.ToString() == "1")
                            mesprox = "Enero";
                        if (IP.dateproximopago.Month.ToString() == "2")
                            mesprox = "Febrero";
                        if (IP.dateproximopago.Month.ToString() == "3")
                            mesprox = "Marzo";
                        if (IP.dateproximopago.Month.ToString() == "4")
                            mesprox = "Abril";
                        if (IP.dateproximopago.Month.ToString() == "5")
                            mesprox = "Mayo";
                        if (IP.dateproximopago.Month.ToString() == "6")
                            mesprox = "Junio";
                        if (IP.dateproximopago.Month.ToString() == "7")
                            mesprox = "Julio";
                        if (IP.dateproximopago.Month.ToString() == "8")
                            mesprox = "Agosto";
                        if (IP.dateproximopago.Month.ToString() == "9")
                            mesprox = "Septiembre";
                        if (IP.dateproximopago.Month.ToString() == "10")
                            mesprox = "Octubre";
                        if (IP.dateproximopago.Month.ToString() == "11")
                            mesprox = "Noviembre";
                        if (IP.dateproximopago.Month.ToString() == "12")
                            mesprox = "Diciembre";

                        fechasHistorial = DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString();
                        fechaMod = IP.dateproximopago.Day.ToString("d") + "/" + mesprox + "/" + IP.dateproximopago.Year.ToString();
                        ids = IP.PagoID;
                        pagar = true;
                        CadaCuando = IP.CadaCuando;
                        AlertaAntes = IP.AlertaAntes;
                        PrecioPago.text = IP.PrecioPago;
                        Categoria.text = IP.NombreCatPago;
                        idIcono = IP.idIcono;
                        idColor = IP.idColor;
                        NombreCategoriaString = IP.NombreCatPago;
                        mesesc = IP.MesNum;
                        añosc = IP.AñoNum;
                        Historial = true;


                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        HistorialX();

                        //GuardarHistorial();

                        NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);

                        //AgregarHist();

                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }


                        if (IP.RecPago == 1)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + IP.NombreCatPago + " asigando a la casa " + IP.nombreCasa + " con un monto de " + float.Parse(IP.PrecioPago).ToString("C", ci) + " con la fecha de " +
                                fechasHistorial + " y su próxima fecha de pago es el " + fechaMod;
                            EnviarRecibo();
                        }
                        ob.Click("Pagado");
                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + IP.NombreCatPago + ", a la casa: " + IP.nombreCasa + ", con un monto de: " + float.Parse(IP.PrecioPago).ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString() + ", su próximo pago será el: " + IP.dateproximopago.Day.ToString("d") + "/" + mesprox + "/" + IP.dateproximopago.Year.ToString();
                        ICMH = null;
                        pagado = false;
                        AhoraBack = false;

                    }
                    if (IP.RecordPago == 0 && past == false && pagado == true)
                    {
                        //ICMH = casas.Casass2[posicionCasa].GetComponent<InfoCasaMH>();
                        //ICMH.ap = this;
                        //ICMH.Cat = IP.NombreCatPago;
                        //ICMH.idCat = IP.idCat;
                        //ICMH.idCol = IP.idColor;
                        //ICMH.idIcon = IP.idIcono;
                        //ICMH.idPagEn = idPagoEn;
                        //ICMH.idPagCon = idPagoCon;
                        //ICMH.idPagEnCol = idPagoEnColor;
                        //ICMH.idPagEnIcono = idPagoEnIcono;
                        //ICMH.MontoPago = IP.PrecioPago;
                        //ICMH.Pago();
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        MesHoy();

                        fechasHistorial = DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString();
                        pagar = true;
                        CadaCuando = IP.CadaCuando;
                        AlertaAntes = IP.AlertaAntes;
                        PrecioPago.text = IP.PrecioPago;
                        Categoria.text = IP.NombreCatPago;
                        idIcono = IP.idIcono;
                        idColor = IP.idColor;
                        NombreCategoriaString = IP.NombreCatPago;
                        mesesc = IP.MesNum;
                        añosc = IP.AñoNum;

                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        HistorialX();

                        //GuardarHistorial();

                        NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);

                        //AgregarHist();


                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }
                        ob.Click("Pagado");
                        if (IP.RecPago == 1)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + NombreCategoriaString + " asigando a la casa " + nombreCasaMenu +
                               " con un monto de: " + apagar.ToString("C", ci) + " con la fecha de " + DateTime.Now.Day.ToString("d") + "/" + meshoy + DateTime.Now.Year.ToString();
                            EnviarRecibo();
                        }
                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + NombreCategoriaString + ", a la casa: " + nombreCasaMenu + ", con un monto de " + apagar.ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString() + ", como es un pago del tipo eventual, no se agregara a su lista de pagos programados";
                        IP.Eliminar();

                        pagarahora = false;
                        pagardentro = false;
                        pagado = false;

                    }

                    if (past == true && pagado == true)
                    {
                        ////ICMH = casas.Casass2[posicionCasa].GetComponent<InfoCasaMH>();
                        ////ICMH.ap = this;
                        ////ICMH.Cat = IP.NombreCatPago;
                        ////ICMH.idCat = IP.idCat;
                        ////ICMH.idCol = IP.idColor;
                        ////ICMH.idIcon = IP.idIcono;
                        ////ICMH.idPagEn = idPagoEn;
                        ////ICMH.idPagCon = idPagoCon;
                        ////ICMH.idPagEnCol = idPagoEnColor;
                        ////ICMH.idPagEnIcono = idPagoEnIcono;
                        ////ICMH.MontoPago = IP.PrecioPago;
                        ////ICMH.Pago();
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        MesHoy();
                        //ids = IP.PagoID;
                        pagar = true;
                        CadaCuando = IP.CadaCuando;
                        AlertaAntes = IP.AlertaAntes;
                        PrecioPago.text = IP.PrecioPago;
                        Categoria.text = IP.NombreCatPago;
                        fechasHistorial = DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString();
                        idIcono = IP.idIcono;
                        idColor = IP.idColor;
                        NombreCategoriaString = IP.NombreCatPago;
                        mesesc = IP.MesNum;
                        añosc = IP.AñoNum;

                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        HistorialX();

                        //GuardarHistorial();

                        if (NombreCategoriaString != "")
                            NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);

                        //AgregarHist();

                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }
                        ob.Click("Pagado");
                        if (IP.RecPago == 1)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + NombreCategoriaString + " asigando a la casa " + IP.nombreCasa +
                               " con un monto de " + float.Parse(IP.PrecioPago).ToString("C", ci) + " con la fecha de " + DateTime.Now.Day.ToString("d") + "/" + meshoy + DateTime.Now.Year.ToString();
                            EnviarRecibo();
                        }
                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + IP.NombreCatPago + ", a la casa: " + IP.nombreCasa + ", con un monto de: " + float.Parse(IP.PrecioPago).ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString() + ", como es un pago vencido, se eliminará de su lista de pagos vencidos";
                        IP.EliminarPasado();
                        //EliminarServ();
                        //ObtenerPagos();
                        past = false;
                        pagado = false;
                        pagarahora = false;
                        pagardentro = false;
                        AhoraBack = false;

                    }
                }
                IP = null;
                Quitar();
                QuitarM();
                #endregion
                break;
            case "Sucursal":
                sameDate = false;

                #region Sucursal
                idPagoEn = 1;
                idPagoEnIcono = 1;
                idPagoEnColor = 1;
                pagadoen = "Sucursal Bancaria";
                ob.Click("OpcionesPagoEnQuitar");
                //ob.Click("CasaPagosHUB");
                pagarahora = true;
                pagado = true;
                if (RecordPago == 0 && pagardentro == true)
                {
                    if (RecordPago == 0 && pagoretrasado == true && pagado == true && pagarahora == true)
                    {
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        fechasHistorial = DiaP.text + "/" + MesP.text + "/" + AñoP.text;
                        MesesC();
                        añosc = Int32.Parse(AñoP.text);



                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        HistorialX();

                        //GuardarHistorial();

                        NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);

                        //AgregarHist();

                        //ICMH.ap = this;
                        //ICMH.Cat = NombreCategoriaString;
                        //ICMH.idCat = idCat;
                        //ICMH.idCol = idColor;
                        //ICMH.idIcon = idIcono;
                        //ICMH.idPagEn = idPagoEn;
                        //ICMH.idPagCon = idPagoCon;
                        //ICMH.idPagEnCol = idPagoEnColor;
                        //ICMH.idPagEnIcono = idPagoEnIcono;
                        //ICMH.MontoPago = PrecioPago.text;
                        //ICMH.Pago();

                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }
                        if (ReciboPago.isOn == true)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + NombreCategoriaString + " asigando a la casa " + ICa.NickNameC +
                               " con un monto de " + apagar.ToString("C", ci) + " con la fecha de " + DiaP.text + "/" + MesP.text + "/" + AñoP.text;
                            EnviarRecibo();
                        }
                        ob.Click("Pagado");

                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + NombreCategoriaString + ", a la casa: " + ICa.NickNameC + ", con un monto de: " + apagar.ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DiaP.text + "/" + MesP.text + "/" + AñoP.text + ", como es un pago vencido, no se agregara a su lista de pagos vencidos";
                        pagarahora = false;
                        pagardentro = false;

                        pagado = false;
                        AhoraBack = false;
                        ReciboPago.isOn = false;

                    }
                    if (RecordPago == 0 && pagoretrasado == false && pagado == true && pagarahora == true)
                    {
                        //ICMH = casas.Casass2[posicionCasa].GetComponent<InfoCasaMH>();
                        //ICMH.ap = this;
                        //ICMH.Cat = NombreCategoriaString;
                        //ICMH.idCat = idCat;
                        //ICMH.idCol = idColor;
                        //ICMH.idIcon = idIcono;
                        //ICMH.idPagEn = idPagoEn;
                        //ICMH.idPagCon = idPagoCon;
                        //ICMH.idPagEnCol = idPagoEnColor;
                        //ICMH.idPagEnIcono = idPagoEnIcono;
                        //ICMH.MontoPago = PrecioPago.text;
                        //ICMH.Pago();
                        pagado = false;
                        pagarahora = false;
                        pagardentro = false;
                        AhoraBack = false;
                        ReciboPago.isOn = false;
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        MesHoy();
                        fechasHistorial = DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString();
                        mesesc = DateTime.Now.Month;
                        añosc = DateTime.Now.Year;
                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        HistorialX();

                        //GuardarHistorial();

                        NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);

                        Eliminar();

                        //AgregarHist();


                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }
                        if (ReciboPago.isOn == true)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + NombreCategoriaString + " asigando a la casa " + ICa.NickNameC +
                               " con un monto de " + apagar.ToString("C", ci) + " con la fecha de " + DateTime.Now.Day.ToString("d") + "/" + meshoy + DateTime.Now.Year.ToString();
                            EnviarRecibo();
                        }
                        ob.Click("Pagado");
                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + NombreCategoriaString + ", a la casa: " + ICa.NickNameC + ", con un monto de: " + apagar.ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString() + ", como es un pago del tipo eventual, no se agregara a su lista de pagos programados";

                    }
                }
                if (RecordPago == 1 && pagardentro == true && pagoretrasado == false)
                {
                    if (RecordPago == 1 && pagado == true && pagarahora == true)
                    {
                        //ICMH = casas.Casass2[posicionCasa].GetComponent<InfoCasaMH>();
                        //ICMH.ap = this;
                        //ICMH.Cat = NombreCategoriaString;
                        //ICMH.idCat = idCat;
                        //ICMH.idCol = idColor;
                        //ICMH.idIcon = idIcono;
                        //ICMH.idPagEn = idPagoEn;
                        //ICMH.idPagCon = idPagoCon;
                        //ICMH.idPagEnCol = idPagoEnColor;
                        //ICMH.idPagEnIcono = idPagoEnIcono;
                        //ICMH.MontoPago = PrecioPago.text;
                        //ICMH.Pago();
                        pagado = false;
                        pagarahora = false;
                        pagardentro = false;
                        AhoraBack = false;
                        ReciboPago.isOn = false;
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        MesHoy();
                        for (int hello = 0; hello < Pagos.Count; hello++)
                        {
                            if (Pagos[hello].GetComponent<InfoPago>().PagoIDLocal == IdPagoLocalB)
                            {
                                IP = Pagos[hello].GetComponent<InfoPago>();
                            }
                        }
                        MesProx();
                        fechasHistorial = DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString();
                        fechaMod = IP.date.Day.ToString("d") + "/" + mesprox + "/" + IP.date.Year.ToString();
                        pagar = true;

                        CadaCuando = IP.CadaCuando;
                        PrecioPago.text = IP.PrecioPago;
                        AlertaAntes = IP.AlertaAntes;
                        idBen = IP.idBen;
                        NotaPago.text = IP.Nota;
                        idColor = IP.idColor;
                        idIcono = IP.idIcono;
                        RecPago = IP.RecPago;
                        RecordPago = IP.RecordPago;
                        if (IP.PagoID == 0)
                        {
                            for (int uh = 0; uh < Pagos.Count; uh++)
                            {
                                if (IP.PagoIDLocal == IdPagoLocalOffline[uh])
                                {
                                    fechaOffline[uh] = fechaMod;
                                }
                            }
                            NoEnviadoPagosAgregar = null;
                            for (int gii = 0; IdPagoLocalOffline.Count > gii; gii++)
                            {
                                if (gii == 0)
                                {
                                    NoEnviadoPagosAgregar = IdPagoLocalOffline[gii] + "," + NombreCategoriaOffline[gii] + "," + idColorsOffline[gii] + "," + idIconosOffline[gii] + "," + IdCasaOffline[gii] + "," +
                                        PrecioPagosOffline[gii] + "," + fechaOffline[gii] + "," + RecordatorioPagosOffline[gii] + "," + CadaCuandosOffline[gii] + "," + AlertasAntesOffline[gii] + "," + IdBeneficiarioOffline[gii] +
                                        "," + NotaOffline[gii] + "," + ReciboPagosOffline[gii] + "," + CodBarrasOffline[gii] + "," + nombreCasasOffline[gii];
                                }
                                else if (gii > 0)
                                {
                                    NoEnviadoPagosAgregar += ";" + IdPagoLocalOffline[gii] + "," + NombreCategoriaOffline[gii] + "," + idColorsOffline[gii] + "," + idIconosOffline[gii] + "," + IdCasaOffline[gii] + "," +
                                        PrecioPagosOffline[gii] + "," + fechaOffline[gii] + "," + RecordatorioPagosOffline[gii] + "," + CadaCuandosOffline[gii] + "," + AlertasAntesOffline[gii] + "," + IdBeneficiarioOffline[gii] +
                                        "," + NotaOffline[gii] + "," + ReciboPagosOffline[gii] + "," + CodBarrasOffline[gii] + "," + nombreCasasOffline[gii];
                                }
                            }
                            PlayerPrefs.SetString("PagosAgregar", NoEnviadoPagosAgregar);

                        }


                        if (IP.CodBarras != "")
                        {
                            codbar = Int32.Parse(IP.CodBarras);
                        }
                        ids = IP.PagoID;
                        IP2 = IP;
                        mesesc = DateTime.Now.Month;
                        añosc = DateTime.Now.Year;
                        Historial = true;
                        if (IP.RecPago == 1)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + IP.NombreCatPago + " asigando a la casa " + ICa.NickNameC + " con un monto de " + float.Parse(IP.PrecioPago).ToString("C", ci) + " con la fecha de " +
                                fechasHistorial + " y su próxima fecha de pago es el " + fechaMod;
                            EnviarRecibo();
                        }
                        //Modificar();

                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        HistorialOff = null;
                        //GuardarHistorial();


                        NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        //HistorialX();

                        AgregarHistorialNE = null;
                        //AgregarHist();

                        HistorialX();
                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }
                        ob.Click("Pagado");
                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + IP2.NombreCatPago + ", a la casa: " + ICa.NickNameC + ", con un monto de: " + float.Parse(IP2.PrecioPago).ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString() + ", su próximo pago será el: " + IP2.dateproximopago.Day.ToString("d") + "/" + mesprox + "/" + IP2.dateproximopago.Year.ToString();
                        ob.Click("OpcionesPagoEnQuitar");

                    }
                }
                if (pagarahora == true)
                {
                    if (AhoraBack == true && past == false)
                    {
                        for (int fy = 0; Pagos.Count > fy; fy++)
                        {
                            if (Pagos[fy].GetComponent<InfoPago>().PagoID == ids)
                            {
                                IP = Pagos[fy].GetComponent<InfoPago>();
                            }
                        }
                        //IP = Pagos[posicion].GetComponent<InfoPago>();
                    }
                    if (AhoraBack == true && past == true)
                    {
                        for (int fy = 0; PagosPasado.Count > fy; fy++)
                        {
                            if (PagosPasado[fy].GetComponent<InfoPago>().PagoID == ids)
                            {
                                IP = PagosPasado[fy].GetComponent<InfoPago>();
                            }
                        }
                        //IP = PagosPasado[posicion].GetComponent<InfoPago>();
                    }

                    if (IP.RecordPago == 1 && pagado == true && past == false)
                    {
                        //ICMH = casas.Casass2[posicionCasa].GetComponent<InfoCasaMH>();
                        //ICMH.ap = this;
                        //ICMH.Cat = IP.NombreCatPago;
                        //ICMH.idCat = IP.idCat;
                        //ICMH.idCol = IP.idColor;
                        //ICMH.idIcon = IP.idIcono;
                        //ICMH.idPagEn = idPagoEn;
                        //ICMH.idPagCon = idPagoCon;
                        //ICMH.idPagEnCol = idPagoEnColor;
                        //ICMH.idPagEnIcono = idPagoEnIcono;
                        //ICMH.MontoPago = IP.PrecioPago;
                        //ICMH.Pago();
                        IP.pagado = true;
                        pagarahora = false;
                        pagardentro = false;
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        MesHoy();

                        if (IP.dateproximopago.Month.ToString() == "1")
                            mesprox = "Enero";
                        if (IP.dateproximopago.Month.ToString() == "2")
                            mesprox = "Febrero";
                        if (IP.dateproximopago.Month.ToString() == "3")
                            mesprox = "Marzo";
                        if (IP.dateproximopago.Month.ToString() == "4")
                            mesprox = "Abril";
                        if (IP.dateproximopago.Month.ToString() == "5")
                            mesprox = "Mayo";
                        if (IP.dateproximopago.Month.ToString() == "6")
                            mesprox = "Junio";
                        if (IP.dateproximopago.Month.ToString() == "7")
                            mesprox = "Julio";
                        if (IP.dateproximopago.Month.ToString() == "8")
                            mesprox = "Agosto";
                        if (IP.dateproximopago.Month.ToString() == "9")
                            mesprox = "Septiembre";
                        if (IP.dateproximopago.Month.ToString() == "10")
                            mesprox = "Octubre";
                        if (IP.dateproximopago.Month.ToString() == "11")
                            mesprox = "Noviembre";
                        if (IP.dateproximopago.Month.ToString() == "12")
                            mesprox = "Diciembre";

                        fechasHistorial = DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString();
                        fechaMod = IP.dateproximopago.Day.ToString("d") + "/" + mesprox + "/" + IP.dateproximopago.Year.ToString();
                        ids = IP.PagoID;
                        pagar = true;
                        CadaCuando = IP.CadaCuando;
                        AlertaAntes = IP.AlertaAntes;
                        PrecioPago.text = IP.PrecioPago;
                        Categoria.text = IP.NombreCatPago;
                        idIcono = IP.idIcono;
                        idColor = IP.idColor;
                        NombreCategoriaString = IP.NombreCatPago;
                        mesesc = IP.MesNum;
                        añosc = IP.AñoNum;
                        Historial = true;


                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        HistorialX();
                        HistorialOff = null;
                        //GuardarHistorial();


                        NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);
                        AgregarHistorialNE = null;
                        //AgregarHist();


                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }


                        if (IP.RecPago == 1)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + IP.NombreCatPago + " asigando a la casa " + IP.nombreCasa + " con un monto de " + float.Parse(IP.PrecioPago).ToString("C", ci) + " con la fecha de " +
                                fechasHistorial + " y su próxima fecha de pago es el " + fechaMod;
                            EnviarRecibo();
                        }
                        ob.Click("Pagado");
                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + IP.NombreCatPago + ", a la casa: " + IP.nombreCasa + ", con un monto de: " + float.Parse(IP.PrecioPago).ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString() + ", su próximo pago será el: " + IP.dateproximopago.Day.ToString("d") + "/" + mesprox + "/" + IP.dateproximopago.Year.ToString();
                        ICMH = null;
                        pagado = false;
                        AhoraBack = false;

                    }
                    if (IP.RecordPago == 0 && past == false && pagado == true)
                    {
                        //ICMH = casas.Casass2[posicionCasa].GetComponent<InfoCasaMH>();
                        //ICMH.ap = this;
                        //ICMH.Cat = IP.NombreCatPago;
                        //ICMH.idCat = IP.idCat;
                        //ICMH.idCol = IP.idColor;
                        //ICMH.idIcon = IP.idIcono;
                        //ICMH.idPagEn = idPagoEn;
                        //ICMH.idPagCon = idPagoCon;
                        //ICMH.idPagEnCol = idPagoEnColor;
                        //ICMH.idPagEnIcono = idPagoEnIcono;
                        //ICMH.MontoPago = IP.PrecioPago;
                        //ICMH.Pago();
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        MesHoy();

                        fechasHistorial = DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString();
                        pagar = true;
                        CadaCuando = IP.CadaCuando;
                        AlertaAntes = IP.AlertaAntes;
                        PrecioPago.text = IP.PrecioPago;
                        Categoria.text = IP.NombreCatPago;
                        idIcono = IP.idIcono;
                        idColor = IP.idColor;
                        NombreCategoriaString = IP.NombreCatPago;
                        mesesc = IP.MesNum;
                        añosc = IP.AñoNum;

                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        HistorialX();
                        HistorialOff = null;
                        //GuardarHistorial();


                        NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);
                        AgregarHistorialNE = null;
                        //AgregarHist();



                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }
                        ob.Click("Pagado");
                        if (IP.RecPago == 1)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + NombreCategoriaString + " asigando a la casa " + nombreCasaMenu +
                               " con un monto de: " + apagar.ToString("C", ci) + " con la fecha de " + DateTime.Now.Day.ToString("d") + "/" + meshoy + DateTime.Now.Year.ToString();
                            EnviarRecibo();
                        }
                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + NombreCategoriaString + ", a la casa: " + nombreCasaMenu + ", con un monto de " + apagar.ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString() + ", como es un pago del tipo eventual, no se agregara a su lista de pagos programados";
                        IP.Eliminar();

                        pagarahora = false;
                        pagardentro = false;
                        pagado = false;

                    }

                    if (past == true && pagado == true)
                    {
                        ////ICMH = casas.Casass2[posicionCasa].GetComponent<InfoCasaMH>();
                        ////ICMH.ap = this;
                        ////ICMH.Cat = IP.NombreCatPago;
                        ////ICMH.idCat = IP.idCat;
                        ////ICMH.idCol = IP.idColor;
                        ////ICMH.idIcon = IP.idIcono;
                        ////ICMH.idPagEn = idPagoEn;
                        ////ICMH.idPagCon = idPagoCon;
                        ////ICMH.idPagEnCol = idPagoEnColor;
                        ////ICMH.idPagEnIcono = idPagoEnIcono;
                        ////ICMH.MontoPago = IP.PrecioPago;
                        ////ICMH.Pago();
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        MesHoy();
                        //ids = IP.PagoID;
                        pagar = true;
                        CadaCuando = IP.CadaCuando;
                        AlertaAntes = IP.AlertaAntes;
                        PrecioPago.text = IP.PrecioPago;
                        Categoria.text = IP.NombreCatPago;
                        fechasHistorial = DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString();
                        idIcono = IP.idIcono;
                        idColor = IP.idColor;
                        NombreCategoriaString = IP.NombreCatPago;
                        mesesc = IP.MesNum;
                        añosc = IP.AñoNum;

                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        HistorialX();
                        HistorialOff = null;
                        //GuardarHistorial();

                        if (NombreCategoriaString != "")
                            NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);
                        AgregarHistorialNE = null;
                        //AgregarHist();


                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }
                        ob.Click("Pagado");
                        if (IP.RecPago == 1)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + NombreCategoriaString + " asigando a la casa " + IP.nombreCasa +
                               " con un monto de " + float.Parse(IP.PrecioPago).ToString("C", ci) + " con la fecha de " + DateTime.Now.Day.ToString("d") + "/" + meshoy + DateTime.Now.Year.ToString();
                            EnviarRecibo();
                        }
                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + IP.NombreCatPago + ", a la casa: " + IP.nombreCasa + ", con un monto de: " + float.Parse(IP.PrecioPago).ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString() + ", como es un pago vencido, se eliminará de su lista de pagos vencidos";
                        IP.EliminarPasado();
                        //EliminarServ();
                        //ObtenerPagos();
                        past = false;
                        pagado = false;
                        pagarahora = false;
                        pagardentro = false;
                        AhoraBack = false;

                    }
                }
                IP = null;
                Quitar();
                QuitarM();
                #endregion
                break;
            case "Conveniencia":
                sameDate = false;

                #region convivencia
                idPagoEn = 2;
                idPagoEnIcono = 2;
                idPagoEnColor = 2;
                pagadoen = "Tienda de conveniencia";
                ob.Click("OpcionesPagoEnQuitar");
                //ob.Click("CasaPagosHUB");
                pagarahora = true;

                pagado = true;
                //print(RecordPago);
                //print(pagardentro);
                if (RecordPago == 0 && pagardentro == true)
                {
                    //print("1");
                    if (RecordPago == 0 && pagoretrasado == true && pagado == true && pagarahora == true)
                    {
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        fechasHistorial = DiaP.text + "/" + MesP.text + "/" + AñoP.text;
                        MesesC();
                        añosc = Int32.Parse(AñoP.text);



                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        HistorialX();
                        HistorialOff = null;
                        //GuardarHistorial();



                        NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);
                        AgregarHistorialNE = null;
                        //AgregarHist();

                        //ICMH.ap = this;
                        //ICMH.Cat = NombreCategoriaString;
                        //ICMH.idCat = idCat;
                        //ICMH.idCol = idColor;
                        //ICMH.idIcon = idIcono;
                        //ICMH.idPagEn = idPagoEn;
                        //ICMH.idPagCon = idPagoCon;
                        //ICMH.idPagEnCol = idPagoEnColor;
                        //ICMH.idPagEnIcono = idPagoEnIcono;
                        //ICMH.MontoPago = PrecioPago.text;
                        //ICMH.Pago();

                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }
                        if (ReciboPago.isOn == true)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + NombreCategoriaString + " asigando a la casa " + ICa.NickNameC +
                               " con un monto de " + apagar.ToString("C", ci) + " con la fecha de " + DiaP.text + "/" + MesP.text + "/" + AñoP.text;
                            EnviarRecibo();
                        }
                        ob.Click("Pagado");

                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + NombreCategoriaString + ", a la casa: " + ICa.NickNameC + ", con un monto de: " + apagar.ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DiaP.text + "/" + MesP.text + "/" + AñoP.text + ", como es un pago vencido, no se agregara a su lista de pagos vencidos";
                        pagarahora = false;
                        pagardentro = false;

                        pagado = false;
                        AhoraBack = false;
                        ReciboPago.isOn = false;

                    }
                    if (RecordPago == 0 && pagoretrasado == false && pagado == true && pagarahora == true)
                    {
                        //ICMH = casas.Casass2[posicionCasa].GetComponent<InfoCasaMH>();
                        //ICMH.ap = this;
                        //ICMH.Cat = NombreCategoriaString;
                        //ICMH.idCat = idCat;
                        //ICMH.idCol = idColor;
                        //ICMH.idIcon = idIcono;
                        //ICMH.idPagEn = idPagoEn;
                        //ICMH.idPagCon = idPagoCon;
                        //ICMH.idPagEnCol = idPagoEnColor;
                        //ICMH.idPagEnIcono = idPagoEnIcono;
                        //ICMH.MontoPago = PrecioPago.text;
                        //ICMH.Pago();
                        pagado = false;
                        pagarahora = false;
                        pagardentro = false;
                        AhoraBack = false;
                        ReciboPago.isOn = false;
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        MesHoy();
                        fechasHistorial = DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString();
                        mesesc = DateTime.Now.Month;
                        añosc = DateTime.Now.Year;
                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        HistorialX();
                        HistorialOff = null;
                        //GuardarHistorial();


                        NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);

                        Eliminar();

                        AgregarHistorialNE = null;
                        //AgregarHist();



                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }
                        if (ReciboPago.isOn == true)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + NombreCategoriaString + " asigando a la casa " + ICa.NickNameC +
                               " con un monto de " + apagar.ToString("C", ci) + " con la fecha de " + DateTime.Now.Day.ToString("d") + "/" + meshoy + DateTime.Now.Year.ToString();
                            EnviarRecibo();
                        }
                        ob.Click("Pagado");
                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + NombreCategoriaString + ", a la casa: " + ICa.NickNameC + ", con un monto de: " + apagar.ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString() + ", como es un pago del tipo eventual, no se agregara a su lista de pagos programados";

                    }
                }
                if (RecordPago == 1 && pagardentro == true && pagoretrasado == false)
                {
                    //print("2");
                    if (RecordPago == 1 && pagado == true && pagarahora == true)
                    {
                        //ICMH = casas.Casass2[posicionCasa].GetComponent<InfoCasaMH>();
                        //ICMH.ap = this;
                        //ICMH.Cat = NombreCategoriaString;
                        //ICMH.idCat = idCat;
                        //ICMH.idCol = idColor;
                        //ICMH.idIcon = idIcono;
                        //ICMH.idPagEn = idPagoEn;
                        //ICMH.idPagCon = idPagoCon;
                        //ICMH.idPagEnCol = idPagoEnColor;
                        //ICMH.idPagEnIcono = idPagoEnIcono;
                        //ICMH.MontoPago = PrecioPago.text;
                        //ICMH.Pago();
                        pagado = false;
                        pagarahora = false;
                        pagardentro = false;
                        AhoraBack = false;
                        ReciboPago.isOn = false;
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        MesHoy();
                        for (int hello = 0; hello < Pagos.Count; hello++)
                        {
                            if (Pagos[hello].GetComponent<InfoPago>().PagoIDLocal == IdPagoLocalB)
                            {
                                IP = Pagos[hello].GetComponent<InfoPago>();
                            }
                        }
                        MesProx();
                        fechasHistorial = DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString();
                        fechaMod = IP.date.Day.ToString("d") + "/" + mesprox + "/" + IP.date.Year.ToString();
                        pagar = true;

                        CadaCuando = IP.CadaCuando;
                        PrecioPago.text = IP.PrecioPago;
                        AlertaAntes = IP.AlertaAntes;
                        idBen = IP.idBen;
                        NotaPago.text = IP.Nota;
                        idColor = IP.idColor;
                        idIcono = IP.idIcono;
                        RecPago = IP.RecPago;
                        RecordPago = IP.RecordPago;
                        if (IP.PagoID == 0)
                        {
                            for (int uh = 0; uh < Pagos.Count; uh++)
                            {
                                if (IP.PagoIDLocal == IdPagoLocalOffline[uh])
                                {
                                    fechaOffline[uh] = fechaMod;
                                }
                            }
                            NoEnviadoPagosAgregar = null;
                            for (int gii = 0; IdPagoLocalOffline.Count > gii; gii++)
                            {
                                if (gii == 0)
                                {
                                    NoEnviadoPagosAgregar = IdPagoLocalOffline[gii] + "," + NombreCategoriaOffline[gii] + "," + idColorsOffline[gii] + "," + idIconosOffline[gii] + "," + IdCasaOffline[gii] + "," +
                                        PrecioPagosOffline[gii] + "," + fechaOffline[gii] + "," + RecordatorioPagosOffline[gii] + "," + CadaCuandosOffline[gii] + "," + AlertasAntesOffline[gii] + "," + IdBeneficiarioOffline[gii] +
                                        "," + NotaOffline[gii] + "," + ReciboPagosOffline[gii] + "," + CodBarrasOffline[gii] + "," + nombreCasasOffline[gii];
                                }
                                else if (gii > 0)
                                {
                                    NoEnviadoPagosAgregar += ";" + IdPagoLocalOffline[gii] + "," + NombreCategoriaOffline[gii] + "," + idColorsOffline[gii] + "," + idIconosOffline[gii] + "," + IdCasaOffline[gii] + "," +
                                        PrecioPagosOffline[gii] + "," + fechaOffline[gii] + "," + RecordatorioPagosOffline[gii] + "," + CadaCuandosOffline[gii] + "," + AlertasAntesOffline[gii] + "," + IdBeneficiarioOffline[gii] +
                                        "," + NotaOffline[gii] + "," + ReciboPagosOffline[gii] + "," + CodBarrasOffline[gii] + "," + nombreCasasOffline[gii];
                                }
                            }
                            PlayerPrefs.SetString("PagosAgregar", NoEnviadoPagosAgregar);

                        }


                        if (IP.CodBarras != "")
                        {
                            codbar = Int32.Parse(IP.CodBarras);
                        }
                        ids = IP.PagoID;
                        IP2 = IP;
                        mesesc = DateTime.Now.Month;
                        añosc = DateTime.Now.Year;
                        Historial = true;
                        if (IP.RecPago == 1)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + IP.NombreCatPago + " asigando a la casa " + ICa.NickNameC + " con un monto de " + float.Parse(IP.PrecioPago).ToString("C", ci) + " con la fecha de " +
                                fechasHistorial + " y su próxima fecha de pago es el " + fechaMod;
                            EnviarRecibo();
                        }
                        //Modificar();

                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        HistorialOff = null;
                        //GuardarHistorial();


                        NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        //HistorialX();

                        AgregarHistorialNE = null;
                        //AgregarHist();

                        HistorialX();
                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }
                        ob.Click("Pagado");
                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + IP2.NombreCatPago + ", a la casa: " + ICa.NickNameC + ", con un monto de: " + float.Parse(IP2.PrecioPago).ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString() + ", su próximo pago será el: " + IP2.dateproximopago.Day.ToString("d") + "/" + mesprox + "/" + IP2.dateproximopago.Year.ToString();
                        ob.Click("OpcionesPagoEnQuitar");

                    }
                }
                if (pagarahora == true)
                {
                    //print("3");
                    if (AhoraBack == true && past == false)
                    {
                        for (int fy = 0; Pagos.Count > fy; fy++)
                        {
                            if (Pagos[fy].GetComponent<InfoPago>().PagoID == ids)
                            {
                                //print("yes");
                                IP = Pagos[fy].GetComponent<InfoPago>();
                            }
                        }
                        //IP = Pagos[posicion].GetComponent<InfoPago>();
                    }
                    if (AhoraBack == true && past == true)
                    {
                        for (int fy = 0; PagosPasado.Count > fy; fy++)
                        {
                            if (PagosPasado[fy].GetComponent<InfoPago>().PagoID == ids)
                            {
                                //print("ah yes");

                                IP = PagosPasado[fy].GetComponent<InfoPago>();
                            }
                        }
                        //IP = PagosPasado[posicion].GetComponent<InfoPago>();
                    }

                    //print("si: " + IP.RecordPago);
                    

                    if (IP.RecordPago == 1 
                        && pagado == true 
                        && past == false)
                    {
                        //ICMH = casas.Casass2[posicionCasa].GetComponent<InfoCasaMH>();
                        //ICMH.ap = this;
                        //ICMH.Cat = IP.NombreCatPago;
                        //ICMH.idCat = IP.idCat;
                        //ICMH.idCol = IP.idColor;
                        //ICMH.idIcon = IP.idIcono;
                        //ICMH.idPagEn = idPagoEn;
                        //ICMH.idPagCon = idPagoCon;
                        //ICMH.idPagEnCol = idPagoEnColor;
                        //ICMH.idPagEnIcono = idPagoEnIcono;
                        //ICMH.MontoPago = IP.PrecioPago;
                        //ICMH.Pago();
                        IP.pagado = true;
                        pagarahora = false;
                        pagardentro = false;
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        MesHoy();

                        if (IP.dateproximopago.Month.ToString() == "1")
                            mesprox = "Enero";
                        if (IP.dateproximopago.Month.ToString() == "2")
                            mesprox = "Febrero";
                        if (IP.dateproximopago.Month.ToString() == "3")
                            mesprox = "Marzo";
                        if (IP.dateproximopago.Month.ToString() == "4")
                            mesprox = "Abril";
                        if (IP.dateproximopago.Month.ToString() == "5")
                            mesprox = "Mayo";
                        if (IP.dateproximopago.Month.ToString() == "6")
                            mesprox = "Junio";
                        if (IP.dateproximopago.Month.ToString() == "7")
                            mesprox = "Julio";
                        if (IP.dateproximopago.Month.ToString() == "8")
                            mesprox = "Agosto";
                        if (IP.dateproximopago.Month.ToString() == "9")
                            mesprox = "Septiembre";
                        if (IP.dateproximopago.Month.ToString() == "10")
                            mesprox = "Octubre";
                        if (IP.dateproximopago.Month.ToString() == "11")
                            mesprox = "Noviembre";
                        if (IP.dateproximopago.Month.ToString() == "12")
                            mesprox = "Diciembre";

                        fechasHistorial = DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString();
                        fechaMod = IP.dateproximopago.Day.ToString("d") + "/" + mesprox + "/" + IP.dateproximopago.Year.ToString();
                        ids = IP.PagoID;
                        pagar = true;
                        CadaCuando = IP.CadaCuando;
                        AlertaAntes = IP.AlertaAntes;
                        PrecioPago.text = IP.PrecioPago;
                        Categoria.text = IP.NombreCatPago;
                        idIcono = IP.idIcono;
                        idColor = IP.idColor;
                        NombreCategoriaString = IP.NombreCatPago;
                        mesesc = IP.MesNum;
                        añosc = IP.AñoNum;
                        Historial = true;


                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        HistorialX();
                        HistorialOff = null;
                        //GuardarHistorial();


                        NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);
                        AgregarHistorialNE = null;
                        //AgregarHist();


                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }


                        if (IP.RecPago == 1)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + IP.NombreCatPago + " asigando a la casa " + IP.nombreCasa + " con un monto de " + float.Parse(IP.PrecioPago).ToString("C", ci) + " con la fecha de " +
                                fechasHistorial + " y su próxima fecha de pago es el " + fechaMod;
                            EnviarRecibo();
                        }
                        ob.Click("Pagado");
                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + IP.NombreCatPago + ", a la casa: " + IP.nombreCasa + ", con un monto de: " + float.Parse(IP.PrecioPago).ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString() + ", su próximo pago será el: " + IP.dateproximopago.Day.ToString("d") + "/" + mesprox + "/" + IP.dateproximopago.Year.ToString();
                        ICMH = null;
                        pagado = false;
                        AhoraBack = false;

                    }
                    if (IP.RecordPago == 0 && past == false && pagado == true)
                    {
                        //ICMH = casas.Casass2[posicionCasa].GetComponent<InfoCasaMH>();
                        //ICMH.ap = this;
                        //ICMH.Cat = IP.NombreCatPago;
                        //ICMH.idCat = IP.idCat;
                        //ICMH.idCol = IP.idColor;
                        //ICMH.idIcon = IP.idIcono;
                        //ICMH.idPagEn = idPagoEn;
                        //ICMH.idPagCon = idPagoCon;
                        //ICMH.idPagEnCol = idPagoEnColor;
                        //ICMH.idPagEnIcono = idPagoEnIcono;
                        //ICMH.MontoPago = IP.PrecioPago;
                        //ICMH.Pago();
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        MesHoy();

                        fechasHistorial = DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString();
                        pagar = true;
                        CadaCuando = IP.CadaCuando;
                        AlertaAntes = IP.AlertaAntes;
                        PrecioPago.text = IP.PrecioPago;
                        Categoria.text = IP.NombreCatPago;
                        idIcono = IP.idIcono;
                        idColor = IP.idColor;
                        NombreCategoriaString = IP.NombreCatPago;
                        mesesc = IP.MesNum;
                        añosc = IP.AñoNum;

                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        HistorialX();
                        HistorialOff = null;
                        //GuardarHistorial();


                        NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);
                        AgregarHistorialNE = null;
                        //AgregarHist();



                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }
                        ob.Click("Pagado");
                        if (IP.RecPago == 1)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + NombreCategoriaString + " asigando a la casa " + nombreCasaMenu +
                               " con un monto de: " + apagar.ToString("C", ci) + " con la fecha de " + DateTime.Now.Day.ToString("d") + "/" + meshoy + DateTime.Now.Year.ToString();
                            EnviarRecibo();
                        }
                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + NombreCategoriaString + ", a la casa: " + nombreCasaMenu + ", con un monto de " + apagar.ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString() + ", como es un pago del tipo eventual, no se agregara a su lista de pagos programados";
                        IP.Eliminar();

                        pagarahora = false;
                        pagardentro = false;
                        pagado = false;

                    }

                    if (past == true && pagado == true)
                    {
                        ////ICMH = casas.Casass2[posicionCasa].GetComponent<InfoCasaMH>();
                        ////ICMH.ap = this;
                        ////ICMH.Cat = IP.NombreCatPago;
                        ////ICMH.idCat = IP.idCat;
                        ////ICMH.idCol = IP.idColor;
                        ////ICMH.idIcon = IP.idIcono;
                        ////ICMH.idPagEn = idPagoEn;
                        ////ICMH.idPagCon = idPagoCon;
                        ////ICMH.idPagEnCol = idPagoEnColor;
                        ////ICMH.idPagEnIcono = idPagoEnIcono;
                        ////ICMH.MontoPago = IP.PrecioPago;
                        ////ICMH.Pago();
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        MesHoy();
                        //ids = IP.PagoID;
                        pagar = true;
                        CadaCuando = IP.CadaCuando;
                        AlertaAntes = IP.AlertaAntes;
                        PrecioPago.text = IP.PrecioPago;
                        Categoria.text = IP.NombreCatPago;
                        fechasHistorial = DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString();
                        idIcono = IP.idIcono;
                        idColor = IP.idColor;
                        NombreCategoriaString = IP.NombreCatPago;
                        mesesc = IP.MesNum;
                        añosc = IP.AñoNum;

                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        HistorialX();
                        HistorialOff = null;
                        //GuardarHistorial();

                        if (NombreCategoriaString != "")
                            NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);
                        AgregarHistorialNE = null;
                        //AgregarHist();


                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }
                        ob.Click("Pagado");
                        if (IP.RecPago == 1)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + NombreCategoriaString + " asigando a la casa " + IP.nombreCasa +
                               " con un monto de " + float.Parse(IP.PrecioPago).ToString("C", ci) + " con la fecha de " + DateTime.Now.Day.ToString("d") + "/" + meshoy + DateTime.Now.Year.ToString();
                            EnviarRecibo();
                        }
                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + IP.NombreCatPago + ", a la casa: " + IP.nombreCasa + ", con un monto de: " + float.Parse(IP.PrecioPago).ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString() + ", como es un pago vencido, se eliminará de su lista de pagos vencidos";
                        IP.EliminarPasado();
                        //EliminarServ();
                        //ObtenerPagos();
                        past = false;
                        pagado = false;
                        pagarahora = false;
                        pagardentro = false;
                        AhoraBack = false;

                    }
                }
                IP = null;
                Quitar();
                QuitarM();
                #endregion
                break;
            case "Autoservicio":
                sameDate = false;

                #region Autoservicio
                idPagoEn = 3;
                idPagoEnIcono = 3;
                idPagoEnColor = 3;
                pagadoen = "Tienda de autoservicio";
                ob.Click("OpcionesPagoEnQuitar");
                //ob.Click("CasaPagosHUB");
                pagarahora = true;
                pagado = true;
                if (RecordPago == 0 && pagardentro == true)
                {
                    if (RecordPago == 0 && pagoretrasado == true && pagado == true && pagarahora == true)
                    {
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        fechasHistorial = DiaP.text + "/" + MesP.text + "/" + AñoP.text;
                        MesesC();
                        añosc = Int32.Parse(AñoP.text);



                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        HistorialX();
                        HistorialOff = null;
                        //GuardarHistorial();


                        NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);
                        AgregarHistorialNE = null;
                        //AgregarHist();

                        //ICMH.ap = this;
                        //ICMH.Cat = NombreCategoriaString;
                        //ICMH.idCat = idCat;
                        //ICMH.idCol = idColor;
                        //ICMH.idIcon = idIcono;
                        //ICMH.idPagEn = idPagoEn;
                        //ICMH.idPagCon = idPagoCon;
                        //ICMH.idPagEnCol = idPagoEnColor;
                        //ICMH.idPagEnIcono = idPagoEnIcono;
                        //ICMH.MontoPago = PrecioPago.text;
                        //ICMH.Pago();

                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }
                        if (ReciboPago.isOn == true)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + NombreCategoriaString + " asigando a la casa " + ICa.NickNameC +
                               " con un monto de " + apagar.ToString("C", ci) + " con la fecha de " + DiaP.text + "/" + MesP.text + "/" + AñoP.text;
                            EnviarRecibo();
                        }
                        ob.Click("Pagado");

                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + NombreCategoriaString + ", a la casa: " + ICa.NickNameC + ", con un monto de: " + apagar.ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DiaP.text + "/" + MesP.text + "/" + AñoP.text + ", como es un pago vencido, no se agregara a su lista de pagos vencidos";
                        pagarahora = false;
                        pagardentro = false;

                        pagado = false;
                        AhoraBack = false;
                        ReciboPago.isOn = false;

                    }
                    if (RecordPago == 0 && pagoretrasado == false && pagado == true && pagarahora == true)
                    {
                        //ICMH = casas.Casass2[posicionCasa].GetComponent<InfoCasaMH>();
                        //ICMH.ap = this;
                        //ICMH.Cat = NombreCategoriaString;
                        //ICMH.idCat = idCat;
                        //ICMH.idCol = idColor;
                        //ICMH.idIcon = idIcono;
                        //ICMH.idPagEn = idPagoEn;
                        //ICMH.idPagCon = idPagoCon;
                        //ICMH.idPagEnCol = idPagoEnColor;
                        //ICMH.idPagEnIcono = idPagoEnIcono;
                        //ICMH.MontoPago = PrecioPago.text;
                        //ICMH.Pago();
                        pagado = false;
                        pagarahora = false;
                        pagardentro = false;
                        AhoraBack = false;
                        ReciboPago.isOn = false;
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        MesHoy();
                        fechasHistorial = DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString();
                        mesesc = DateTime.Now.Month;
                        añosc = DateTime.Now.Year;
                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        HistorialX();
                        HistorialOff = null;
                        //GuardarHistorial();


                        NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);

                        Eliminar();

                        AgregarHistorialNE = null;
                        //AgregarHist();



                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }
                        if (ReciboPago.isOn == true)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + NombreCategoriaString + " asigando a la casa " + ICa.NickNameC +
                               " con un monto de " + apagar.ToString("C", ci) + " con la fecha de " + DateTime.Now.Day.ToString("d") + "/" + meshoy + DateTime.Now.Year.ToString();
                            EnviarRecibo();
                        }
                        ob.Click("Pagado");
                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + NombreCategoriaString + ", a la casa: " + ICa.NickNameC + ", con un monto de: " + apagar.ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString() + ", como es un pago del tipo eventual, no se agregara a su lista de pagos programados";

                    }
                }
                if (RecordPago == 1 && pagardentro == true && pagoretrasado == false)
                {
                    if (RecordPago == 1 && pagado == true && pagarahora == true)
                    {
                        //ICMH = casas.Casass2[posicionCasa].GetComponent<InfoCasaMH>();
                        //ICMH.ap = this;
                        //ICMH.Cat = NombreCategoriaString;
                        //ICMH.idCat = idCat;
                        //ICMH.idCol = idColor;
                        //ICMH.idIcon = idIcono;
                        //ICMH.idPagEn = idPagoEn;
                        //ICMH.idPagCon = idPagoCon;
                        //ICMH.idPagEnCol = idPagoEnColor;
                        //ICMH.idPagEnIcono = idPagoEnIcono;
                        //ICMH.MontoPago = PrecioPago.text;
                        //ICMH.Pago();
                        pagado = false;
                        pagarahora = false;
                        pagardentro = false;
                        AhoraBack = false;
                        ReciboPago.isOn = false;
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        MesHoy();
                        for (int hello = 0; hello < Pagos.Count; hello++)
                        {
                            if (Pagos[hello].GetComponent<InfoPago>().PagoIDLocal == IdPagoLocalB)
                            {
                                IP = Pagos[hello].GetComponent<InfoPago>();
                            }
                        }
                        MesProx();
                        fechasHistorial = DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString();
                        fechaMod = IP.date.Day.ToString("d") + "/" + mesprox + "/" + IP.date.Year.ToString();
                        pagar = true;

                        CadaCuando = IP.CadaCuando;
                        PrecioPago.text = IP.PrecioPago;
                        AlertaAntes = IP.AlertaAntes;
                        idBen = IP.idBen;
                        NotaPago.text = IP.Nota;
                        idColor = IP.idColor;
                        idIcono = IP.idIcono;
                        RecPago = IP.RecPago;
                        RecordPago = IP.RecordPago;
                        if (IP.PagoID == 0)
                        {
                            for (int uh = 0; uh < Pagos.Count; uh++)
                            {
                                if (IP.PagoIDLocal == IdPagoLocalOffline[uh])
                                {
                                    fechaOffline[uh] = fechaMod;
                                }
                            }
                            NoEnviadoPagosAgregar = null;
                            for (int gii = 0; IdPagoLocalOffline.Count > gii; gii++)
                            {
                                if (gii == 0)
                                {
                                    NoEnviadoPagosAgregar = IdPagoLocalOffline[gii] + "," + NombreCategoriaOffline[gii] + "," + idColorsOffline[gii] + "," + idIconosOffline[gii] + "," + IdCasaOffline[gii] + "," +
                                        PrecioPagosOffline[gii] + "," + fechaOffline[gii] + "," + RecordatorioPagosOffline[gii] + "," + CadaCuandosOffline[gii] + "," + AlertasAntesOffline[gii] + "," + IdBeneficiarioOffline[gii] +
                                        "," + NotaOffline[gii] + "," + ReciboPagosOffline[gii] + "," + CodBarrasOffline[gii] + "," + nombreCasasOffline[gii];
                                }
                                else if (gii > 0)
                                {
                                    NoEnviadoPagosAgregar += ";" + IdPagoLocalOffline[gii] + "," + NombreCategoriaOffline[gii] + "," + idColorsOffline[gii] + "," + idIconosOffline[gii] + "," + IdCasaOffline[gii] + "," +
                                        PrecioPagosOffline[gii] + "," + fechaOffline[gii] + "," + RecordatorioPagosOffline[gii] + "," + CadaCuandosOffline[gii] + "," + AlertasAntesOffline[gii] + "," + IdBeneficiarioOffline[gii] +
                                        "," + NotaOffline[gii] + "," + ReciboPagosOffline[gii] + "," + CodBarrasOffline[gii] + "," + nombreCasasOffline[gii];
                                }
                            }
                            PlayerPrefs.SetString("PagosAgregar", NoEnviadoPagosAgregar);

                        }


                        if (IP.CodBarras != "")
                        {
                            codbar = Int32.Parse(IP.CodBarras);
                        }
                        ids = IP.PagoID;
                        IP2 = IP;
                        mesesc = DateTime.Now.Month;
                        añosc = DateTime.Now.Year;
                        Historial = true;
                        if (IP.RecPago == 1)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + IP.NombreCatPago + " asigando a la casa " + ICa.NickNameC + " con un monto de " + float.Parse(IP.PrecioPago).ToString("C", ci) + " con la fecha de " +
                                fechasHistorial + " y su próxima fecha de pago es el " + fechaMod;
                            EnviarRecibo();
                        }
                        //Modificar();

                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        HistorialOff = null;
                        //GuardarHistorial();


                        NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        //HistorialX();

                        AgregarHistorialNE = null;
                        //AgregarHist();

                        HistorialX();
                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }
                        ob.Click("Pagado");
                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + IP2.NombreCatPago + ", a la casa: " + ICa.NickNameC + ", con un monto de: " + float.Parse(IP2.PrecioPago).ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString() + ", su próximo pago será el: " + IP2.dateproximopago.Day.ToString("d") + "/" + mesprox + "/" + IP2.dateproximopago.Year.ToString();
                        ob.Click("OpcionesPagoEnQuitar");

                    }
                }
                if (pagarahora == true)
                {
                    if (AhoraBack == true && past == false)
                    {
                        for (int fy = 0; Pagos.Count > fy; fy++)
                        {
                            if (Pagos[fy].GetComponent<InfoPago>().PagoID == ids)
                            {
                                IP = Pagos[fy].GetComponent<InfoPago>();
                            }
                        }
                        //IP = Pagos[posicion].GetComponent<InfoPago>();
                    }
                    if (AhoraBack == true && past == true)
                    {
                        for (int fy = 0; PagosPasado.Count > fy; fy++)
                        {
                            if (PagosPasado[fy].GetComponent<InfoPago>().PagoID == ids)
                            {
                                IP = PagosPasado[fy].GetComponent<InfoPago>();
                            }
                        }
                        //IP = PagosPasado[posicion].GetComponent<InfoPago>();
                    }

                    if (IP.RecordPago == 1 && pagado == true && past == false)
                    {
                        //ICMH = casas.Casass2[posicionCasa].GetComponent<InfoCasaMH>();
                        //ICMH.ap = this;
                        //ICMH.Cat = IP.NombreCatPago;
                        //ICMH.idCat = IP.idCat;
                        //ICMH.idCol = IP.idColor;
                        //ICMH.idIcon = IP.idIcono;
                        //ICMH.idPagEn = idPagoEn;
                        //ICMH.idPagCon = idPagoCon;
                        //ICMH.idPagEnCol = idPagoEnColor;
                        //ICMH.idPagEnIcono = idPagoEnIcono;
                        //ICMH.MontoPago = IP.PrecioPago;
                        //ICMH.Pago();
                        IP.pagado = true;
                        pagarahora = false;
                        pagardentro = false;
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        MesHoy();

                        if (IP.dateproximopago.Month.ToString() == "1")
                            mesprox = "Enero";
                        if (IP.dateproximopago.Month.ToString() == "2")
                            mesprox = "Febrero";
                        if (IP.dateproximopago.Month.ToString() == "3")
                            mesprox = "Marzo";
                        if (IP.dateproximopago.Month.ToString() == "4")
                            mesprox = "Abril";
                        if (IP.dateproximopago.Month.ToString() == "5")
                            mesprox = "Mayo";
                        if (IP.dateproximopago.Month.ToString() == "6")
                            mesprox = "Junio";
                        if (IP.dateproximopago.Month.ToString() == "7")
                            mesprox = "Julio";
                        if (IP.dateproximopago.Month.ToString() == "8")
                            mesprox = "Agosto";
                        if (IP.dateproximopago.Month.ToString() == "9")
                            mesprox = "Septiembre";
                        if (IP.dateproximopago.Month.ToString() == "10")
                            mesprox = "Octubre";
                        if (IP.dateproximopago.Month.ToString() == "11")
                            mesprox = "Noviembre";
                        if (IP.dateproximopago.Month.ToString() == "12")
                            mesprox = "Diciembre";

                        fechasHistorial = DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString();
                        fechaMod = IP.dateproximopago.Day.ToString("d") + "/" + mesprox + "/" + IP.dateproximopago.Year.ToString();
                        ids = IP.PagoID;
                        pagar = true;
                        CadaCuando = IP.CadaCuando;
                        AlertaAntes = IP.AlertaAntes;
                        PrecioPago.text = IP.PrecioPago;
                        Categoria.text = IP.NombreCatPago;
                        idIcono = IP.idIcono;
                        idColor = IP.idColor;
                        NombreCategoriaString = IP.NombreCatPago;
                        mesesc = IP.MesNum;
                        añosc = IP.AñoNum;
                        Historial = true;


                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        HistorialX();
                        HistorialOff = null;
                        //GuardarHistorial();


                        NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);
                        AgregarHistorialNE = null;
                        //AgregarHist();


                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }


                        if (IP.RecPago == 1)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + IP.NombreCatPago + " asigando a la casa " + IP.nombreCasa + " con un monto de " + float.Parse(IP.PrecioPago).ToString("C", ci) + " con la fecha de " +
                                fechasHistorial + " y su próxima fecha de pago es el " + fechaMod;
                            EnviarRecibo();
                        }
                        ob.Click("Pagado");
                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + IP.NombreCatPago + ", a la casa: " + IP.nombreCasa + ", con un monto de: " + float.Parse(IP.PrecioPago).ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString() + ", su próximo pago será el: " + IP.dateproximopago.Day.ToString("d") + "/" + mesprox + "/" + IP.dateproximopago.Year.ToString();
                        ICMH = null;
                        pagado = false;
                        AhoraBack = false;

                    }
                    if (IP.RecordPago == 0 && past == false && pagado == true)
                    {
                        //ICMH = casas.Casass2[posicionCasa].GetComponent<InfoCasaMH>();
                        //ICMH.ap = this;
                        //ICMH.Cat = IP.NombreCatPago;
                        //ICMH.idCat = IP.idCat;
                        //ICMH.idCol = IP.idColor;
                        //ICMH.idIcon = IP.idIcono;
                        //ICMH.idPagEn = idPagoEn;
                        //ICMH.idPagCon = idPagoCon;
                        //ICMH.idPagEnCol = idPagoEnColor;
                        //ICMH.idPagEnIcono = idPagoEnIcono;
                        //ICMH.MontoPago = IP.PrecioPago;
                        //ICMH.Pago();
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        MesHoy();

                        fechasHistorial = DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString();
                        pagar = true;
                        CadaCuando = IP.CadaCuando;
                        AlertaAntes = IP.AlertaAntes;
                        PrecioPago.text = IP.PrecioPago;
                        Categoria.text = IP.NombreCatPago;
                        idIcono = IP.idIcono;
                        idColor = IP.idColor;
                        NombreCategoriaString = IP.NombreCatPago;
                        mesesc = IP.MesNum;
                        añosc = IP.AñoNum;

                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        HistorialX();
                        HistorialOff = null;
                        //GuardarHistorial();


                        NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);
                        AgregarHistorialNE = null;
                        //AgregarHist();


                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }
                        ob.Click("Pagado");
                        if (IP.RecPago == 1)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + NombreCategoriaString + " asigando a la casa " + nombreCasaMenu +
                               " con un monto de: " + apagar.ToString("C", ci) + " con la fecha de " + DateTime.Now.Day.ToString("d") + "/" + meshoy + DateTime.Now.Year.ToString();
                            EnviarRecibo();
                        }
                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + NombreCategoriaString + ", a la casa: " + nombreCasaMenu + ", con un monto de " + apagar.ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString() + ", como es un pago del tipo eventual, no se agregara a su lista de pagos programados";
                        IP.Eliminar();

                        pagarahora = false;
                        pagardentro = false;
                        pagado = false;

                    }

                    if (past == true && pagado == true)
                    {
                        ////ICMH = casas.Casass2[posicionCasa].GetComponent<InfoCasaMH>();
                        ////ICMH.ap = this;
                        ////ICMH.Cat = IP.NombreCatPago;
                        ////ICMH.idCat = IP.idCat;
                        ////ICMH.idCol = IP.idColor;
                        ////ICMH.idIcon = IP.idIcono;
                        ////ICMH.idPagEn = idPagoEn;
                        ////ICMH.idPagCon = idPagoCon;
                        ////ICMH.idPagEnCol = idPagoEnColor;
                        ////ICMH.idPagEnIcono = idPagoEnIcono;
                        ////ICMH.MontoPago = IP.PrecioPago;
                        ////ICMH.Pago();
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        MesHoy();
                        //ids = IP.PagoID;
                        pagar = true;
                        CadaCuando = IP.CadaCuando;
                        AlertaAntes = IP.AlertaAntes;
                        PrecioPago.text = IP.PrecioPago;
                        Categoria.text = IP.NombreCatPago;
                        fechasHistorial = DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString();
                        idIcono = IP.idIcono;
                        idColor = IP.idColor;
                        NombreCategoriaString = IP.NombreCatPago;
                        mesesc = IP.MesNum;
                        añosc = IP.AñoNum;

                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        HistorialX();
                        HistorialOff = null;
                        //GuardarHistorial();

                        if (NombreCategoriaString != "")
                            NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);
                        AgregarHistorialNE = null;
                        //AgregarHist();


                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }
                        ob.Click("Pagado");
                        if (IP.RecPago == 1)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + NombreCategoriaString + " asigando a la casa " + IP.nombreCasa +
                               " con un monto de " + float.Parse(IP.PrecioPago).ToString("C", ci) + " con la fecha de " + DateTime.Now.Day.ToString("d") + "/" + meshoy + DateTime.Now.Year.ToString();
                            EnviarRecibo();
                        }
                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + IP.NombreCatPago + ", a la casa: " + IP.nombreCasa + ", con un monto de: " + float.Parse(IP.PrecioPago).ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString() + ", como es un pago vencido, se eliminará de su lista de pagos vencidos";
                        IP.EliminarPasado();
                        //EliminarServ();
                        //ObtenerPagos();
                        past = false;
                        pagado = false;
                        pagarahora = false;
                        pagardentro = false;
                        AhoraBack = false;

                    }
                }
                IP = null;
                Quitar();
                QuitarM();
                #endregion
                break;
            case "Farmacia":
                sameDate = false;

                #region Farmacia
                idPagoEn = 4;
                idPagoEnIcono = 4;
                idPagoEnColor = 4;
                pagadoen = "Farmacia";
                ob.Click("OpcionesPagoEnQuitar");
                //ob.Click("CasaPagosHUB");
                pagarahora = true;
                pagado = true;
                if (RecordPago == 0 && pagardentro == true)
                {
                    if (RecordPago == 0 && pagoretrasado == true && pagado == true && pagarahora == true)
                    {
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        fechasHistorial = DiaP.text + "/" + MesP.text + "/" + AñoP.text;
                        MesesC();
                        añosc = Int32.Parse(AñoP.text);



                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        HistorialX();
                        HistorialOff = null;
                        //GuardarHistorial();


                        NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);
                        AgregarHistorialNE = null;
                        //AgregarHist();

                        //ICMH.ap = this;
                        //ICMH.Cat = NombreCategoriaString;
                        //ICMH.idCat = idCat;
                        //ICMH.idCol = idColor;
                        //ICMH.idIcon = idIcono;
                        //ICMH.idPagEn = idPagoEn;
                        //ICMH.idPagCon = idPagoCon;
                        //ICMH.idPagEnCol = idPagoEnColor;
                        //ICMH.idPagEnIcono = idPagoEnIcono;
                        //ICMH.MontoPago = PrecioPago.text;
                        //ICMH.Pago();

                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }
                        if (ReciboPago.isOn == true)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + NombreCategoriaString + " asigando a la casa " + ICa.NickNameC +
                               " con un monto de " + apagar.ToString("C", ci) + " con la fecha de " + DiaP.text + "/" + MesP.text + "/" + AñoP.text;
                            EnviarRecibo();
                        }
                        ob.Click("Pagado");

                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + NombreCategoriaString + ", a la casa: " + ICa.NickNameC + ", con un monto de: " + apagar.ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DiaP.text + "/" + MesP.text + "/" + AñoP.text + ", como es un pago vencido, no se agregara a su lista de pagos vencidos";
                        pagarahora = false;
                        pagardentro = false;

                        pagado = false;
                        AhoraBack = false;
                        ReciboPago.isOn = false;

                    }
                    if (RecordPago == 0 && pagoretrasado == false && pagado == true && pagarahora == true)
                    {
                        //ICMH = casas.Casass2[posicionCasa].GetComponent<InfoCasaMH>();
                        //ICMH.ap = this;
                        //ICMH.Cat = NombreCategoriaString;
                        //ICMH.idCat = idCat;
                        //ICMH.idCol = idColor;
                        //ICMH.idIcon = idIcono;
                        //ICMH.idPagEn = idPagoEn;
                        //ICMH.idPagCon = idPagoCon;
                        //ICMH.idPagEnCol = idPagoEnColor;
                        //ICMH.idPagEnIcono = idPagoEnIcono;
                        //ICMH.MontoPago = PrecioPago.text;
                        //ICMH.Pago();
                        pagado = false;
                        pagarahora = false;
                        pagardentro = false;
                        AhoraBack = false;
                        ReciboPago.isOn = false;
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        MesHoy();
                        fechasHistorial = DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString();
                        mesesc = DateTime.Now.Month;
                        añosc = DateTime.Now.Year;
                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        HistorialX();
                        HistorialOff = null;
                        //GuardarHistorial();


                        NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);

                        Eliminar();

                        AgregarHistorialNE = null;
                        //AgregarHist();



                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }
                        if (ReciboPago.isOn == true)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + NombreCategoriaString + " asigando a la casa " + ICa.NickNameC +
                               " con un monto de " + apagar.ToString("C", ci) + " con la fecha de " + DateTime.Now.Day.ToString("d") + "/" + meshoy + DateTime.Now.Year.ToString();
                            EnviarRecibo();
                        }
                        ob.Click("Pagado");
                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + NombreCategoriaString + ", a la casa: " + ICa.NickNameC + ", con un monto de: " + apagar.ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString() + ", como es un pago del tipo eventual, no se agregara a su lista de pagos programados";

                    }
                }
                if (RecordPago == 1 && pagardentro == true && pagoretrasado == false)
                {
                    if (RecordPago == 1 && pagado == true && pagarahora == true)
                    {
                        //ICMH = casas.Casass2[posicionCasa].GetComponent<InfoCasaMH>();
                        //ICMH.ap = this;
                        //ICMH.Cat = NombreCategoriaString;
                        //ICMH.idCat = idCat;
                        //ICMH.idCol = idColor;
                        //ICMH.idIcon = idIcono;
                        //ICMH.idPagEn = idPagoEn;
                        //ICMH.idPagCon = idPagoCon;
                        //ICMH.idPagEnCol = idPagoEnColor;
                        //ICMH.idPagEnIcono = idPagoEnIcono;
                        //ICMH.MontoPago = PrecioPago.text;
                        //ICMH.Pago();
                        pagado = false;
                        pagarahora = false;
                        pagardentro = false;
                        AhoraBack = false;
                        ReciboPago.isOn = false;
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        MesHoy();
                        for (int hello = 0; hello < Pagos.Count; hello++)
                        {
                            if (Pagos[hello].GetComponent<InfoPago>().PagoIDLocal == IdPagoLocalB)
                            {
                                IP = Pagos[hello].GetComponent<InfoPago>();
                            }
                        }
                        MesProx();
                        fechasHistorial = DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString();
                        fechaMod = IP.date.Day.ToString("d") + "/" + mesprox + "/" + IP.date.Year.ToString();
                        pagar = true;

                        CadaCuando = IP.CadaCuando;
                        PrecioPago.text = IP.PrecioPago;
                        AlertaAntes = IP.AlertaAntes;
                        idBen = IP.idBen;
                        NotaPago.text = IP.Nota;
                        idColor = IP.idColor;
                        idIcono = IP.idIcono;
                        RecPago = IP.RecPago;
                        RecordPago = IP.RecordPago;
                        if (IP.PagoID == 0)
                        {
                            for (int uh = 0; uh < Pagos.Count; uh++)
                            {
                                if (IP.PagoIDLocal == IdPagoLocalOffline[uh])
                                {
                                    fechaOffline[uh] = fechaMod;
                                }
                            }
                            NoEnviadoPagosAgregar = null;
                            for (int gii = 0; IdPagoLocalOffline.Count > gii; gii++)
                            {
                                if (gii == 0)
                                {
                                    NoEnviadoPagosAgregar = IdPagoLocalOffline[gii] + "," + NombreCategoriaOffline[gii] + "," + idColorsOffline[gii] + "," + idIconosOffline[gii] + "," + IdCasaOffline[gii] + "," +
                                        PrecioPagosOffline[gii] + "," + fechaOffline[gii] + "," + RecordatorioPagosOffline[gii] + "," + CadaCuandosOffline[gii] + "," + AlertasAntesOffline[gii] + "," + IdBeneficiarioOffline[gii] +
                                        "," + NotaOffline[gii] + "," + ReciboPagosOffline[gii] + "," + CodBarrasOffline[gii] + "," + nombreCasasOffline[gii];
                                }
                                else if (gii > 0)
                                {
                                    NoEnviadoPagosAgregar += ";" + IdPagoLocalOffline[gii] + "," + NombreCategoriaOffline[gii] + "," + idColorsOffline[gii] + "," + idIconosOffline[gii] + "," + IdCasaOffline[gii] + "," +
                                        PrecioPagosOffline[gii] + "," + fechaOffline[gii] + "," + RecordatorioPagosOffline[gii] + "," + CadaCuandosOffline[gii] + "," + AlertasAntesOffline[gii] + "," + IdBeneficiarioOffline[gii] +
                                        "," + NotaOffline[gii] + "," + ReciboPagosOffline[gii] + "," + CodBarrasOffline[gii] + "," + nombreCasasOffline[gii];
                                }
                            }
                            PlayerPrefs.SetString("PagosAgregar", NoEnviadoPagosAgregar);

                        }


                        if (IP.CodBarras != "")
                        {
                            codbar = Int32.Parse(IP.CodBarras);
                        }
                        ids = IP.PagoID;
                        IP2 = IP;
                        mesesc = DateTime.Now.Month;
                        añosc = DateTime.Now.Year;
                        Historial = true;
                        if (IP.RecPago == 1)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + IP.NombreCatPago + " asigando a la casa " + ICa.NickNameC + " con un monto de " + float.Parse(IP.PrecioPago).ToString("C", ci) + " con la fecha de " +
                                fechasHistorial + " y su próxima fecha de pago es el " + fechaMod;
                            EnviarRecibo();
                        }
                        //Modificar();

                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        HistorialOff = null;
                        //GuardarHistorial();


                        NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        //HistorialX();

                        AgregarHistorialNE = null;
                        //AgregarHist();

                        HistorialX();
                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }
                        ob.Click("Pagado");
                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + IP2.NombreCatPago + ", a la casa: " + ICa.NickNameC + ", con un monto de: " + float.Parse(IP2.PrecioPago).ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString() + ", su próximo pago será el: " + IP2.dateproximopago.Day.ToString("d") + "/" + mesprox + "/" + IP2.dateproximopago.Year.ToString();
                        ob.Click("OpcionesPagoEnQuitar");

                    }
                }
                if (pagarahora == true)
                {
                    if (AhoraBack == true && past == false)
                    {
                        for (int fy = 0; Pagos.Count > fy; fy++)
                        {
                            if (Pagos[fy].GetComponent<InfoPago>().PagoID == ids)
                            {
                                IP = Pagos[fy].GetComponent<InfoPago>();
                            }
                        }
                        //IP = Pagos[posicion].GetComponent<InfoPago>();
                    }
                    if (AhoraBack == true && past == true)
                    {
                        for (int fy = 0; PagosPasado.Count > fy; fy++)
                        {
                            if (PagosPasado[fy].GetComponent<InfoPago>().PagoID == ids)
                            {
                                IP = PagosPasado[fy].GetComponent<InfoPago>();
                            }
                        }
                        //IP = PagosPasado[posicion].GetComponent<InfoPago>();
                    }

                    if (IP.RecordPago == 1 && pagado == true && past == false)
                    {
                        //ICMH = casas.Casass2[posicionCasa].GetComponent<InfoCasaMH>();
                        //ICMH.ap = this;
                        //ICMH.Cat = IP.NombreCatPago;
                        //ICMH.idCat = IP.idCat;
                        //ICMH.idCol = IP.idColor;
                        //ICMH.idIcon = IP.idIcono;
                        //ICMH.idPagEn = idPagoEn;
                        //ICMH.idPagCon = idPagoCon;
                        //ICMH.idPagEnCol = idPagoEnColor;
                        //ICMH.idPagEnIcono = idPagoEnIcono;
                        //ICMH.MontoPago = IP.PrecioPago;
                        //ICMH.Pago();
                        IP.pagado = true;
                        pagarahora = false;
                        pagardentro = false;
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        MesHoy();

                        if (IP.dateproximopago.Month.ToString() == "1")
                            mesprox = "Enero";
                        if (IP.dateproximopago.Month.ToString() == "2")
                            mesprox = "Febrero";
                        if (IP.dateproximopago.Month.ToString() == "3")
                            mesprox = "Marzo";
                        if (IP.dateproximopago.Month.ToString() == "4")
                            mesprox = "Abril";
                        if (IP.dateproximopago.Month.ToString() == "5")
                            mesprox = "Mayo";
                        if (IP.dateproximopago.Month.ToString() == "6")
                            mesprox = "Junio";
                        if (IP.dateproximopago.Month.ToString() == "7")
                            mesprox = "Julio";
                        if (IP.dateproximopago.Month.ToString() == "8")
                            mesprox = "Agosto";
                        if (IP.dateproximopago.Month.ToString() == "9")
                            mesprox = "Septiembre";
                        if (IP.dateproximopago.Month.ToString() == "10")
                            mesprox = "Octubre";
                        if (IP.dateproximopago.Month.ToString() == "11")
                            mesprox = "Noviembre";
                        if (IP.dateproximopago.Month.ToString() == "12")
                            mesprox = "Diciembre";

                        fechasHistorial = DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString();
                        fechaMod = IP.dateproximopago.Day.ToString("d") + "/" + mesprox + "/" + IP.dateproximopago.Year.ToString();
                        ids = IP.PagoID;
                        pagar = true;
                        CadaCuando = IP.CadaCuando;
                        AlertaAntes = IP.AlertaAntes;
                        PrecioPago.text = IP.PrecioPago;
                        Categoria.text = IP.NombreCatPago;
                        idIcono = IP.idIcono;
                        idColor = IP.idColor;
                        NombreCategoriaString = IP.NombreCatPago;
                        mesesc = IP.MesNum;
                        añosc = IP.AñoNum;
                        Historial = true;


                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        HistorialX();
                        HistorialOff = null;
                        //GuardarHistorial();


                        NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);
                        AgregarHistorialNE = null;
                        //AgregarHist();


                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }


                        if (IP.RecPago == 1)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + IP.NombreCatPago + " asigando a la casa " + IP.nombreCasa + " con un monto de " + float.Parse(IP.PrecioPago).ToString("C", ci) + " con la fecha de " +
                                fechasHistorial + " y su próxima fecha de pago es el " + fechaMod;
                            EnviarRecibo();
                        }
                        ob.Click("Pagado");
                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + IP.NombreCatPago + ", a la casa: " + IP.nombreCasa + ", con un monto de: " + float.Parse(IP.PrecioPago).ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString() + ", su próximo pago será el: " + IP.dateproximopago.Day.ToString("d") + "/" + mesprox + "/" + IP.dateproximopago.Year.ToString();
                        ICMH = null;
                        pagado = false;
                        AhoraBack = false;

                    }
                    if (IP.RecordPago == 0 && past == false && pagado == true)
                    {
                        //ICMH = casas.Casass2[posicionCasa].GetComponent<InfoCasaMH>();
                        //ICMH.ap = this;
                        //ICMH.Cat = IP.NombreCatPago;
                        //ICMH.idCat = IP.idCat;
                        //ICMH.idCol = IP.idColor;
                        //ICMH.idIcon = IP.idIcono;
                        //ICMH.idPagEn = idPagoEn;
                        //ICMH.idPagCon = idPagoCon;
                        //ICMH.idPagEnCol = idPagoEnColor;
                        //ICMH.idPagEnIcono = idPagoEnIcono;
                        //ICMH.MontoPago = IP.PrecioPago;
                        //ICMH.Pago();
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        MesHoy();

                        fechasHistorial = DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString();
                        pagar = true;
                        CadaCuando = IP.CadaCuando;
                        AlertaAntes = IP.AlertaAntes;
                        PrecioPago.text = IP.PrecioPago;
                        Categoria.text = IP.NombreCatPago;
                        idIcono = IP.idIcono;
                        idColor = IP.idColor;
                        NombreCategoriaString = IP.NombreCatPago;
                        mesesc = IP.MesNum;
                        añosc = IP.AñoNum;

                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        HistorialX();
                        HistorialOff = null;
                        //GuardarHistorial();


                        NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);
                        AgregarHistorialNE = null;
                        //AgregarHist();



                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }
                        ob.Click("Pagado");
                        if (IP.RecPago == 1)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + NombreCategoriaString + " asigando a la casa " + nombreCasaMenu +
                               " con un monto de: " + apagar.ToString("C", ci) + " con la fecha de " + DateTime.Now.Day.ToString("d") + "/" + meshoy + DateTime.Now.Year.ToString();
                            EnviarRecibo();
                        }
                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + NombreCategoriaString + ", a la casa: " + nombreCasaMenu + ", con un monto de " + apagar.ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString() + ", como es un pago del tipo eventual, no se agregara a su lista de pagos programados";
                        IP.Eliminar();

                        pagarahora = false;
                        pagardentro = false;
                        pagado = false;

                    }

                    if (past == true && pagado == true)
                    {
                        ////ICMH = casas.Casass2[posicionCasa].GetComponent<InfoCasaMH>();
                        ////ICMH.ap = this;
                        ////ICMH.Cat = IP.NombreCatPago;
                        ////ICMH.idCat = IP.idCat;
                        ////ICMH.idCol = IP.idColor;
                        ////ICMH.idIcon = IP.idIcono;
                        ////ICMH.idPagEn = idPagoEn;
                        ////ICMH.idPagCon = idPagoCon;
                        ////ICMH.idPagEnCol = idPagoEnColor;
                        ////ICMH.idPagEnIcono = idPagoEnIcono;
                        ////ICMH.MontoPago = IP.PrecioPago;
                        ////ICMH.Pago();
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        MesHoy();
                        //ids = IP.PagoID;
                        pagar = true;
                        CadaCuando = IP.CadaCuando;
                        AlertaAntes = IP.AlertaAntes;
                        PrecioPago.text = IP.PrecioPago;
                        Categoria.text = IP.NombreCatPago;
                        fechasHistorial = DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString();
                        idIcono = IP.idIcono;
                        idColor = IP.idColor;
                        NombreCategoriaString = IP.NombreCatPago;
                        mesesc = IP.MesNum;
                        añosc = IP.AñoNum;

                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        HistorialX();
                        HistorialOff = null;
                        //GuardarHistorial();

                        if (NombreCategoriaString != "")
                            NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);
                        AgregarHistorialNE = null;
                        //AgregarHist();


                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }
                        ob.Click("Pagado");
                        if (IP.RecPago == 1)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + NombreCategoriaString + " asigando a la casa " + IP.nombreCasa +
                               " con un monto de " + float.Parse(IP.PrecioPago).ToString("C", ci) + " con la fecha de " + DateTime.Now.Day.ToString("d") + "/" + meshoy + DateTime.Now.Year.ToString();
                            EnviarRecibo();
                        }
                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + IP.NombreCatPago + ", a la casa: " + IP.nombreCasa + ", con un monto de: " + float.Parse(IP.PrecioPago).ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString() + ", como es un pago vencido, se eliminará de su lista de pagos vencidos";
                        IP.EliminarPasado();
                        //EliminarServ();
                        //ObtenerPagos();
                        past = false;
                        pagado = false;
                        pagarahora = false;
                        pagardentro = false;
                        AhoraBack = false;

                    }
                }
                IP = null;
                Quitar();
                QuitarM();
                #endregion
                break;
            case "Tesoreria":
                sameDate = false;

                #region Tesoreria
                idPagoEn = 5;
                idPagoEnIcono = 5;
                idPagoEnColor = 5;
                pagadoen = "Tesorería / Proveedor";
                ob.Click("OpcionesPagoEnQuitar");
                //ob.Click("CasaPagosHUB");
                pagarahora = true;
                pagado = true;
                if (RecordPago == 0 && pagardentro == true)
                {
                    if (RecordPago == 0 && pagoretrasado == true && pagado == true && pagarahora == true)
                    {
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        fechasHistorial = DiaP.text + "/" + MesP.text + "/" + AñoP.text;
                        MesesC();
                        añosc = Int32.Parse(AñoP.text);



                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        HistorialX();
                        HistorialOff = null;
                        //GuardarHistorial();


                        NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);
                        AgregarHistorialNE = null;
                        //AgregarHist();

                        //ICMH.ap = this;
                        //ICMH.Cat = NombreCategoriaString;
                        //ICMH.idCat = idCat;
                        //ICMH.idCol = idColor;
                        //ICMH.idIcon = idIcono;
                        //ICMH.idPagEn = idPagoEn;
                        //ICMH.idPagCon = idPagoCon;
                        //ICMH.idPagEnCol = idPagoEnColor;
                        //ICMH.idPagEnIcono = idPagoEnIcono;
                        //ICMH.MontoPago = PrecioPago.text;
                        //ICMH.Pago();

                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }
                        if (ReciboPago.isOn == true)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + NombreCategoriaString + " asigando a la casa " + ICa.NickNameC +
                               " con un monto de " + apagar.ToString("C", ci) + " con la fecha de " + DiaP.text + "/" + MesP.text + "/" + AñoP.text;
                            EnviarRecibo();
                        }
                        ob.Click("Pagado");

                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + NombreCategoriaString + ", a la casa: " + ICa.NickNameC + ", con un monto de: " + apagar.ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DiaP.text + "/" + MesP.text + "/" + AñoP.text + ", como es un pago vencido, no se agregara a su lista de pagos vencidos";
                        pagarahora = false;
                        pagardentro = false;

                        pagado = false;
                        AhoraBack = false;
                        ReciboPago.isOn = false;

                    }
                    if (RecordPago == 0 && pagoretrasado == false && pagado == true && pagarahora == true)
                    {
                        //ICMH = casas.Casass2[posicionCasa].GetComponent<InfoCasaMH>();
                        //ICMH.ap = this;
                        //ICMH.Cat = NombreCategoriaString;
                        //ICMH.idCat = idCat;
                        //ICMH.idCol = idColor;
                        //ICMH.idIcon = idIcono;
                        //ICMH.idPagEn = idPagoEn;
                        //ICMH.idPagCon = idPagoCon;
                        //ICMH.idPagEnCol = idPagoEnColor;
                        //ICMH.idPagEnIcono = idPagoEnIcono;
                        //ICMH.MontoPago = PrecioPago.text;
                        //ICMH.Pago();
                        pagado = false;
                        pagarahora = false;
                        pagardentro = false;
                        AhoraBack = false;
                        ReciboPago.isOn = false;
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        MesHoy();
                        fechasHistorial = DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString();
                        mesesc = DateTime.Now.Month;
                        añosc = DateTime.Now.Year;
                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        HistorialX();
                        HistorialOff = null;
                        //GuardarHistorial();


                        NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);

                        Eliminar();

                        AgregarHistorialNE = null;
                        //AgregarHist();



                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }
                        if (ReciboPago.isOn == true)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + NombreCategoriaString + " asigando a la casa " + ICa.NickNameC +
                               " con un monto de " + apagar.ToString("C", ci) + " con la fecha de " + DateTime.Now.Day.ToString("d") + "/" + meshoy + DateTime.Now.Year.ToString();
                            EnviarRecibo();
                        }
                        ob.Click("Pagado");
                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + NombreCategoriaString + ", a la casa: " + ICa.NickNameC + ", con un monto de: " + apagar.ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString() + ", como es un pago del tipo eventual, no se agregara a su lista de pagos programados";

                    }
                }
                if (RecordPago == 1 && pagardentro == true && pagoretrasado == false)
                {
                    if (RecordPago == 1 && pagado == true && pagarahora == true)
                    {
                        //ICMH = casas.Casass2[posicionCasa].GetComponent<InfoCasaMH>();
                        //ICMH.ap = this;
                        //ICMH.Cat = NombreCategoriaString;
                        //ICMH.idCat = idCat;
                        //ICMH.idCol = idColor;
                        //ICMH.idIcon = idIcono;
                        //ICMH.idPagEn = idPagoEn;
                        //ICMH.idPagCon = idPagoCon;
                        //ICMH.idPagEnCol = idPagoEnColor;
                        //ICMH.idPagEnIcono = idPagoEnIcono;
                        //ICMH.MontoPago = PrecioPago.text;
                        //ICMH.Pago();
                        pagado = false;
                        pagarahora = false;
                        pagardentro = false;
                        AhoraBack = false;
                        ReciboPago.isOn = false;
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        MesHoy();
                        for (int hello = 0; hello < Pagos.Count; hello++)
                        {
                            if (Pagos[hello].GetComponent<InfoPago>().PagoIDLocal == IdPagoLocalB)
                            {
                                IP = Pagos[hello].GetComponent<InfoPago>();
                            }
                        }
                        MesProx();
                        fechasHistorial = DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString();
                        fechaMod = IP.date.Day.ToString("d") + "/" + mesprox + "/" + IP.date.Year.ToString();
                        pagar = true;

                        CadaCuando = IP.CadaCuando;
                        PrecioPago.text = IP.PrecioPago;
                        AlertaAntes = IP.AlertaAntes;
                        idBen = IP.idBen;
                        NotaPago.text = IP.Nota;
                        idColor = IP.idColor;
                        idIcono = IP.idIcono;
                        RecPago = IP.RecPago;
                        RecordPago = IP.RecordPago;
                        if (IP.PagoID == 0)
                        {
                            for (int uh = 0; uh < Pagos.Count; uh++)
                            {
                                if (IP.PagoIDLocal == IdPagoLocalOffline[uh])
                                {
                                    fechaOffline[uh] = fechaMod;
                                }
                            }
                            NoEnviadoPagosAgregar = null;
                            for (int gii = 0; IdPagoLocalOffline.Count > gii; gii++)
                            {
                                if (gii == 0)
                                {
                                    NoEnviadoPagosAgregar = IdPagoLocalOffline[gii] + "," + NombreCategoriaOffline[gii] + "," + idColorsOffline[gii] + "," + idIconosOffline[gii] + "," + IdCasaOffline[gii] + "," +
                                        PrecioPagosOffline[gii] + "," + fechaOffline[gii] + "," + RecordatorioPagosOffline[gii] + "," + CadaCuandosOffline[gii] + "," + AlertasAntesOffline[gii] + "," + IdBeneficiarioOffline[gii] +
                                        "," + NotaOffline[gii] + "," + ReciboPagosOffline[gii] + "," + CodBarrasOffline[gii] + "," + nombreCasasOffline[gii];
                                }
                                else if (gii > 0)
                                {
                                    NoEnviadoPagosAgregar += ";" + IdPagoLocalOffline[gii] + "," + NombreCategoriaOffline[gii] + "," + idColorsOffline[gii] + "," + idIconosOffline[gii] + "," + IdCasaOffline[gii] + "," +
                                        PrecioPagosOffline[gii] + "," + fechaOffline[gii] + "," + RecordatorioPagosOffline[gii] + "," + CadaCuandosOffline[gii] + "," + AlertasAntesOffline[gii] + "," + IdBeneficiarioOffline[gii] +
                                        "," + NotaOffline[gii] + "," + ReciboPagosOffline[gii] + "," + CodBarrasOffline[gii] + "," + nombreCasasOffline[gii];
                                }
                            }
                            PlayerPrefs.SetString("PagosAgregar", NoEnviadoPagosAgregar);

                        }


                        if (IP.CodBarras != "")
                        {
                            codbar = Int32.Parse(IP.CodBarras);
                        }
                        ids = IP.PagoID;
                        IP2 = IP;
                        mesesc = DateTime.Now.Month;
                        añosc = DateTime.Now.Year;
                        Historial = true;
                        if (IP.RecPago == 1)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + IP.NombreCatPago + " asigando a la casa " + ICa.NickNameC + " con un monto de " + float.Parse(IP.PrecioPago).ToString("C", ci) + " con la fecha de " +
                                fechasHistorial + " y su próxima fecha de pago es el " + fechaMod;
                            EnviarRecibo();
                        }
                        //Modificar();

                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        HistorialOff = null;
                        //GuardarHistorial();


                        NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        //HistorialX();

                        AgregarHistorialNE = null;
                        //AgregarHist();

                        HistorialX();
                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }
                        ob.Click("Pagado");
                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + IP2.NombreCatPago + ", a la casa: " + ICa.NickNameC + ", con un monto de: " + float.Parse(IP2.PrecioPago).ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString() + ", su próximo pago será el: " + IP2.dateproximopago.Day.ToString("d") + "/" + mesprox + "/" + IP2.dateproximopago.Year.ToString();
                        ob.Click("OpcionesPagoEnQuitar");

                    }
                }
                if (pagarahora == true)
                {
                    if (AhoraBack == true && past == false)
                    {
                        for (int fy = 0; Pagos.Count > fy; fy++)
                        {
                            if (Pagos[fy].GetComponent<InfoPago>().PagoID == ids)
                            {
                                IP = Pagos[fy].GetComponent<InfoPago>();
                            }
                        }
                        //IP = Pagos[posicion].GetComponent<InfoPago>();
                    }
                    if (AhoraBack == true && past == true)
                    {
                        for (int fy = 0; PagosPasado.Count > fy; fy++)
                        {
                            if (PagosPasado[fy].GetComponent<InfoPago>().PagoID == ids)
                            {
                                IP = PagosPasado[fy].GetComponent<InfoPago>();
                            }
                        }
                        //IP = PagosPasado[posicion].GetComponent<InfoPago>();
                    }

                    if (IP.RecordPago == 1 && pagado == true && past == false)
                    {
                        //ICMH = casas.Casass2[posicionCasa].GetComponent<InfoCasaMH>();
                        //ICMH.ap = this;
                        //ICMH.Cat = IP.NombreCatPago;
                        //ICMH.idCat = IP.idCat;
                        //ICMH.idCol = IP.idColor;
                        //ICMH.idIcon = IP.idIcono;
                        //ICMH.idPagEn = idPagoEn;
                        //ICMH.idPagCon = idPagoCon;
                        //ICMH.idPagEnCol = idPagoEnColor;
                        //ICMH.idPagEnIcono = idPagoEnIcono;
                        //ICMH.MontoPago = IP.PrecioPago;
                        //ICMH.Pago();
                        IP.pagado = true;
                        pagarahora = false;
                        pagardentro = false;
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        MesHoy();

                        if (IP.dateproximopago.Month.ToString() == "1")
                            mesprox = "Enero";
                        if (IP.dateproximopago.Month.ToString() == "2")
                            mesprox = "Febrero";
                        if (IP.dateproximopago.Month.ToString() == "3")
                            mesprox = "Marzo";
                        if (IP.dateproximopago.Month.ToString() == "4")
                            mesprox = "Abril";
                        if (IP.dateproximopago.Month.ToString() == "5")
                            mesprox = "Mayo";
                        if (IP.dateproximopago.Month.ToString() == "6")
                            mesprox = "Junio";
                        if (IP.dateproximopago.Month.ToString() == "7")
                            mesprox = "Julio";
                        if (IP.dateproximopago.Month.ToString() == "8")
                            mesprox = "Agosto";
                        if (IP.dateproximopago.Month.ToString() == "9")
                            mesprox = "Septiembre";
                        if (IP.dateproximopago.Month.ToString() == "10")
                            mesprox = "Octubre";
                        if (IP.dateproximopago.Month.ToString() == "11")
                            mesprox = "Noviembre";
                        if (IP.dateproximopago.Month.ToString() == "12")
                            mesprox = "Diciembre";

                        fechasHistorial = DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString();
                        fechaMod = IP.dateproximopago.Day.ToString("d") + "/" + mesprox + "/" + IP.dateproximopago.Year.ToString();
                        ids = IP.PagoID;
                        pagar = true;
                        CadaCuando = IP.CadaCuando;
                        AlertaAntes = IP.AlertaAntes;
                        PrecioPago.text = IP.PrecioPago;
                        Categoria.text = IP.NombreCatPago;
                        idIcono = IP.idIcono;
                        idColor = IP.idColor;
                        NombreCategoriaString = IP.NombreCatPago;
                        mesesc = IP.MesNum;
                        añosc = IP.AñoNum;
                        Historial = true;


                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        HistorialX();
                        HistorialOff = null;
                        //GuardarHistorial();


                        NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);
                        AgregarHistorialNE = null;
                        //AgregarHist();


                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }


                        if (IP.RecPago == 1)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + IP.NombreCatPago + " asigando a la casa " + IP.nombreCasa + " con un monto de " + float.Parse(IP.PrecioPago).ToString("C", ci) + " con la fecha de " +
                                fechasHistorial + " y su próxima fecha de pago es el " + fechaMod;
                            EnviarRecibo();
                        }
                        ob.Click("Pagado");
                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + IP.NombreCatPago + ", a la casa: " + IP.nombreCasa + ", con un monto de: " + float.Parse(IP.PrecioPago).ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString() + ", su próximo pago será el: " + IP.dateproximopago.Day.ToString("d") + "/" + mesprox + "/" + IP.dateproximopago.Year.ToString();
                        ICMH = null;
                        pagado = false;
                        AhoraBack = false;

                    }
                    if (IP.RecordPago == 0 && past == false && pagado == true)
                    {
                        //ICMH = casas.Casass2[posicionCasa].GetComponent<InfoCasaMH>();
                        //ICMH.ap = this;
                        //ICMH.Cat = IP.NombreCatPago;
                        //ICMH.idCat = IP.idCat;
                        //ICMH.idCol = IP.idColor;
                        //ICMH.idIcon = IP.idIcono;
                        //ICMH.idPagEn = idPagoEn;
                        //ICMH.idPagCon = idPagoCon;
                        //ICMH.idPagEnCol = idPagoEnColor;
                        //ICMH.idPagEnIcono = idPagoEnIcono;
                        //ICMH.MontoPago = IP.PrecioPago;
                        //ICMH.Pago();
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        MesHoy();

                        fechasHistorial = DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString();
                        pagar = true;
                        CadaCuando = IP.CadaCuando;
                        AlertaAntes = IP.AlertaAntes;
                        PrecioPago.text = IP.PrecioPago;
                        Categoria.text = IP.NombreCatPago;
                        idIcono = IP.idIcono;
                        idColor = IP.idColor;
                        NombreCategoriaString = IP.NombreCatPago;
                        mesesc = IP.MesNum;
                        añosc = IP.AñoNum;

                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        HistorialX();
                        HistorialOff = null;
                        //GuardarHistorial();


                        NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);
                        AgregarHistorialNE = null;
                        //AgregarHist();



                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }
                        ob.Click("Pagado");
                        if (IP.RecPago == 1)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + NombreCategoriaString + " asigando a la casa " + nombreCasaMenu +
                               " con un monto de: " + apagar.ToString("C", ci) + " con la fecha de " + DateTime.Now.Day.ToString("d") + "/" + meshoy + DateTime.Now.Year.ToString();
                            EnviarRecibo();
                        }
                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + NombreCategoriaString + ", a la casa: " + nombreCasaMenu + ", con un monto de " + apagar.ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString() + ", como es un pago del tipo eventual, no se agregara a su lista de pagos programados";
                        IP.Eliminar();

                        pagarahora = false;
                        pagardentro = false;
                        pagado = false;

                    }

                    if (past == true && pagado == true)
                    {
                        ////ICMH = casas.Casass2[posicionCasa].GetComponent<InfoCasaMH>();
                        ////ICMH.ap = this;
                        ////ICMH.Cat = IP.NombreCatPago;
                        ////ICMH.idCat = IP.idCat;
                        ////ICMH.idCol = IP.idColor;
                        ////ICMH.idIcon = IP.idIcono;
                        ////ICMH.idPagEn = idPagoEn;
                        ////ICMH.idPagCon = idPagoCon;
                        ////ICMH.idPagEnCol = idPagoEnColor;
                        ////ICMH.idPagEnIcono = idPagoEnIcono;
                        ////ICMH.MontoPago = IP.PrecioPago;
                        ////ICMH.Pago();
                        for (int ju = 0; ju < casa.Casass.Count; ju++)
                        {
                            if (idCasaBack == casa.Casass[ju].GetComponent<InfoCasa>().idCasa)
                            {
                                ICa = casa.Casass[ju].GetComponent<InfoCasa>();
                            }
                        }
                        MesHoy();
                        //ids = IP.PagoID;
                        pagar = true;
                        CadaCuando = IP.CadaCuando;
                        AlertaAntes = IP.AlertaAntes;
                        PrecioPago.text = IP.PrecioPago;
                        Categoria.text = IP.NombreCatPago;
                        fechasHistorial = DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString();
                        idIcono = IP.idIcono;
                        idColor = IP.idColor;
                        NombreCategoriaString = IP.NombreCatPago;
                        mesesc = IP.MesNum;
                        añosc = IP.AñoNum;

                        NombreCatOff.Add(NombreCategoriaString);
                        idCatOff.Add(idCat);
                        idColorOff.Add(idColor);
                        idIconoOff.Add(idIcono);
                        idPagoEnOff.Add(idPagoEn);
                        idPagoConOff.Add(idPagoCon);
                        precioHistorialOff.Add(PrecioPago.text);
                        fechaHistorialOff.Add(fechasHistorial);
                        idPagoEnColorOff.Add(idPagoEnColor);
                        idPagoEnIconoOff.Add(idPagoEnIcono);
                        mesHistorialOff.Add(mesesc);
                        añoHistorialOff.Add(añosc);
                        idCasaHistorialOff.Add(idCasaBack);
                        nombreCasasHist.Add(nombreCasaMenu);
                        precioHistorial = PrecioPago.text;
                        fechaHistorial = fechasHistorial;
                        mesHistorial = mesesc;
                        añoHistorial = añosc;
                        IdCasaHistorial = idCasaBack;
                        HistorialX();
                        HistorialOff = null;
                        //GuardarHistorial();

                        if (NombreCategoriaString != "")
                            NombreCatAgregarHist.Add(NombreCategoriaString);
                        idCatAgregarHist.Add(idCat);
                        idColorAgregarHist.Add(idColor);
                        idIconoAgregarHist.Add(idIcono);
                        idPagoEnAgregarHist.Add(idPagoEn);
                        idPagoConAgregarHist.Add(idPagoCon);
                        precioHistorialAgregarHist.Add(PrecioPago.text);
                        fechaHistorialAgregarHist.Add(fechasHistorial);
                        idPagoEnColorAgregarHist.Add(idPagoEnColor);
                        idPagoEnIconoAgregarHist.Add(idPagoEnIcono);
                        mesHistorialAgregarHist.Add(mesesc);
                        añoHistorialAgregarHist.Add(añosc);
                        idCasaHistorialAgregarHist.Add(idCasaBack);
                        nombreCasasAgregarHist.Add(nombreCasaMenu);
                        AgregarHistorialNE = null;
                        //AgregarHist();


                        if (idCasaHistorialAgregarHist[0] != 0)
                        {
                            AgregarHistorial();
                        }
                        ob.Click("Pagado");
                        if (IP.RecPago == 1)
                        {
                            CuerpoMens = "Se marcó como pagado la categoria de " + NombreCategoriaString + " asigando a la casa " + IP.nombreCasa +
                               " con un monto de " + float.Parse(IP.PrecioPago).ToString("C", ci) + " con la fecha de " + DateTime.Now.Day.ToString("d") + "/" + meshoy + DateTime.Now.Year.ToString();
                            EnviarRecibo();
                        }
                        ob.PagadoTexto.text = "Se marcó como pagada la categoría: " + IP.NombreCatPago + ", a la casa: " + IP.nombreCasa + ", con un monto de: " + float.Parse(IP.PrecioPago).ToString("C", ci)
                            + " y se hizo el pago en la fecha de: " + DateTime.Now.Day.ToString("d") + "/" + meshoy + "/" + DateTime.Now.Year.ToString() + ", como es un pago vencido, se eliminará de su lista de pagos vencidos";
                        IP.EliminarPasado();
                        //EliminarServ();
                        //ObtenerPagos();
                        past = false;
                        pagado = false;
                        pagarahora = false;
                        pagardentro = false;
                        AhoraBack = false;

                    }
                }
                IP = null;
                Quitar();
                QuitarM();
                #endregion
                break;
        }
        load = true;
        obtenido = true;
        StartCoroutine(AcomodoSeconds());
    }
    public void Alertas()
    {
        if (pagoretrasado == true)
        {
            ob.DesError.text = "No se puede agregar una alerta ya que el pago es antes de la fecha actual";
            ob.Click("Errors");
        }
        else
        {
            ob.Click("Alertas");
        }
    }
    public void Pagar()
    {

    }
    IEnumerator DelayAcomodo()
    {
        yield return new WaitForSeconds(.1f);
        Acomodo();
        yield return 1;
    }
    public void Limpiar()
    {
        for (int lp = 0; VPM.Mes.Count > lp; lp++)
        {
            VPM.Mes[lp].transform.SetParent(contenidos.transform);
        }
        for (int ap = 0; VPM.Mes2.Count > ap; ap++)
        {
            VPM.Mes2[ap].transform.SetParent(contenidos.transform);
        }
        for (int ep = 0; VPM.Mes3.Count > ep; ep++)
        {
            VPM.Mes3[ep].transform.SetParent(contenidos.transform);
        }
    }
    public void Acomodo()
    {
        //for(int lo = 0; Pagos.Count)
        for (int lp = 0; Pagos.Count > lp; lp++)
        {
            Pagos[lp].transform.SetParent(contenidos.transform);
            //VPM.Mes[lp].transform.SetParent(contenidos.transform);
        }
        /*for (int ap = 0; VPM.Mes2.Count > ap; ap++)
        {
            VPM.Mes2[ap].transform.SetParent(contenidos.transform);
        }
        for (int ep = 0; VPM.Mes3.Count > ep; ep++)
        {
            VPM.Mes3[ep].transform.SetParent(contenidos.transform);
        }*/

        if (obtenido == true)
        {
            obtenido = false;
            fechasComparation.Clear();
            for (int i = 0; i < Pagos.Count; i++)
            {
                fechasComparation.Add(Pagos[i].GetComponent<InfoPago>().date);
            }
            //fechasComparation = fechasComparation.OrderBy(x => x.Date).ToList();

            PagosIndex.Clear();
            for (int i = 0; fechasComparation.Count > i; i++)
            {
                mismafecha = false;

                for (int j = 0; j < Pagos.Count; j++)
                {
                    if ((Pagos[j].GetComponent<InfoPago>().date.Month == fechasComparation[i].Month && (Pagos[j].GetComponent<InfoPago>().date.Year == fechasComparation[i].Year)) && mismafecha == false)
                    {
                        bool mismo = false;
                        for (int k = 0; k < PagosIndex.Count; k++)
                        {
                            if (PagosIndex[k].name == Pagos[j].name)
                            {
                                mismo = true;
                            }
                        }
                        if (mismo == false)
                        {
                            Pagos[j].GetComponent<RectTransform>().SetSiblingIndex(i);
                            PagosIndex.Add(Pagos[j]);
                            mismafecha = true;
                        }
                    }
                }


                //for (zz = 0; Pagos.Count > zz; zz++)
                //{
                //    FechaComp = Pagos[zz].GetComponent<InfoPago>().dates;
                //    //tick = FechaComp.Ticks;
                //    index1 = Pagos[zz].GetComponent<RectTransform>().GetSiblingIndex();
                //    indexback = index1;
                //    if (acomodo == true)
                //    {

                //        for (zzz = 0; Pagos.Count > zzz; zzz++)
                //        {
                //            FechaComp2 = Pagos[zzz].GetComponent<InfoPago>().dates;
                //            index2 = Pagos[zzz].GetComponent<RectTransform>().GetSiblingIndex();
                //            indexback2 = index2;
                //            if (FechaComp > FechaComp2  && zz != zzz)
                //            {

                //                if(index2 > index1)
                //                    {
                //                        PagosIndex[indexback] = Pagos[zzz];
                //                        PagosIndex[indexback2] = Pagos[zz];
                //                        Pagos[zzz].GetComponent<RectTransform>().SetSiblingIndex(indexback);
                //                        Pagos[zz].GetComponent<RectTransform>().SetSiblingIndex(indexback2);
                //                        index1 = index2;
                //                        indexback = indexback2;
                //                    }
                //                else if(index1 > index2)
                //                    {
                //                        PagosIndex[indexback2] = Pagos[zzz];
                //                        PagosIndex[indexback] = Pagos[zz];
                //                    }
                //            }

                //        }
                //        if (zz == Pagos.Count - 1)
                //        {
                //            acomodo = false;
                //        }
                //    }
                //}

                if (zz == Pagos.Count && load == true)
                {
                    acomodo = false;
                    ob.limpiar = true;
                    load = false;
                    VPM.X();
                    zzz = 0;
                    zz = 0;
                    change = false;
                }
            }


        }
        if (load == true)
        {

            ob.limpiar = true;
            load = false;
            VPM.X();
            zzz = 0;
            zz = 0;
            change = false;
        }
    }

    public void Acomodados()
    {
        //for(int lo = 0; Pagos.Count)

        for (int lp = 0; VPM.Mes.Count > lp; lp++)
        {
            VPM.Mes[lp].transform.SetParent(contenidos.transform);
        }
        for (int ap = 0; VPM.Mes2.Count > ap; ap++)
        {
            VPM.Mes2[ap].transform.SetParent(contenidos.transform);
        }
        for (int ep = 0; VPM.Mes3.Count > ep; ep++)
        {
            VPM.Mes3[ep].transform.SetParent(contenidos.transform);
        }
        for (zz = 0; PagosIndex.Count > zz; zz++)
        {
            FechaComp = PagosIndex[zz].GetComponent<InfoPago>().dates;
            //tick = FechaComp.Ticks;
            index1 = PagosIndex[zz].GetComponent<RectTransform>().GetSiblingIndex();
            indexback = index1;
            if (acomodo == true)
            {

                for (zzz = 0; PagosIndex.Count > zzz; zzz++)
                {
                    FechaComp2 = PagosIndex[zzz].GetComponent<InfoPago>().dates;
                    index2 = PagosIndex[zzz].GetComponent<RectTransform>().GetSiblingIndex();
                    indexback2 = index2;
                    if (FechaComp > FechaComp2 && zz != zzz)
                    {
                        indexback2 = index1;
                        PagosIndex[zzz].GetComponent<RectTransform>().SetSiblingIndex(indexback2);
                        PagosIndex[indexback] = PagosIndex[zzz];
                        indexback = index2;
                        indexback2 = index1;
                        index1 = indexback;
                        PagosIndex[zz].GetComponent<RectTransform>().SetSiblingIndex(indexback);
                        PagosIndex[indexback] = PagosIndex[zz];
                    }

                }
                if (zz == Pagos.Count)
                {
                    acomodo = false;
                }
            }
        }
        if (zz == Pagos.Count && load == true)
        {

            ob.limpiar = true;
            load = false;
            VPM.X();
            zzz = 0;
            zz = 0;
            change = false;
        }
    }
    public void AcomodoPasado()
    {
        for (int lq = 0; VPM.MesPasado.Count > lq; lq++)
        {
            VPM.MesPasado[lq].transform.SetParent(contenidosPasados.transform);
        }

        for (mm = 0; (posicionPasado) > mm; mm++)
        {
            FechaCompPasado = PagosPasado[mm].GetComponent<InfoPago>().dates;
            //tick = FechaComp.Ticks;
            indexPasado = PagosPasado[mm].GetComponent<RectTransform>().GetSiblingIndex();
            indexbackPasado = indexPasado;
            for (mmm = 0; (posicionPasado) > mmm; mmm++)
            {
                FechaComp2Pasado = PagosPasado[mmm].GetComponent<InfoPago>().dates;
                index2Pasado = PagosPasado[mmm].GetComponent<RectTransform>().GetSiblingIndex();
                indexback2Pasado = index2Pasado;
                if (FechaCompPasado > FechaComp2Pasado && index2Pasado > indexPasado)
                {
                    PagosPasado[mmm].GetComponent<RectTransform>().SetSiblingIndex(indexbackPasado);
                    PagosIndexPasado[indexbackPasado - 1] = Pagos[mmm];
                    PagosPasado[mm].GetComponent<RectTransform>().SetSiblingIndex(indexback2Pasado);
                    PagosIndexPasado[indexback2Pasado - 1] = PagosPasado[mm];
                }

            }

        }
        if (mm == posicion && loadPasado == true)
        {
            loadPasado = false;
            VPM.PasadoX();
            mmm = 0;
            mm = 0;
            change = false;
        }
    }

    public void AgregarPago()
    {
        //Debug.Log("se va a agregar pago");
        //fecha = DiaP.text + "/" + MesP.text + "/" + AñoP.text;
        System.Random rnd = new System.Random();
        WWWForm Form = new WWWForm();
        if (NombreCategoriaOffline.Count != 0)
        {
            Form.AddField("IdUsuario", reg.id);

            Form.AddField("NombrePago", NombreCategoriaOffline[0]);
            Form.AddField("ColorPago", idColorsOffline[0]);
            Form.AddField("IconoPago", idIconosOffline[0]);
            Form.AddField("IdCasa", IdCasaOffline[0]);
            Form.AddField("MontoPago", PrecioPagosOffline[0]);
            Form.AddField("FechaPago", fechaOffline[0]);
            Form.AddField("RecurrentePago", RecordatorioPagosOffline[0]);
            Form.AddField("RepetirPago", CadaCuandosOffline[0]);
            Form.AddField("FechaRecordatorioPago", AlertasAntesOffline[0]);
            Form.AddField("IdBeneficiario", IdBeneficiarioOffline[0]);
            Form.AddField("NotaPago", NotaOffline[0]);
            Form.AddField("ReciboPago", ReciboPagosOffline[0]);
            Form.AddField("CodigoPago", CodBarrasOffline[0]);

            //reg.carga = true;
            WebServices.AgregarPago(Form, SuccessAgregarPago, ErrorAgregarPago);
            ob.DesError.text = "No se pudo agregar el pago programado, checa tu conexión e intentalo de nuevo";
        }
    }
    void SuccessAgregarPago(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            error = ("" + ((MessageArg<string>)e).responseValue);
            reg.carga = false;
        }
        else if (e is MessageArg<DBPagos>)
        {
            //Debug.Log("se agrega pago");

            //right = true;
            //register1 = 1;
            //PlayerPrefs.SetInt("Registrado", register1);
            DBPagos dbpago = ((MessageArg<DBPagos>)e).responseValue;
            ids = dbpago.IdPago;
            for (int hu = 0; Pagos.Count > hu; hu++)
            {
                //string[] separators2 = { "Pago" };
                //string[] words20 = Pagos[hu].name.Split(separators2, StringSplitOptions.RemoveEmptyEntries);
                //int number;
                //Int32.TryParse(words20[0], out number);
                if (Pagos[hu].GetComponent<InfoPago>().PagoIDLocal == IdPagoLocalOffline[0])
                {
                    IdPago[hu] = ids;
                    Pagos[hu].GetComponent<InfoPago>().PagoID = ids;
                    for (int huhuh = 0; huhuh < IdPagoLocal.Count; huhuh++)
                    {
                        if (IdPagoLocal[huhuh] == Pagos[hu].GetComponent<InfoPago>().PagoIDLocal)
                        {
                            Offline = null;
                            for (int yh = 0; yh < Pagos.Count; yh++)
                            {
                                if (yh == 0)
                                    Offline = IdPagoLocal[yh] + "," + IdPago[yh] + "," + IdCasa[yh] + "," + idBeneficiario[yh] + "," + CadaCuandos[yh] + "," + AlertasAntes[yh] + "," + idCategoria[yh] + "," + CodBarras[yh] + ","
                                        + PrecioPagos[yh] + "," + Dia[yh] + "," + Mes[yh] + "," + Año[yh] + "," + Nota[yh] + "," + ReciboPagos[yh] + "," + RecordatorioPagos[yh] + "," + idColors[yh] + "," +
                                        idIconos[yh] + "," + IdPPred[yh] + "," + Checarpasado[yh] + "," + NombreCategoria[yh] + "," + PagoPersonalizado[yh] + "," + nombreCasas[yh];
                                else if (yh > 0)
                                    Offline += ";" + IdPagoLocal[yh] + "," + IdPago[yh] + "," + IdCasa[yh] + "," + idBeneficiario[yh] + "," + CadaCuandos[yh] + "," + AlertasAntes[yh] + "," + idCategoria[yh] + "," + CodBarras[yh] + ","
                                        + PrecioPagos[yh] + "," + Dia[yh] + "," + Mes[yh] + "," + Año[yh] + "," + Nota[yh] + "," + ReciboPagos[yh] + "," + RecordatorioPagos[yh] + "," + idColors[yh] + "," +
                                        idIconos[yh] + "," + IdPPred[yh] + "," + Checarpasado[yh] + "," + NombreCategoria[yh] + "," + PagoPersonalizado[yh] + "," + nombreCasas[yh];
                            }
                            PlayerPrefs.SetString("Pagos", Offline);
                        }
                    }


                }
            }
            IdPagoLocalOffline.RemoveAt(0);
            NombreCategoriaOffline.RemoveAt(0);
            idColorsOffline.RemoveAt(0);
            idIconosOffline.RemoveAt(0);
            IdCasaOffline.RemoveAt(0);
            PrecioPagosOffline.RemoveAt(0);
            fechaOffline.RemoveAt(0);
            RecordatorioPagosOffline.RemoveAt(0);
            CadaCuandosOffline.RemoveAt(0);
            AlertasAntesOffline.RemoveAt(0);
            IdBeneficiarioOffline.RemoveAt(0);
            NotaOffline.RemoveAt(0);
            ReciboPagosOffline.RemoveAt(0);
            CodBarrasOffline.RemoveAt(0);
            nombreCasasOffline.RemoveAt(0);




            NoEnviadoPagosAgregar = null;
            for (int gii = 0; IdPagoLocalOffline.Count > gii; gii++)
            {
                if (gii == 0)
                {
                    NoEnviadoPagosAgregar = IdPagoLocalOffline[gii] + "," + NombreCategoriaOffline[gii] + "," + idColorsOffline[gii] + "," + idIconosOffline[gii] + "," + IdCasaOffline[gii] + "," +
                        PrecioPagosOffline[gii] + "," + fechaOffline[gii] + "," + RecordatorioPagosOffline[gii] + "," + CadaCuandosOffline[gii] + "," + AlertasAntesOffline[gii] + "," + IdBeneficiarioOffline[gii] +
                        "," + NotaOffline[gii] + "," + ReciboPagosOffline[gii] + "," + CodBarrasOffline[gii] + "," + nombreCasasOffline[gii];
                }
                else if (gii > 0)
                {
                    NoEnviadoPagosAgregar += ";" + IdPagoLocalOffline[gii] + "," + NombreCategoriaOffline[gii] + "," + idColorsOffline[gii] + "," + idIconosOffline[gii] + "," + IdCasaOffline[gii] + "," +
                        PrecioPagosOffline[gii] + "," + fechaOffline[gii] + "," + RecordatorioPagosOffline[gii] + "," + CadaCuandosOffline[gii] + "," + AlertasAntesOffline[gii] + "," + IdBeneficiarioOffline[gii] +
                        "," + NotaOffline[gii] + "," + ReciboPagosOffline[gii] + "," + CodBarrasOffline[gii] + "," + nombreCasasOffline[gii];
                }
            }
            PlayerPrefs.SetString("PagosAgregar", NoEnviadoPagosAgregar);

            if (IdPagoLocalOffline.Count != 0 && IdCasaOffline[0] != 0 && (Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork || Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork))
            {
                Debug.Log("se repite codigo");
                AgregarPago();
            }
            //ob.limpiar = true;
            //EliminarServ();
            ob.avisotexto.text = "Se agregó correctamente el pago programado";
            ob.Click("Aviso");


            //ReadCardPagos();
            //Quitar();
            //ob.Click("CasaPagosHUB");
            agregando = false;
            error = dbpago.IdPago.ToString();
            //id = user.Id.Value;

        }
    }
    public void ModPagos()
    {
        modp = true;
        ModPago();
    }
    public void ModPago()
    {
        if (modp == true)
        {
            modp = false;
            fechaMod = DiaP.text + "/" + MesP.text + "/" + AñoP.text;
        }
        //pagar = false;
        if (IdPagoModOffline.Count != 0)
        {
            WWWForm Form = new WWWForm();
            Form.AddField("IdUsuario", reg.id);
            Form.AddField("IdPago", IdPagoModOffline[0]);
            Form.AddField("NombrePago", NombreCategoriaModOffline[0]);
            Form.AddField("ColorPago", idColorsModOffline[0]);
            Form.AddField("IconoPago", idIconosModOffline[0]);
            Form.AddField("IdCasa", IdCasaModOffline[0]);
            Form.AddField("MontoPago", PrecioPagosModOffline[0]);
            Form.AddField("FechaPago", fechaModOffline[0]);
            Form.AddField("RecurrentePago", RecordatorioPagosModOffline[0]);
            Form.AddField("RepetirPago", CadaCuandosModOffline[0]);
            Form.AddField("FechaRecordatorioPago", AlertasAntesModOffline[0]);
            Form.AddField("IdBeneficiario", IdBeneficiarioModOffline[0]);
            Form.AddField("NotaPago", NotaModOffline[0]);
            Form.AddField("ReciboPago", ReciboPagosModOffline[0]);
            if(CodBarrasModOffline[0]==null)
            {
                CodBarrasModOffline[0] = "";
            }
            Form.AddField("CodigoPago", CodBarrasModOffline[0]);
            //reg.carga = true;
            WebServices.ModPago(Form, SuccessModPago, ErrorModPago);
            ob.DesError.text = "No se pudo modificar el pago programado, checa tu conexión e intentalo de nuevo";
        }
    }
    void SuccessModPago(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            error = ("" + ((MessageArg<string>)e).responseValue);
            reg.carga = false;
            Debug.Log("");
        }
        else if (e is MessageArg<DBPagos>)
        {
            //right = true;
            //register1 = 1;
            //PlayerPrefs.SetInt("Registrado", register1);
            DBPagos dbpago = ((MessageArg<DBPagos>)e).responseValue;
            error = dbpago.IdPago.ToString();
            for (int yuy = 0; yuy < Pagos.Count; yuy++)
            {
                if (Pagos[yuy].GetComponent<InfoPago>().PagoID == IdPagoModOffline[0])
                {
                    Pagos[yuy].GetComponent<InfoPago>().pas = true;
                }
            }
            IdPagoModOffline.RemoveAt(0);
            NombreCategoriaModOffline.RemoveAt(0);
            idColorsModOffline.RemoveAt(0);
            idIconosModOffline.RemoveAt(0);
            IdCasaModOffline.RemoveAt(0);
            PrecioPagosModOffline.RemoveAt(0);
            fechaModOffline.RemoveAt(0);
            RecordatorioPagosModOffline.RemoveAt(0);
            CadaCuandosModOffline.RemoveAt(0);
            AlertasAntesModOffline.RemoveAt(0);
            IdBeneficiarioModOffline.RemoveAt(0);
            NotaModOffline.RemoveAt(0);
            ReciboPagosModOffline.RemoveAt(0);
            CodBarrasModOffline.RemoveAt(0);
            ModificarPagosNE = null;
            for (int gii = 0; IdPagoModOffline.Count > gii; gii++)
            {
                if (gii == 0)
                {
                    ModificarPagosNE = IdPagoModOffline[gii] + "," + NombreCategoriaModOffline[gii] + "," + idColorsModOffline[gii] + "," + idIconosModOffline[gii] + "," + IdCasaModOffline[gii] + "," +
                        PrecioPagosModOffline[gii] + "," + fechaModOffline[gii] + "," + RecordatorioPagosModOffline[gii] + "," + CadaCuandosModOffline[gii] + "," + AlertasAntesModOffline[gii] + "," + IdBeneficiarioModOffline[gii] +
                        "," + NotaModOffline[gii] + "," + ReciboPagosModOffline[gii] + "," + CodBarrasModOffline[gii];
                }
                else if (gii > 0)
                {
                    ModificarPagosNE += ";" + IdPagoModOffline[gii] + "," + NombreCategoriaModOffline[gii] + "," + idColorsModOffline[gii] + "," + idIconosModOffline[gii] + "," + IdCasaModOffline[gii] + "," +
                        PrecioPagosModOffline[gii] + "," + fechaModOffline[gii] + "," + RecordatorioPagosModOffline[gii] + "," + CadaCuandosModOffline[gii] + "," + AlertasAntesModOffline[gii] + "," + IdBeneficiarioModOffline[gii] +
                        "," + NotaModOffline[gii] + "," + ReciboPagosModOffline[gii] + "," + CodBarrasModOffline[gii];
                }
            }
            PlayerPrefs.SetString("PagosModificar", ModificarPagosNE);


            if (IdPagoModOffline.Count != 0)
            {
                ModPago();
            }
            //        ob.limpiar = true;
            //        EliminarServ();
            //        ReadCardPagos();
            //        if (Historial == true)
            //        {
            //            Historial = false;
            //            AgregarHistorial();
            //        }
            //        else
            //        {
            //            ob.Click("Aviso");
            //            ob.avisotexto.text = "Se modificó correctamente el pago programado";
            //            Quitar();
            //botonph.SetActive (true);
            //            ob.Click("CasaPagosHUB");
            //        }
            //id = user.Id.Value;

        }
    }
    public void ElimPago()
    {
        if (EliminarPagoOffline.Count != 0)
        {
            //print("pago eliminado");
            WWWForm Form = new WWWForm();
            Form.AddField("IdUsuario", reg.id);
            Form.AddField("IdPago", EliminarPagoOffline[0]);
            //print(EliminarPagoOffline[0]);
            //reg.carga = true;
            WebServices.ElimPago(Form, SuccessElimPago, ErrorEliminarPago);
            ob.DesError.text = "No se pudo eliminar el pago programado, checa tu conexión e intentalo de nuevo";
        }
    }
    void SuccessElimPago(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            error = ("" + ((MessageArg<string>)e).responseValue);
            reg.carga = false;
            Debug.Log("");
        }
        else if (e is MessageArg<DBPagos>)
        {
            //right = true;
            //register1 = 1;
            //PlayerPrefs.SetInt("Registrado", register1);
            DBPagos dbpago = ((MessageArg<DBPagos>)e).responseValue;
            EliminarPagoOffline.RemoveAt(0);
            EliminarPagoNE = null;
            for (int yu = 0; yu < EliminarPagoOffline.Count; yu++)
            {
                if (yu == 0)
                {
                    EliminarPagoNE = EliminarPagoOffline[yu] + ";";
                }
                else if (yu > 0)
                {
                    EliminarPagoNE += EliminarPagoOffline[yu] + ";";
                }
            }
            PlayerPrefs.SetString("PagosEliminar", EliminarPagoNE);
            if (EliminarPagoOffline.Count != 0)
            {
                ElimPago();
            }
            Quitar();
            error = dbpago.IdPago.ToString();
            //ob.Click("CasaPagosHUB");
            //ob.Click("Aviso");
            //ob.avisotexto.text ="Se eliminó correctamente el pago programado";
            //EliminarServ();
            //ReadCardPagos();
            //Quitar();


            //ob.Click("CasaPagosHUB");
            //id = user.Id.Value;

        }
    }
    void Error(System.Object Sender, EventArgs e)
    {
        error = ("" + ((MessageArg<string>)e).responseValue);
        reg.carga = false;
        //ob.Click("Errors");
        //print(error);
    }
    void ErrorAgregarHistorial(System.Object Sender, EventArgs e)
    {

        error = ("" + ((MessageArg<string>)e).responseValue);
        //reg.carga = false;
        //ob.Click("Errors");
        AgregarHistorial();

    }
    void ErrorEliminarPago(System.Object Sender, EventArgs e)
    {
        error = ("" + ((MessageArg<string>)e).responseValue);
        //reg.carga = false;
        //ob.Click("Errors");
        ElimPago();

    }
    void ErrorEliminarPagoPasado(System.Object Sender, EventArgs e)
    {
        error = ("" + ((MessageArg<string>)e).responseValue);
        //reg.carga = false;
        //ob.Click("Errors");
        ElimPagoPasado();
    }
    void ErrorAgregarPagoPasado(System.Object Sender, EventArgs e)
    {
        error = ("" + ((MessageArg<string>)e).responseValue);
        AgregarPagoPasado();

    }
    void ErrorModPago(System.Object Sender, EventArgs e)
    {
        error = ("" + ((MessageArg<string>)e).responseValue);
        reg.carga = false;
        ModPago();
        //ob.Click("Errors");

    }
    void ErrorAgregarPago(System.Object Sender, EventArgs e)
    {
        error = ("" + ((MessageArg<string>)e).responseValue);
        reg.carga = false;
        AgregarPago();
        //ob.Click("Errors");

    }
    public void ObtenerPagos()
    {
        WWWForm Form = new WWWForm();
        Form.AddField("IdUsuario", reg.id);
        //print("Voy por pagos ");
        WebServices.ObtPago(Form, SuccessObtPagos, Error);
        ob.DesError.text = "No se pudo obtener los pagos programados, checa tu conexión e intentalo de nuevo";
    }
    void SuccessObtPagos(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            error = ("" + ((MessageArg<string>)e).responseValue);
            dbpagos.Clear();
            Offline = null;
            PlayerPrefs.SetString("Pagos", Offline);
            Debug.Log("");

        }
        else if (e is MessageArg<List<DBPagos>>)
        {
            //right = true;
            //register1 = 1;
            //User user = ((MessageArg<User>)e).responseValue;
            //PlayerPrefs.SetInt("Registrado", register1);
            //User user = ((MessageArg<User>)e).responseValue;
           dbpagos = null;
            dbpagos = ((MessageArg<List<DBPagos>>)e).responseValue;
            //if(norepetido)
            StartCoroutine(DelayGetPagos());

            //user.casas = ((MessageArg<List<DBCasate>>)e).responseValue;
            //= ((MessageArg<List<DBCasa>>)e).responseValue;
            //Casas = user.casas;
            //error = dbcasas.Id.ToString();
            //mp.ObtenerMP();
            //ingresa = true;
            //mp.Ver();

            //ob.Click("Inicios");

        }
    }
    public void AgregarPagoPasado()
    {
       // print("Se agrega pasado 1");
        fecha = DiaP.text + "/" + MesP.text + "/" + AñoP.text;
        //if(nombreCasasPasadoOffline)
        WWWForm Form = new WWWForm();
        if (NombreCategoriaPasadoOffline.Count != 0)
        {
            Form.AddField("IdUsuario", reg.id);
            Form.AddField("NombrePagoPasado", NombreCategoriaPasadoOffline[0]);
            Form.AddField("ColorPagoPasado", idColorsPasadoOffline[0]);
            Form.AddField("IconoPagoPasado", idIconosPasadoOffline[0]);
            Form.AddField("IdCasaPasado", IdCasaPasadoOffline[0]);
            Form.AddField("MontoPagoPasado", PrecioPagosPasadoOffline[0]);
            Form.AddField("FechaPagoPasado", fechaPasadoOffline[0]);
            Form.AddField("RecurrentePagoPasado", RecordatorioPagosPasadoOffline[0]);
            Form.AddField("RepetirPagoPasado", CadaCuandosPasadoOffline[0]);
            Form.AddField("FechaRecordatorioPagoPasado", AlertasAntesPasadoOffline[0]);
            Form.AddField("IdBeneficiarioPasado", IdBeneficiarioPasadoOffline[0]);
            Form.AddField("NotaPagoPasado", NotaPasadoOffline[0]);
            Form.AddField("ReciboPagoPasado", ReciboPagosPasadoOffline[0]);
            Form.AddField("CodigoPagoPasado", "0");
            WebServices.AgregarPagoPasado(Form, SuccessAgregarPagoPasado, ErrorAgregarPagoPasado);
            //reg.carga = true;
            ob.DesError.text = "No se pudo agregar el pago vencido, checa tu conexión e intentalo de nuevo";
        }
    }

    void SuccessAgregarPagoPasado(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
           // print("Se agrega pasado 2");

            error = ("" + ((MessageArg<string>)e).responseValue);
            reg.carga = false;
            Debug.Log("");
        }
        else if (e is MessageArg<DBPagosPasado>)
        {
           // print("Se agrega pasado 3");

            //right = true;
            //register1 = 1;
            //PlayerPrefs.SetInt("Registrado", register1);
            DBPagosPasado dbpagopasado = ((MessageArg<DBPagosPasado>)e).responseValue;
            //ob.Click("CasaPagosHUB");
            //ob.Click("Aviso");
            //ob.avisotexto.text = "Se agregó correctamente el pago vencido";
            //EliminarPasadoServ();
            //ReadCardPagosPasado();
            //Quitar();
            //ids = dbpagopasado.IdPagoPasado;
            for (int ko = 0; ko < PagosPasado.Count; ko++)
            {
                if (PagosPasado[ko].GetComponent<InfoPago>().PagoIDLocal == IdPagoLocalPasadoOffline[0])
                {
                    PagosPasado[ko].GetComponent<InfoPago>().PagoID = dbpagopasado.IdPagoPasado;
                    IdPagoPasado[ko] = dbpagopasado.IdPagoPasado;
                }
            }
            OfflinePasado = null;
            for (int yh = 0; yh < PagosPasado.Count; yh++)
            {
                if (yh == 0)
                    OfflinePasado = IdPagoPasadoLocal[yh] + "," + IdPagoPasado[yh] + "," + IdCasaPasado[yh] + "," + idBeneficiarioPasado[yh] + "," + idCategoriaPasado[yh] + "," + CodBarrasPasado[yh] + ","
                        + PrecioPagosPasado[yh] + "," + DiaPasado[yh] + "," + MesPasado[yh] + "," + AñoPasado[yh] + "," + NotaPasado[yh] + "," + ReciboPagosPasado[yh] + "," + idColorsPasado[yh] + "," +
                        idIconosPasado[yh] + "," + IdPPredPasado[yh] + "," + Checarpasado[yh] + "," + NombreCategoriaPasado[yh] + "," + PagoPersonalizadoPasado[yh];
                else if (yh > 0)
                    OfflinePasado += ";" + IdPagoPasadoLocal[yh] + "," + IdPagoPasado[yh] + "," + IdCasaPasado[yh] + "," + idBeneficiarioPasado[yh] + "," + idCategoriaPasado[yh] + "," + CodBarrasPasado[yh] + ","
                        + PrecioPagosPasado[yh] + "," + DiaPasado[yh] + "," + MesPasado[yh] + "," + AñoPasado[yh] + "," + NotaPasado[yh] + "," + ReciboPagosPasado[yh] + "," + idColorsPasado[yh] + "," +
                        idIconosPasado[yh] + "," + IdPPredPasado[yh] + "," + Checarpasado[yh] + "," + NombreCategoriaPasado[yh] + "," + PagoPersonalizadoPasado[yh];
            }
            PlayerPrefs.SetString("PagosPasado", OfflinePasado);
            IdPagoLocalPasadoOffline.RemoveAt(0);
            NombreCategoriaPasadoOffline.RemoveAt(0);
            idColorsPasadoOffline.RemoveAt(0);
            idIconosPasadoOffline.RemoveAt(0);
            IdCasaPasadoOffline.RemoveAt(0);
            PrecioPagosPasadoOffline.RemoveAt(0);
            fechaPasadoOffline.RemoveAt(0);
            RecordatorioPagosPasadoOffline.RemoveAt(0);
            CadaCuandosPasadoOffline.RemoveAt(0);
            AlertasAntesPasadoOffline.RemoveAt(0);
            IdBeneficiarioPasadoOffline.RemoveAt(0);
            NotaPasadoOffline.RemoveAt(0);
            ReciboPagosPasadoOffline.RemoveAt(0);
            CodBarrasPasadoOffline.RemoveAt(0);
            AgregarPagoPasadoNE = null;
            for (int hu = 0; hu < IdPagoLocalPasadoOffline.Count; hu++)
            {
                if (hu == 0)
                {
                    AgregarPagoPasadoNE = IdPagoLocalPasadoOffline[hu] + "," + NombreCategoriaPasadoOffline[hu] + "," + idColorsPasadoOffline[hu] + "," + idIconosPasadoOffline[hu] +
                        "," + IdCasaPasadoOffline[hu] + "," + PrecioPagosPasadoOffline[hu] + "," + fechaPasadoOffline[hu] + "," + RecordatorioPagosPasadoOffline[hu] + "," +
                        CadaCuandosPasadoOffline[hu] + "," + AlertasAntesPasadoOffline[hu] + "," + IdBeneficiarioPasadoOffline[hu] + "," + NotaPasadoOffline[hu] + "," +
                        ReciboPagosPasadoOffline[hu] + "," + CodBarrasPasadoOffline[hu];
                }
                else if (hu > 0)
                {
                    AgregarPagoPasadoNE += ";" + IdPagoLocalPasadoOffline[hu] + "," + NombreCategoriaPasadoOffline[hu] + "," + idColorsPasadoOffline[hu] + "," + idIconosPasadoOffline[hu] +
                        "," + IdCasaPasadoOffline[hu] + "," + PrecioPagosPasadoOffline[hu] + "," + fechaPasadoOffline[hu] + "," + RecordatorioPagosPasadoOffline[hu] + "," +
                        CadaCuandosPasadoOffline[hu] + "," + AlertasAntesPasadoOffline[hu] + "," + IdBeneficiarioPasadoOffline[hu] + "," + NotaPasadoOffline[hu] + "," +
                        ReciboPagosPasadoOffline[hu] + "," + CodBarrasPasadoOffline[hu];
                }
            }
            PlayerPrefs.SetString("PagosPasadoAgregar", AgregarPagoPasadoNE);
            if (IdPagoLocalPasadoOffline.Count != 0)
            {
                AgregarPagoPasado();
            }
            error = dbpagopasado.IdPagoPasado.ToString();
            //id = user.Id.Value;

        }
    }

    public void ElimPagoPasado()
    {
        reg.StartLoad();
        WWWForm Form = new WWWForm();
        Form.AddField("IdUsuario", reg.id);
        Form.AddField("IdPagoPasado", EliminarPasadoOffline[0]);
        WebServices.ElimPagoPasado(Form, SuccessElimPagoPasado, ErrorEliminarPagoPasado);
        //reg.carga = true;
        ob.DesError.text = "No se pudo eliminar el pago vencido, checa tu conexión e intentalo de nuevo";

    }
    void SuccessElimPagoPasado(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            error = ("" + ((MessageArg<string>)e).responseValue);
            reg.carga = false;
            Debug.Log("");
        }
        else if (e is MessageArg<DBPagosPasado>)
        {
            //right = true;
            //register1 = 1;
            //PlayerPrefs.SetInt("Registrado", register1);
            DBPagosPasado dbpagopasado = ((MessageArg<DBPagosPasado>)e).responseValue;
            error = dbpagopasado.IdPagoPasado.ToString();
            //EliminarPasadoServ();
            //ReadCardPagosPasado();
            //ob.Click("Aviso");
            //ob.avisotexto.text = "Se eliminó correctamente el pago vencido";
            EliminarPasadoOffline.RemoveAt(0);
            EliminarPagoPasadoNE = null;
            for (int count = 0; count < EliminarPasadoOffline.Count; count++)
            {
                if (count == 0)
                {
                    EliminarPagoPasadoNE = EliminarPasadoOffline[count] + ";";
                }
                else if (count != 0)
                {
                    EliminarPagoPasadoNE += EliminarPasadoOffline[count] + ";";
                }
            }
            PlayerPrefs.SetString("PagosPasadoEliminar", EliminarPagoPasadoNE);
            if (EliminarPasadoOffline.Count != 0)
            {
                ElimPagoPasado();
            }
            //ob.Click("CasaPagosHUB");
            //id = user.Id.Value;
            reg.EndLoad();

        }
    }
    public void ObtenerPagosPasado()
    {
        WWWForm Form = new WWWForm();
        Form.AddField("IdUsuario", reg.id);
        WebServices.ObtPagoPasado(Form, SuccessObtPagosPasado, Error);
        ob.DesError.text = "No se pudo obtener los pagos vencidos, checa tu conexión e intentalo de nuevo";
    }
    void SuccessObtPagosPasado(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            error = ("" + ((MessageArg<string>)e).responseValue);
            dbpagospasado.Clear();
            OfflinePasado = null;
            PlayerPrefs.SetString("PagosPasado", OfflinePasado);
            Debug.Log("");
        }
        else if (e is MessageArg<List<DBPagosPasado>>)
        {
            //right = true;
            //register1 = 1;
            //User user = ((MessageArg<User>)e).responseValue;
            //PlayerPrefs.SetInt("Registrado", register1);
            //User user = ((MessageArg<User>)e).responseValue;
            dbpagospasado = null;
            dbpagospasado = ((MessageArg<List<DBPagosPasado>>)e).responseValue;

            StartCoroutine(DelayGetPagosPasado());

        }
    }
    public void AgregarHistorial()
    {
        WWWForm Form = new WWWForm();
        Form.AddField("IdUsuario", reg.id);
        if (fechaHistorialAgregarHist.Count != 0)
        {
            Form.AddField("FechaHistorial", fechaHistorialAgregarHist[0]);
            Form.AddField("IdPagadoCon", idPagoConAgregarHist[0]);
            Form.AddField("IdPagadoEn", idPagoEnAgregarHist[0]);
            Form.AddField("IdCategoria", idCatAgregarHist[0]);
            Form.AddField("IconoHistorial", idIconoAgregarHist[0]);
            Form.AddField("ColorHistorial", idColorAgregarHist[0]);
            Form.AddField("NombreCategoriaHistorial", NombreCatAgregarHist[0]);
            Form.AddField("MontoPagoHistorial", precioHistorialAgregarHist[0]);
            Form.AddField("IdCasaHistorial", idCasaHistorialAgregarHist[0]);
            Form.AddField("MesHistorial", mesHistorialAgregarHist[0]);
            Form.AddField("AnoHistorial", añoHistorialAgregarHist[0]);

            WebServices.AgregarHistorial(Form, SuccessAgregarHistorial, ErrorAgregarHistorial);
            //reg.carga = true;
            ob.DesError.text = "No se pudo realizar el pago, checa tu conexión e intentalo de nuevo";
        }
    }

    void SuccessAgregarHistorial(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            error = ("" + ((MessageArg<string>)e).responseValue);
            reg.carga = false;
            Debug.Log("");
        }
        else if (e is MessageArg<DBHistorial>)
        {
            //right = true;
            //register1 = 1;
            //PlayerPrefs.SetInt("Registrado", register1);
            DBHistorial dbhistorial = ((MessageArg<DBHistorial>)e).responseValue;


            fechaHistorialAgregarHist.RemoveAt(0);
            idPagoConAgregarHist.RemoveAt(0);
            idPagoEnAgregarHist.RemoveAt(0);
            idCatAgregarHist.RemoveAt(0);
            idIconoAgregarHist.RemoveAt(0);
            idColorAgregarHist.RemoveAt(0);
            NombreCatAgregarHist.RemoveAt(0);
            precioHistorialAgregarHist.RemoveAt(0);
            idCasaHistorialAgregarHist.RemoveAt(0);
            mesHistorialAgregarHist.RemoveAt(0);
            añoHistorialAgregarHist.RemoveAt(0);
            nombreCasasAgregarHist.RemoveAt(0);
            idPagoEnColorAgregarHist.RemoveAt(0);
            idPagoEnIconoAgregarHist.RemoveAt(0);

            AgregarHistorialNE = null;
            AgregarHist();

            if (idCasaHistorialAgregarHist[0] != 0)
            {
                AgregarHistorial();
            }

            //if recurrente eliminar pago
            if(RecordatorioPagosOffline[0]==9)
            {
               // ElimPago();
            }
            
            //error = dbhistorial.IdHistorial.ToString();
            //ob.Click("CasaPagosHUB");
            //EliminarHistorial();
            //ReadCardHistorial();

            //id = user.Id.Value;

        }
    }

    public void EnviarRecibo()
    {
        WWWForm Form = new WWWForm();
        Form.AddField("EmailUsuario", reg.emailusuario);
        Form.AddField("Asunto", "PayHome: Recibo de Pago");
        Form.AddField("Cuerpo", CuerpoMens);
        WebServices.EnviarRecibo(Form, SuccessRecibo, Error);
    }

    void SuccessRecibo(System.Object Sender, EventArgs e)
    {
        if (e is MessageArg<string>)
        {
            error = ("" + ((MessageArg<string>)e).responseValue);
            Debug.Log("");
        }
        else if (e is MessageArg<DBPagosPasado>)
        {
            //right = true;
            //register1 = 1;
            //PlayerPrefs.SetInt("Registrado", register1);
            DBHistorial dbhistorial = ((MessageArg<DBHistorial>)e).responseValue;
            error = dbhistorial.IdHistorial.ToString();
            //ob.Click("CasaPagosHUB");
            EliminarHistorial();
            ReadCardHistorial();

            //id = user.Id.Value;

        }
    }
    public void ObtenerHistorial()
    {
        WWWForm Form = new WWWForm();
        Form.AddField("IdUsuario", reg.id);
        //Form.AddField("IdCasaHistorial", IdCasaHistorial);

        WebServices.ObtHistorial(Form, SuccessObtHistorial, Error);
        //StartCoroutine(uploadForm(Form));
        ob.DesError.text = "No se pudo obtener el historial de pagos, checa tu conexión e intentalo de nuevo";
    }

    public IEnumerator UploadForm(WWWForm form)
    {
        UnityEngine.Networking.UnityWebRequest a = UnityEngine.Networking.UnityWebRequest.Post("https://payhome.azurewebsites.net/serv/obtenerhistorial.php", form);
        yield return a.SendWebRequest();
        if (a.isNetworkError || a.isHttpError)
        {
            Debug.Log(a.error);
        }
        else
        {
            Debug.Log(a.downloadHandler.text);
        }
    }

    void SuccessObtHistorial(System.Object Sender, EventArgs e)
    {
        fechaHistorialOff.Clear();
        idPagoConOff.Clear();
        idPagoEnOff.Clear();
        idCatOff.Clear();
        idIconoOff.Clear();
        idColorOff.Clear();
        NombreCatOff.Clear();
        precioHistorialOff.Clear();
        idCasaHistorialOff.Clear();
        mesHistorialOff.Clear();
        añoHistorialOff.Clear();
        nombreCasasHist.Clear();
        idPagoEnColorOff.Clear();
        idPagoEnIconoOff.Clear();
        HistorialOff = null;
        GuardarHistorial();

        if (e is MessageArg<string>)
        {


            error = ("" + ((MessageArg<string>)e).responseValue);
            //print(error);
            dbhistorial = null;
            //Debug.Log("");

        }
        else if (e is MessageArg<List<DBHistorial>>)
        {
            dbhistorial = ((MessageArg<List<DBHistorial>>)e).responseValue;
            if (!isGetHist)
            {
                isGetHist = true;
                StartCoroutine(DelayGetHistorial());
            }

            //printList(dbhistorial);
        }
    }
    public void PrintList<T>(List<T> list)
    {
        for (int i = 0; i < list.Count; i++)
            print("List[" + i + "]: " + list[i].ToString());
    }
    public void ReadCardPagos()
    {
        //reg.cargando = true;
        if (!isGetPagos)
        {
            isGetPagos = true;
            ObtenerPagos();
        }
        //StartCoroutine(DelayGetPagos());
        //ob.Click("MisCasasHUB");
        //if (ID == save.id)
        //{

        //}
    }
    public void ReadCardPagosPasado()
    {
        //reg.cargando = true;
        EliminarPasadoServ();
        ObtenerPagosPasado();

        //StartCoroutine(DelayGetPagosPasado());
        //ob.Click("MisCasasHUB");
        //if (ID == save.id)
        //{

        //}
    }
    IEnumerator DelayGetPagos()
    {
        if (Pagos.Count == 0)
        {
            //print("clear");
            IdPagoLocal.Clear();
        }
        if (reg.ingresa == false)
        {
            //reg.CargateMenor();
        }
        yield return new WaitForSeconds(2f);
        if (posicion < 0)
        {
            posicion = 0;
        }
        if (reg.ingresa == true)
            ReadCardHistorial();
        //reg.cargando = true;
        for (y = 0; y < dbpagos.Count; y++)
        {

            //reg.cargando = true;
            norepetido = false;
            pagate = dbpagos[y];
            casaencontrada = false;
            idCasa = pagate.IdCasa;

            ids = pagate.IdPago;
            for (int mu = 0; mu < casa.Casass.Count; mu++)
            {

                if (casa.Casass[mu].GetComponent<InfoCasa>().idCasa == idCasa)
                {
                    nombreCasa = casa.Casass[mu].GetComponent<InfoCasa>().NickNameC;
                    casaencontrada = true;
                }
            }
            //print(Pagos.Count);
            for (int kl = 0; kl < Pagos.Count; kl++)
            {
                if (ids == Pagos[kl].GetComponent<InfoPago>().PagoID)
                {
                    norepetido = true;
                }
            }
            //print("NR: " + norepetido +"CE: " + casaencontrada);
            if (norepetido == false && casaencontrada == true)
            {
                //reg.cargando = true;
                casaencontrada = false;

                i++;
                //print("este es i "+i);
                IdPagoLocal.Insert(0,i);

                IdPago.Add(pagate.IdPago);
                NombreCategoria.Add(pagate.NombrePago);
                //IcoColor = tp.Colo[pagate.ColorPago];
                //Ico = tp.Icon[pagate.IconoPago];
                //Icon.Add(Ico);
                //Colo.Add(IcoColor);
                //idColor = pagate.ColorPago;
                //idIcono = pagate.IconoPago;
                idColors.Add(pagate.ColorPago);
                idIconos.Add(pagate.IconoPago);
                IdCasa.Add(pagate.IdCasa);
                PrecioPagos.Add(pagate.MontoPago);
                string[] fecha = pagate.FechaPago.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                Dia.Add(fecha[0]);
                Mes.Add(fecha[1]);
                idCategoria.Add(0);
                Año.Add(fecha[2]);
                Nota.Add(pagate.NotaPago);
                for (int k = 0; k < 10; k++)
                {
                    if (tp.CatPagos[k].GetComponent<InfoCategoria>().NombreCatPago == pagate.NombrePago)
                    {
                        pp = true;
                    }
                }
                if (pp == true)
                {
                    IdPPred.Add(pagate.IconoPago);
                    PagoPersonalizado.Add(0);

                }
                else
                {
                    PagoPersonalizado.Add(1);
                    IdPPred.Add(pagate.IconoPago);
                }
                pp = false;

                RecordatorioPagos.Add(pagate.RecurrentePago);
                CadaCuandos.Add(pagate.RepetirPago);
                AlertasAntes.Add(pagate.FechaRecordatorioPago);
                ReciboPagos.Add(pagate.ReciboPago);
                CodBarras.Add(pagate.CodigoPago);
                //print("es cero");
                Checarpasado.Add(0);
                idBeneficiario.Add(pagate.IdBeneficiario);
                //print("Agrego casa " + nombreCasaMenu);
                nombreCasas.Add(nombreCasaMenu);
                //idCategoria.Add(pagate.id)
                //codbar = Int32.Parse(pagate.CodigoPago);
                //else {
                //    FotoTomada = true;
                //}
                Posicion.Insert(0,posicion);
                posicion++;
                //i = categoriate.id;

                X();
            }
            if(IdPagoLocal.Count == 0)
            {
                print("yes");
                //StartCoroutine(DelayGetPagos());
            }
        }
        if (y == dbpagos.Count)
        {

            //reg.cargando = false;
            acomodo = true;
            obtenido = true;
            StartCoroutine(AcomodoSeconds());
            sw.cambioPago = true;
        }
        isGetPagos = false;
    }

    IEnumerator DelayGetPagosPasado()
    {
        //reg.cargando = true;
        if (reg.ingresa == false)
        {
            //reg.CargateMenor();
        }
        yield return new WaitForSeconds(5f);
        if (reg.ingresa == true)
        {
            ReadCardPagos();
        }
        for (y = 0; y < dbpagospasado.Count; y++)
        {
            //reg.cargando = true;
            pasadote = dbpagospasado[y];
            if ((IdPagoPasado.Count < dbpagospasado.Count))
            {
                IdPagoPasado.Add(pasadote.IdPagoPasado);
                zoo = pasadote.IdPagoPasado;
                f++;
                IdPagoPasadoLocal.Add(f);
                NombreCategoriaPasado.Add(pasadote.NombrePagoPasado);
                //IcoColor = tp.Colo[pagate.ColorPago];
                //Ico = tp.Icon[pagate.IconoPago];
                //Icon.Add(Ico);
                //Colo.Add(IcoColor);
                //idColor = pagate.ColorPago;
                //idIcono = pagate.IconoPago;
                idColorsPasado.Add(pasadote.ColorPagoPasado);
                idIconosPasado.Add(pasadote.IconoPagoPasado);
                idCasa = pasadote.IdCasaPasado;
                IdCasaPasado.Add(pasadote.IdCasaPasado);
                idCategoriaPasado.Add(0);
                PrecioPagosPasado.Add(pasadote.MontoPagoPasado);
                string[] fechaPasado = pasadote.FechaPagoPasado.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                DiaPasado.Add(fechaPasado[0]);
                MesPasado.Add(fechaPasado[1]);
                AñoPasado.Add(fechaPasado[2]);
                NotaPasado.Add(pasadote.NotaPagoPasado);
                for (int l = 0; l < 10; l++)
                {
                    if (tp.CatPagos[l].GetComponent<InfoCategoria>().NombreCatPago == pasadote.NombrePagoPasado)
                    {
                        pp2 = true;
                    }
                }
                if (pp2 == true)
                {
                    PagoPersonalizadoPasado.Add(0);
                    IdPPredPasado.Add(pasadote.IconoPagoPasado);
                }
                else
                {
                    PagoPersonalizadoPasado.Add(1);
                    IdPPredPasado.Add(pasadote.IconoPagoPasado);
                }
                pp2 = false;
                RecordatorioPagosPasado.Add(pasadote.RecurrentePagoPasado);
                CadaCuandosPasado.Add(pasadote.RepetirPagoPasado);
                AlertasAntesPasado.Add(pasadote.FechaRecordatorioPagoPasado);
                ReciboPagosPasado.Add(pasadote.ReciboPagoPasado);
                CodBarrasPasado.Add(pasadote.CodigoPagoPasado);
                Checarpasado.Add(1);
                idBeneficiarioPasado.Add(pasadote.IdBeneficiarioPasado);
                OfflinePasado = null;
                for (int yh = 0; yh < IdPagoPasadoLocal.Count; yh++)
                {
                    if (yh == 0)
                        OfflinePasado = IdPagoPasadoLocal[yh] + "," + IdPagoPasado[yh] + "," + IdCasaPasado[yh] + "," + idBeneficiarioPasado[yh] + "," + idCategoriaPasado[yh] + "," + CodBarrasPasado[yh] + ","
                            + PrecioPagosPasado[yh] + "," + DiaPasado[yh] + "," + MesPasado[yh] + "," + AñoPasado[yh] + "," + NotaPasado[yh] + "," + ReciboPagosPasado[yh] + "," + idColorsPasado[yh] + "," +
                            idIconosPasado[yh] + "," + IdPPredPasado[yh] + "," + Checarpasado[yh] + "," + NombreCategoriaPasado[yh] + "," + PagoPersonalizadoPasado[yh];
                    else if (yh > 0)
                        OfflinePasado += ";" + IdPagoPasadoLocal[yh] + "," + IdPagoPasado[yh] + "," + IdCasaPasado[yh] + "," + idBeneficiarioPasado[yh] + "," + idCategoriaPasado[yh] + "," + CodBarrasPasado[yh] + ","
                            + PrecioPagosPasado[yh] + "," + DiaPasado[yh] + "," + MesPasado[yh] + "," + AñoPasado[yh] + "," + NotaPasado[yh] + "," + ReciboPagosPasado[yh] + "," + idColorsPasado[yh] + "," +
                            idIconosPasado[yh] + "," + IdPPredPasado[yh] + "," + Checarpasado[yh] + "," + NombreCategoriaPasado[yh] + "," + PagoPersonalizadoPasado[yh];
                }
                PlayerPrefs.SetString("PagosPasado", OfflinePasado);
                //idCategoria.Add(pagate.id)
                //codbar = Int32.Parse(pagate.CodigoPago);
                //else {
                //    FotoTomada = true;
                //}
                PosicionPasado.Add(posicionPasado);
                posicionPasado++;
                //i = categoriate.id;
                PasadoX();

            }

        }
        if (y == dbpagospasado.Count)
        {
            /*ob.Click("CasaPagosHUB")*/
            //;
            //reg.cargando = false;
        }
    }
    IEnumerator DelayGetHistorial()
    {
        //print("delay get");
        //reg.cargando = true;
        if (reg.ingresa == false)
        {
            //reg.CargateMenor();
        }
        reg.ingresa = false;

        yield return new WaitForSeconds(.01f);
        //print("delay get 2");

        for (y = 0; y < dbhistorial.Count; y++)
        {
            //print("y: " + y);
            if (NombreCatOff.Count < dbhistorial.Count)
            {
                //reg.cargando = true;
                historialote = dbhistorial[y];
                NombreCategoriaString = historialote.NombreCategoriaHistorial;
                idCat = historialote.IdCategoria;
                idColor = historialote.ColorHistorial;
                idIcono = historialote.IconoHistorial;
                idPagoEn = historialote.IdPagadoEn;
                idPagoCon = historialote.IdPagadoCon;
                precioHistorial = historialote.MontoPagoHistorial;
                fechaHistorial = historialote.FechaHistorial;
                idPagoEnColor = historialote.IdPagadoEn;
                idPagoEnIcono = historialote.IdPagadoEn;
                mesHistorial = historialote.MesHistorial;
                añoHistorial = historialote.AnoHistorial;
                IdCasaHistorial = historialote.IdCasaHistorial;

                //print("Estoy llenando" + IdCasaHistorial);
                //print("----"+NombreCatOff.Count+"----");
                NombreCatOff.Add(NombreCategoriaString);
                idCatOff.Add(idCat);
                idColorOff.Add(idColor);
                idIconoOff.Add(idIcono);
                idPagoEnOff.Add(idPagoEn);
                idPagoConOff.Add(idPagoCon);
                precioHistorialOff.Add(precioHistorial);
                fechaHistorialOff.Add(fechaHistorial);
                idPagoEnColorOff.Add(idPagoEnColor);
                idPagoEnIconoOff.Add(idPagoEnIcono);
                mesHistorialOff.Add(mesHistorial);
                añoHistorialOff.Add(añoHistorial);
                idCasaHistorialOff.Add(IdCasaHistorial);
                encontrado = false;
                for (int iui = 0; iui < casa.Casass2.Count; iui++)
                {
                    if (casa.Casass2[iui].GetComponent<InfoCasaMH>().CasaID == historialote.IdCasaHistorial)
                    {
                        encontrado = true;
                        nombreCasasHist.Add(casa.Casass2[iui].GetComponent<InfoCasaMH>().NickName);
                        nombreCasa = casa.Casass2[iui].GetComponent<InfoCasaMH>().NickName;
                    }

                }
                if (encontrado == false)
                {
                    nombreCasasHist.Add("");
                }
                else
                {
                    encontrado = false;
                }
                HistorialOff = null;
                GuardarHistorial();

                //i = categoriate.id;
                HistorialX();
                if (NombreCatOff.Count == dbhistorial.Count - 1)
                {
                    //reg.carga = false;
                    //reg.cargando = false;
                }
            }
        }
        yield return new WaitForSeconds(1f);
        isGetHist = false;
    }
    public void HistorialX()
    {

        for (int ui = 0; ui < casas.Casass2.Count; ui++)
        {
            if (casas.Casass2[ui].GetComponent<InfoCasaMH>().CasaID == IdCasaHistorial || casa.Casass2[ui].GetComponent<InfoCasaMH>().NickName == nombreCasaMenu)
            {
                ICMH = casas.Casass2[ui].GetComponent<InfoCasaMH>();
                casaHistorial = true;
            }
        }
        if (casaHistorial == true)
        {

            casaHistorial = false;
            ICMH.ap = this;
            ICMH.Cat = NombreCategoriaString;
            ICMH.idCat = idCat;
            ICMH.idCol = idColor;
            ICMH.idIcon = idIcono;
            ICMH.idPagEn = idPagoEn;
            ICMH.idPagCon = idPagoCon;
            ICMH.idPagEnCol = idPagoEnColor;
            ICMH.idPagEnIcono = idPagoEnIcono;
            ICMH.MontoPago = precioHistorial;
            ICMH.fecha = fechaHistorial;
            ICMH.meses = mesHistorial.ToString();
            ICMH.años = añoHistorial.ToString();
            ICMH.Pago();
        }
    }
    public void EliminarHistorial()
    {
        NombreCatOff.Clear();
        idCatOff.Clear();
        idColorOff.Clear();
        idIconoOff.Clear();
        idPagoEnOff.Clear();
        idPagoConOff.Clear();
        precioHistorialOff.Clear();
        fechaHistorialOff.Clear();
        idPagoEnColorOff.Clear();
        idPagoEnIconoOff.Clear();
        mesHistorialOff.Clear();
        añoHistorialOff.Clear();
        idCasaHistorialOff.Clear();
        for (ups = 0; ups < casa.Casass2.Count; ups++)
        {
            ICMH = casa.Casass2[ups].GetComponent<InfoCasaMH>();
            //ICMH.MesAñoP.Clear();
            ICMH.MesP.Clear();
            ICMH.MesAño.Clear();
            ICMH.PagadoCon.Clear();
            ICMH.PagadoEn.Clear();
            ICMH.Categoria.Clear();
            ICMH.idCategoria.Clear();
            ICMH.idIcono.Clear();
            ICMH.idColor.Clear();
            ICMH.idPagadoEn.Clear();
            ICMH.idPagadoCon.Clear();
            ICMH.idPagadoEnColor.Clear();
            ICMH.idPagadoEnIcono.Clear();
            ICMH.PrecioPago.Clear();
            ICMH.FechaPago.Clear();
            ICMH.Pagos.Clear();
            ICMH.PagoCreado.Clear();
            VPM.Mes.Clear();
            VPM.Mes2.Clear();
            VPM.Mes3.Clear();
            //print("Limpio casa " + ups);
        }

        if (ups == casa.Casass2.Count && check == true)
        {
            check = false;
            ReadCardHistorial();
        }
    }
    public void ReadCardHistorial()
    {
        //reg.cargando = true;
        //EliminarHistorial();
        ObtenerHistorial();
        //StartCoroutine(DelayGetHistorial());
        //ob.Click("MisCasasHUB");
        //if (ID == save.id)
        //{

        //}
    }
    public IEnumerator AcomodoSeconds()
    {
        yield return new WaitForEndOfFrame();
        //Quitar();
        acomodo = true;
        obtenido = true;
        load = true;
        Acomodo();
    }

    public void AceptarProgramacionPago()
    {
        lastCadaCuando = CadaCuando;
        lastNotas = NotaPago.text;
        lastRecordarPago = RecordPago;
        lastRepetirCadaCuandoText = RepetirCadaTexto.text;
        lastAlertaAntesText = AlertasTexto.text;
        lastAlertasAntes = AlertaAntes;
    }

    public void CancelarProgramacionPago()
    {
        CadaCuando = lastCadaCuando;
        NotaPago.text = lastNotas;
        RecordPago = lastRecordarPago;
        RepetirCadaTexto.text = lastRepetirCadaCuandoText;
        AlertasTexto.text = lastAlertaAntesText;
        AlertaAntes = lastAlertasAntes;

        if (editar)
        {
            años = IP.AñoNum;
            meses = IP.MesNum;
            dias = IP.DiaNum;
        }
        else
        {
            años = theYear;
            meses = theMonth;
            dias = theDay;
        }

        AñoP.text = años + "";
        MesP.text = GetMonth(meses);
        DiaP.text = dias + "";

        Fecha.text = "Fecha de pago: " + DiaP.text + "/" + MesP.text + "/" + AñoP.text;
    }

}

