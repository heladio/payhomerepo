﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MFP : MonoBehaviour {
    public InputField TC, NT, Ap, Nick;
    public Text Banc, Tip, An, M;
    public string TCt, TCm, NombTitu, Apell, Mes, Ano, NickName, Banco, Tipo, Marca;
    public ObjectManager ob;
    public int i = 0, b = 0, ids, c, h;
    public Toggle Visa, MasterCard, American;
    public GameObject tarjeta, mover;
    public Vector2 G, Ward, Ward2;
    public InfoCard Info, Info2;
    public Guardar save;
    public FP fp;
    public float s = 0, d;

    public ScrollRect scroll;
	// Use this for initialization
	void Start () {
        //if (i > b)
        //{
        //    b++;
        //    Instantiate(tarjeta, new Vector2(0, 0), Quaternion.identity, tarjetas.transform);
        //}
        
	}
	
	// Update is called once per frame
	void Update () {
        TCt = TC.text;
        NombTitu = NT.text;
        Apell = Ap.text;
        Mes = M.text;
        Ano = An.text;
        NickName = Nick.text;
        Banco = Banc.text;
        Tipo = Tip.text;
        
        if (TCt.Length == 16)
        {
            TCm = TCt.Substring(12, 4);
            Debug.Log(TCt.Length);
        }

        if (Visa.isOn == true)
        {
            Marca = "Visa";
        }
        if (MasterCard.isOn == true)
        {
            Marca = "MasterCard";
        }
        if (American.isOn == true)
        {
            Marca = "American Express";
        }

    }

    public void ReadCard()
    {
        
        //if (ID == save.id)
        //{

        //}
    }

    public void Guardar()
    {
        if (TCt.Length == 16 && NombTitu != "" && Apell != "" && Mes != "Mes" && Ano != "Año" && NickName != "" && Banco != "Banco" && Tipo != "Tipo de Tarjeta")
        {
            //scroll.movementType = ScrollRect.MovementType.Elastic;
            //scroll.elasticity = 0;
            Info.Titular.text = NickName;
            Info.NumTerminacion.text = TCm;
            Info.TipoTarjeta.text = Tipo;
            Info.Titu = NombTitu;
            Info.Ap = Apell;
            Info.NT = TCt;
            Info.Ano = Ano;
            Info.Mes = Mes;
            Info.Banco = Banco;
            Info.Tipo = Tipo;
            Info.Marca = Marca;
            Info.NN = NickName;
            //Info.idCard = (i - 1);

            fp.TCL[ids] = TCt;
            fp.TCmL[ids] = TCm;
            fp.NTL[ids] = NombTitu;
            fp.ApL[ids] = Apell;
            fp.ML[ids] = Mes;
            fp.AnL[ids] = Ano;
            fp.NickL[ids] = NickName;
            fp.BancL[ids] = Banco;
            fp.TipoL[ids] = Tipo;
            fp.MarcaL[ids] = Marca;
            //TCL.Add(TCt);
            //TCmL.Add(TCm);
            //NTL.Add(NombTitu);
            //ApL.Add(Apell);
            //ML.Add(Mes);
            //AnL.Add(Ano);
            //NickL.Add(NickName);
            //BancL.Add(Banco);
            //TipoL.Add(Tipo);
            //MarcaL.Add(Marca);
            //ID.Add(i);
            //scroll.movementType = ScrollRect.MovementType.Elastic;
            //scroll.elasticity = 3.25f;
            ob.Click("MisCasasHUB");
        }
        else
        {
            ob.Click("ErrorTarjeta");
        }
    }
    public void Eliminar()
    {
        //scroll.movementType = ScrollRect.MovementType.Elastic;
        //scroll.elasticity = 0;
        //Info.idCard = (i - 1);
        c = ids;
        h = c - 1;
        Ward = fp.Tarjs[ids].transform.position;
        for (int d = ids; d < fp.i - 1; d++)
        {
            c++;
            Ward2 = fp.Tarjs[c].transform.position;
            fp.ID[c]--;
            fp.Tarjs[c].transform.position = Ward;
            mover = fp.Tarjs[c];
            Info2 = mover.GetComponent<InfoCard>();
            Info2.idCard--;
            Ward = Ward2;
            
        }
        if (h > 0)
        {
            fp.s += (fp.d);
        }
        //if(h == 0)
        //{
        //    fp.s = 0;
        //}

        //if (c == fp.i && fp.i == 0)
        //{
        //    fp.s = fp.tarjetax.transform.position.y;
        //    i++;
        //}
        
        Destroy(tarjeta);
        fp.TCL.RemoveAt(ids);
        fp.TCmL.RemoveAt(ids);
        fp.NTL.RemoveAt(ids);
        fp.ApL.RemoveAt(ids);
        fp.ML.RemoveAt(ids);
        fp.AnL.RemoveAt(ids);
        fp.NickL.RemoveAt(ids);
        fp.BancL.RemoveAt(ids);
        fp.TipoL.RemoveAt(ids);
        fp.MarcaL.RemoveAt(ids);
        fp.ID.RemoveAt(ids);
        fp.Tarjs.RemoveAt(ids);
        fp.b--;
            fp.i--;
        c = 0;
        //fp.TCL[ids] = TCt;
        //fp.TCmL[ids] = TCm;
        //fp.NTL[ids] = NombTitu;
        //fp.ApL[ids] = Apell;
        //fp.ML[ids] = Mes;
        //fp.AnL[ids] = Ano;
        //fp.NickL[ids] = NickName;
        //fp.BancL[ids] = Banco;
        //fp.TipoL[ids] = Tipo;
        //fp.MarcaL[ids] = Marca;
        //TCL.Add(TCt);
        //TCmL.Add(TCm);
        //NTL.Add(NombTitu);
        //ApL.Add(Apell);
        //ML.Add(Mes);
        //AnL.Add(Ano);
        //NickL.Add(NickName);
        //BancL.Add(Banco);
        //TipoL.Add(Tipo);
        //MarcaL.Add(Marca);
        //ID.Add(i);
        //scroll.movementType = ScrollRect.MovementType.Elastic;
        //        scroll.elasticity = 3.25f;
        ob.Click("Atras");
        
    }
}
