﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class InfoCasa : MonoBehaviour {
    public Text NickNameCasa;
    public string NickNameC, UbicacionC, CodigoC, FotoCasa;
    public GameObject Casa, OC;
    public Image casita, casapersonalizada;
    public Sprite foto;
    public int idCasa, fotos, idFoto, posicion, idCasaLocal;
    public GameObject casaper;
    public ObjectManager ob;
    public MFC mfc;
    public bool FotoTomada, cambio;
    public Texture2D fotota;
    public Casas casa;
    public InfoCasaMH ICMH;
    public InfoCasaPago ICP;
    public bool foundFoto = false;
    // Use this for initialization
    void Start () {
        Casa = this.gameObject;
        this.GetComponent<Button>().onClick.AddListener(ReadCard);
        OC = GameObject.Find("ModMisCasas");
        mfc = OC.GetComponent<MFC>();
        //StartCoroutine(Delay());
        /*if (File.Exists(Application.persistentDataPath + "/" + idCasa + ".jpg"))
        {
            byte[] bytes = File.ReadAllBytes(Application.persistentDataPath + "/" + idCasa + ".jpg");
            fotota = new Texture2D(1600, 1600);
            fotota.LoadImage(bytes);
            foto = Sprite.Create(fotota, new Rect(/*(fotota.width / 4)/0, /*(fotota.height / 4)/0, (fotota.width /* 2/), (fotota.height /* 2/)), Vector2.zero);

            for (int u = 0; casa.Casass2.Count > u; u++)
            {
                if (casa.Casass2[u].GetComponent<InfoCasaMH>().CasaID == idCasa)
                {
                    ICMH = casa.Casass2[u].GetComponent<InfoCasaMH>();
                    ICMH.foto = foto;
                    ICMH.FotoTomada = true;
                }
                if (casa.CasaPagos[u].GetComponent<InfoCasaPago>().CasaID == idCasa)
                {
                    ICP = casa.CasaPagos[u].GetComponent<InfoCasaPago>();
                    ICP.foto = foto;
                    ICP.FotoTomada = true;
                }
            }
            print("house");
            casaper.SetActive(true);
            casapersonalizada.sprite = foto;
            casapersonalizada.preserveAspect = true;

            ICP.casafoto.SetActive(true);
            ICP.casapersonalizada.sprite = foto;
            ICP.casapersonalizada.preserveAspect = true;

            ICMH.casaper.SetActive(true);
            ICMH.casapersonalizada.sprite = foto;
            ICMH.casapersonalizada.preserveAspect = true;

            OrderImages(casapersonalizada);
            OrderImages(ICP.casapersonalizada);
            OrderImages(ICMH.casapersonalizada);

        }*/
        //else
        //{
            if (FotoCasa != "" && FotoCasa != null)
            {
                StartCoroutine(Starts(FotoCasa));
        }
        //}

        System.GC.Collect();

    }

    //private void OnEnable()
    //{
    // StartCoroutine(Delay());
    //}

    // Update is called once per frame
    public void ReadCard()
    {
        mfc.Info = this;
        mfc.casat = this.gameObject;
        mfc.casat2 = mfc.casa.Casass2[posicion];
        mfc.casat2 = mfc.casa.CasaPagos[posicion];
        mfc.NickCasa.text = NickNameC;
        mfc.NickCasaBack = NickNameC;
        mfc.UbicacionCasa.text = UbicacionC;
        mfc.CodigoAsociacionCasa.text = CodigoC;
        mfc.idCasa = idCasa;
        mfc.ids = posicion;
        mfc.foundFoto = foundFoto;
        mfc.idFot = idFoto;
        mfc.foto = fotos;
        if(FotoTomada == true)
        {
            mfc.FotoCasa = foto;
        }
        mfc.Back();
        //FotoTomada = false;
        //mfc.FotoTomada = false;

        //Apell.text = Ap;
        //M.text = Mes;
        //An.text = Ano;
        //Ban.text = Banco;
        //Tip.text = Tipo;
        //Mar.text = Marca;
        //NumTarj.text = NT;
        ob.Click("ModMisCasas");
        mfc.eliminar = true;

        mfc.UpdateImage();
    }
    IEnumerator Starts(string endDir)
    {
        //print("starts");
        // Start a download of the given URL
        string url = "https://payhome.azurewebsites.net/serv/";
        WWW www = new WWW(url + endDir);

        // Wait for download to complete
        yield return www;

        // assign texture
        
        Texture2D texture = www.texture;
        byte[] bytes = texture.EncodeToJPG(50);
        File.WriteAllBytes(Application.persistentDataPath + "/" +idCasa + ".jpg", bytes);

        fotota = www.texture;
        foto = Sprite.Create(fotota, new Rect(/*(fotota.width / 4)*/0, /*(fotota.height / 4)*/0, (fotota.width /* 2*/), (fotota.height /* 2*/)), Vector2.zero);


        for (int u = 0; casa.Casass2.Count > u; u++)
        {
            if(casa.Casass2[u].GetComponent<InfoCasaMH>().CasaID == idCasa)
            {
                ICMH = casa.Casass2[u].GetComponent<InfoCasaMH>();
                ICMH.foto = foto;
                ICMH.FotoTomada = true;
            }
            if (casa.CasaPagos[u].GetComponent<InfoCasaPago>().CasaID == idCasa)
            {
                ICP = casa.CasaPagos[u].GetComponent<InfoCasaPago>();
                ICP.foto = foto;
                ICP.FotoTomada = true;
            }
        }
        //print("voy por fotos");
        casaper.SetActive(true);
        casapersonalizada.sprite = foto;
        casapersonalizada.preserveAspect = true;

        ICP.casafoto.SetActive(true);
        ICP.casapersonalizada.sprite = foto;
        ICP.casapersonalizada.preserveAspect = true;

        ICMH.casaper.SetActive(true);
        ICMH.casapersonalizada.sprite = foto;
        ICMH.casapersonalizada.preserveAspect = true;

        OrderImages(casapersonalizada);
        OrderImages(ICP.casapersonalizada);
        OrderImages(ICMH.casapersonalizada);

        //ReadCard();
        //InfoC.FotoTomada = true;
        //InfoCP.FotoTomada = true;
        //InfoMH.FotoTomada = true;
        //InicioFoto.sprite = FotoPerfil;
        //MPFoto.sprite = FotoPerfil;

        //FotoRegistro.SetActive(true);
        //Foto.SetActive(true);
        //FotoTomada = true;
        //FotoInicio.SetActive(true);
        //reg.casa.ReadCard();
        //Ver();
        //oj.sprite = FotoPerfil;
        System.GC.Collect();

    }

    public void OrderImages(Image img)
    {
        if (img.sprite != null)
        {
            img.GetComponent<AspectRatioFitter>().aspectMode = AspectRatioFitter.AspectMode.EnvelopeParent;
            if (img.sprite.texture.width > img.sprite.texture.height)
            {
                img.GetComponent<AspectRatioFitter>().aspectRatio = 2f;
            }
            else if (img.sprite.texture.width < img.sprite.texture.height)
            {
                //casapersonalizada.GetComponent<AspectRatioFitter>().aspectMode = AspectRatioFitter.AspectMode.WidthControlsHeight;
                casapersonalizada.GetComponent<AspectRatioFitter>().aspectRatio = 0.001f;
            }
        }
       
    }
    IEnumerator Delay()
    {
        yield return new WaitForSeconds(1f);
        if (File.Exists(Application.persistentDataPath + "/" + idCasa + ".jpg"))
        {
            byte[] bytes = File.ReadAllBytes(Application.persistentDataPath + "/" + idCasa + ".jpg");
            fotota = new Texture2D(1600, 1600);
            fotota.LoadImage(bytes);
            foto = Sprite.Create(fotota, new Rect(/*(fotota.width / 4)*/0, /*(fotota.height / 4)*/0, (fotota.width /* 2*/), (fotota.height /* 2*/)), Vector2.zero);

            casaper.SetActive(true);
            casapersonalizada.sprite = foto;
                casapersonalizada.preserveAspect = true;
        }
    }
    void Update()
    {
        if (File.Exists(Application.persistentDataPath + "/" + idCasa + ".jpg"))
        {
            casaper.SetActive(true);
            //OrderImages(casapersonalizada);
            FotoTomada = true;
        }
        else
        {
            casaper.SetActive(false);
            FotoTomada = false;
        }
        if (cambio == true)
        {
            cambio = false;
            if (File.Exists(Application.persistentDataPath + "/" + idCasa + ".jpg"))
            {
                byte[] bytes = File.ReadAllBytes(Application.persistentDataPath + "/" + idCasa + ".jpg");
                Texture2D fotota = new Texture2D(1600, 1600);
                fotota.LoadImage(bytes);
                foto = Sprite.Create(fotota, new Rect(/*(fotota.width /* 4)*/0, /*(fotota.height / 4)*/0, (fotota.width /* 2*/), (fotota.height /* 2*/)), Vector2.zero);

                casaper.SetActive(true);
                casapersonalizada.sprite = foto;
                casapersonalizada.preserveAspect = true;
                casapersonalizada.SetNativeSize();
                OrderImages(casapersonalizada);
            }
            System.GC.Collect();

        }

        if (casaper.activeInHierarchy)
        {
            foundFoto = true;
        }
        else
        {
            foundFoto = false;
        }
    }
}
