﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Globalization;
using System.IO;
using sharpPDF;
using sharpPDF.Enumerators;

public class InfoPagoHistorial : MonoBehaviour {
    CultureInfo ci = new CultureInfo("es-mx");
    public int CasaID, idIconCat, idColor, idIconoPagadoEn, idColorPagadoEn, idCategoria, idPagadoEn, idPagadoCon;
    public float PrecioPagoInt, totalpago;
    public string PrecioPagoTexto, PagadoConTexto, FechaPagoTexto, PagadoEnTexto, CategoriaTexto, NombreDocumento, fechapago, pagoen, pagocon, categorias, 
        casa, mes, es;
    public AP ap;
    public Text PrecioPago, PagadoCon, FechaPago, PagadoEn, Categoria, TotalPago, direccion;
    public Image IconoCategoria, Color, IconoPagadoEn;
    public InfoCasaMH ICMH;
    public string[] las;
    string[] info;
    string namePDF;
    string paymentName;
    string path ;
    public Detalles det;
    public ObjectManager ob;
    public DateTime fecha;
    // Use this for initialization
    void Start () {
        det = GameObject.Find("HistorialesPagosCasasDetalles").GetComponent<Detalles>();
        ob = GameObject.Find("ObjectManager").GetComponent<ObjectManager>();
        if (idCategoria < 10)
        {
            
            PagadoEnTexto = ap.PagEn[idPagadoEn];
            PrecioPagoInt = float.Parse(PrecioPagoTexto);
            PrecioPago.text = PrecioPagoInt.ToString("C", ci);
            PagadoConTexto = ap.PagCon[idPagadoCon];
            PagadoCon.text = PagadoConTexto;
            //fecha = DateTime.ParseExact(FechaPagoTexto, "d/M/yyyy", ci);
            //if (fecha.Month == 1)
            //    mes = "Enero";
            //if (fecha.Month == 2)
            //    mes = "Febrero";
            //if (fecha.Month == 3)
            //    mes = "Marzo";
            //if (fecha.Month == 4)
            //    mes = "Abril";
            //if (fecha.Month == 5)
            //    mes = "Mayo";
            //if (fecha.Month == 6)
            //    mes = "Junio";
            //if (fecha.Month == 7)
            //    mes = "Julio";
            //if (fecha.Month == 8)
            //    mes = "Agosto";
            //if (fecha.Month == 9)
            //    mes = "Septiembre";
            //if (fecha.Month == 10)
            //    mes = "Octubre";
            //if (fecha.Month == 11)
            //    mes = "Noviembre";
            //if (fecha.Month == 12)
            //    mes = "Diciembre";
            //FechaPago.text = fecha.Day + "/" + mes + "/" + fecha.Year;
            FechaPago.text = FechaPagoTexto;
            PagadoEn.text = PagadoEnTexto;
            Categoria.text = CategoriaTexto;
        }
        if (idCategoria >= 10)
        {
            
            PagadoEnTexto = ap.PagEn[idPagadoEn];
            PrecioPagoInt = float.Parse(PrecioPagoTexto);
            PrecioPago.text = PrecioPagoInt.ToString("C", ci);
            PagadoConTexto = ap.PagCon[idPagadoCon];
            PagadoCon.text = PagadoConTexto;
            //fecha = DateTime.ParseExact(FechaPagoTexto, "d/M/yyyy", ci);
            //if (fecha.Month == 1)
            //    mes = "Enero";
            //if (fecha.Month == 2)
            //    mes = "Febrero";
            //if (fecha.Month == 3)
            //    mes = "Marzo";
            //if (fecha.Month == 4)
            //    mes = "Abril";
            //if (fecha.Month == 5)
            //    mes = "Mayo";
            //if (fecha.Month == 6)
            //    mes = "Junio";
            //if (fecha.Month == 7)
            //    mes = "Julio";
            //if (fecha.Month == 8)
            //    mes = "Agosto";
            //if (fecha.Month == 9)
            //    mes = "Septiembre";
            //if (fecha.Month == 10)
            //    mes = "Octubre";
            //if (fecha.Month == 11)
            //    mes = "Noviembre";
            //if (fecha.Month == 12)
            //    mes = "Diciembre";
            //FechaPago.text = fecha.Day + "/" + mes + "/" + fecha.Year;
            FechaPago.text = FechaPagoTexto;
            PagadoEn.text = PagadoEnTexto;
            Categoria.text = CategoriaTexto;
        }
        ICMH.total += PrecioPagoInt;
        las[0] = FechaPagoTexto;
        las[1] = PrecioPagoInt.ToString("C", ci);
        las[2] = CategoriaTexto;
        las[3] = PagadoConTexto;
        las[4] = PagadoEnTexto;
    }
	
	// Update is called once per frame
	void Update () {
        categorias = CategoriaTexto.Replace(" ", string.Empty);
        fechapago = FechaPagoTexto.Replace("/", string.Empty);
        pagoen = PagadoEnTexto.Replace(" ", string.Empty);
        pagocon = PagadoConTexto.Replace(" ", string.Empty);
        casa = ICMH.NickName;
        direccion.text = es;
	}
    public void Click()
    {

        det.leer = true;
        det.IdCasa = CasaID;
        det.idCategoria = idCategoria;
        det.IdColorCat = idColor;
        det.IdIconoCat = idIconCat;
        det.IdPagadoEn = idPagadoEn;
        det.IdPagadoCon = idPagadoCon;
        det.MontoInt = PrecioPagoInt;
        det.ICMH = ICMH;
		det.Categoria.text = CategoriaTexto;
        det.FechaPagoString = FechaPagoTexto;
        ob.Click("Detalles");
    }
    public void CrearPDF()
    {

        NombreDocumento = categorias + casa + fechapago + ".pdf";
        ActivateCreatePDF(las, NombreDocumento);
        path = es;
//#if UNITY_ANDROID
//        path = Application.persistentDataPath + "/files" + NombreDocumento;
//        #endif
//#if UNITY_IOS
//        path = Application.persistentDataPath + "/files" + NombreDocumento;
//#endif
//#if UNITY_EDITOR
//        path = Application.persistentDataPath + NombreDocumento;
//#endif
        Application.OpenURL(es);
    }

    private void ActivateCreatePDF(string[] newInfo, string nameDoc)
    {
        info = newInfo;
        paymentName = nameDoc;
        StartCoroutine(CreatePDF());
    }

    // Update is called once per frame
    public IEnumerator CreatePDF()
    {
        namePDF = "Payment register";
        pdfDocument myDoc = new pdfDocument(namePDF, "Me", false);
        pdfPage myFirstPage = myDoc.addPage();



        //		Debug.Log ( "Continue to create PDF");
        myFirstPage.addText("Registro de pago", 10, 730, predefinedFont.csHelveticaOblique, 30, new pdfColor(predefinedColor.csOrange));



        /*Table's creation*/
        pdfTable myTable = new pdfTable();
        //Set table's border
        myTable.borderSize = 1;
        myTable.borderColor = new pdfColor(predefinedColor.csDarkBlue);

        /*Add Columns to a grid*/
        myTable.tableHeader.addColumn(new pdfTableColumn("Fecha de pago", predefinedAlignment.csCenter, 120));
        myTable.tableHeader.addColumn(new pdfTableColumn("Monto", predefinedAlignment.csCenter, 120));
        myTable.tableHeader.addColumn(new pdfTableColumn("Categoria", predefinedAlignment.csCenter, 120));
        myTable.tableHeader.addColumn(new pdfTableColumn("Metodo de pago", predefinedAlignment.csCenter, 120));
        myTable.tableHeader.addColumn(new pdfTableColumn("Lugar de pago", predefinedAlignment.csCenter, 120));


        pdfTableRow myRow = myTable.createRow();
        myRow[0].columnValue = info[0];
        myRow[1].columnValue = info[1];
        myRow[2].columnValue = info[2];
        myRow[3].columnValue = info[3];
        myRow[4].columnValue = info[4];

        myTable.addRow(myRow);

        //pdfTableRow myRow1 = myTable.createRow();
        //myRow1[0].columnValue = "B";
        //myRow1[1].columnValue = "130 km/h";
        //myRow1[2].columnValue = "150Kg";
        //myRow1[3].columnValue = "Yellow";

        //myTable.addRow(myRow1);



        /*Set Header's Style*/
        myTable.tableHeaderStyle = new pdfTableRowStyle(predefinedFont.csCourierBoldOblique, 12, new pdfColor(predefinedColor.csBlack), new pdfColor(predefinedColor.csLightOrange));
        /*Set Row's Style*/
        myTable.rowStyle = new pdfTableRowStyle(predefinedFont.csCourier, 8, new pdfColor(predefinedColor.csBlack), new pdfColor(predefinedColor.csWhite));
        /*Set Alternate Row's Style*/
        myTable.alternateRowStyle = new pdfTableRowStyle(predefinedFont.csCourier, 8, new pdfColor(predefinedColor.csBlack), new pdfColor(predefinedColor.csLightYellow));
        /*Set Cellpadding*/
        myTable.cellpadding = 10;
        /*Put the table on the page object*/
        myFirstPage.addTable(myTable, 5, 700);


        //yield return StartCoroutine ( myFirstPage.newAddImage (  "FILE://picture1.jpg",2,100 ) );

        myDoc.createPDF(Application.persistentDataPath + paymentName);
        es = Application.persistentDataPath + paymentName;
        myTable = null;

        yield return null;
    }
}
