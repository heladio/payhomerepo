﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class Share: MonoBehaviour
{
    //public ObjectManager ob;
    // Use this for initialization
    void Start()
    {
       // ob.Click("CerrarMenu");
    }

    // Update is called once per frame
    public void ShareMessage()
    {

        StartCoroutine(TakeSSAndShare());
      
    }

    private IEnumerator TakeSSAndShare()
    {
        yield return new WaitForEndOfFrame();
        //ob.Click("CerrarMenu");
       /* Texture2D ss = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        ss.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        ss.Apply();

        string filePath = Path.Combine(Application.temporaryCachePath, "shared img.png");
        File.WriteAllBytes(filePath, ss.EncodeToPNG());

        // To avoid memory leaks

        Destroy(ss);*/string filePath = Path.Combine(Application.streamingAssetsPath, "shared img.png");
        new NativeShare().AddFile(filePath).SetSubject("Descubre PayHome").SetText("Te invito a descargar la aplicación PayHome, con ella podrás administrar tus gastos y pagos. Descárgala en https://play.google.com/apps/testing/com.Origis.PayHome").Share();

        // Share on WhatsApp only, if installed (Android only)
        //if( NativeShare.TargetExists( "com.whatsapp" ) )
        //  new NativeShare().AddFile( filePath ).SetText( "Hello world!" ).SetTarget( "com.whatsapp" ).Share();
    }
}
