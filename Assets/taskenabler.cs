﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class taskenabler : MonoBehaviour {

    public GameObject taksCounter;
    public int countTouch;
    public int numberofClicks=5;
    public GameObject Debuger;

    // Use this for initialization
	void Start () {
        Debuger.SetActive(false);
        countTouch = numberofClicks;
    }
	
	// Update is called once per frame
	void Update () {
		if(countTouch<=0)
        {
            Debuger.SetActive(true);
            countTouch = numberofClicks;
        }
    }

    public void OnClick()
    {
        countTouch = countTouch - 1;
    }
}
